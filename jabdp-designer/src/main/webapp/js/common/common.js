/**
 * 封装ajax方法，作统一异常错误处理
 * @param options
 */
function fnFormAjaxWithJson(options, notShowProgress){
	//默认显示进度条，当notShowProgress为true时，不显示
	notShowProgress = notShowProgress || false;
	var tmpFun = options.success;
	var callBackFun = function(data){
	//	$.each(data,function(k,v){alert(k+":"+v);});
		if(!notShowProgress) {$(document.body).unmask();/*$.messager.progress('close');*/}
		if(typeof tmpFun==='function'&&tmpFun){
			if(data) {
				if(data.flag=="1") {
					tmpFun(data);
				} else if(data.flag=="3") {
					$.messager.alert('Error', "<div style='height:300px'>" + data.msg + "</div>",'error');
				} else {
					$.messager.alert('Error', "<div style='height:300px'>" + data.msg + "</div>" ,'error');
				}
			}
		}
		
	};
	options.success = undefined;
	options = $.extend({
		url: false,
		type : 'post',
		dataType : 'json',
		success:callBackFun,
		beforeSend:function(xhr) {
			if(!notShowProgress) {$(document.body).mask("Please Waiting...");/*$.messager.progress({text:'Please waiting...',interval:20});*/}
		},
		error:function(xhr, ts, errorThrown) {
			if(!notShowProgress) {$(document.body).unmask();/*$.messager.progress('close');*/}
			$.messager.alert('Error', 'Sorry, Something is wrong!' ,'error');
		},
		complete: function (XMLHttpRequest, textStatus) {
			//$.messager.progress('close');
		}
		},options);
	$.ajax(options);
}

//时间格式化
Date.prototype.format = function(format){
   /*
  * eg:format="yyyy-MM-dd hh:mm:ss";
    */
   if(!format){
      format = "yyyy-MM-dd hh:mm:ss";
    }

  var o = {
           "M+": this.getMonth() + 1, // month
           "d+": this.getDate(), // day
           "h+": this.getHours(), // hour
            "m+": this.getMinutes(), // minute
           "s+": this.getSeconds(), // second
           "q+": Math.floor((this.getMonth() + 3) / 3), // quarter
            "S": this.getMilliseconds()
            // millisecond
   };

    if (/(y+)/.test(format)) {
       format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
   }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) { 
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" +o[k]).length));
        }
    }
    return format;
};

var $MsgUtil = {
		alert:function(opt, method) {
			var op = method || "warning";
			top.toastr.options = {
				"closeButton":true,
				"positionClass": "toast-top-center"
			}
			top.toastr[op](opt);
		},
		info:function(opt) {
			$MsgUtil.alert(opt, "info");
		}
};

	window.alert = function(msg) {
		$MsgUtil.alert(msg);
	};

$(document).keydown(function(e){ 
    var keyEvent; 
    if(e.keyCode==8){ 
        var d=e.srcElement||e.target; 
        if(d.tagName.toUpperCase()=='INPUT'||d.tagName.toUpperCase()=='TEXTAREA'){ 
            keyEvent=d.readOnly||d.disabled; 
        }else{ 
            keyEvent=true; 
        } 
    }else{ 
        keyEvent=false; 
    } 
    if(keyEvent){ 
        e.preventDefault(); 
    } 
}); 