<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title>JABDP iDesigner</title>
	<%@ include file="/common/meta.jsp" %>
	<link href="${ctx}/js/easyui/themes/blue/panel.css" rel="stylesheet" type="text/css"/>
<%--	<link href="${ctx}/js/uploadify/uploadify.css" rel="stylesheet" type="text/css"/>--%>
	<link rel="stylesheet" href="${ctx}/js/webUploader/webuploader.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/js/easyui/themes/icon-add.css"/>
<%--	<script type="text/javascript" src="${ctx}/js/uploadify/jquery.uploadify-3.1.js"></script>--%>
<%--	<script type="text/javascript" src="${ctx}/js/uploadify/jquery.uploadify-3.1.min.js"></script>--%>
	<script  src="${ctx}/js/webUploader/webuploader.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/easyui/easyui.xpPanel.js"></script>

	<!-- fontIconPicker JS -->
	<script type="text/javascript" src="${ctx}/js/fontIconPickerMaster/jquery.fonticonpicker.min.js"></script>

	<!-- fontIconPicker core CSS -->
	<link rel="stylesheet" type="text/css" href="${ctx}/js/fontIconPickerMaster/css/jquery.fonticonpicker.min.css" />

	<!-- required default theme -->
	<link rel="stylesheet" type="text/css" href="${ctx}/js/fontIconPickerMaster/themes/grey-theme/jquery.fonticonpicker.grey.min.css" />

	<!-- optional themes -->
	<link rel="stylesheet" type="text/css" href="${ctx}/js/fontIconPickerMaster/themes/dark-grey-theme/jquery.fonticonpicker.darkgrey.min.css" />
	<link rel="stylesheet" type="text/css" href="${ctx}/js/fontIconPickerMaster/themes/bootstrap-theme/jquery.fonticonpicker.bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="${ctx}/js/fontIconPickerMaster/themes/inverted-theme/jquery.fonticonpicker.inverted.min.css" />

	<!-- Font -->
	<link rel="stylesheet" type="text/css" href="${ctx}/js/fontIconPickerMaster/demo/fontello-7275ca86/css/style.css" />
	<link rel="stylesheet" type="text/css" href="${ctx}/js/fontIconPickerMaster/demo/icomoon/style.css" />

	<link href="${ctx}/js/easyui/themes/blue/panel.css" rel="stylesheet" type="text/css"/>

	<style type="text/css">
		span.tmpzTreeMove_arrow{z-index:999999;}
		ul.ztree.zTreeDragUL {z-index:999999;}
		#picker{display: inline-block;line-height: 1.428571429;vertical-align: middle;margin: 0 12px 0 0;}
		#picker>div:last-child{width:100%!important;height: 100%!important;}
		.progress {background-image: -webkit-linear-gradient(top,#ebebeb 0,#f5f5f5 100%);background-image: linear-gradient(to bottom,#ebebeb 0,#f5f5f5 100%);background-repeat: repeat-x;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffebebeb',endColorstr='#fff5f5f5',GradientType=0)}
		.progress-bar {background-image: -webkit-linear-gradient(top,#428bca 0,#3071a9 100%);background-image: linear-gradient(to bottom,#428bca 0,#3071a9 100%);background-repeat: repeat-x;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff428bca',endColorstr='#ff3071a9',GradientType=0)}
		.progress-striped .progress-bar {background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,0.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,0.15) 50%,rgba(255,255,255,0.15) 75%,transparent 75%,transparent);background-image: linear-gradient(45deg,rgba(255,255,255,0.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,0.15) 50%,rgba(255,255,255,0.15) 75%,transparent 75%,transparent);background-size: 40px 40px}
		.progress.active .progress-bar {-webkit-animation: progress-bar-stripes 2s linear infinite;animation: progress-bar-stripes 2s linear infinite;}
		.progress {height: 20px;margin-bottom: 20px;overflow: hidden;background-color: #f5f5f5;border-radius: 4px;-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);box-shadow: inset 0 1px 2px rgba(0,0,0,0.1)}
		.progress-bar {float: left;width: 0;height: 100%;font-size: 12px;line-height: 20px;color: #fff;text-align: center;background-color: #428bca;-webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,0.15);box-shadow: inset 0 -1px 0 rgba(0,0,0,0.15);-webkit-transition: width .6s ease;transition: width .6s ease}
	</style>
	<script type="text/javascript">
        function addTab(title, url) {
            if ($('#main_tabs_div').tabs('exists',title)){
                $('#main_tabs_div').tabs('select', title);
            } else {
                var content = "<iframe scrolling='auto' frameborder='0'  src='" + url + "' style='width:100%;height:100%;'></iframe>";
                $("#main_tabs_div").tabs("add",{
                    title: title,
                    //href:url,
                    content:content,
                    closable:true,
                    cache:true,
                    fit:true,
                    tools:[{
                        iconCls:'icon-mini-refresh',
                        handler:function(){
                            var tab = $('#main_tabs_div').tabs('getSelected');
                            $('#main_tabs_div').tabs('update', {
                                tab: tab,
                                options: {
                                    content:content
                                }
                            });
                        }
                    }]
                });
            }
        }
        var treeNodeId = "";
        //保存所有的module页面数据
        function refreshTabData() {
            var tbs = $('#main_tabs_div').tabs('tabs');
            for(var i=0;i<tbs.length;i++){
                var tb = tbs[i];
                if(tb) {
                    var tbby = tb.panel("body");
                    if(tbby) {
                        var tb_ifr = tbby.find("iframe");
                        if(tb_ifr) {
                            var win = tb_ifr[0].contentWindow.window;
                            if(win && win.saveModule) {
                                win.saveModule();
                            }
                        }
                    }
                }
            }
        }

        //根据标题获取tab页窗口对象
        function getTabDataWin(title) {
            var obj = null;
            var tb = $('#main_tabs_div').tabs('getTab',title);
            if(tb) {
                var tbby = tb.panel("body");
                if(tbby) {
                    var tb_ifr = tbby.find("iframe");
                    if(tb_ifr) {
                        var win = tb_ifr[0].contentWindow.window;
                        obj = win;
                    }
                }
            }
            return obj;
        }

        //关闭tab窗口
        function colseTab(title){
            $('#main_tabs_div').tabs('close',title);
        }

        function getTime(){
			/* var date =  new Date();
			 var year=date.getFullYear();
			 var month=covTime(date.getMonth()+1);
			 var day=covTime(date.getDay());
			 var hours=covTime(date.getHours());
			 var min=covTime(date.getMinutes());
			 var second=covTime(date.getSeconds());
			 var mill=date.getMilliseconds();
			 return year+month+day+hours+min+second+mill;*/
            var date = new Date();
            var time = date.format("yyyyMMddhhmmssS");
            //alert(time);
            return time;
        }
		/* function covTime(time){
		 if(time<10){
		 var covtime="0"+time;
		 return covtime;
		 }
		 return time;
		 } */

		/**
		 * 登录
		 */
        function doLogin() {
            if($("#loginForm").form("validate")) {
                var data = {};
                var url = $("#loginUrl").combobox("getText");
                data["urlType"] = $("input[name=urlType]:checked").val();
                data["loginName"] = $("#loginName").val();
                data["passwd"] = $("#passwd").val();
                data["loginUrl"] = url;
                var options = {
                    data:data,
                    url:"${ctx}/design/login",
                    success:function(data) {
                        $('#openFileWin').window('close');
                        setCookie("loginUrl",url);//添加cookie
                        initMenus();
                        initServerTitle();
                    }
                };
                fnFormAjaxWithJson(options);
            }
        }

		/**
		 * 添加cookie
		 * 将链接url的值放入cookie中
		 * @param name
		 * @param value
		 */
		function setCookie(name,value){
            var val = getCookie(name);
            var date = new Date();
            var ms = 365*24*3600*1000;
            date.setTime(date.getTime() + ms);
            if(val){
                var urls = val.split(';');
                var index = $.inArray(value, urls);
                if(index===0){
                    if(urls.length===1){
                        val = val.replace(value,"");
                    }else{
                        val = val.replace(value+";","");
                    }
                }else if(index>0){
                    val = val.replace(";"+value,"");
                }
                if(urls.length===1&&index===0){
                    val = value;
                }else{
                    val = val+";"+value;
                }
                //val = "http://127.0.0.1/jwpf";
                //console.log(val);
                document.cookie = name + "="+ escape (val)+";expires="+ date.toGMTString();
            }else{
                document.cookie = name + "="+ escape (value)+";expires="+ date.toGMTString();
            }
        }
        function getCookie(name){
            var cookies = document.cookie.split(';');
            if (!cookies.length) return '';
            for(var i=0; i<cookies.length; i++){
                var pair = cookies[i].split('=');
                if ($.trim(pair[0]) == name){
                    return $.trim(unescape(pair[1]));
                }
            }
            return '';
        }
        function initServerTitle() {
            var urlType = $("input[name=urlType]:checked").val();
            if(urlType == "remote") {
                var loginUrl = $("#loginUrl").combobox("getText");
                var titleSpan = "<span style='color:red;'>您连接的是远程服务器（" + loginUrl + "）</span>";
                $("#dv_server").html(titleSpan);
            } else {
                var titleSpan = "<span style='color:red;'>您连接的是本地配置文件</span>";
                $("#dv_server").html(titleSpan);
            }
        }
        //更新服务器端帐套
        function doUpgrade() {
            var updataTreeId = $("input[name=updataService]:checked").val();
            if(updataTreeId){
                var data = {};
                var url = $("#loginUrl").combobox("getText");
                data["loginName"] = $("#loginName").val();
                data["passwd"] = $("#passwd").val();
                data["loginUrl"] = url;
                data["treeId"] = updataTreeId;
                data["isFullUpdate"] = ($("input[name=isFullUpdate]:checked").val()=="1"?true:false);
                var options = {
                    data:data,
                    url: "${ctx}/design/upgrade",
                    success:function(rpData) {
                        if(rpData.flag == "1") {
                            if(rpData.msg) {
                                if(rpData.msg.flag == "0") {
                                    $.messager.confirm('更新提示', '您要部署的jwp包和服务器上的版本不一致，您确定要覆盖更新吗？', function(r) {
                                        if (r) {
                                            var jwpName = rpData.msg.msg;
                                            data["jwpName"] = jwpName;
                                            var options = {
                                                url : '${ctx}/design/forceUpgrade',
                                                data : data,
                                                success : function(rdata) {
                                                    if(rdata.msg) {
                                                        $.messager.alert('提示', 'jwp文件覆盖更新服务器成功！','info');
                                                    }
                                                }
                                            };
                                            fnFormAjaxWithJson(options);
                                        }
                                    });
                                } else {
                                    setCookie("loginUrl",url);
                                    $.messager.alert('更新提示', '更新服务器成功!' ,'info');
                                }
                            }
                        } else {
                            $.messager.alert('更新提示', '更新服务器失败，请重试!' ,'info');
                        }
                    }
                };
                fnFormAjaxWithJson(options);
            }else{
                $.messager.alert('提示', '请先选择帐套再更新!' ,'info');
            }
        }
        //下载帐套到本地客户端
        function downloadFile(){
            var downloadTreeId = $("input[name=updataService]:checked").val();
            //var menuTitle =  $('input[name=urlType]:checked').parent().attr('title');
            var menuTitle = $("input[name=updataService]:checked").parents("div.menu_panel").find("div.panel-title").text();
            menuTitle += getTime();
            if(downloadTreeId){
                window.open("${ctx}/design/download?treeId="+downloadTreeId+"&menuTitle="+menuTitle);
				/* data:{
				 "treeId":downloadTreeId
				 },
				 url: "${ctx}/design/download",
				 success:function(data) {
				 $.messager.alert('下载提示', '下载到本地成功!' ,'info');
				 }
				 };*/

            }else{
                $.messager.alert('提示', '请先选择帐套再下载!' ,'info');
            }
        }
        /**
		 * 新建账套
		 */
        function doNewFile() {

            //	$.messager.confirm('提示', '确定创建新的帐套?', function(r){
            //	if (r){
            $(".xpstyle-panel .xpstyle").panel("collapse");
            var titleName = $('#accountName').val();
            var accountName = {"name":titleName};
            var treeJson = $.extend({'id':1,'type':'menu','name':'业务模型设计','key':'menu','iconSkin':'icon01','children':[{'id':11,'type':'moduless','name':'业务管理（电脑端）','key':'module','iconSkin':'icon08','children':[{'id':1101,'type':'dicts','name':'业务字典','key':'dicts','iconSkin':'icon03'},{'id':1102,'type':'forms','name':'自定义表单','key':'forms','iconSkin':'icon03'}]},{'id':12,'type':'moduless','name':'业务管理（移动端）','key':'app','iconSkin':'icon-gnome-pda'},{'id':13,'type':'moduless','name':'系统管理','key':'system','iconSkin':'icon09'}]},accountName);
            var menuTree = getTime();
            treeNodeId=menuTree;
            var str =  "<div class='easyui-panel  xpstyle' id=xp"+menuTree+" style='width:180px;' collapsible='true' collapsed='true' title="+titleName+"><input type='radio' name='updataService' id=radio"+menuTree+" value="+menuTree+">单击选中本帐套</input><input style='width:110px;height:12px;margin: 0px 3px 0px 5px;' type='text' name='keyWords' id=keyWords"+menuTree+"></input><button type='button' id='_bt_query_'>查询</button><ul id="+menuTree+" class='ztree'></ul></div>";;
            $("#local_panel").append(str);
            $.fn.zTree.init($("#"+menuTree), setting,treeJson);
            saveZtreeModule();
            $("#local_panel .easyui-panel").panel({cls:"menu_panel",onExpand:onExpandPanel});
            openAllNode(menuTree);
            //	$(".xpstyle-panel .xpstyle").panel("expand");
            $("#xp"+menuTree).panel("expand");
            $("#createNewAccount").window("close");
            //	}
            //});

        }

        /**
		 * 用于捕获节点被展开的事件回调函数
		 * 账套面板展开时，需要让【单机选中本账套】这个单选框按钮选中
		 */
        function onExpandPanel() {
            $(this).find("input:radio[name=updataService]").attr("checked", "checked");
            return true;
        }

        //如果重命名的帐套名称，面板标题名称也跟着改变，并保存
        function renameTitleAndSave() {
            var zTree = $.fn.zTree.getZTreeObj(treeNodeId);
            var tree_nodes = zTree.getSelectedNodes();
            if(tree_nodes[0].type =="menu"){
                var menuName = tree_nodes[0].name;
                $("ul#"+treeNodeId).parents("div.menu_panel").find("div.panel-title").text(menuName);

            }
            saveZtreeModule();

        }

        //将整个页面数据保存成字符串json
        function saveZtreeModule() {

            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var nodes = treeObj.getNodes();
            //	return;
            //	var menuProp = $.fn.config.getMenu();
            var jsonStr = JSON.stringify(nodes[0]);
            var options = {
                data : {
                    "treeId":treeNodeId,
                    "menuJson" : jsonStr
                },
                url: "${ctx}/design/config/saveMenu",
                success:function(data) {
                }
            };
            fnFormAjaxWithJson(options);
        }

        //改变节点位置
        function changeZtreeModule(parentNode,node,newPath,oldPath,targetNode,isSilent){
            var options = {
                data : {
                    "treeId":treeNodeId,
                    "newPath" : newPath,
                    "oldPath" : oldPath
                },
                url: "${ctx}/design/config/changeMenu",
                success:function(data) {
                    var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
                    if(targetNode){
                        treeObj.moveNode(targetNode,node,isSilent);
                    }
                    else {
                        treeObj.removeNode(node);
                        treeObj.addNodes(parentNode, node);
                    }
                    saveZtreeModule();
                }
            };
            fnFormAjaxWithJson(options);
        }

        //初始化时加载zTree
        function initMenus() {
            // var urlType=$("input[name=urlType]:checked").val();
            var urlType = $("#rad_local").val();
            var options = {
                data:{
                    "urlType":urlType
                },
                url: "${ctx}/design/config/getMenu",
                success: initTree
            };
            fnFormAjaxWithJson(options,true);
        }
        //删除本地xml文件及文件夹
        function deleteFile(path,node){
            var options = {
                data:{
                    "treeId":treeNodeId,
                    "sPath":path
                },
                url: "${ctx}/design/config/deleteXml",
                success:function(data){
                    if(data){
                        var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
                        treeObj.removeNode(node);
                        saveZtreeModule();
                    }else{
                        $.messager.alert('提示', '删除失败!' ,'info');
                    }
                }
            };
            fnFormAjaxWithJson(options,true);
        }

        // 关闭tab
        function closeTab(title)
        {
            if ($('#main_tabs_div').tabs('exists',title)){
                $('#main_tabs_div').tabs('close', title);
            }
        }
		/* /* function initAccordion(menus) {
		 for(var i=0;i<menus.length;i++) {
		 var menu = menus[i];
		 var content = [];
		 if(menu.childs) {
		 for(var j=0;j<menu.childs.length;j++) {
		 var cmenu = menu.childs[j];
		 var icon = cmenu.resourceIcon;
		 if(!icon) {
		 icon = "icon-leaf";
		 }
		 var ct = "<div class=\"inde
		 x_menu\"><span class=\"" + icon + "\"></span><a href=\"javascript:void(0);\" url=\"${ctx}" + cmenu.resourceUrl
		 + "\" title=\"" + cmenu.resourceName + "\">" + cmenu.resourceName + "</a></div>";
		 content.push(ct);
		 }
		 }
		 var icon = cmenu.resourceIcon;
		 if(!icon) {
		 icon = "icon-folder";
		 }
		 $('#dv_menu').accordion('add',{
		 title:menu.resourceName,
		 content:content.join("\n"),
		 iconCls:icon
		 });
		 }
		 $("#dv_menu div.index_menu a").click(function() {
		 addTab($(this).attr("title"), $(this).attr("url"));
		 });
		 }	*/

        var modulessKey={};
        var moduleKey={};
        var updataFlag=false;
        //初始化树
        function initTree(data){
            //	$.each(data.mdg,function(k,v){alert(k+":"+v);});
            //	$.fn.zTree.init($("#menuTree"), setting, data.msg);

            var  menus = data.msg;
            for(var key in  menus){
                var  menu = 	menus[key]['children'];
                //没有手机节点，则添加
                if(menu&&menu.length==2){
                    var hasMobileNode = false;
                    for(var i=0;i<menu.length;i++){
                        if('业务管理'==menu[i].name){
                            menu[i].name = '业务管理（电脑端）';
                        }
                        else if('系统管理'==menu[i].name){
                            menu[2]=menu[i];
                            menu[2].id='13';
                        }
                        else if('业务管理（移动端）'==menu[i].name){
                            hasMobileNode=true;
                        }
                    }
                    if(!hasMobileNode) menu[1]={'id':'12','type':'moduless','name':'业务管理（移动端）','key':'app','iconSkin':'icon-gnome-pda'};
                }
            }

            var type =  $('input[name=urlType]:checked').val();
            var linkIP = $("#loginUrl").combobox("getText");
            var reg = new RegExp("[0-9.]");
            var remoteIP="";
            for(var i=0;i<linkIP.length;i++){
                if(reg.test(linkIP.charAt(i))){
                    remoteIP +=linkIP.charAt(i);
                }
            }
            //	alert("updataFlag:"+updataFlag);
            if(updataFlag){
                $("#local_panel").html("");
                //	alert(data.msg.length);
                var num = 1;
                $.each(menus,function(k,v){
                    //	   alert("k"+k+"  v"+v);
                    var str =  "<div class='easyui-panel  xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+v.name+"><input type='radio'  name='updataService' id=radio"+k+" value="+k+">单击选中本帐套</input><input style='width:110px;height:12px;margin: 0px 3px 0px 5px;' type='text' name='keyWords' id=keyWords"+k+"></input><button type='button' id='_bt_query_'>查询</button><ul id="+k+" class='ztree'></ul></div>";
                    $("#local_panel").append(str);
                    $("#local_panel .easyui-panel").panel({cls:"menu_panel",onExpand:onExpandPanel});
                    $.fn.zTree.init($("#"+k), setting, v);
                    openAllNode(k);
                    if(num=="1"){
                        $(".xpstyle-panel .xpstyle").panel("expand");
                    }
                    var moduless=v.children;
                    initModulessKey(k, moduless);
                    num++;
                });
                $('#linkTree').tabs('select','本地');
                updataFlag=false;
            }else{
                if(type=="local"){
                    $("#local_panel").html("");
                    var num = 1;
                    $.each(menus,function(k,v){
                        //	var menuTree = k;
                        var str =  "<div class='easyui-panel  xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="+v.name+"><input  type='radio'  name='updataService' id=radio"+k+" value="+k+">单击选中本帐套</input><input style='width:110px;height:12px;margin: 0px 3px 0px 5px;' type='text' name='keyWords' id=keyWords"+k+"></input><button type='button' id='_bt_query_'>查询</button><ul id="+k+" class='ztree'></ul></div>";
                        $("#local_panel").append(str);
                        $("#local_panel .easyui-panel").panel({cls:"menu_panel",onExpand:onExpandPanel});
                        initXpStylePanel();
                        $.fn.zTree.init($("#"+k), setting, v);
                        openAllNode(k);
                        if(num=="1"){
                            $(".xpstyle-panel .xpstyle").panel("expand");
                            $("div.xpstyle-panel").find("div.panel-title").addClass("panel-title-bold");
                        }
                        var moduless=v.children;
                        initModulessKey(k, moduless);
                        num++;
                    });
                    $('#linkTree').tabs('select','本地');
                    //	$(".xpstyle-panel .xpstyle").panel("expand");
                }else if(type=="remote"){
                    $.each(menus,function(k,v){
                        //	var menuTree = getTime();
                        var str =  "<div class='easyui-panel  xpstyle' style='width:180px;' collapsible='true' collapsed='true' title=远程:"+remoteIP+"><input type='radio'  name='updataService' id=radio"+k+" value="+k+">单击选中本帐套</input><input style='width:110px;height:12px;margin: 0px 3px 0px 5px;' type='text' name='keyWords' id=keyWords"+k+"></input><button type='button' id='_bt_query_'>查询</button><ul id="+k+" class='ztree'></ul></div>";
                        $("#remote_panel").html(str);
                        $("#remote_panel .easyui-panel").panel({cls:"menu_panel",onExpand:onExpandPanel});
                        $.fn.zTree.init($("#"+k), setting, v);
                        openAllNode(k);
                        var moduless=v.children;
                        initModulessKey(k, moduless);
                    });

                    $('#linkTree').tabs('select','远程');
                    $(".xpstyle-panel .xpstyle").panel("expand");

                }
            }
        }

        function initModulessKey(key, moduless) {
            modulessKey[key] = [];
            moduleKey[key] = [];
            for(var i=0;i<moduless.length;i++){
                if(moduless[i].children&&moduless[i].children.length>0){
                    $.each(moduless[i].children,function(ki,vi){
                        modulessKey[key].push(vi.key);
                        $.each(vi.children,function(ck,cv){
                            moduleKey[key].push(cv.key);
                        });
                    });
                }
            }
        }

        function addModulessKey(key) {
            if(modulessKey[treeNodeId]) {
                modulessKey[treeNodeId].push(key);
            }
        }

        function addModuleKey(key) {
            if(moduleKey[treeNodeId]) {
                moduleKey[treeNodeId].push(key);
            }
        }

        //删除帐套
        function deleteAccount(){
            var options = {
                data:{
                    "treeId":treeNodeId
                },
                url: "${ctx}/design/config/deleteAccount",
                success:function(data){
                    if(data){
                        initMenus();
                    }else{
                        $.messager.alert('提示', '删除失败!' ,'info');
                    }
                }
            };
            fnFormAjaxWithJson(options,true);
        }
        //更改节点名称
        function treeNode_rename(){
            var ztree = $.fn.zTree.getZTreeObj(treeNodeId);
            var tree_nodes = ztree.getSelectedNodes();
            var panert_node =tree_nodes[0];
            ztree.editName(panert_node);

        }
        //单击获得节点属性
        function zTreeOnClick(event, treeId, treeNode, clickFlag){
            //$("#treeModule").data("treeId",treeId);
            //alert(treeId);
            treeNodeId = treeId;
            $("#"+treeId).data("node",treeNode);
        }
        //双击打开模版
        function zTreeOnDblClick(event, treeId, treeNode) {
            //alert(treeNode['id']);
			/* var allKey = getNodePath(treeNode);
			 if(treeNode && treeNode.type == "module") {
			 var tabUrl = "${ctx}/jsp/module.jsp"+"?key="+treeNode['key']+"&nodeId="+treeNode['id']+"&allKey="+allKey;
			 addTab(treeNode.name, tabUrl);
			 } */
            //$("#treeModule").data("treeId",treeId);
            treeNodeId = treeId;
			/* if(treeNode["type"]=="dict"){
			 var parNode = treeNode.getParentNode();
			 createDictPage(parNode,treeNode);
			 }else{ */

            openModule(treeNode);
            //	}

        };
        //添加右键事件
        function zTreeOnRightClick(event, treeId, treeNode) {
            event.preventDefault();
            //$("#treeModule").data("treeId",treeId);
            //alert(treeId);
            treeNodeId = treeId;
            var treeObj = $.fn.zTree.getZTreeObj(treeId);
            treeObj.selectNode(treeNode,false);
            if(treeNode) {
                if(treeNode["type"] == "moduless") {
                    if(treeNode["key"] == "app") {
                        $("#treeModule").data("node",treeNode);
                        $('#menu_appss').menu('show', {
                            left: event.pageX,
                            top: event.pageY
                        });
                    }else{
                        $("#treeModule").data("node",treeNode);
                        $('#menu_moduless').menu('show', {
                            left: event.pageX,
                            top: event.pageY
                        });
                    }
                } else if(treeNode["type"] == "modules") {
                    $("#treeModule").data("node",treeNode);
                    $('#menu_modules').menu('show', {
                        left: event.pageX,
                        top: event.pageY
                    });
                } else if(treeNode["type"] == "module"){
                    $("#treeModule").data("node",treeNode);
                    $("#menu_module").menu('show', {
                        left: event.pageX,
                        top: event.pageY
                    });
                }else if(treeNode["type"]=="menu"){
                    $("#menu_reName").menu('show', {
                        left: event.pageX,
                        top: event.pageY
                    });
                }else if(treeNode["type"]=="dicts"){
                    $("#treeModule").data("node",treeNode);
                    $("#menu_dicts").menu('show', {
                        left: event.pageX,
                        top: event.pageY
                    });

                }else if (treeNode["type"]=="dict"){
                    $("#treeModule").data("node",treeNode);
                    $("#menu_dict").menu('show', {
                        left: event.pageX,
                        top: event.pageY
                    });
                }else if(treeNode["type"]=="forms"){
                    $("#treeModule").data("node",treeNode);
                    $("#menu_forms").menu('show', {
                        left: event.pageX,
                        top: event.pageY
                    });
                }else if(treeNode["type"] == "apps") {
                    $("#treeModule").data("node",treeNode);
                    $('#menu_apps').menu('show', {
                        left: event.pageX,
                        top: event.pageY
                    });
                }else /*if(treeNode["type"]=="form" || treeNode["type"]=="app")*/{
                    $("#treeModule").data("node",treeNode);
                    $("#menu_form").menu('show', {
                        left: event.pageX,
                        top: event.pageY
                    });

                }
            }
        }

        //对ztree的一级节点以及不能上下移动的模块限制拖动
        function zTreeBeforeDrag(treeId, treeNodes) {
            var type = treeNodes[0].type;
			/*if(type=='dict' || type=='dicts' || type=='module' || type=='modules') {
			 return true;
			 }
			 return false;*/
        }

        //只能在同级模块下确认移动
        function zTreeBeforeDrop(treeId, treeNodes, targetNode, moveType) {
            treeNodeId = treeId;
            var type = treeNodes[0].type;
            //console.log(treeNodes[0]);
            //console.log($('#treeModule').data());
            //console.log(targetNode);
            //console.log($('#treeModule').data('newNode'));
            if(true/*type=='module'*/) {
                //可在被拖至能被粘贴的模块下级
                var type = targetNode.getParentNode().type;
                var a = treeNodes[0].getParentNode().id;
                var b = targetNode.getParentNode().id;
                //在同一个父类下可被拖动
                if(a==b && moveType!="inner") {
                    return true;
                } else if((type=='dicts'||type=='modules'||type=='moduless') && moveType!="inner") {
                    cutNode(treeNodes[0]);
                    $("#treeModule").data("node",targetNode.getParentNode());
                    createNewModulessNodeWin(null,targetNode,moveType);
                } else if((targetNode.type=='dicts'||targetNode.type=='modules'||targetNode.type=='moduless') && moveType=="inner") {
                    //var treeObj = $.fn.zTree.getZTreeObj(treeId);
                    //treeObj.selectNode(treeNodes[0],false);
                    //$("#treeModule").data("node",treeNodes[0]);
                    cutNode(treeNodes[0]);
                    $("#treeModule").data("node",targetNode);
                    //treeObj.selectNode(targetNode,false);
                    createNewModulessNodeWin(null);
                } else {
                    alert("别拖到我身后，我不会接受你的！！");
                }
            }else{
                //只能在同一个父类下拖动
                var a = treeNodes[0].getParentNode().id;
                var b = targetNode.getParentNode().id;
                if(a==b && moveType!="inner") {
                    return true;
                }
                if(a!=b){
                    alert("亲，这个模块不能跨模块集合拖动");
                }
            }
            return false;
        }

        function zTreeOnDrop() {
            saveZtreeModule();
        }

        function getAllKey(treeNode){
            if('module'!=treeNode.getParentNode().key&&'app'!=treeNode.getParentNode().key){
                return getAllKey(treeNode.getParentNode());
            } else {
                return treeNode.getParentNode().key+"."+treeNode.key;
            }
        }
        var i = 1;
        function createModule(treeNode) {
// 			var allKey=treeNode.getParentNode().key+"."+treeNode["key"];
//             var allKey=getAllKey(treeNode)+"."+treeNode["key"];
            var allKey=getAllKey(treeNode);
            var moduleName = "";
            var node = null;
            var treeNodeType = $("#dv_moduleType input:checked").val();
            if(treeNodeType == "dict"){
                moduleName = $("#newModuleName").val();
                node = createDictsNode();
            }else if(treeNodeType == "form"){
                moduleName = $("#newModuleName").val();
                node = createFormNode();
            }else if(treeNodeType == "app") {
                moduleName = $("#newModuleName").val();
                node = createFormNode("app");
            }else {
                moduleName = $("#newModuleName").val();
                node = createTreeNode();
            }
			/*if(treeNode["type"]=="dicts"){
			 moduleName = $("#newModuleName").val();
			 node = createDictsNode();
			 }else if(treeNode["type"]=="forms"){
			 moduleName = $("#newModuleName").val();
			 node = createFormNode();
			 }else{
			 moduleName = $("#newModuleName").val();
			 node = createTreeNode();
			 }*/

            //var treeName = treeNode.getParentNode().getParentNode().name;
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var treeName = treeObj.getNodes()[0].name;
            var name= encodeURI(moduleName);
            var tabUrl = "${ctx}/jsp/module.jsp"+"?nodeId="+node['id']+"&key="+node["key"]+"&allKey="+allKey+"&name="+name+"&treeId="+treeNodeId+"&nodesType="+node["type"]+"&title="+treeTabTitle;

            addTab(treeName+"-"+moduleName, tabUrl);
        }

		/* function createDictPage(treeNode,newNode){
		 var allKey=treeNode.getParentNode().key+"."+treeNode["key"];
		 var node =newNode;
		 var moduleName =$("#newDictsName").val();
		 if(!moduleName){
		 moduleName = node.name;
		 }
		 var name= encodeURI(moduleName);
		 var tabUrl = "${ctx}/jsp/module.jsp"+"?nodeId="+node['id']+"&key="+node["key"]+"&allKey="+allKey+"&name="+name+"&treeId="+treeNodeId+"&nodesType="+node.type;
		 addTab(moduleName, tabUrl);
		 }  */
        //获得最后一个子节点id
        function getLastChildrenNodeId(node){
					var nodes = node.children;
					var nodeList = nodes.length;
					var flag = 0;
					for(var i=0;i<nodeList;i++){
						var id = parseInt(nodes[i].id);
						flag = flag>id?flag:id;
					}
					return flag;
					/*
					if(i>=1){
					 return	nodes[i-1].id;
					}
					*/
        }
        //获得父节点及祖父节点key值
        function getNodePath(node){
            if(node){
                var pNode = node.getParentNode();
                var pNodeKey = pNode.key;
                var gpNode = pNode.getParentNode();
                var gpNodeKey = gpNode.key;
                var allKey = gpNodeKey+"."+pNodeKey;
                return allKey;
            }
        }
        //更改节点name属性
        function updataNodeName(nodeId,name){
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var nodes = treeObj.getNodesByParam("id", nodeId, null);
            nodes[0].name=name;
            treeObj.updateNode(nodes[0]);
            //getNodePath(nodes[0]);
            saveZtreeModule();

        }
        //更改节点key属性
        function updataNodeKey(nodeId,key){
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var nodes = treeObj.getNodesByParam("id", nodeId, null);
            nodes[0].key=key;
            treeObj.updateNode(nodes[0]);
            saveZtreeModule();
        }
        //删除节点
        function removeNodes(node){
            if(node){
                $.messager.confirm('温馨提示！','一经删除不能恢复,确定删除该节点', function(r){
                    if (r){
                        var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
                        var treeName = treeObj.getNodes()[0].name;
                        var parentNode = node.getParentNode();
                        var falg = false;
                        var path ="";
                        if(node["type"]=="module"){
                            path=getAllKey(node).replace(".","\\")+"\\"+node["key"]+".xml";
                            colseTab(treeName+"-"+node.name);
                            deleteFile(path,node);
                        }else if(node["type"]=="modules"){
                            path = getAllKey(node).replace(".","\\")+"\\"+node["key"];
                            deleteFile(path,node);
                            var child_nodes=treeObj.transformToArray(node);
                            for(var i=0;i<child_nodes.length;i++){
                                path = getAllKey(child_nodes[i]).replace(".","\\")+"\\"+child_nodes[i]["key"]+".xml";
                                deleteFile(path,child_nodes[i]);
                                colseTab(treeName+"-"+child_nodes[i].name);
                            }
// 							 colseTab(treeName+"-"+node.name);
                        }else if (node['type']=="dict" || node['type']=="form" || node['type']=="app"){
                            path=getAllKey(node).replace(".","\\")+"\\"+node["key"]+".xml";
                            colseTab(treeName+"-"+node.name);
                            deleteFile(path,node);
                        }else if(node["type"]=="apps"){
                            path = getAllKey(node).replace(".","\\")+"\\"+node["key"];
                            deleteFile(path,node);
                            var child_nodes=treeObj.transformToArray(node);
                            for(var i=0;i<child_nodes.length;i++){
                                path = getAllKey(child_nodes[i]).replace(".","\\")+"\\"+child_nodes[i]["key"]+".xml";
                                deleteFile(path,child_nodes[i]);
                                colseTab(treeName+"-"+child_nodes[i].name);
                            }
// 							 colseTab(treeName+"-"+node.name);
                        }/*else if (node['type']=="form"){
						 //path =parentNode.getParentNode().key+"\\"+parentNode["key"]+"\\"+node["key"]+".xml";
						 path=getAllKey(node).replace(".","\\")+"\\"+node["key"]+".xml";
						 colseTab(treeName+"-"+node.name);
						 deleteFile(path,node);
						 }*/
                        else{
                            $.messager.alert('温馨提示！','该节点不允许删除','info');
                        }
                    }
                });
            }

        }
        //新建模块节点
        function createTreeNode() {
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var nodes = treeObj.getSelectedNodes();
            var moduleName = $("#newModuleName").val();
            var moduleKey = $("#newModuleKey").val();
            var iconSkin= $("#moduleIconSkin").combobox("getValue");
            if(!iconSkin){
                iconSkin = "icon-default";
            }
            //	var date = new Date();
            //	var time =date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDay()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
            var newNode = null;
            if(nodes && nodes[0]) {
                //如果有子节点新生成的节点id在最后一个子节点上加1若没有生成节点id长度增加1位
                if(nodes[0].children && nodes[0].children.length){
                    var lastId=parseInt(getLastChildrenNodeId(nodes[0]));
                    newNode = {id:lastId+1, pId:nodes[0].id,key:moduleKey, name:moduleName, iconSkin:iconSkin, type:"module"};
                    treeObj.addNodes(nodes[0], newNode);
                }else{
                    var newId = parseInt(nodes[0].id)*100;
                    newNode = {id:newId + 1 , pId:nodes[0].id,key:moduleKey, name:moduleName, iconSkin:iconSkin, type:"module"};
                    treeObj.addNodes(nodes[0], newNode);
                }
            }
            saveZtreeModule();
            return newNode;
        }
        //新建自定义模块节点
        function createFormNode(type) {
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var nodes = treeObj.getSelectedNodes();
            var formName = $("#newModuleName").val();
            var formKey = $("#newModuleKey").val();
            var   iconSkin;
            if(type=='app'){
                iconSkin = $("#moduleIconSkin").val();

                iconSkin =iconSkin? "\\u"+parseInt(iconSkin,10).toString(16):"\ue025";
            }else{
                iconSkin= $("#moduleIconSkin").combobox("getValue");
                if(!iconSkin){
                    iconSkin = "icon-folder-default";
                }
            }
            var newNode = null;
            if(nodes && nodes[0]) {
                //如果有子节点新生成的节点id在最后一个子节点上加1若没有生成节点id长度增加1位
                if(nodes[0].children && nodes[0].children.length){
                    var lastId=parseInt(getLastChildrenNodeId(nodes[0]));
                    newNode = {id:lastId+1, pId:nodes[0].id,key:formKey, name:formName, iconSkin:iconSkin, type:type||"form"};
                    treeObj.addNodes(nodes[0], newNode);
                }else{
                    var newId = parseInt(nodes[0].id)*100;
                    newNode = {id:newId + 1 , pId:nodes[0].id,key:formKey, name:formName, iconSkin:iconSkin, type:type||"form"};
                    treeObj.addNodes(nodes[0], newNode);
                }
            }
            saveZtreeModule();
            return newNode;
        }

        //展开所有节点
        function openAllNode(tree_name){
            var treeObj = $.fn.zTree.getZTreeObj(tree_name);
            treeObj.expandAll(true);
        }
        //上移
        function moveUp(node){
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var preNode = node.getPreNode();
            if(preNode){
                treeObj.moveNode(preNode, node, "prev");
                saveZtreeModule();
            }else{
                $.messager.alert('提示！','该节点不能再上移了','info');
            }

        }
        //下移
        function moveDown(node){
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var nextNode = node.getNextNode();
            if(nextNode){
                treeObj.moveNode(nextNode, node, "next");
                saveZtreeModule();
            }else{
                $.messager.alert('提示！','该节点不能再下移了','info');
            }
        }
        //剪切节点
        function cutNode(node){
            var parentNode = node.getParentNode();
            var path =getAllKey(node).replace(".","\\")+"\\"+node["key"]+".xml";
            $("#treeModule").data("oldPath",path);
            $("#treeModule").data("pasteType","cut");
            $("#treeModule").data("cutNode",node);
        }
        //复制节点
        //	var newTreeNode={};
        function copyNode(childrenNode){
            var tempId = treeNodeId;
            $("#treeModule").data("copyTreeId",tempId);
            if(childrenNode){
                if(childrenNode["type"]=="moduless"||childrenNode["type"]=="menu"){
                    $.messager.alert('警告！','该节点不允许复制!','warning');
                }else{
                    var node = $.extend({},childrenNode);
                    node["name"] = "复制_"+childrenNode["name"];
                    node["key"] = "Copy_"+childrenNode["key"];
                    $("#treeModule").data("pasteType","copy");
                    $("#treeModule").data("newNode",node);
                    $("#treeModule").data("oldKey",childrenNode["key"]);
                }
            }
        }
        //粘贴节点
        function pasteNode(parentNode,targetNode,isSilent){
            //	alert(parentNode);
            $("#createModuleWin").window("close");
            var nodes=[];
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var newTreeNode = null;
            var type = $("#treeModule").data("pasteType");
            if(type=="copy"){
                newTreeNode = $("#treeModule").data("newNode");
            }else if(type=="cut"){
                newTreeNode = $("#treeModule").data("cutNode");
            }


            if(newTreeNode){
                if(parentNode){/*设置粘贴的节点id*/
                    if(parentNode.children && parentNode.children.length){
                        var lastId=parseInt(getLastChildrenNodeId(parentNode));
                        newTreeNode["id"] = lastId+1;
                    }else{
                        newTreeNode["id"] = parentNode["id"]+"01";
                    }
                }

                if(newTreeNode){
                    $('#createModulessWin').window('close');
                    if(/*newTreeNode["type"]=="module"&&*/ parentNode["type"]=="modules" && type=="copy"){
                        newTreeNode["name"] = $("#newModuleName").val();/*模块集合名称*/
                        var oldKey = $("#treeModule").data("oldKey");
                        newTreeNode["key"] = $("#newModuleKey").val();/*模块属性key*/
                        treeObj.addNodes(parentNode, newTreeNode);
                        newAllKey = getAllKey(parentNode)+"."+newTreeNode["key"];
                        saveZtreeModule();
                        addModuleKey(newTreeNode["key"]);
                        openModule(newTreeNode,newAllKey,oldKey);
                    }else if(/*newTreeNode["type"]=="module"&&*/ parentNode["type"]=="modules" && type=="cut"){
                        var oldPath = $("#treeModule").data("oldPath");
// 						var newPath = parentNode.getParentNode().key+"\\"+parentNode["key"]+"\\";
                        var newPath = getAllKey(parentNode).replace(".","\\")+"\\";
                        changeZtreeModule(parentNode,newTreeNode,newPath,oldPath,targetNode,isSilent);
                    }else if(/*newTreeNode["type"]=="module"&&*/ parentNode["type"]=="apps" && type=="copy"){
                        newTreeNode["type"] ="app";
                        //newTreeNode["iconSkin"] ="";
                        newTreeNode["name"] = $("#newModuleName").val();
                        var oldKey = $("#treeModule").data("oldKey");
                        newTreeNode["key"] = $("#newModuleKey").val();
                        treeObj.addNodes(parentNode, newTreeNode);
                        newAllKey = getAllKey(parentNode)+"."+newTreeNode["key"];
                        saveZtreeModule();
                        addModuleKey(newTreeNode["key"]);
                        openModule(newTreeNode,newAllKey,oldKey);
                    }else if(/*newTreeNode["type"]=="module"&&*/ parentNode["type"]=="apps" && type=="cut"){
                        var oldPath = $("#treeModule").data("oldPath");
// 						var newPath = parentNode.getParentNode().key+"\\"+parentNode["key"]+"\\";
                        var newPath = getAllKey(parentNode).replace(".","\\")+"\\";
                        changeZtreeModule(parentNode,newTreeNode,newPath,oldPath,targetNode,isSilent);
                    }else if(newTreeNode["type"]=="modules"&&parentNode["type"]=="moduless"){
                        var len = $("#treeModule").data("newNode").children.length;
                        var newNodeName = $("#newNodeName").val();
                        var newNodeKey = $("#newNodeKey").val();
                        newTreeNode["name"]=newNodeName;
                        var moduleOldKey = newTreeNode["key"];
                        newTreeNode["key"]= newNodeKey;
                        for(var i=0;i<len;i++){
                            nodes[i] = $("#treeModule").data("newNode").children[i];
                            //	newAllKey = parentNode.key+"."+newNodeKey+"."+nodes[i].key;
// 							newAllKey = parentNode.key+"."+moduleOldKey+"."+nodes[i].key;
                            newAllKey = getAllKey(parentNode)+"."+nodes[i].key;
                            openModule(nodes[i],newAllKey,nodes[i].key,newNodeKey);
                        }
                        treeObj.addNodes(parentNode, newTreeNode);
                        saveZtreeModule();
                    }else if(newTreeNode["type"]=="dict"&&parentNode["type"]=="dicts"){
                        newTreeNode["name"] = $("#newModuleName").val();
                        var oldKey = $("#treeModule").data("oldKey");
                        newTreeNode["key"] = $("#newModuleKey").val();
                        treeObj.addNodes(parentNode, newTreeNode);
// 						newAllKey = parentNode.getParentNode().key+"."+parentNode["key"]+"."+newTreeNode["key"];
                        newAllKey = getAllKey(newTreeNode)+"."+newTreeNode["key"];
                        saveZtreeModule();
                        addModuleKey(newTreeNode["key"]);
                        openModule(newTreeNode,newAllKey,oldKey);
                    }else if(newTreeNode["type"]=="form"&&parentNode["type"]=="forms"){
                        newTreeNode["name"] = $("#newModuleName").val();
                        var oldKey = $("#treeModule").data("oldKey");
                        newTreeNode["key"] = $("#newModuleKey").val();
                        treeObj.addNodes(parentNode, newTreeNode);
// 						newAllKey = parentNode.getParentNode().key+"."+parentNode["key"]+"."+newTreeNode["key"];
                        newAllKey = getAllKey(parentNode)+"."+newTreeNode["key"];
                        saveZtreeModule();
                        addModuleKey(newTreeNode["key"]);
                        openModule(newTreeNode,newAllKey,oldKey);
                    }else if(newTreeNode["type"]=="module"&&parentNode["type"]=="forms"){
                        newTreeNode["type"]="form";
                        newTreeNode["name"] = $("#newModuleName").val();
                        var oldKey = $("#treeModule").data("oldKey");
                        newTreeNode["key"] = $("#newModuleKey").val();
                        treeObj.addNodes(parentNode, newTreeNode);
// 						newAllKey = parentNode.getParentNode().key+"."+parentNode["key"]+"."+newTreeNode["key"];
                        newAllKey = getAllKey(parentNode)+"."+newTreeNode["key"];
                        saveZtreeModule();
                        addModuleKey(newTreeNode["key"]);
                        openModule(newTreeNode,newAllKey,oldKey);
                    }else{
                        if(!newTreeNode["type"]){
                            $.messager.alert('警告！','粘贴为空请先复制再进行粘贴!');
                        }else{
                            $.messager.alert('警告！','请粘贴节点到对应父级节点下面!','warning');
                            return false;
                        }
                    }
                }

            }else{
                createModule(parentNode);
            }
            return true;
        }
        //打开模块
        function openModule(treeNode,newKey,oldKey,flag){
            var allKey = getAllKey(treeNode);
            if(treeNode/* && (treeNode.type == "module" || treeNode.type == "dict"
			 || treeNode.type == "form" || treeNode.type == "app")*/) {
                var name= encodeURI(treeNode["name"]);
                var tabUrl = "";
                //	var tree_name = treeNode.getParentNode().getParentNode().getParentNode().name;
                //	alert("treeTabTitle:"+treeTabTitle);
                //	var tabUrl = "${ctx}/jsp/module.jsp"+"?key="+treeNode['key']+"&nodeId="+treeNode['id']+"&allKey="+allKey+"&newKey="+newKey+"&name="+name+"&oldKey="+oldKey+"&flag="+flag+"&treeId="+treeNodeId;
                //var tabTitle = 	treeTabTitle;
                if(newKey||oldKey||flag){
                    if(treeNode.type=="dict"){
                        tabUrl = "${ctx}/jsp/module.jsp"+"?key="+treeNode['key']+"&nodeId="+treeNode['id']+"&allKey="+allKey+"&newKey="+newKey+"&name="+name+"&oldKey="+oldKey+"&flag="+flag+"&treeId="+treeNodeId+"&copyTreeId="+$("#treeModule").data("copyTreeId")+"&tabTitle="+treeTabTitle+"&nodesType="+treeNode["type"];
                    }else if(treeNode.type=="form" || treeNode.type=="view"){
                        tabUrl = "${ctx}/jsp/module.jsp"+"?key="+treeNode['key']+"&nodeId="+treeNode['id']+"&allKey="+allKey+"&newKey="+newKey+"&name="+name+"&oldKey="+oldKey+"&flag="+flag+"&treeId="+treeNodeId+"&copyTreeId="+$("#treeModule").data("copyTreeId")+"&title="+treeTabTitle+"&nodesType="+treeNode["type"];
                    }else if(treeNode.type=="app"){
                        tabUrl = "${ctx}/jsp/module.jsp"+"?key="+treeNode['key']+"&nodeId="+treeNode['id']+"&allKey="+allKey+"&newKey="+newKey+"&name="+name+"&oldKey="+oldKey+"&flag="+flag+"&treeId="+treeNodeId+"&copyTreeId="+$("#treeModule").data("copyTreeId")+"&title="+treeTabTitle+"&nodesType="+treeNode["type"];
                    }else{
                        tabUrl = "${ctx}/jsp/module.jsp"+"?key="+treeNode['key']+"&nodeId="+treeNode['id']+"&allKey="+allKey+"&newKey="+newKey+"&name="+name+"&oldKey="+oldKey+"&flag="+flag+"&treeId="+treeNodeId+"&copyTreeId="+$("#treeModule").data("copyTreeId")+"&tabTitle="+treeTabTitle+"&nodesType="+treeNode["type"];
                    }
                    // tabUrl = "${ctx}/jsp/module.jsp"+"?key="+treeNode['key']+"&nodeId="+treeNode['id']+"&allKey="+allKey+"&newKey="+newKey+"&name="+name+"&oldKey="+oldKey+"&flag="+flag+"&treeId="+treeNodeId+"&copyTreeId="+$("#treeModule").data("copyTreeId")+"&tabTitle="+treeTabTitle;
                }else{
                    tabUrl = "${ctx}/jsp/module.jsp"+"?key="+treeNode['key']+"&nodeId="+treeNode['id']+"&allKey="+allKey+"&newKey="+newKey+"&name="+name+"&oldKey="+oldKey+"&flag="+flag+"&treeId="+treeNodeId+"&title="+treeTabTitle+"&nodesType="+treeNode["type"];
                }
                var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
                var tree_name = treeObj.getNodes()[0].name;
                addTab(tree_name+"-"+treeNode.name, tabUrl);
            }
        }
        //创建新模块集合
        function createModuleNode() {
            var nodesName = $("#nodesName").val();
            var nodeKey = $("#nodeKey").val();
            var iconSkin = $("#iconSkin").combobox("getValue");
            if(!iconSkin){
                iconSkin = "icon-default_Document";
            }
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var nodes = treeObj.getSelectedNodes();
            if(nodes && nodes[0]) {
                //如果有子节点新生成的节点id在最后一个子节点上加1若没有生成节点id长度增加1位
                if(nodes[0].children && nodes[0].children.length){
                    var lastId=parseInt(getLastChildrenNodeId(nodes[0]));
                    var newNode = {id:lastId+1, pId:nodes[0].id, name:nodesName, iconSkin:iconSkin, type:"modules", key: nodeKey};
                    treeObj.addNodes(nodes[0], newNode);
                }else{
                    var newId = parseInt(nodes[0].id)*100;
                    var newNode = {id:newId + 1, pId:nodes[0].id, name:nodesName, iconSkin:iconSkin, type:"modules", key: nodeKey};
                    treeObj.addNodes(nodes[0], newNode);
                }
            }
            saveZtreeModule();
            addModulessKey(nodeKey);
            $('#openNodeWin').window('close');
        }

        //创建新app模块集合
        function createAppModuleNode() {
            var nodesName = $("#appNodesName").val();
            var nodeKey = $("#appNodeKey").val();
            var iconSkin = $("#appIconSkin").val();
            if(!iconSkin){
                iconSkin = "\ue025";
            }else{
                iconSkin="\\u"+parseInt(iconSkin,10).toString(16);
            }
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var nodes = treeObj.getSelectedNodes();
            if(nodes && nodes[0]) {
                //如果有子节点新生成的节点id在最后一个子节点上加1若没有生成节点id长度增加1位
                if(nodes[0].children && nodes[0].children.length){
                    var lastId=parseInt(getLastChildrenNodeId(nodes[0]));
                    var newNode = {id:lastId+1, pId:nodes[0].id, name:nodesName, iconSkin:iconSkin, type:"apps", key: nodeKey};
                    treeObj.addNodes(nodes[0], newNode);
                }else{
                    var newId = parseInt(nodes[0].id)*100;
                    var newNode = {id:newId + 1, pId:nodes[0].id, name:nodesName, iconSkin:iconSkin, type:"apps", key: nodeKey};
                    treeObj.addNodes(nodes[0], newNode);
                }
            }
            saveZtreeModule();
            addModulessKey(nodeKey);
            $('#openAppNodeWin').window('close');
        }

        function createModuleNodeWin() {
            $("#createModulessWin").prop("title","模块集合");
            var nodesName = "模块集合" + i;
            var nodeKey = "modules" + i++;
            $("#nodesName").val(nodesName);
            $("#nodeKey").val(nodeKey);
            initImageCombox("iconSkin");
            $('#openNodeWin').window('open');
        }

        function createAppModuleNodeWin() {
            $("#createModulessWin").prop("title","模块集合");
            var nodesName = "模块集合" + i;
            var nodeKey = "modules" + i++;
            $("#appNodesName").val(nodesName);
            $("#appNodeKey").val(nodeKey);
            fontIconPicker("appIconSkin");
            $('#openAppNodeWin').window('open');
        }

        // 创建业务字典节点
        function createDictsNode(){
            var nodesName = $("#newModuleName").val();
            var nodeKey = $("#newModuleKey").val();
            var iconSkin= $("#moduleIconSkin").combobox("getValue");
            if(!iconSkin){
                iconSkin = "icon-address_Book";
            }
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var nodes = treeObj.getSelectedNodes();
            var newNode = "";
            if(nodes && nodes[0]) {
                //如果有子节点新生成的节点id在最后一个子节点上加1若没有生成节点id长度增加1位
                if(nodes[0].children && nodes[0].children.length){
                    var lastId=parseInt(getLastChildrenNodeId(nodes[0]));
                    newNode = {id:lastId+1, pId:nodes[0].id, name:nodesName, iconSkin:iconSkin, type:"dict", key: nodeKey};
                    treeObj.addNodes(nodes[0], newNode);
                }else{
                    var newId = parseInt(nodes[0].id)*100;
                    newNode = {id:newId + 1, pId:nodes[0].id, name:nodesName, iconSkin:iconSkin, type:"dict", key: nodeKey};
                    treeObj.addNodes(nodes[0], newNode);
                }
            }
            saveZtreeModule();
            $('#createDictsWin').window('close');
            //	createDictPage(nodes[0],newNode);
            return newNode;
        }

        function createNewModulessNodeWin(newNode,targetNode,isSilent) {
            //alert(newNode.type+":"+newNode.name+":"+newNode.key);
            if(newNode==null){
                if($("#treeModule").data("pasteType")=="cut"){
                    var x = pasteNode($('#treeModule').data('node'),targetNode,isSilent);
                    if(x){
                        return true;
                    }
                    else{
                        return false;
                    }
                }else{
                    $("#treeModule").data("newNode",null);
                    $("#newModuleName").val("ModuleName");
                    $("#newModuleKey").val("ModuleKey");
                    initImageCombox("moduleIconSkin");
                    setRadioEvent();
                    $('#createModuleWin').window('open');
                }
            }else if(newNode["type"]=="modules"){
                var newNodeName = newNode["name"];
                var newNodeKey = newNode["key"];
                $("#newNodeName").val(newNodeName);
                $("#newNodeKey").val(newNodeKey);
                initImageCombox("moduleIconSkin");
                setRadioEvent();
                $('#createModulessWin').window('open');
            }else if(newNode["type"]=="module"){
                if($("#treeModule").data("pasteType")=="cut"){
                    pasteNode($('#treeModule').data('node'));
                }else{
                    var newModuleName = newNode["name"];
                    var newModuleKey = newNode["key"];
                    $("#newModuleName").val(newModuleName);
                    $("#newModuleKey").val(newModuleKey);
                    $("#moduleType_module").attr("checked",true);
                    initImageCombox("moduleIconSkin");
                    setRadioEvent();
                    $('#createModuleWin').window('open');
                }
            }else if(newNode["type"]=="dict"){
				/* $('#createModuleWin').window('open');
				 $("#newDictsName").val("dictsName");
				 $("#newDictsKey").val("dictsKey"); */
                var newModuleName = newNode["name"];
                var newModuleKey = newNode["key"];
                $("#newModuleName").val(newModuleName);
                $("#newModuleKey").val(newModuleKey);
                $("#moduleType_dict").attr("checked",true);
                initImageCombox("moduleIconSkin");
                setRadioEvent();
                $('#createModuleWin').window('open');
            }else if(newNode["type"]=="form"){
                var newModuleName = newNode["name"];
                var newModuleKey = newNode["key"];
                $("#newModuleName").val(newModuleName);
                $("#newModuleKey").val(newModuleKey);
                $("#moduleType_form").attr("checked",true);
                initImageCombox("moduleIconSkin");
                setRadioEvent();
                $('#createModuleWin').window('open');
            }else if(newNode["type"]=="app"){
                if($("#treeModule").data("pasteType")=="cut"){
                    pasteNode($('#treeModule').data('node'));
                }else{
                    var newModuleName = newNode["name"];
                    var newModuleKey  = newNode["key"];
                    $("#newModuleName").val(newModuleName);
                    $("#newModuleKey").val(newModuleKey);
                    $("#moduleType_app").attr("checked",true);
                    setRadioEvent();
                    fontIconPicker("moduleIconSkin");
                    $('#createModuleWin').window('open');
                }
            }



        }
        var setting = {
            data: {
                simpleData: {
                    enable: true
                }
            },
            callback: {
                onClick: zTreeOnClick,
                onDblClick: zTreeOnDblClick,
                onRightClick: zTreeOnRightClick,
                onRename: renameTitleAndSave,
                beforeDrag: zTreeBeforeDrag,
                beforeDrop: zTreeBeforeDrop,
                onDrop: zTreeOnDrop
            },
            edit: {
                enable: true,
                showRemoveBtn: false,
                showRenameBtn: false
            }
        };
        function openFileWin() {
            $('#openFileWin').window('setTitle', "打开文件");
            $("#bt_login").show();
            $("#tr_updateType").hide();
            $('#openFileWin').window('open');
        }

        function doOpenUpgrade() {
            $('#openFileWin').window('setTitle', "更新服务器");
            $("#bt_upgrade").show(); /*更新按钮显示*/
            $("#rad_local").hide();/*服务器本地单选框隐藏*/
            $("#lab_local").hide();/*服务器本地文字隐藏*/
            $("#loginTr").show();/*链接地址显示*/
            $("#tr_updateType").show();/*更新方式显示*/
            $("#rad_remote").attr("checked", "checked");/*远程服务器勾选上*/
            $('#openFileWin').window('open');/*打开更新服务器窗口*/
        }

        var treeTabTitle="";
        $(document).ready(function() {
			/*上传账套事件  2019-8-20*/
			var $list = $('#thelist');
            var uploader = WebUploader.create({
                // swf文件路径
                swf: '${ctx}/js/webUploader/Uploader.swf',
                // 文件接收服务端。
                server: '${ctx}/design/uploadFile',
                // 选择文件的按钮。可选。
                // 内部根据当前运行是创建，可能是input元素，也可能是flash.
                pick: {
                    id: '#picker',
                    innerHTML: '打开账套文件',
                    multiple: false
                },
                auto: true,
				method: 'POST',
                accept: {
                    title: '请选择jwp文件',
                    extensions: 'jwp',
                    mimeTypes: '.jwp' // 扩展名
                },
                fileNumLimit: 1 //可上传的文件个数
            })
			// 当有文件被添加进队列的时候
			uploader.on('fileQueued', function (file) {
				$list.append('<div id="' + file.id + '" class="item">' +
						'<h4 class="info">' + file.name + '</h4>' +
						'<p class="state">等待上传...</p>' +
						'</div>');
			});
			// 文件上传过程中创建进度条实时显示。
			uploader.on('uploadProgress', function (file, percentage) {
				var $li = $('#' + file.id),
						$percent = $li.find('.progress .progress-bar');
				// 避免重复创建
				if (!$percent.length) {
					$percent = $('<div class="progress progress-striped active">' +
							'<div class="progress-bar" role="progressbar" style="width: 0%">' +
							'</div>' +
							'</div>').appendTo($li).find('.progress-bar');
				}
				$li.find('p.state').text('上传中');
				$percent.css('width', percentage * 100 + '%');
			})
			uploader.on('uploadSuccess', function (file,response) {
				$('#' + file.id).find('p.state').text('已上传');
				updataFlag = "true";
				if(response){
					//alert("file"+file+" data:"+data+" response:"+response);
					initMenus();

				}
			});
			uploader.on('uploadError', function (file) {
				$('#' + file.id).find('p.state').text('上传出错');
				alert("文件上传失败");
			});
			uploader.on('uploadComplete', function (file) {
				$('#' + file.id).find('.progress').fadeOut();
				$list.children().remove();
				$("#openFileWin").window("close");
			})
           /* $("#uploadTree").uploadify({
                'swf' : '${ctx}/js/uploadify/uploadify.swf',
                //		'cancelImage':'${ctx}/js/uploadify/uploadify-cancel.png',
                'uploader': "${ctx}/design/uploadFile",
                'method':'Post',
                'buttonText' : '打开帐套文件',
                'fileTypeDesc' : '请选择jwp文件',// 文件说明
                'fileTypeExts' : '*.jwp', // 扩展名
                'auto': true,
                'multi': false, /!*是否多文件上传*!/
                'queueSizeLimit':1,//可上传的文件个数
                'onUploadError': function(event, queueID, fileObj)
                {
                    alert("文件上传失败");
                } ,
                'onUploadSuccess' : function(file, data, response) {
                    $("#openFileWin").window("close");
                    console.log(response)
                    updataFlag = "true";
                    if(response){
                        //alert("file"+file+" data:"+data+" response:"+response);
                        initMenus();

                    }
                }
            });*/

            $("#linkTree").tabs({"onSelect":function(title){
                if(title=="本地"){
                    treeTabTitle="local";

                }else if(title=="远程"){
                    treeTabTitle="remote";
                }
            }});
            //    $("#uploadify").hide();
			/*
			 $("#loginUrl").combobox({
			 valueField:'id',
			 textField:'text'
			 });*/
            //$("#loginUrl").combobox("loadData", [{"id":1,"text":"http://127.0.0.1/gsweb"}]);
            //	$.fn.zTree.init($("#menuTree"), setting, zNodes);
            //initMenus();
            $(document).bind('keydown', 'Ctrl+c',function (evt){evt.preventDefault();copyNode($("#treeModule").data("node"));});
            //快捷方式复制
            $(document).bind('keydown', 'Ctrl+v',function (evt){evt.preventDefault();createNewModulessNodeWin($("#treeModule").data("newNode"));});
            //快捷方式删除
            $(document).bind('keydown', 'Ctrl+d',function (evt){evt.preventDefault();removeNodes($("#treeModule").data("node"));});

            $("#loginName").bind('keydown', function (evt){
                if(evt.keyCode==13){
                    $("#passwd").focus();
                }
            });
            $("#passwd").bind('keydown', function (evt){
                if(evt.keyCode==13){
                    if(!$("#bt_login").is(":hidden")&& $("#bt_upgrade").is(":hidden")){
                        $("#bt_login").trigger("click");
                    }else if($("#bt_login").is(":hidden")&& !$("#bt_upgrade").is(":hidden")){
                        $("#bt_upgrade").trigger("click");
                    }
                }
            });
			/* var modulessKey=[];
			 var moduleKey=[]; */
            //添加key通过属性名自动生成
            $("#newModuleName").live("blur",function (){	//自动转换模块key
                var temp= $.fn.toPinyin($(this).val());
                var newTemp ="";
                $.each(temp.split("_"),function(k,v){
                    newTemp +=v;
                });
                var flag = $.fn.module.checkKey("module",moduleKey[treeNodeId],newTemp);
                if(flag){
                    $("#newModuleName").val($(this).val()+"*");
                    $("#newModuleKey").val(newTemp+"*");
                }else{
                    $("#newModuleKey").val(newTemp);
                }
            });
            $("#nodesName").live("blur ",function (){		//自动转换模块集合key
                var temp= $.fn.toPinyin($(this).val());
                var newTemp ="";
                $.each(temp.split("_"),function(k,v){
                    newTemp +=v;
                });
                var flag = $.fn.module.checkKey("moduless",modulessKey[treeNodeId],newTemp);
                if(flag){
                    $("#nodesName").val($(this).val()+"*");
                    $("#nodeKey").val(newTemp+"*");
                }else{
                    $("#nodeKey").val(newTemp);
                }
            });

            $("#appNodesName").live("blur ",function (){		//自动转换模块集合key
                var temp= $.fn.toPinyin($(this).val());
                var newTemp ="";
                $.each(temp.split("_"),function(k,v){
                    newTemp +=v;
                });
                var flag = $.fn.module.checkKey("moduless",modulessKey[treeNodeId],newTemp);
                if(flag){
                    $("#appNodesName").val($(this).val()+"*");
                    $("#appNodeKey").val(newTemp+"*");
                }else{
                    $("#appNodeKey").val(newTemp);
                }
            });

            $("#newDictsName").live("blur ",function (){		//自动转换模块集合key
                var temp= $.fn.toPinyin($(this).val());
                var newTemp ="";
                $.each(temp.split("_"),function(k,v){
                    newTemp +=v;
                });
                var flag = $.fn.module.checkKey("moduless",modulessKey[treeNodeId],newTemp);
                if(flag){
                    $("#newDictsName").val($(this).val()+"*");
                    $("#newDictsKey").val(newTemp+"*");
                }else{
                    $("#newDictsKey").val(newTemp);
                }
            });
            $("#newNodeName").live("blur",function (){		//粘贴时自动转换模块集合key
                var temp= $.fn.toPinyin($(this).val());
                var newTemp ="";
                $.each(temp.split("_"),function(k,v){
                    newTemp +=v;
                });
                var flag = $.fn.module.checkKey("moduless",modulessKey[treeNodeId],newTemp);
                if(flag){
                    $("#newNodeName").val($(this).val()+"*");
                    $("#newNodeKey").val(newTemp+"*");
                }else{
                    $("#newNodeKey").val(newTemp);
                }
            });

            //key修改后进行判断
            $("#newModuleKey").live("keyup   ",function (){
                var temp = $("#newModuleKey").val();
                var flag = $.fn.module.checkKey("module",moduleKey[treeNodeId],temp);
                if(flag){
                    $("#newModuleKey").val(temp+"*");
                }
            });
            $("#nodeKey").live("keyup   ",function (){
                var temp = $("#nodeKey").val();
                var flag=$.fn.module.checkKey("moduless",modulessKey[treeNodeId],temp);
                if(flag){
                    $("#nodeKey").val(temp+"*");
                }
            });
            $("#newNodeKey").live("keyup   ",function (){
                var temp = $("#newNodeKey").val();
                var flag=$.fn.module.checkKey("moduless",modulessKey[treeNodeId],temp);
                if(flag){
                    $("#newNodeKey").val(temp+"*");
                }
            });

            $('#openFileWin').window({
                onClose:function() {
                    $("#bt_upgrade").hide();
                    $("#bt_login").hide();
                    $("#rad_local").show();
                    $("#lab_local").show();
                },
                onOpen:function(){
                    var url = getCookie("loginUrl");
                    if(url){
                        var urls = url.split(';');
                        var lastUrl = urls[urls.length-1];
                        var data = [];
                        for(var i=0; i<urls.length; i++){
                            var json = {
                                "id":null,
                                "text":null
                            };
                            json["id"]=urls[i];
                            json["text"]=urls[i];
                            data.push(json);
                        }
                        //console.log(data);
                        $("#loginUrl").combobox({
                            data : data,
                            valueField:'id',
                            textField:'text'
                        });
                        //$("#loginUrl").combobox("loadData",data);
                        $("#loginUrl").combobox("setValue",lastUrl);
                        //$("#loginUrl").combobox("setText",lastUrl);

                    }
                    $("#loginName").focus();
                }
            });

            $('#main_tabs_div').tabs({
                onBeforeClose: function(title,index){
                    if(confirm("关闭之前要保存（" + title + "）数据吗？")) {
                        var win = getTabDataWin(title);
                        if(win && win.saveModule) {
                            win.saveModule();
                        }
                        return true;
                    } else {
                        return true;
                    }
                }
            });

            setInterval(getSession,300000);
						$('body').on('click', '#_bt_query_', function() {
							/*给动态生成的元素绑定查询事件*/
							queryFunction(this);
						});
        });
        //查询框
        function queryFunction(temp) {
            var queryValue =  $(temp).prev().val();
            var ulId = $(temp).next().attr("ID");
            var treeObj = $.fn.zTree.getZTreeObj(ulId);
            var allNodes = treeObj.getNodes();
            var nodes = allNodes[0].children;
            var childNodes = nodes[0].children;
            var len = childNodes.length;
            for (var i = 0; i < len; i++) {
                var deepNode = childNodes[i].children;
                var deepLen = deepNode.length;
                for (var j = 0;j<deepLen;j++){
                    var funName = deepNode[j].name;
                    var isSelect = funName.indexOf(queryValue);
                    if (isSelect > -1) {
                        /*选中指定节点*/
                        treeObj.selectNode(deepNode[j]);
                        break;
                    }
                }
            }
        }
        function getSession(){
            var options = {
                url: "${ctx}/design/getSession",
                success:function(data) {
                    if(!data.msg){
                        window.location.replace("${ctx}/design/index");
                    }
                }
            };
            fnFormAjaxWithJson(options,true);
        }
        var pasteRowObj = [], pasteTableObj = {},pasteModuleObj={};
        var formKey = "";
        //复制表的KEY
        function copyFormKey(key){
            formKey = key;
        }
        //获取表的KEY
        function getFormKey(){
            return formKey;
        }
        //复制表格行
        function copyRows(data){
            pasteRowObj = data;
        }
        //粘贴表格行
        function getPasteRows(){
            return pasteRowObj;
        }
        //复制表
        function copyTables(data){
            pasteTableObj = $.extend(true,{}, data);
        }
        //粘贴表
        function getPasteTables(){
            return pasteTableObj;
        }
        //复制sql语句
        function copySqls(data){
            pasteSqlObj = $.extend(true,{}, data);
        }
        //粘贴sql语句
        function getPasteSqls(){
            return pasteSqlObj;
        }
        function doOpenAboutWin() {
            $("#aboutWin").window("open");
        }
        function showUrl(type){
            if(type){
                $("#loginTr").hide();
            }else{
                $("#loginTr").show();
            }
        }
        //初始化图标资源控件
        function initImageCombox(id,selVal){
            $("#"+id).combobox({
                url: "${ctx}/js/easyui/iconSkin-data.json",
                valueField:'id',
                textField:'text',
                formatter:function(row){
                    if(row.id)
                        return "<span style='width: 16px;height:16px;display:inline-block;vertical-align:middle;' class='"+row.id+"'></span><span class='item-text'>"+row.text+"</span>";
                }
            });
            if(selVal){
                $("#"+id).combobox("setValue",selVal);
            }
        }
        //打开设置图标窗口
        function openIconSkinWin(node){
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var iconSkin = node.iconSkin;
            var  parentType = node.getParentNode().type;
            if(node.type=='app' ||node.type=="apps"){
                fontIconPicker("setAppIconSkin",iconSkin)
                $("#appIconSkinWin").window("open");
            }else{
                initImageCombox("setIconSkin",iconSkin);
                $("#iconSkinWin").window("open");
            }
        }
        function setIconSkin(node){
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var newIconSkin;
            if(node.type =="app"){
                newIconSkin = $("#setAppIconSkin").val();
                newIconSkin = newIconSkin?"\\u"+parseInt(newIconSkin,10).toString(16):"\ue025";
                $("#appIconSkinWin").window("close");
            }else{
                newIconSkin = $("#setIconSkin").combobox("getValue");
                $("#iconSkinWin").window("close");
            }
            node.iconSkin=newIconSkin;
            treeObj.updateNode(node);
            saveZtreeModule();
        }
        //添加自定义表单
        function createFormsNode(){
            var treeObj = $.fn.zTree.getZTreeObj(treeNodeId);
            var node = treeObj.getNodeByParam("type", "forms", null);
            if(!node){
                var nodes = treeObj.getSelectedNodes();
                if(nodes && nodes[0]) {
                    if(nodes[0].children && nodes[0].children.length){
                        var lastId=parseInt(getLastChildrenNodeId(nodes[0]));
                        var newNode = {id:lastId+1, pId:nodes[0].id, name:"自定义表单", iconSkin:"icon03", type:"forms", key: "forms"};
                        treeObj.addNodes(nodes[0], newNode);
                    }
                }
                saveZtreeModule();
            }else{
                $.messager.alert('警告！','自定义表单节点已存在!','warning');
            }
        }

        function setRadioEvent(){
            var  radio = $("input[name='moduleType']");
            radio.change(function(){
                var  newValue = $(this).val();
                if(newValue=='app'){
                    fontIconPicker("moduleIconSkin");
                }else{
                    var moduleIconSkin =  $('#moduleIconSkin')
                    if(moduleIconSkin.attr("style")!="width: 200px; display: none;"){
                        var  content = '<td><input id="moduleIconSkin" name="moduleIconSkin" style="width:200px;" ></input></td>';
                        moduleIconSkin.parent().replaceWith(content);
                        initImageCombox("moduleIconSkin");
                    }
                }
            })
        }


        function fontIconPicker(id,defaultIcon){
            $('#'+id).parent().children("span").remove();

            var icm_icons = {
                'Web Applications' : [57436, 57437, 57438, 57439, 57524, 57525, 57526, 57527, 57528, 57531, 57532, 57533, 57534, 57535, 57536, 57537, 57541, 57545, 57691, 57692],
                'Business Icons' : [57347, 57348, 57375, 57376, 57377, 57379, 57403, 57406, 57432, 57433, 57434, 57435, 57450, 57453, 57456, 57458, 57460, 57461, 57463],
                'eCommerce' : [57392, 57397, 57398, 57399, 57402],
                'Currency Icons' : [],
                'Form Control Icons' : [57383, 57384, 57385, 57386, 57387, 57388, 57484, 57594, 57595, 57600, 57603, 57604, 57659, 57660, 57693],
                'User Action & Text Editor' : [57442, 57443, 57444, 57445, 57446, 57447, 57472, 57473, 57474, 57475, 57476, 57477, 57539, 57662, 57668, 57669, 57670, 57671, 57674, 57675, 57688, 57689],
                'Charts and Codes' : [57493],
                'Attentive' : [57543, 57588, 57590, 57591, 57592, 57593, 57596],
                'Multimedia Icons' : [57356, 57357, 57362, 57363, 57448, 57485, 57547, 57548, 57549, 57605, 57606, 57609, 57610, 57611, 57614, 57617, 57618, 57620, 57621, 57622, 57623, 57624, 57625, 57626],
                'Location and Contact' : [57344, 57345, 57346, 57404, 57405, 57408, 57410, 57411, 57413, 57414, 57540],
                'Date and Time' : [57415, 57416, 57417, 57421, 57422, 57423],
                'Devices' : [57359, 57361, 57364, 57425, 57426, 57430],
                'Tools' : [57349, 57350, 57352, 57355, 57365, 57478, 57479, 57480, 57481, 57482, 57483, 57486, 57487, 57488, 57663, 57664],
                'Social and Networking' : [57694, 57700, 57701, 57702, 57703, 57704, 57705, 57706, 57707, 57709, 57710, 57711, 57717, 57718, 57719, 57736, 57737, 57738, 57739, 57740, 57741, 57742, 57746, 57747, 57748, 57755, 57756, 57758, 57759, 57760, 57761, 57763, 57764, 57765, 57766, 57767, 57776],
                'Brands' : [57743, 57750, 57751, 57752, 57753, 57754, 57757, 57773, 57774, 57775, 57789, 57790, 57792, 57793],
                'Files & Documents' : [57378, 57380, 57381, 57382, 57390, 57391, 57778, 57779, 57780, 57781, 57782, 57783, 57784, 57785, 57786, 57787],
                'Like & Dislike Icons' : [57542, 57544, 57550, 57551, 57552, 57553, 57554, 57555, 57556, 57557],
                'Emoticons' : [57558, 57559, 57560, 57561, 57562, 57563, 57564, 57565, 57566, 57567, 57568, 57569, 57570, 57571, 57572, 57573, 57574, 57575, 57576, 57577, 57578, 57579, 57580, 57581, 57582, 57583],
                'Directional Icons' : [57584, 57585, 57586, 57587, 57631, 57632, 57633, 57634, 57635, 57636, 57637, 57638, 57639, 57640, 57641, 57642, 57643, 57644, 57645, 57646, 57647, 57648, 57649, 57650, 57651, 57652, 57653, 57654],
                'Other Icons' : [57351, 57353, 57354, 57358, 57360, 57366, 57367, 57368, 57369, 57370, 57371, 57372, 57373, 57374, 57389, 57393, 57394, 57395, 57396, 57400, 57401, 57407, 57409, 57412, 57418, 57419, 57420, 57424, 57427, 57428, 57429, 57431, 57440, 57441, 57449, 57451, 57452, 57454, 57455, 57457, 57459, 57462, 57464, 57465, 57466, 57467, 57468, 57469, 57470, 57471, 57489, 57490, 57491, 57492, 57494, 57495, 57496, 57497, 57498, 57499, 57500, 57501, 57502, 57503, 57504, 57505, 57506, 57507, 57508, 57509, 57510, 57511, 57512, 57513, 57514, 57515, 57516, 57517, 57518, 57519, 57520, 57521, 57522, 57523, 57529, 57530, 57538, 57546, 57589, 57597, 57598, 57599, 57601, 57602, 57607, 57608, 57612, 57613, 57615, 57616, 57619, 57627, 57628, 57629, 57630, 57655, 57656, 57657, 57658, 57661, 57665, 57666, 57667, 57672, 57673, 57676, 57677, 57678, 57679, 57680, 57681, 57682, 57683, 57684, 57685, 57686, 57687, 57690, 57695, 57696, 57697, 57698, 57699, 57708, 57712, 57713, 57714, 57715, 57716, 57720, 57721, 57722, 57723, 57724, 57725, 57726, 57727, 57728, 57729, 57730, 57731, 57732, 57733, 57734, 57735, 57744, 57745, 57749, 57762, 57768, 57769, 57770, 57771, 57772, 57777, 57788, 57791, 57794]
            };

            //var icm_icons = {"Web Applications":["&#xe05c","&#xe05d","&#xe05e","&#xe05f","&#xe0b4","&#xe0b5","&#xe0b6","&#xe0b7","&#xe0b8","&#xe0bb","&#xe0bc","&#xe0bd","&#xe0be","&#xe0bf","&#xe0c0","&#xe0c1","&#xe0c5","&#xe0c9","&#xe15b","&#xe15c"],"Business Icons":["&#xe003","&#xe004","&#xe01f","&#xe020","&#xe021","&#xe023","&#xe03b","&#xe03e","&#xe058","&#xe059","&#xe05a","&#xe05b","&#xe06a","&#xe06d","&#xe070","&#xe072","&#xe074","&#xe075","&#xe077"],"eCommerce":["&#xe030","&#xe035","&#xe036","&#xe037","&#xe03a"],"Currency Icons":[],"Form Control Icons":["&#xe027","&#xe028","&#xe029","&#xe02a","&#xe02b","&#xe02c","&#xe08c","&#xe0fa","&#xe0fb","&#xe100","&#xe103","&#xe104","&#xe13b","&#xe13c","&#xe15d"],"User Action & Text Editor":["&#xe062","&#xe063","&#xe064","&#xe065","&#xe066","&#xe067","&#xe080","&#xe081","&#xe082","&#xe083","&#xe084","&#xe085","&#xe0c3","&#xe13e","&#xe144","&#xe145","&#xe146","&#xe147","&#xe14a","&#xe14b","&#xe158","&#xe159"],"Charts and Codes":["&#xe095"],"Attentive":["&#xe0c7","&#xe0f4","&#xe0f6","&#xe0f7","&#xe0f8","&#xe0f9","&#xe0fc"],"Multimedia Icons":["&#xe00c","&#xe00d","&#xe012","&#xe013","&#xe068","&#xe08d","&#xe0cb","&#xe0cc","&#xe0cd","&#xe105","&#xe106","&#xe109","&#xe10a","&#xe10b","&#xe10e","&#xe111","&#xe112","&#xe114","&#xe115","&#xe116","&#xe117","&#xe118","&#xe119","&#xe11a"],"Location and Contact":["&#xe000","&#xe001","&#xe002","&#xe03c","&#xe03d","&#xe040","&#xe042","&#xe043","&#xe045","&#xe046","&#xe0c4"],"Date and Time":["&#xe047","&#xe048","&#xe049","&#xe04d","&#xe04e","&#xe04f"],"Devices":["&#xe00f","&#xe011","&#xe014","&#xe051","&#xe052","&#xe056"],"Tools":["&#xe005","&#xe006","&#xe008","&#xe00b","&#xe015","&#xe086","&#xe087","&#xe088","&#xe089","&#xe08a","&#xe08b","&#xe08e","&#xe08f","&#xe090","&#xe13f","&#xe140"],"Social and Networking":["&#xe15e","&#xe164","&#xe165","&#xe166","&#xe167","&#xe168","&#xe169","&#xe16a","&#xe16b","&#xe16d","&#xe16e","&#xe16f","&#xe175","&#xe176","&#xe177","&#xe188","&#xe189","&#xe18a","&#xe18b","&#xe18c","&#xe18d","&#xe18e","&#xe192","&#xe193","&#xe194","&#xe19b","&#xe19c","&#xe19e","&#xe19f","&#xe1a0","&#xe1a1","&#xe1a3","&#xe1a4","&#xe1a5","&#xe1a6","&#xe1a7","&#xe1b0"],"Brands":["&#xe18f","&#xe196","&#xe197","&#xe198","&#xe199","&#xe19a","&#xe19d","&#xe1ad","&#xe1ae","&#xe1af","&#xe1bd","&#xe1be","&#xe1c0","&#xe1c1"],"Files & Documents":["&#xe022","&#xe024","&#xe025","&#xe026","&#xe02e","&#xe02f","&#xe1b2","&#xe1b3","&#xe1b4","&#xe1b5","&#xe1b6","&#xe1b7","&#xe1b8","&#xe1b9","&#xe1ba","&#xe1bb"],"Like & Dislike Icons":["&#xe0c6","&#xe0c8","&#xe0ce","&#xe0cf","&#xe0d0","&#xe0d1","&#xe0d2","&#xe0d3","&#xe0d4","&#xe0d5"],"Emoticons":["&#xe0d6","&#xe0d7","&#xe0d8","&#xe0d9","&#xe0da","&#xe0db","&#xe0dc","&#xe0dd","&#xe0de","&#xe0df","&#xe0e0","&#xe0e1","&#xe0e2","&#xe0e3","&#xe0e4","&#xe0e5","&#xe0e6","&#xe0e7","&#xe0e8","&#xe0e9","&#xe0ea","&#xe0eb","&#xe0ec","&#xe0ed","&#xe0ee","&#xe0ef"],"Directional Icons":["&#xe0f0","&#xe0f1","&#xe0f2","&#xe0f3","&#xe11f","&#xe120","&#xe121","&#xe122","&#xe123","&#xe124","&#xe125","&#xe126","&#xe127","&#xe128","&#xe129","&#xe12a","&#xe12b","&#xe12c","&#xe12d","&#xe12e","&#xe12f","&#xe130","&#xe131","&#xe132","&#xe133","&#xe134","&#xe135","&#xe136"],"Other Icons":["&#xe007","&#xe009","&#xe00a","&#xe00e","&#xe010","&#xe016","&#xe017","&#xe018","&#xe019","&#xe01a","&#xe01b","&#xe01c","&#xe01d","&#xe01e","&#xe02d","&#xe031","&#xe032","&#xe033","&#xe034","&#xe038","&#xe039","&#xe03f","&#xe041","&#xe044","&#xe04a","&#xe04b","&#xe04c","&#xe050","&#xe053","&#xe054","&#xe055","&#xe057","&#xe060","&#xe061","&#xe069","&#xe06b","&#xe06c","&#xe06e","&#xe06f","&#xe071","&#xe073","&#xe076","&#xe078","&#xe079","&#xe07a","&#xe07b","&#xe07c","&#xe07d","&#xe07e","&#xe07f","&#xe091","&#xe092","&#xe093","&#xe094","&#xe096","&#xe097","&#xe098","&#xe099","&#xe09a","&#xe09b","&#xe09c","&#xe09d","&#xe09e","&#xe09f","&#xe0a0","&#xe0a1","&#xe0a2","&#xe0a3","&#xe0a4","&#xe0a5","&#xe0a6","&#xe0a7","&#xe0a8","&#xe0a9","&#xe0aa","&#xe0ab","&#xe0ac","&#xe0ad","&#xe0ae","&#xe0af","&#xe0b0","&#xe0b1","&#xe0b2","&#xe0b3","&#xe0b9","&#xe0ba","&#xe0c2","&#xe0ca","&#xe0f5","&#xe0fd","&#xe0fe","&#xe0ff","&#xe101","&#xe102","&#xe107","&#xe108","&#xe10c","&#xe10d","&#xe10f","&#xe110","&#xe113","&#xe11b","&#xe11c","&#xe11d","&#xe11e","&#xe137","&#xe138","&#xe139","&#xe13a","&#xe13d","&#xe141","&#xe142","&#xe143","&#xe148","&#xe149","&#xe14c","&#xe14d","&#xe14e","&#xe14f","&#xe150","&#xe151","&#xe152","&#xe153","&#xe154","&#xe155","&#xe156","&#xe157","&#xe15a","&#xe15f","&#xe160","&#xe161","&#xe162","&#xe163","&#xe16c","&#xe170","&#xe171","&#xe172","&#xe173","&#xe174","&#xe178","&#xe179","&#xe17a","&#xe17b","&#xe17c","&#xe17d","&#xe17e","&#xe17f","&#xe180","&#xe181","&#xe182","&#xe183","&#xe184","&#xe185","&#xe186","&#xe187","&#xe190","&#xe191","&#xe195","&#xe1a2","&#xe1a8","&#xe1a9","&#xe1aa","&#xe1ab","&#xe1ac","&#xe1b1","&#xe1bc","&#xe1bf","&#xe1c2"]};

            var icm_icon_search = {
                'Web Applications' : ['Box add', 'Box remove', 'Download', 'Upload', 'List', 'List 2', 'Numbered list', 'Menu', 'Menu 2', 'Cloud download', 'Cloud upload', 'Download 2', 'Upload 2', 'Download 3', 'Upload 3', 'Globe', 'Attachment', 'Bookmark', 'Embed', 'Code'],
                'Business Icons' : ['Office', 'Newspaper', 'Book', 'Books', 'Library', 'Profile', 'Support', 'Address book', 'Cabinet', 'Drawer', 'Drawer 2', 'Drawer 3', 'Bubble', 'Bubble 2', 'User', 'User 2', 'User 3', 'User 4', 'Busy'],
                'eCommerce' : ['Tag', 'Cart', 'Cart 2', 'Cart 3', 'Calculate'],
                'Currency Icons' : [],
                'Form Control Icons' : ['Copy', 'Copy 2', 'Copy 3', 'Paste', 'Paste 2', 'Paste 3', 'Settings', 'Cancel circle', 'Checkmark circle', 'Spell check', 'Enter', 'Exit', 'Radio checked', 'Radio unchecked', 'Console'],
                'User Action & Text Editor' : ['Undo', 'Redo', 'Flip', 'Flip 2', 'Undo 2', 'Redo 2', 'Zoomin', 'Zoomout', 'Expand', 'Contract', 'Expand 2', 'Contract 2', 'Link', 'Scissors', 'Bold', 'Underline', 'Italic', 'Strikethrough', 'Table', 'Table 2', 'Indent increase', 'Indent decrease'],
                'Charts and Codes' : ['Pie'],
                'Attentive' : ['Eye blocked', 'Warning', 'Question', 'Info', 'Info 2', 'Blocked', 'Spam'],
                'Multimedia Icons' : ['Image', 'Image 2', 'Play', 'Film', 'Forward', 'Equalizer', 'Brightness medium', 'Brightness contrast', 'Contrast', 'Play 2', 'Pause', 'Forward 2', 'Play 3', 'Pause 2', 'Forward 3', 'Previous', 'Next', 'Volume high', 'Volume medium', 'Volume low', 'Volume mute', 'Volume mute 2', 'Volume increase', 'Volume decrease'],
                'Location and Contact' : ['Home', 'Home 2', 'Home 3', 'Phone', 'Phone hang up', 'Envelope', 'Location', 'Location 2', 'Map', 'Map 2', 'Flag'],
                'Date and Time' : ['History', 'Clock', 'Clock 2', 'Stopwatch', 'Calendar', 'Calendar 2'],
                'Devices' : ['Camera', 'Headphones', 'Camera 2', 'Keyboard', 'Screen', 'Tablet'],
                'Tools' : ['Pencil', 'Pencil 2', 'Pen', 'Paint format', 'Dice', 'Key', 'Key 2', 'Lock', 'Lock 2', 'Unlocked', 'Wrench', 'Cog', 'Cogs', 'Cog 2', 'Filter', 'Filter 2'],
                'Social and Networking' : ['Share', 'Googleplus', 'Googleplus 2', 'Googleplus 3', 'Googleplus 4', 'Google drive', 'Facebook', 'Facebook 2', 'Facebook 3', 'Twitter', 'Twitter 2', 'Twitter 3', 'Vimeo', 'Vimeo 2', 'Vimeo 3', 'Github', 'Github 2', 'Github 3', 'Github 4', 'Github 5', 'Wordpress', 'Wordpress 2', 'Tumblr', 'Tumblr 2', 'Yahoo', 'Soundcloud', 'Soundcloud 2', 'Reddit', 'Linkedin', 'Lastfm', 'Lastfm 2', 'Stumbleupon', 'Stumbleupon 2', 'Stackoverflow', 'Pinterest', 'Pinterest 2', 'Yelp'],
                'Brands' : ['Joomla', 'Apple', 'Finder', 'Android', 'Windows', 'Windows 8', 'Skype', 'Paypal', 'Paypal 2', 'Paypal 3', 'Chrome', 'Firefox', 'Opera', 'Safari'],
                'Files & Documents' : ['File', 'File 2', 'File 3', 'File 4', 'Folder', 'Folder open', 'File pdf', 'File openoffice', 'File word', 'File excel', 'File zip', 'File powerpoint', 'File xml', 'File css', 'Html 5', 'Html 52'],
                'Like & Dislike Icons' : ['Eye', 'Eye 2', 'Star', 'Star 2', 'Star 3', 'Heart', 'Heart 2', 'Heart broken', 'Thumbs up', 'Thumbs up 2'],
                'Emoticons' : ['Happy', 'Happy 2', 'Smiley', 'Smiley 2', 'Tongue', 'Tongue 2', 'Sad', 'Sad 2', 'Wink', 'Wink 2', 'Grin', 'Grin 2', 'Cool', 'Cool 2', 'Angry', 'Angry 2', 'Evil', 'Evil 2', 'Shocked', 'Shocked 2', 'Confused', 'Confused 2', 'Neutral', 'Neutral 2', 'Wondering', 'Wondering 2'],
                'Directional Icons' : ['Point up', 'Point right', 'Point down', 'Point left', 'Arrow up left', 'Arrow up', 'Arrow up right', 'Arrow right', 'Arrow down right', 'Arrow down', 'Arrow down left', 'Arrow left', 'Arrow up left 2', 'Arrow up 2', 'Arrow up right 2', 'Arrow right 2', 'Arrow down right 2', 'Arrow down 2', 'Arrow down left 2', 'Arrow left 2', 'Arrow up left 3', 'Arrow up 3', 'Arrow up right 3', 'Arrow right 3', 'Arrow down right 3', 'Arrow down 3', 'Arrow down left 3', 'Arrow left 3'],
                'Other Icons' : ['Quill', 'Blog', 'Droplet', 'Images', 'Music', 'Pacman', 'Spades', 'Clubs', 'Diamonds', 'Pawn', 'Bullhorn', 'Connection', 'Podcast', 'Feed', 'Stack', 'Tags', 'Barcode', 'Qrcode', 'Ticket', 'Coin', 'Credit', 'Notebook', 'Pushpin', 'Compass', 'Alarm', 'Alarm 2', 'Bell', 'Print', 'Laptop', 'Mobile', 'Mobile 2', 'Tv', 'Disk', 'Storage', 'Reply', 'Bubbles', 'Bubbles 2', 'Bubbles 3', 'Bubbles 4', 'Users', 'Users 2', 'Quotes left', 'Spinner', 'Spinner 2', 'Spinner 3', 'Spinner 4', 'Spinner 5', 'Spinner 6', 'Binoculars', 'Search', 'Hammer', 'Wand', 'Aid', 'Bug', 'Stats', 'Bars', 'Bars 2', 'Gift', 'Trophy', 'Glass', 'Mug', 'Food', 'Leaf', 'Rocket', 'Meter', 'Meter 2', 'Dashboard', 'Hammer 2', 'Fire', 'Lab', 'Magnet', 'Remove', 'Remove 2', 'Briefcase', 'Airplane', 'Truck', 'Road', 'Accessibility', 'Target', 'Shield', 'Lightning', 'Switch', 'Powercord', 'Signup', 'Tree', 'Cloud', 'Earth', 'Bookmarks', 'Notification', 'Close', 'Checkmark', 'Checkmark 2', 'Minus', 'Plus', 'Stop', 'Backward', 'Stop 2', 'Backward 2', 'First', 'Last', 'Eject', 'Loop', 'Loop 2', 'Loop 3', 'Shuffle', 'Tab', 'Checkbox checked', 'Checkbox unchecked', 'Checkbox partial', 'Crop', 'Font', 'Text height', 'Text width', 'Omega', 'Sigma', 'Insert template', 'Pilcrow', 'Lefttoright', 'Righttoleft', 'Paragraph left', 'Paragraph center', 'Paragraph right', 'Paragraph justify', 'Paragraph left 2', 'Paragraph center 2', 'Paragraph right 2', 'Paragraph justify 2', 'Newtab', 'Mail', 'Mail 2', 'Mail 3', 'Mail 4', 'Google', 'Instagram', 'Feed 2', 'Feed 3', 'Feed 4', 'Youtube', 'Youtube 2', 'Lanyrd', 'Flickr', 'Flickr 2', 'Flickr 3', 'Flickr 4', 'Picassa', 'Picassa 2', 'Dribbble', 'Dribbble 2', 'Dribbble 3', 'Forrst', 'Forrst 2', 'Deviantart', 'Deviantart 2', 'Steam', 'Steam 2', 'Blogger', 'Blogger 2', 'Tux', 'Delicious', 'Xing', 'Xing 2', 'Flattr', 'Foursquare', 'Foursquare 2', 'Libreoffice', 'Css 3', 'IE', 'IcoMoon']
            };
            // Init the icon picker with custom source and search
            $('#'+id).fontIconPicker({
                source: icm_icons,
                searchSource: icm_icon_search,
                useAttribute: true,
                attributeName: 'data-icomoon',
                convertToHex: true,
                theme: 'fip-bootstrap'
            });
            if(defaultIcon){
                defaultIcon = JSON.parse('"'+defaultIcon+'"');
                $("#appIconSkinWin").find(".selected-icon").html('<i data-icomoon="'+defaultIcon+'"></i>' );
            }

        }

        function  closeNewModulessNodeWin(){
            var  radio = $("input[name='moduleType']:first");
            radio.attr("checked","checked");
            var  iconSkin = '<td><input id="moduleIconSkin" name="moduleIconSkin" style="width:200px;" ></input></td>';
            $('#moduleIconSkin').parent().replaceWith(iconSkin);
            $('#createModuleWin').window('close');
        }
        /*设置新模块属性窗口确认按钮的事件——2019/8/1 15:56*/
        function  confirmNewModulessNodeWin(){
            pasteNode($('#treeModule').data('node'));
            var  radio = $("input[name='moduleType']:first");
            radio.attr("checked","checked");
            var  iconSkin = '<td><input id="moduleIconSkin" name="moduleIconSkin" style="width:200px;" ></input></td>';
            $('#moduleIconSkin').parent().replaceWith(iconSkin);
        }
	</script>

</head>
<body class="easyui-layout">
<div region="north" <%-- iconCls="icon-home" title="<s:text name="system.index.title"/>" split="true" --%> style="height:60px;padding:0px;overflow:hidden;" border="false">
	<div class="index_top_bg" style="height: 32px;">
		<div style="float:right;padding-left:5px;" class="index_top_system"></div>
		<div style="float:left;padding-left:5px;">
			<div style="padding:2px;">
				<a href="javascript:void(0)" id="mb1" class="easyui-menubutton" menu="#mm1">文件(F)</a>
				<a href="javascript:void(0)" id="mb2" class="easyui-menubutton" menu="#mm2">查看(V)</a>
			</div>
			<div id="mm1" style="width:100px;">
				<div iconCls="icon-new" onclick="javascript:$('#createNewAccount').window('open');">新建</div>
				<div iconCls="icon-open" onclick="openFileWin();">打开</div>
				<!--	<div iconCls="icon-save" onclick="alert('save this page')">保存</div>  -->
				<div iconCls="icon-save" onclick="refreshTabData();">保存所有</div>
				<div iconCls="icon-upgrade" onclick="doOpenUpgrade();">更新服务器</div>
				<div iconCls="icon-upgrade" onclick="alert('功能开发中.....')">打开历史</div>
			</div>
			<div id="mm2" style="width:100px;">
				<div iconCls="icon-help">帮助</div>
				<div iconCls="icon-tip" onclick="doOpenAboutWin();">关于</div>
			</div>
		</div>
		<div id="dv_server" style="float:left;padding:8px 100px;text-align:right;">
		</div>
		<div style="float:right;padding:0px 10px;text-align:right;">
			<img src="${imgPath}/logo.png" height="32px"/>
		</div>
		<div style="clear:both;"></div>
	</div>
	<div style="height:28px;padding:2px;background:#E0ECFF;border-top: 1px solid #8DB2E3;">
		<div>

			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-new" plain="true" title="新建" onclick="javascript:$('#createNewAccount').window('open');"></a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-open" plain="true" title="打开" onclick="openFileWin();"></a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" title="保存所有" onclick="refreshTabData();"></a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-upgrade" plain="true" title="更新服务器" onclick="doOpenUpgrade();"></a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-menu" plain="true" title="下载到本地" onclick="downloadFile();"></a>
			<!-- <a href="javascript:$('#uploadTree').upload();" class="easyui-linkbutton" iconCls="icon-upgrade" plain="true" title="上传本地文件到服务器" ></a> -->
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-help" plain="true" title="帮助"></a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-tip" plain="true" title="关于" onclick="doOpenAboutWin();"></a>

		</div>
	</div>
</div>
<div region="center" <%-- iconCls="icon-user" title="<s:text name="system.index.workspace.title"/>" --%> style="overflow:hidden;">
	<div class="easyui-layout" fit="true">
		<div id="treeModule" region="west"  split="true" title="目录结构" style="width:205px;padding:1px;">
			<div id="linkTree" class="easyui-tabs">
				<div  title="本地" >

					<!-- <div class="xpstyle-panel" id="local_panel">
                       <div class='easyui-panel  xpstyle' style='width:180px;' collapsible='true' collapsed='true' title="链接本地">
                           <ul id="menuTree" class="ztree"></ul>
                       </div>
                  </div> -->
					<div id="local_panel" class="xpstyle-panel">
					</div>
				</div>
				<div  title="远程" >
					<div class="xpstyle-panel" id="remote_panel">
					</div>
				</div>
			</div>
		</div>
		<div region="center" title="" border="false">
			<div id="main_tabs_div" class="easyui-tabs" fit="true" border="false">
			</div>
		</div>
	</div>
</div>
<div id="openFileWin" class="easyui-window" title="打开文件" minimizable="false" modal="true" style="width:500px;height:280px;padding:5px;">
	<div class="easyui-layout" fit="true">

		<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
			<div id="tt" class="easyui-tabs" >
				<div title="打开">
					<form action="" name="loginForm" id="loginForm">
						<table border="0" cellpadding="0" cellspacing="1" class="table_form">
							<tbody>
							<tr><th><label for="loginName">链接方式:</label></th>
								<td><input id="rad_local" onclick="showUrl('hide')" type="radio" name="urlType" value="local" checked="checked"/><label id="lab_local">服务器本地</label>&nbsp;&nbsp;
									<input id="rad_remote"  onclick="showUrl()" type="radio" name="urlType" value="remote"/><label>远程服务器</label>
								</td>
							</tr>
							<tr id="loginTr" style="display:none;"><th><label for="loginUrl">链接地址:</label></th>
								<td><input id="loginUrl" name="loginUrl" style="width:200px;" class="easyui-combobox"/>
									<!-- 	<option value="0" selected="selected">http://127.0.0.1/jwpf</option>
                                        <option value="1">http://192.168.1.129/jwpf</option>
                                    </select>-->
								</td>
							</tr>
							<tr><th><label for="loginName">登录名:</label></th>
								<td><input type="text" name="loginName" id="loginName" style="width:200px;" class="easyui-validatebox" required="true" value="admin"></input></td>
							</tr>
							<tr>
								<th><label for="passwd">密码:</label></th>
								<td><input type="password" name="passwd" id="passwd" style="width:200px;" class="easyui-validatebox" required="true" value="jabdp"></input></td>
							</tr>
							<tr id="tr_updateType" style="display:none;"><th><label for="rad_isFullUpdate_0">更新方式:</label></th>
								<td><input id="rad_isFullUpdate_0" type="radio" name="isFullUpdate" value="0" checked="checked"/><label>增量更新</label>&nbsp;&nbsp;
									<input id="rad_isFullUpdate_1" type="radio" name="isFullUpdate" value="1"/><label>全量更新</label>
								</td>
							</tr>
							</tbody>
						</table>
				</div>
				<div title="打开本地">
<%--					<input type="file" name="uploadTree" id="uploadTree"/>--%>
					<div id="uploader" class="wu-example">
						<!--用来存放文件信息-->
						<div class="btns">
							<div id="picker"></div>
						</div>
						<div id="thelist" class="uploader-list"></div>
					</div>
				</div>
			</div>
			</form>


		</div>
		<div region="south" border="false" style="text-align:right;padding:5px 0;">
			<a id="bt_login" class="easyui-linkbutton" iconCls="icon-ok" href="javascript:void(0)" onclick="javascript:doLogin();">确定</a>
			<a id="bt_upgrade" style="display:none;" class="easyui-linkbutton" iconCls="icon-ok" href="javascript:void(0)" onclick="javascript:doUpgrade();">更新</a>
			<a class="easyui-linkbutton" iconCls="icon-cancel" href="javascript:void(0)" onclick="javascript:$('#openFileWin').window('close');">关闭</a>
		</div>
	</div>
</div>

<div id="openNodeWin" class="easyui-window" title="设置模块集合属性" closed="true" modal="true" minimizable="false" maximizable="false" collapsible="false" closable="false" style="width:340px;height:210px;padding:5px;">
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
				<tr><th><label for="loginName">模块名称:</label></th>
					<td><input type="text" name="nodeName" id="nodesName" style="width:200px;"></input></td>
				</tr>
				<tr>
					<th><label for="passwd">模块属性:</label></th>
					<td><input type="text" name="nodeKey" id="nodeKey" style="width:200px;"></input></td>
				</tr>
				<tr>
					<th><label for="iconSkin">设置图标:</label></th>
					<td><input id="iconSkin" name="iconSkin" style="width:200px;" ></input></td>
				</tr>
				</tbody>
			</table>
		</div>
		<div region="south" border="false" style="text-align:right;padding:5px 0;">
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:createModuleNode();">确定</a>
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#openNodeWin').window('close');">关闭</a>
		</div>
	</div>
</div>

<div id="openAppNodeWin" class="easyui-window" title="设置移动端模块集合属性" closed="true" modal="true" minimizable="false" maximizable="false" collapsible="false" closable="false" style="width:550px;height:550px;padding:5px;">
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
				<tr><th><label for="loginName">模块集合名称:</label></th>
					<td><input type="text" name="appNodeName" id="appNodesName" style="width:200px;"></input></td>
				</tr>
				<tr>
					<th><label for="passwd">模块集合属性:</label></th>
					<td><input type="text" name="appNodeKey" id="appNodeKey" style="width:200px;"></input></td>
				</tr>
				<tr>
					<th><label for="appIconSkin">设置图标:</label></th>
					<td><input id="appIconSkin" name="appIconSkin" style="width:200px;" ></input></td>
				</tr>
				</tbody>
			</table>
		</div>
		<div region="south" border="false" style="text-align:right;padding:5px 0;">
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:createAppModuleNode();">确定</a>
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#openAppNodeWin').window('close');">关闭</a>
		</div>
	</div>
</div>

<div id="createModulessWin" class="easyui-window" title="粘贴请重新设置模块集合属性" closed="true" modal="true" minimizable="false" maximizable="false" collapsible="false" closable="false" style="width:340px;height:180px;padding:5px;">
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
				<tr><th><label for="loginName">模块集合名称:</label></th>
					<td><input type="text" name="nodeName" id="newNodeName" style="width:200px;"></input></td>
				</tr>
				<tr>
					<th><label for="passwd">模块属性Key:</label></th>
					<td><input type="text" name="nodeKey" id="newNodeKey" style="width:200px;"></input></td>
				</tr>

				</tbody>
			</table>
		</div>
		<div region="south" border="false" style="text-align:right;padding:5px 0;">
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:pasteNode($('#treeModule').data('node'));">确定</a>
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#createModulessWin').window('close');">关闭</a>
		</div>
	</div>
</div>

<div id="createModuleWin" class="easyui-window" title="设置新模块属性" closed="true" modal="true" minimizable="false" maximizable="false" collapsible="false" closable="false" style="width:600px;height:600px;padding:5px;">
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
				<tr><th><label for="loginName">模块集合名称:</label></th>
					<td><input type="text" name="nodeName" id="newModuleName" style="width:200px;"></input></td>
				</tr>
				<tr>
					<th><label for="passwd">模块属性Key:</label></th>
					<td><input type="text" name="nodeKey" id="newModuleKey" style="width:200px;"></input></td>
				</tr>
				<tr>
					<th><label for="iconSkin">模块类型:</label></th>
					<td>
						<div id="dv_moduleType">
							<input id="moduleType_module" type="radio" name="moduleType" value="module" checked="checked"/><label for="moduleType_module">普通模块</label><br/>
							<input id="moduleType_form" type="radio" name="moduleType" value="form"/><label for="moduleType_form">自定义表单</label><br/>
							<input id="moduleType_dict" type="radio" name="moduleType" value="dict"/><label for="moduleType_dict">业务字典</label><br/>
							<input id="moduleType_app" type="radio" name="moduleType" value="app"/><label for="moduleType_app">App表单</label>
						</div>
					</td>
				</tr>
				<tr>
					<th><label for="iconSkin">设置模块图标:</label></th>
					<td><input id="moduleIconSkin" name="moduleIconSkin" style="width:200px;" ></input></td>
				</tr>
				</tbody>
			</table>
		</div>
		<div region="south" border="false" style="text-align:right;padding:5px 0;">
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:confirmNewModulessNodeWin()">确定</a>
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:closeNewModulessNodeWin()">关闭</a>
		</div>
	</div>
</div>

<div id="createDictsWin" class="easyui-window" title="设置字典模块属性" closed="true" modal="true" minimizable="false" maximizable="false" collapsible="false" closable="false" style="width:340px;height:180px;padding:5px;">
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
				<tr><th><label for="loginName">字典模块名称:</label></th>
					<td><input type="text" name="dictsName" id="newDictsName" style="width:200px;"></input></td>
				</tr>
				<tr>
					<th><label for="passwd">模块属性Key:</label></th>
					<td><input type="text" name="dictsKey" id="newDictsKey" style="width:200px;"></input></td>
				</tr>

				</tbody>
			</table>
		</div>
		<div region="south" border="false" style="text-align:right;padding:5px 0;">
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:createModule($('#treeModule').data('node'));">确定</a>
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#createDictsWin').window('close');">关闭</a>
		</div>
	</div>
</div>
<div id="iconSkinWin" class="easyui-window" title="设置图标" closed="true" modal="true" minimizable="false" maximizable="false" collapsible="false" closable="false" style="width:340px;height:200px;padding:5px;">
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
				<tr>
					<th><label for="iconSkin">设置图标:</label></th>
					<td><input id="setIconSkin" name="iconSkin" style="width:200px;" ></input></td>
				</tr>
				</tbody>
			</table>
		</div>
		<div region="south" border="false" style="text-align:right;padding:5px 0;">
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:setIconSkin($('#treeModule').data('node'));">确定</a>
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#iconSkinWin').window('close');">关闭</a>
		</div>
	</div>
</div>

<div id="appIconSkinWin" class="easyui-window" title="设置图标" closed="true" modal="true" minimizable="false" maximizable="false" collapsible="false" closable="false" style="width:500px;height:500px;padding:5px;">
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
				<tr>
					<td><input id="setAppIconSkin" name="appIconSkin" style="width:200px;" ></input></td>
				</tr>
				</tbody>
			</table>
		</div>
		<div region="south" border="false" style="text-align:right;padding:5px 0;">
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:setIconSkin($('#treeModule').data('node'));">确定</a>
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#appIconSkinWin').window('close');">关闭</a>
		</div>
	</div>
</div>

<div id="createNewAccount" class="easyui-window" title="新建帐套" closed="true" modal="true" minimizable="false" maximizable="false" collapsible="false" closable="false" style="width:340px;height:180px;padding:5px;">
	<div class="easyui-layout" fit="true">
		<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
				<tr><th><label for="loginName">帐套名称:</label></th>
					<td><input type="text" name="accountName" id="accountName" style="width:200px;"></input></td>
				</tr>
				</tbody>
			</table>
		</div>
		<div region="south" border="false" style="text-align:right;padding:5px 0;">
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:doNewFile()">新建</a>
			<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#createNewAccount').window('close');">取消</a>
		</div>
	</div>
</div>
<div id="menu_reName" class="easyui-menu" style="width:80px;">
	<div onclick="javascript:treeNode_rename()">重命名</div>
	<div onclick="javascript:deleteAccount()">删除</div>
</div>
<div id="menu_moduless" class="easyui-menu" style="width:120px;">
	<div onclick="javascript:createModuleNodeWin();">新建</div>
	<div onclick="javascript:createFormsNode();">新建自定义表单</div>
	<div onclick="javascript:createNewModulessNodeWin($('#treeModule').data('newNode'));">粘贴</div>
</div>
<div id="menu_modules" class="easyui-menu" style="width:120px;">
	<div  onclick="javascript:moveUp($('#treeModule').data('node'));" >上移</div>
	<div  onclick="javascript:moveDown($('#treeModule').data('node'));" >下移</div>
	<div onclick="javascript:createNewModulessNodeWin();">新建单个模块</div>
	<div onclick="javascript:createModuleNodeWin();">新建模块集合</div>
	<div  onclick="javascript:copyNode($('#treeModule').data('node'));" >复制</div>
	<div  onclick="javascript:createNewModulessNodeWin($('#treeModule').data('newNode'));" >粘贴</div>
	<div  onclick="javascript:removeNodes($('#treeModule').data('node'));" >删除</div>
	<div onclick="javascript:openIconSkinWin($('#treeModule').data('node'));">设置图标</div>
	<div onclick="javascript:treeNode_rename()">重命名</div>
</div >
<div id="menu_module" class="easyui-menu" style="width:120px;">
	<div  onclick="javascript:moveUp($('#treeModule').data('node'));" >上移</div>
	<div  onclick="javascript:moveDown($('#treeModule').data('node'));" >下移</div>
	<div  onclick="javascript:cutNode($('#treeModule').data('node'));" >剪切</div>
	<div  onclick="javascript:copyNode($('#treeModule').data('node'));" >复制</div>
	<div  onclick="javascript:removeNodes($('#treeModule').data('node'));" >删除</div>
	<div onclick="javascript:openIconSkinWin($('#treeModule').data('node'));">设置图标</div>
</div>
<div id="menu_dicts" class="easyui-menu" style="width:80px;">
	<div onclick="javascript:moveUp($('#treeModule').data('node'));" >上移</div>
	<div onclick="javascript:moveDown($('#treeModule').data('node'));" >下移</div>
	<div onclick="javascript:createNewModulessNodeWin();">新建</div>
	<div onclick="javascript:createNewModulessNodeWin($('#treeModule').data('newNode'));">粘贴</div>
	<div onclick="javascript:openIconSkinWin($('#treeModule').data('node'));">设置图标</div>
	<div onclick="javascript:treeNode_rename()">重命名</div>
</div>
<div id="menu_dict" class="easyui-menu" style="width:80px;">
	<div  onclick="javascript:moveUp($('#treeModule').data('node'));" >上移</div>
	<div  onclick="javascript:moveDown($('#treeModule').data('node'));" >下移</div>
	<div  onclick="javascript:copyNode($('#treeModule').data('node'));" >复制</div>
	<div  onclick="javascript:removeNodes($('#treeModule').data('node'))" >删除</div>
	<div onclick="javascript:openIconSkinWin($('#treeModule').data('node'));">设置图标</div>
</div>
<div id="menu_forms" class="easyui-menu" style="width:80px;">
	<div onclick="javascript:createNewModulessNodeWin();">新建</div>
	<div onclick="javascript:createNewModulessNodeWin($('#treeModule').data('newNode'));">粘贴</div>
</div>
<div id="menu_form" class="easyui-menu" style="width:80px;">
	<div  onclick="javascript:copyNode($('#treeModule').data('node'));" >复制</div>
	<div  onclick="javascript:removeNodes($('#treeModule').data('node'))" >删除</div>
	<div onclick="javascript:openIconSkinWin($('#treeModule').data('node'));">设置图标</div>
</div>

<div id="menu_appss" class="easyui-menu" style="width:120px;">
	<div onclick="javascript:createAppModuleNodeWin();">新建</div>
	<div onclick="javascript:createNewAppModulessNodeWin($('#treeModule').data('newNode'));">粘贴</div>
</div>

<div id="menu_apps" class="easyui-menu" style="width:120px;">
	<div  onclick="javascript:moveUp($('#treeModule').data('node'));" >上移</div>
	<div  onclick="javascript:moveDown($('#treeModule').data('node'));" >下移</div>
	<div onclick="javascript:createNewModulessNodeWin();">新建单个模块</div>
	<div  onclick="javascript:copyNode($('#treeModule').data('node'));" >复制</div>
	<div  onclick="javascript:createNewModulessNodeWin($('#treeModule').data('newNode'));" >粘贴</div>
	<div  onclick="javascript:removeNodes($('#treeModule').data('node'));" >删除</div>
	<div onclick="javascript:openIconSkinWin($('#treeModule').data('node'));">设置图标</div>
	<div onclick="javascript:treeNode_rename()">重命名</div>
</div >

<div style="display:none;">
	<div id="aboutWin" class="easyui-window" closed="true" minimizable="false" modal="true" title="关于" style="width:500px;height:110px;">
		<div style="text-align:center;padding:10px 10px 0px 10px;">JABDP iDesigner V1.0.0</div>
		<div style="text-align:center;padding:5px 10px 5px 10px;">Copyright © 2017 JABDP ®. All rights reserved. Java敏捷业务设计平台 版权所有</div>
		<div style="text-align:center;padding:2px;"><a style="text-decoration: none;" href="javascript:void(0);" onclick="$('#authWin').window('open');">授权信息</a></div>
	</div>
	<div id="authWin" class="easyui-window" closed="true" minimizable="false" href="${ctx}/design/authInfo"
		 modal="true" title="授权信息" style="width:400px;height:210px;padding:10px 10px;font-size:18px;">
	</div>
</div>

</body>
</html>

