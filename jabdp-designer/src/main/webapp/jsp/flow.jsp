<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<script type="text/javascript">
	var __flowPath = "";
	var __modulePath = "";
	var __entityName = "";
	var tabTitle="";
	var __fieldToSqlParam = {};
	function loadFlowTable() {
		$("#flowTable").datagrid("loadData", $.fn.module.getFlowsProp());
	}

	function openFlowEditor(flowName) {
		var param = __flowPath + "|" + flowName;
		var url = "/ae/service/editor?id=" + encodeURIComponent(encodeURIComponent(param));
		window.open(url);
	}

	function getFlowPath() {
		var fp = "";
		var options = {
				data : {
					"treeId":"${param.treeId}"
				},
				async:false,
				url: "${ctx}/design/module/getFlowPath",
				success:function(data) {
					fp=data.msg;
				}
		};
		fnFormAjaxWithJson(options);
		return fp;
	}

	function delSelectFlow(name) {
		var options = {
				data : {
					"flowName":name,
					"treeId":"${param.treeId}"
				},
				async:false,
				url: "${ctx}/design/module/delFlow",
				success:function(data) {
					$.fn.module.delFlowsProp(name);	
				}
		};
		fnFormAjaxWithJson(options);
	}

	function getSelectFlow() {
		var rows = $("#flowTable").datagrid('getSelections');
		if(rows && rows.length > 0) {
			return rows;
		} else {
			$.messager.alert(
					'提示','请选择一个流程','info');
			return null;
		}
	}
	
	/**
	*给下推规则设值*/
	 function setRuleSql(){
		//	alert(tabTitle);
			var key = $("#ruleKey").val();
	    	var caption = $("#ruleCaption").val();
	    	var stype = $("#sqlRule").attr("checked");
	    	var ptype = $("#procRule").attr("checked"); 
			var oldKey = null;
	 		if(ruleFlagKey&&ruleFlagKey.length>0){
	 			oldKey = ruleFlagKey;
	 		}
	 		if(key&&caption&&stype){
			    //	if(tabTitle == "字段下推"){
			    	var type = $("#sqlRule").val();
			    		var sourTable = sourTabKey;
			    		var destTable = destTabKey;
			    		var rows = $("#fieldRuleTable").edatagrid("getRows");
			    		$.each(rows,function(k,v){
			    			var rowIndex = $("#fieldRuleTable").edatagrid("getRowIndex",v);
			    			$("#fieldRuleTable").edatagrid("endEdit",rowIndex);
			    		});
			    		var ruleRows = $("#fieldRuleTable").edatagrid("getRows");
			    		var sqlWhereRows = $("#sqlWhereTable").edatagrid("getRows");
			    		var content = $("#ruleSqlStr").val();
			    		var data ={"key":key,"caption":caption,"type":type,"content":content,"source":sourTable,"dest":destTable,"data":ruleRows,"whereRows":sqlWhereRows};
			    		var flag = $.fn.module.setFieldRule(data,oldKey,"sql");
			    		//alert(flag);
			    		if(flag){
			    			$('#ruleWin').window('close');
			    		}else{
			    			alert("规则key重复！");
			    		}
			    /* 	}else if(tabTitle == "sql下推"){
			    		
			    		
			    		var sqlStr = $("#ruleSqlStr").val();
			    		var data = {"key":key,"caption":caption,"type":type};
			    		var flag = $.fn.module.setSqlRule(data,sqlStr,oldKey);
			    		if(flag){
			    			$('#ruleWin').window('close');
			    		}else{
			    			alert("规则key重复！");
			    		}
			    	} */
	 		}else if(key&&caption&&ptype){	
	 			var type = $("#procRule").val();
	 			var content = $("#ruleProcStr").val();
	 			var data ={"key":key,"caption":caption,"type":type,"content":content};
	    		var flag = $.fn.module.setFieldRule(data,oldKey,"proc");
	    		if(flag){
	    			$('#ruleWin').window('close');
	    		}else{
	    			alert("规则key重复！");
	    		}
	 		}else{
	 			alert("请填完整规则key，名称，及类型后再保存");
	 		}
	    	 getRule();
	    }
	 /**
	*加载时候获得下推规则*/
	
	function getRule(){
		var sqlRuleData = $.fn.module.getSqlRule();
		$("#ruleTable").edatagrid("loadData",sqlRuleData);	
	}
	
	 
	/**
	*修改下推规则*/
	var ruleFlagKey;
	function editRule(){
		ruleFlagKey = "";
		$("#ruleWin").window("open");	
		var row = $("#ruleTable").edatagrid("getSelected");
		if(row.type=="sql"){
		editSql();	
		initRoleTable();
		// $("#sqlWhereTable").edatagrid({});
		 $("#sqlWhereTable").edatagrid({onRowContextMenu:function(e, rowIndex, rowData){
			 e.preventDefault(); 
			 $("#sqlWhereTable").edatagrid("selectRow",rowIndex);
				$('#delQueryWhere').menu('show',{
		    			left:e.pageX,
		    			top:e.pageY
		    	});  
		 }})/* .edatagrid({onDblClickRow:function(rowIndex, rowData){
			 $("#sqlWhereTable").edatagrid("beginEdit",rowIndex);
		 }}) */;
		 $("#sqlWhereTable").edatagrid("addRow",{});
		/*  $("#ruleMainTabs input").live('keydown', 'return',function (evt){$("#sqlWhereTable").edatagrid("addRow",{}); return false; }); */
		 $("#ruleKey").val(row.key);
	     $("#ruleCaption").val(row.caption);
	     $("#procRule").parent().hide(); 
	     $("#sqlRule").attr("checked",true); 
	     $("#fieldRuleTable").edatagrid("loadData",{"total":0,"rows":[]});
	     $("#sqlWhereTable").edatagrid("loadData",{"total":0,"rows":[]});
	     $("#sourMul").html("");
	     $("#destMul").html("");
		if(row.table){
			tabTitle = 	"字段下推";
			 $("#ruleTabs").tabs("select","字段下推");
			 var columnList =row.table.columnList;
			 var load_data = {"total":0,"rows":columnList};
			 var where_data = {"total":0,"rows":row.whereData};
			 $("#sqlWhereTable").edatagrid("loadData",where_data);
			 $("#fieldRuleTable").edatagrid("loadData",load_data);
			var rows = $("#fieldRuleTable").edatagrid("getRows");
	    		$.each(rows,function(k,v){
	    			var rowIndex = $("#fieldRuleTable").edatagrid("getRowIndex",v);
	    			//alert(rowIndex);
	    			$("#fieldRuleTable").edatagrid("beginEdit",rowIndex);
	    		}); 
			 sourTabKey = row.table.source;
			 destTabKey = row.table.dest;
			/*  var sourJson =[ {"key":row.table.source,"caption":row.table.source, "selected":true  }];
			 var destJson = [{"key":row.table.dest,"caption":row.table.dest, "selected":true  }];
			 $("#sourTab").combobox({  
				    "data":sourJson,  
				    "valueField":"key",  
				    "textField":"caption" ,
				    "onSelect":function(record){
				    	destTabKey = record.key;
				    	getAllField("${param.treeId}",destTabKey,"sourMul");
				    }
				});  
				$("#destTab").combobox({  
				    "data":destJson,  
				    "valueField":"key",  
				    "textField":"caption",
				    "onSelect":function(record){
				    	destTabKey = record.key;
				    	getAllField("${param.treeId}",destTabKey,"destMul");
				    }
				});  */
				 $("#ruleSqlStr").val(row.sqlStr);
				getAllField("${param.treeId}",row.table.source,"",sourTabKey,destTabKey);
			//	getAllField("${param.treeId}",row.table.source,"sourMul",sourTabKey,destTabKey);
			//	getAllField("${param.treeId}",row.table.dest,"destMul",sourTabKey,destTabKey);
		}else{
			tabTitle = 	"sql下推";
			 $("#ruleTabs").tabs("select","sql下推");
			 $("#fieldRuleTable").edatagrid({});
			 $("#ruleSqlStr").val(row.sqlStr);
			}
		}else if(row.type=="proc"){
			 editProc();
			 $("#ruleKey").val(row.key);
		     $("#ruleCaption").val(row.caption);
		     $("#sqlRule").parent().hide(); 
		     $("#procRule").attr("checked",true); 
		     $("#ruleProcStr").val(row.sqlStr);
		}
		ruleFlagKey = row.key;
		// var data = $.fn.module.getSqlRule();
		// $("#sqlRuleTable").edatagrid({});
		// $("#sqlRuleTable").edatagrid("addRow",row);

	    
	   // $("input[name='ruleType']").val(row.type);
	  
  		
  		
  		//	$("#sqlRuleTable").edatagrid("selectRow",0); 
	}
	/**
	*添加下推规则*/
	function addRule(){
		ruleFlagKey = "";
		$("#ruleWin").window("open");
		editSql();
		initRoleTable();
		$("#ruleKey").val("");
		$("#ruleCaption").val("");
		$("#ruleSqlStr").val("");
		 $("#sourMul").html("");
	     $("#destMul").html("");
		 $("#fieldRuleTable").edatagrid("loadData",{"total":0,"rows":[]});
		 $("input[@type=radio][id=sqlRule]").attr("checked",true); 
		 $("#sqlWhereTable").edatagrid({onRowContextMenu:function(e, rowIndex, rowData){
			 e.preventDefault(); 
			 $("#sqlWhereTable").edatagrid("selectRow",rowIndex);
				$('#delQueryWhere').menu('show',{
		    			left:e.pageX,
		    			top:e.pageY
		    	});  
		 }});
		$("#sqlWhereTable").edatagrid("loadData",{"total":0,"rows":[]});
		// $("#ruleTabs").tabs({});
		$("#ruleProcStr").val("");
		 
		  $("#ruleTabs").tabs({"onSelect":function(title){
			  tabTitle=title;
			  if(title=="下推条件"){
					 /*  $("#sqlWhereTable").edatagrid({
						  width:680,
						  height:280,
						  columns:[[  
						            {field:'flag',title:'源表字段',width:120,formatter:flagType,editor:"{type:'combobox',options:{valueField:'key',textField:'caption',data:flag_data}"},  
						            {field:'column',title:'目标字段',width:120,formatter:fieldType,editor:"{type:'combobox',options:{valueField:'key',textField:'caption',data:field_data}"},  
						            {field:'key',title:'转换规则',width:120,editor:"{type:'validatebox'}"},
						            {field:'order',title:'排序方式',width:120,formatter:orderByType,editor:"{type:'combobox',options:{valueField:'key',textField:'caption',data:orderBy_data}"}
						            
						        ]]  
					  }); */
					  
					//$("#sqlWhereTable").attr("class","");
					
			  }
	          	/* if(title=="字段下推"){
	          		// $("#fieldRuleTable").edatagrid({"total":0,"rows":[]});
	          		
	          		//获得所有的tab并展示到下拉菜单里
	          		  // var tabsList =  
	          			$("#sourTab").combobox({  
	          			    data:tabsList,  
	          			    valueField:'id',  
	          			    textField:'text'  
	          			}); 
	          			$("#destTab").combobox({  
	          			    data:tabsList,  
	          			    valueField:'id',  
	          			    textField:'text'  
	          			}); 
	          			
	          			
	          		 
	          	}else if(title=="sql下推"){
	          		
	          		
	          	}*/ 
	          }});  
	}
	function delRule(){
		var row = $("#ruleTable").edatagrid("getSelected");
		if(row){
			$.fn.module.deleteRule(row);
			getRule();
		}else{
			$.messager.alert(
					'提示','请选择一个规则','info');
		}
		
	}
	
    $(function(){
    	__flowPath = getFlowPath();
    	__modulePath = $.fn.module.getModuleProp().key;
    	__entityName = $.fn.module.getEntityName();
    	__fieldToSqlParam = $.fn.module.getSystemFieldToSqlParamMap();
    	 
        $("#flowTable").datagrid({
            rownumbers:true,
            singleSelect:true,
            frozenColumns:[[
			                {field:'ck',checkbox:true}
			]],
        	columns:[[  
        	          {field:'key',title:'流程名称',width:200}
        	]],
        	toolbar:[{
				text:'新增流程',
				iconCls:'icon-add',
				handler:function(){
					var flag = $.fn.module.addFlowsProp({"key":__modulePath});
					if(flag) {
						openFlowEditor(__modulePath);
						loadFlowTable();
					} else {
						$.messager.alert(
								'提示','流程名称重名，请修改名称','info');
					}
				}
			},{
					text:'修改流程',
					iconCls:'icon-edit',
					handler:function(){
						var rows = getSelectFlow();
						if(rows) {
							openFlowEditor(rows[0].key);
						}
					}
				},{
					text:'刷新流程',
					iconCls:'icon-reload',
					handler:function(){
						loadFlowTable();		
					}
				},{
					text:'删除流程',
					iconCls:'icon-remove',
					handler:function(){
						var rows = getSelectFlow();
						if(rows) {
							delSelectFlow(rows[0].key);
							loadFlowTable();
						}
					}
				}]
        });
        loadFlowTable();
        
    
        $("#ruleTable").edatagrid({
            rownumbers:true,
            singleSelect:true,
            frozenColumns:[[
			                {field:'ck',checkbox:true}
			]],
        	columns:[[  
					  {field:'sqlKey',title:'引用规则key',width:200,
					   formatter: function(value,row,index){
						  return __entityName + "." + row["key"];
					  }},
        	          {field:'key',title:'规则key',width:100},
        	          {field:'caption',title:'规则名称',width:200},
        	          {field:'type',title:'规则类型',width:100}
        	]],
        	toolbar:[{
				text:'新增规则',
				iconCls:'icon-add',
				handler:function(){
					 addRule();
					 getAllModule();
					 rows=[];
					
				}
			},{
					text:'修改规则',
					iconCls:'icon-edit',
					handler:function(){
						editRule();
					}
				},{
					text:'刷新规则',
					iconCls:'icon-reload',
					handler:function(){
						getRule();
					}
				},{
					text:'删除规则',
					iconCls:'icon-remove',
					handler:function(){
						delRule();
					}
				}]
        });
        getRule();
        
    });
    function editProc(){
    	$("#procRule").parent().show(); 
    	$('#ruleTabs').hide();
    	$('#proc').show();
    }
    function editSql(){
    	$("#sqlRule").parent().show(); 
    	$('#ruleTabs').show();
    	$('#proc').hide();
    }
    var sourTabKey ="";
    var destTabKey ="";
    //获得单个帐套中所有的模块
    function getAllModule(){
    	var treeId = "${param.treeId}";
    	var urlType = "${param.title}";
    	var options = {
				data : {
					"treeId":treeId,
					"urlType":urlType
				},
				url: "${ctx}/design/doXml/getTabName",
				success:function(data) {
					var dataJson =[];
					var flag = 0;
					$.each(data.msg,function(k,v){
						//alert(k+":"+v);
						if(flag==0){
							dataJson.push({"key":k,"caption":v,"selected":true });
						}else{
							dataJson.push({"key":k,"caption":v});
						}
						flag++;
					});
					$("#sourTab").combobox({  
					    "data":dataJson,  
					    "valueField":"key",  
					    "textField":"caption" ,
					    "onSelect":function(record){
					    	sourTabKey = record.key;
					    	getAllField(treeId,sourTabKey,"sourMul");
					    }
					});  
					$("#destTab").combobox({  
					    "data":dataJson,  
					    "valueField":"key",  
					    "textField":"caption" ,
					    "onSelect":function(record){
					    	destTabKey = record.key;
					    	getAllField(treeId,destTabKey,"destMul");
					    }
					}); 
					/* $("#sourTab").combobox({"onSelect":function(record){
						alert(record.key);
						}
					}); */
				}
		};
		fnFormAjaxWithJson(options);
    }
  	function getAllField(treeId,formKey,flag,source,dest){
  	//	field_data = [];
    	var urlType = "${param.title}";
    	if(source&&dest){
    		var opt = {
    				data : {
    					"treeId":treeId,
    					"urlType":urlType
    				},
    				url: "${ctx}/design/doXml/getTabName",
    				success:function(data) {
    				//	alert(source);
    				//	alert(data.msg[source]+":"+data.msg[dest]);
    					 var sourJson =[ {"key":source,"caption":data.msg[source.toUpperCase()], "selected":true  }];
    					 var destJson = [{"key":dest,"caption":data.msg[dest.toUpperCase()], "selected":true  }];
    					 
    					/*  if(flag=="sourMul"){
    							field_data = new Array();
    							$.each(data.msg,function(k,v){
    								field_data.push({"key":k,"caption":v});
    							});
    							
    						}  */
    					 $("#sourTab").combobox({  
    						    "data":sourJson,  
    						    "valueField":"key",  
    						    "textField":"caption" ,
    						    "onSelect":function(record){
    						    	destTabKey = record.key;
    						    	getAllField("${param.treeId}",destTabKey,"sourMul");
    						    }
    						});  
    						$("#destTab").combobox({  
    						    "data":destJson,  
    						    "valueField":"key",  
    						    "textField":"caption",
    						    "onSelect":function(record){
    						    	destTabKey = record.key;
    						    	getAllField("${param.treeId}",destTabKey,"destMul");
    						    }
    						}); 
    						getAllField("${param.treeId}",source,"sourMul");
    						getAllField("${param.treeId}",dest,"destMul");
    				}
    			};
    		fnFormAjaxWithJson(opt);
    	}else{
  		 var options = {
				data : {
					"treeId":treeId,
					"formKey":formKey
				},
				url: "${ctx}/design/doXml/getFileName",
				success:function(data) {
					if(flag=="sourMul"){
					 field_data.splice(0,field_data.length); 
					}
					var dataStr ="";
					$.each(data.msg,function(k,v){
						//alert(k+":"+v);
						dataStr +="<option value="+k+">"+v+"</option>";
						 if(flag=="sourMul"){
							 field_data.push({"key":k,"caption":v});
						 }
					});
					$("#"+flag).html(dataStr);
					/*  if(flag=="sourMul"){
						//field_data = new Array();
						//field_data = [];
						 field_data.splice(0,field_data.length);  
						$.each(data.msg,function(k,v){
							field_data.push({"key":k,"caption":v});
						});
						//$("#sqlWhereTable").data("fieldArrayData",fieldArrayData);
					}   */
				}
			}; 
  		
  		fnFormAjaxWithJson(options);
    	}
  	}
  	var rows=[];
  	function sourTOdest(){
  		var dest = $("#destMul").val();
  		var sour = $("#sourMul").val();
  		if(dest&&sour){
  			var flag = findCell(sour,dest);
  			if(flag){
  				row = {"source":sour[0],"dest":dest[0],"rule":""};
  				$("#fieldRuleTable").edatagrid("addRow",row);
  				var data = $("#fieldRuleTable").edatagrid("getData");
	    		var sourfieldStr = "";
	    		var destfieldStr = "";
	    		$.each(data.rows,function(k,v){
	    			destfieldStr += v.dest+",";
	    			if(__fieldToSqlParam[v.source]) {
	    				//console.log(v.source + ":" + __fieldToSqlParam[v.source]);
	    				sourfieldStr += ":" + __fieldToSqlParam[v.source] + ",";
	    			} else {
	    				sourfieldStr += v.source+",";
	    			}
	    		});
	    		var sourStr = sourfieldStr.substring(0, sourfieldStr.length-1);
	    		var destStr = destfieldStr.substring(0, destfieldStr.length-1);
	    		var content = "INSERT INTO "+destTabKey+"("+destStr+")  SELECT "+sourStr +" FROM "+sourTabKey;
	    		$("#ruleSqlStr").val(content);
  			
  			}else{
  				alert("该规则已经生成！");
  			}
  			
  			//$("#fieldRuleTable").edatagrid("loadTableData",{"total":0,"rows":rows});
  		}else{
  			alert("请选中两个表中字段再映射");
  		}
  	}
  //	查找映射字段是否重复
  	function findCell(sour,dest){
	  var rows = $("#fieldRuleTable").edatagrid("getRows");
	  for(var i=0;i<rows.length;i++){
		 // alert(rows[i].source+":"+sour[0]+"     "+rows[i].dest+":"+dest[0]);
		 	var row =rows[i]; 
		 if(row["source"] == sour[0] && row["dest"] == dest[0]){
			// alert("111");
			  return false;
		  }
	  }
		return true;
  	}
 
  function delRow(index){
	  $("#fieldRuleTable").edatagrid("deleteRow",index);
  }
  function initRoleTable(){
	  $("#fieldRuleTable").edatagrid({
		  width:260,
		  height:250,
		  columns:[[  
		            {field:'source',title:'源表字段',width:70,editor:"{type:'label'}"},  
		            {field:'dest',title:'目标字段',width:70,editor:"{type:'label'}"},  
		            {field:'rule',title:'转换规则',width:70,editor:"{type:'validatebox'}"},
		            {field:'delRow',title:'',width:50,
						formatter:function(value,rec){
							var index =$("#fieldRuleTable").edatagrid("getRowIndex",rec);
							return "<a href='javascript:delRow("+index+")' class='easyui-linkbutton'>删除</a>";
						}
					}
		            
		        ]]  
	  });
  }
  var field_data = [];
  				function fieldType(value){
  					for(var i=0;i<field_data.length;i++){
  						if(field_data[i].key==value)return field_data[i].caption;
  					}
  					return value;
  				}
  
  var flag_data = [	{key:'sqlWhere',caption:"查询条件"},
					/* {key:'setValue',caption:"设置参数"},
					{key:'groupBy',caption:"分组查询"}, */
					{key:'orderBy',caption:"排序方式"}
				];
				function flagType(value){
		 			for(var i=0; i<flag_data.length; i++){
		 				if (flag_data[i].key == value) return flag_data[i].caption;
		 			}
		 			return value;
		 		}
	var orderBy_data = [{key:'ASC',caption:"升序"},
				     	{key:'DESC',caption:"降序"},
				     	{key:'',caption:"空值"}		     	
				    ];
					function orderByType(value){
			 			for(var i=0; i<orderBy_data.length; i++){
			 				if (orderBy_data[i].key == value) return orderBy_data[i].caption;
			 			}
			 			return value;
			 		}
					
					
	function initSqlWhereGrid(){
		$("#ruleTabs").tabs("select",1);
		//$("#ruleTabs").tabs("disableTab",2);
		/*  $("#sqlWhereTable").edatagrid({onRowContextMenu:function(e, rowIndex, rowData){
			 e.preventDefault(); 
			 $("#sqlWhereTable").edatagrid("selectRow",rowIndex);
				$('#delQueryWhere').menu('show',{
		    			left:e.pageX,
		    			top:e.pageY
		    	});  
		 }});
		
		$("#sqlWhereTable").edatagrid("loadData",{"total":0,"rows":[]}); */
		$("#sqlWhereTable").edatagrid({});
		$("#sqlWhereTable").edatagrid("addRow",{});
		
	}
	
	function createPLSql(){
		$("#ruleTabs").tabs("select",2);
		var ruleRows = $("#fieldRuleTable").edatagrid("getRows");
		$("#sqlWhereTable").edatagrid("saveRow");
		var whereRows = $("#sqlWhereTable").edatagrid("getRows");
		var sourfieldStr = "";
		var destfieldStr = "";
		var sqlWhereStr = "";
		var orderByStr = "";
		$.each(ruleRows,function(k,v){
			sourfieldStr += v.source+",";
			destfieldStr += v.dest+",";
		});
		$.each(whereRows,function(k,v){
			if(v.flag=="sqlWhere"){
				sqlWhereStr += " "+v.column+" "+v.operator+" "+v.value +" and";
			}else if(v.flag=="orderBy"){
				orderByStr +=" "+ v.column + " "+v.order+",";
			}
		});
		var sourStr = sourfieldStr.substring(0, sourfieldStr.length-1);
		var destStr = destfieldStr.substring(0, destfieldStr.length-1);
		var whereStr = sqlWhereStr.substring(0, sqlWhereStr.length-3);
		var orderStr = orderByStr.substring(0, orderByStr.length-1);
		var content = "INSERT INTO "+destTabKey+"("+destStr+")  SELECT "+sourStr +" FROM "+sourTabKey+" WHERE "+whereStr +"ORDER BY "+orderStr;
		if(sqlWhereStr != "" && orderByStr != ""){
			$("#ruleSqlStr").val(content);
		}else if(sqlWhereStr != "" && orderByStr == ""){
			$("#ruleSqlStr").val("INSERT INTO "+destTabKey+"("+destStr+")  SELECT "+sourStr +" FROM "+sourTabKey+" WHERE "+whereStr);
		}
		
		//alert(111);
	}
	function delQueryWhereRow(){
		var rowData = $("#sqlWhereTable").edatagrid("getSelected");
		var index = $("#sqlWhereTable").edatagrid("getRowIndex",rowData);
		$("#sqlWhereTable").edatagrid("deleteRow",index);
		//alert(index);
		/* $("#sqlWhereTable").edatagrid("insertRow",{index: index+1,	
			row: {
				
			}}); */
	}
	function amendQueryWhereRow(){
		var rowData = $("#sqlWhereTable").edatagrid("getSelected");
		var index = $("#sqlWhereTable").edatagrid("getRowIndex",rowData);
		$("#sqlWhereTable").edatagrid("beginEdit",index);
	}
		
</script>
<div class="easyui-layout" fit="true">
	<div region="center">
		<table id="flowTable"></table>
	</div>
	<div region="south" title="规则设置" style="height:300px">
		<table id="ruleTable"></table>
	</div>
</div>
<div id="ruleWin" class="easyui-window" title="规则设置" closed="true" style="width:700px;height:430px"  >
	<div id="ruleMainTabs" class="easyui-layout" fit="true">
		<div region="north" style="padding:5px 5px;overflow:hidden;">
			<span>规则key：<input id="ruleKey" type="text"/></span>
			<span>规则名称：<input id="ruleCaption" type="text"/></span>
			<span><input id="sqlRule" onclick="editSql()" name="ruleType" type="radio" value="sql"/>sql</span>
			<span><input id="procRule" onclick="editProc()" name="ruleType" type="radio" value="proc"/>proc</span>
		</div>
		<div region="center">
			<div id="ruleTabs"  class="easyui-tabs" > <!-- class="easyui-tabs" -->
				<div title="字段下推">
						<!-- <table id="addRuleTable"  >
							<thead>
								<tr>
								<th field="key" width="100" editor="{type:'validatebox'}">规则key</th>
								<th field="caption" width="100" editor="{type:'validatebox'}">规则名称</th>
								<th field="type" width="100" editor="{type:'validatebox'}">规则类型</th>
								</tr>
							</thead>
						</table> -->
						<div style="float:left;padding:5px;">
							<div style="text-align:center;">源表</div>
							<div><input id="sourTab" class="easyui-combobox"  style="width:150px;"></input></div>
							<div><select  id="sourMul" multiple="multiple" style="width :150px;height:250px"></select></div>
							
						</div>
						<div style="float:left;padding:5px;"> 
							<div style="text-align:center;">目标表</div>
							<div><input id="destTab" class="easyui-combobox"  style="width:150px;"></input></div>
							<div><select id="destMul" multiple="multiple" style="width :150px;height:250px"></select></div>
						</div>
						<div style="float:left;padding-top:100px;">
							<input type="button" value="添加映射" onclick="sourTOdest()">
						</div>
						<div style="float:right;padding:5px;"> 
							<div style="text-align:center;">
								字段映射
							</div>
							<div>
							<table id="fieldRuleTable">
							<!-- <thead>
								<tr>
									<th field="source" width="70" editor="{type:'label'}">源表字段</th>
									<th field="dest" width="70" editor="{type:'label'}">目标字段</th>
									<th field="rule" width="70" editor="{type:'validatebox'}">转换规则</th>
									<th field="delRow" width="50" formatter="returnCell" }">删除</th> 
								</tr>
							</thead> -->
							</table>
							</div>
						</div>	
						<input type="button" value="下一步" onclick="initSqlWhereGrid()">
						<div style="clear:both;"></div>
				</div>
				<div title="下推条件" >
				
						<table id="sqlWhereTable"  rownumbers="true" singleSelect="true" style="width:680px;height:270px">
							 <thead>
								<tr>
									<th field="flag" width="100" formatter="flagType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:flag_data}}">节点类型</th> 
									<th field="column" width="110" formatter="fieldType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:field_data}}">字段名(column)</th>
									<th field="operator" width="110" formatter="sqlQueryType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:_sqlQueryWhere}}">运算条件(operator)</th>
									<th field="value" width="110" editor="{type:'validatebox'}">值</th>
									<th field="order" width="110" formatter="orderByType" editor="{type:'combobox',options:{valueField:'key',textField:'caption',data:orderBy_data}}">排序方式(order)</th>
								</tr>
							</thead>  
							</table>
						    <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'"   onclick="javascript:createPLSql()">生成sql语句</a>  
				</div>
				<div title="sql下推">
						<!-- <table id="sqlRuleTable" >
							<thead>
								<tr>
									<th field="key" width="100" editor="{type:'validatebox'}">规则key</th>
									<th field="caption" width="100" editor="{type:'validatebox'}">规则名称</th>
									<th field="type" width="100" editor="{type:'validatebox'}">规则类型</th>
								</tr>
							</thead>
						</table> -->
					<div style="padding:5px;"><textarea id="ruleSqlStr" style="width:98%;height:280px;"></textarea></div>
				</div>				
			</div>
			<div id="proc" style="display:none;">				
						<div  style="padding:5px;"><textarea id="ruleProcStr" style="width:98%;height:280px;"></textarea></div>		
			</div>
			<div region="south" style="text-align:right;padding:3px 0;">
				<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="setRuleSql();">设置</a>
				<a class="easyui-linkbutton"  href="javascript:void(0)" onclick="javascript:$('#ruleWin').window('close');">关闭</a>
			</div>
		</div>
	</div>
</div>


			<!-- 删除公式sql属性   -->
			<div id="delQueryWhere" class="easyui-menu" style="width:100px;">
				<div iconCls="icon-remove" onclick="delQueryWhereRow();">删除</div>
				<div iconCls="icon-edit" onclick="amendQueryWhereRow();">修改</div>
			</div>