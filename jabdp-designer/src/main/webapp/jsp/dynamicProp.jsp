<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>动态属性设置</title>
	<%@ include file="/common/meta.jsp" %>
<script type="text/javascript">
$(document).ready(function() {
  	doInitComboSql();
  	doInitEdatagrid();
});
var tableIndex = "${param.tableIndex}";
var fieldIndex = "${param.fieldIndex}";
var entityName = parent.getEntityName();

	function doInitComboSql() {  		
  		var data = parent.$.fn.module.getSqlComboData(tableIndex,fieldIndex);
  		var sqlArry = [];
  		for(var i=0;i<data.length;i++){
  			var sqlObj = {};
  			sqlObj["key"] = entityName + "." + data[i].key;
  			sqlObj["caption"] = data[i].caption;
			sqlArry.push(sqlObj);
 		}	
  		var field = parent.$.fn.module.getFieldProp(tableIndex,fieldIndex);
  		var value= null;
  		var fdp =field.dynamicProp;
  		if(fdp){
  			if(fdp.dynamicTableItem) {
  				value = fdp.dynamicTableItem.key;
  			}
  			if(fdp.dynamicTable){
  				$('#ton').attr("checked","checked");
  			}
  		}
  		$('#dsRefKey').combobox({  
  		    data:sqlArry,  
  		    valueField:'key',  
  		    textField:'caption',
  		    width:200
  		});

  		if(value) {
  			$("#dsRefKey").combobox("setValue", value);
  		}
  		
  	}
	
	function doInitEdatagrid() {
		var sqlData = parent.$.fn.module.getSqlComboData(tableIndex,fieldIndex);
		var sqlArry = [];
		for(var i=0;i<sqlData.length;i++){
  			var sqlObj = {};
  			sqlObj["key"] = entityName + "." + sqlData[i].key;
  			sqlObj["caption"] = sqlData[i].caption;
			sqlArry.push(sqlObj);
 		}
		var formList = parent.$.fn.module.getModule().dataSource.formList;
		var childTableData = [];
	 		 for(var i=0,len=formList.length;i<len;i++) {
				if(!formList[i]["isMaster"]){		
					childTableData.push(formList[i]);	
				}
			}
	$('#dynamicFieldList').edatagrid({
		width : 'auto',
		height : 'auto',
		nowrap: true,
		striped: true,	
		rownumbers:true,
		frozenColumns:[[
		                {field:'ck',checkbox:true}
		]],
		columns :[[
			{field:'key',title:'动态子表字段sqlkey',width:180, sortable:true,		
			editor : {							
              	type:'combobox',
                options:{
                          valueField:'key',
					      textField:'caption',
					      data:sqlArry
					}
				},
				
			 formatter:function(value, rowData, rowIndex){
			    	 for(var i=0;i<sqlArry.length;i++){
			    		if(sqlArry[i].key==value){
			    			return	sqlArry[i].caption;
			    			}
			    		}	 				    	                                                            
                  }
			},
			{field:'value',title:'动态子表的Id',width:180, sortable:true,
			editor: {							
              	type:'combobox',
                options:{
                          valueField:'key',
					      textField:'caption',
					      data:childTableData
					}
				},
				
			 formatter:function(value, rowData, rowIndex){
			    	for(var i=0;i<childTableData.length;i++){			    		
			    		if(childTableData[i].key==value){
			    			return	childTableData[i].caption;
			    			}
			    		}	 				    	                                                            
                  }
			}	
	]],
		toolbar:[{
			id:'mb_add',
			text:'添加',
			iconCls:'icon-add',
			handler:function(){			
			$("#dynamicFieldList").edatagrid('addRow',{"key":"","value":""});
			}
		},'-',{
			id:'mb_del',
			text:'删除',
			iconCls:'icon-remove',
			handler:function(){				
				$("#dynamicFieldList").edatagrid('destroyRow');
			}
		},'-',{
				id:'mb_reload',
				text:'刷新',
				iconCls:'icon-reload',
				handler:function(){				
					doInitEdatagrid();
				}
		}]
	});
	var field = parent.$.fn.module.getFieldProp(tableIndex,fieldIndex);
	var data = {"total":0,"rows":[]};
	if(field.dynamicProp){
		var dataList = field.dynamicProp.dynamicColumnItems;
		if(dataList) {
			$.each(dataList,function(k,v){
				 data.rows.push($.extend({},v));	 						
			});
		} else {
			dataList = [];
		}
		data.total=dataList.length;
		if(field.dynamicProp.dynamicColumn){
			$('#fon').attr("checked","checked");
		}
	}
	$('#dynamicFieldList').edatagrid("loadData",data);
}
	function doSave(){
		if($('#ton').attr("checked")){//开启动态表
			onDynamicTable(true);
		}else if($('#toff').attr("checked")){//关闭
			onDynamicTable(false);
		}
		
		if($('#fon').attr("checked")){//开启动态字段
			onDynamicFields(true);
		}else if($('#foff').attr("checked")){
			onDynamicFields(false);
		}
		closed();
	}
	function onDynamicTable(flag){
		var form = parent.$.fn.module.getFormProp(tableIndex);
		var field = parent.$.fn.module.getFieldProp(tableIndex,fieldIndex);
		var val = $("#dsRefKey").combobox("getValue");		
		form.dynamicShow = flag;
		
		var fdp = field.dynamicProp;
		if(!fdp || fdp=="null"){
			var itemProp={
	  				"key":"",
	  				"caption":"",
	  				"column":"",
	  				"order":"",
	  				"operator":"",
	  				"value":""
	  		};
			fdp ={
						"dynamicTable":false,
						"dynamicTableItem":itemProp,
						"dynamicColumn":false,
						"dynamicColumnItems" :[]
			};
  		}
		fdp.dynamicTable = flag;
		fdp.dynamicTableItem.key = val;
		field.dynamicProp = fdp;
	}
	function onDynamicFields(flag){
		var field = parent.$.fn.module.getFieldProp(tableIndex,fieldIndex);	
		$('#dynamicFieldList').edatagrid("saveRow");
  		var dfJson = $('#dynamicFieldList').edatagrid("getRows");	  	
  		var list = [];
  	
  		$.each(dfJson , function(k, v) {
  			if(v.key && v.value) {
  				var item={};
  				item.key = v.key;
  				item.value = v.value;
  				list.push(item);
  			}
  		});
  		var fdp = field.dynamicProp;
  		if(!fdp){
  			var itemProp={
  	  				"key":"",
  	  				"caption":"",
  	  				"column":"",
  	  				"order":"",
  	  				"operator":"",
  	  				"value":""
  	  		};
  			fdp ={
  					"dynamicTable":false,
  					"dynamicTableItem":itemProp,
  					"dynamicColumn":false,
  					"dynamicColumnItems" :[]
  			};
  		}
  		fdp.dynamicColumn = flag;
  		fdp.dynamicColumnItems = list;
  	
  		var formList = parent.$.fn.module.getModule().dataSource.formList;
  		for(var j=0,len1=list.length;j<len1;j++){//将对应的字段动态显示设为true
  		 for(var i=0,len2=formList.length;i<len2;i++) {
				if(!formList[i]["isMaster"]){
					if(formList[i].key==list[j].value){
						formList[i].dynamicShow = flag;
					}else{
						formList[i].dynamicShow = false;
					}						
				}
			}
  		}
  		field.dynamicProp = fdp;
  		
	}
	function closed(){
		parent.showLineInfo(tableIndex,fieldIndex);
		parent.$('#dynamicPropWin').window('close');
	}
</script>
</head>
<body  class="easyui-layout" >
	
	<div region="center" border="false">
		<div id="tabs" class="easyui-tabs" fit="true"  tools="#tab-tools1">	
			<div title="动态显示子表设置" style="padding: 5px;">
				<div class="easyui-layout" fit="true">
					<div region="north" border="false"
						style="padding: 5px; background: #fff; ">
						<input type="radio" name="checkTable" value="0" id="ton" />开启 <input
							type="radio" name="checkTable" value="1" id="toff"
							checked="checked" />关闭
					</div>
					<div region="center" border="false">
						<div id="sqlView">
							<input id="dsRefKey" name="dsRefKey" />
							<button onclick="doInitComboSql()">刷新sql列表</button>
						</div>
					</div>
				</div>
			</div>
			<div title="动态显示子表字段设置" style="padding: 5px;">
				<div class="easyui-layout" fit="true">
					<div region="north" border="false"
						style="padding: 5px; background: #fff; ">
						<input type="radio" name="checkField" value="0" id="fon" />开启 <input
							type="radio" name="checkField" value="1" id="foff"
							checked="checked" />关闭
					</div>
					<div region="center" border="false">
						<table id="dynamicFieldList" fit="true">
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div region="south" border="false" style="text-align: right; ">
			<a id="mm" class="easyui-linkbutton" iconCls="icon-ok" 
			href="javascript:void(0);" onclick="doSave();">设置</a> 
			<a class="easyui-linkbutton" iconCls="icon-cancel" href="javascript:void(0);"
								onclick="closed();">关闭 </a>
		</div>
	<div id="tab-tools1">			
		<a href="#" class="easyui-linkbutton" plain="true"  iconCls="icon-search" onclick="javascript:parent.$('#sqlEditWin').window('open');" >sql编辑</a>			
	</div>
</body>
</html>