package com.qyxx.designer.miniweb.web;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.qyxx.designer.miniweb.service.ZtreeMenuManager;
import com.qyxx.designer.modules.mapper.JsonMapper;
import com.qyxx.designer.modules.utils.ZipUtils;
import com.qyxx.designer.modules.utils.sevenziputils.SevenZipServer;




@Controller
@SessionAttributes({"loginUrl","loginName","configPath","passwd"})
@RequestMapping(value="/design")
public class UploadFileController {
	@Autowired
	private ZtreeMenuManager ztreeMenuManager;
	private JsonMapper jm = new JsonMapper();
	/**
	 * SpringMVC上传文件
	 * @param request
	 * @param response
	 * @param model
	 * @return null
	 * */
	@RequestMapping(method=RequestMethod.POST,value="uploadFile")
	public void uploadFile(HttpServletRequest request, HttpServletResponse response, ModelMap model){
		
		 CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
		    
		 commonsMultipartResolver.setDefaultEncoding("utf-8");
		    
		    if (commonsMultipartResolver.isMultipart(request)) {   
		    
			    MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;   
			    
			    Iterator<String> iter = multipartRequest.getFileNames();   
			    
			    while (iter.hasNext()) {   
			    
			       MultipartFile file = multipartRequest.getFile((String) iter.next());   
			    
			       if (file != null) {   
			           String fileName = "";  
			           fileName = file.getOriginalFilename();  
			           if (fileName.lastIndexOf(".") >= 0) {
			        	//   System.out.println(request.getSession().getServletContext());
			        	   String cp =DesignController.getRealPath(request)+DesignController.URL_TYPE_LOCAL+"/";
			        	   String time = DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmssSSS");
				           String filePath = cp+time+"/";
				           String allPath =filePath+fileName;  
				           try {
				        	   File folder = new File(filePath);
				        	   folder.mkdir();
				        	   File fl = new File(allPath);
				        	   file.transferTo(fl);  
				        	   ZipUtils.unzip(allPath, filePath);
				        	   FileUtils.deleteQuietly(new File(allPath));
				        	 
				           	}catch(Exception e) {
				    			e.printStackTrace();
				    		}
				           Object config = model.get("configPath");
							if(config==null){
								Map<String,String> m = DesignController.getLocalFile(cp);
								model.addAttribute("configPath", m);
							}else{
								Map<String,String> path = (Map<String,String>)config;
								Map<String,String> m = DesignController.getLocalFile(cp);
								 Set<Map.Entry<String, String>> set = m.entrySet();
								 for (Iterator<Map.Entry<String, String>> it = set.iterator(); it.hasNext();) {
							            Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
							            path.put(entry.getKey(), entry.getValue());
								 }
								model.addAttribute("configPath",path );
							}
				          }
			          }
			       }
			    
		     }
		   
	}
	
}
