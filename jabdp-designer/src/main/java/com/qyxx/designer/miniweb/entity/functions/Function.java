package com.qyxx.designer.miniweb.entity.functions;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;



/**
 *  树形根节点
 *  
 *  @author thy
 *  @version 1.0 
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="function")
public class Function {
	
	@JsonProperty("children")
	@XmlElement
	private List<Methods> methods = new ArrayList<Methods>();

	public List<Methods> getMethods() {
		return methods;
	}

	public void setMethods(List<Methods> methods) {
		this.methods = methods;
	}
}
