package com.qyxx.platform.sysmng.tie.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 留言贴主表
 */
@Entity
@Table(name = "SYS_TIE")
public class Tie {
	private Long id;
	private String content;//内容
	private Long parentId;//如果是回复贴，关联被评论父贴的ID，否则留null
	private Long relevanceId;//关联留言贴ID
	private String moduleName;//关联模块名
	private Long user;//帖子创建者
	private Date time;//帖子创建时间
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_ATTACH_LIST_ID")
	@SequenceGenerator(name="SEQ_SYS_ATTACH_LIST_ID", sequenceName="SEQ_SYS_ATTACH_LIST_ID")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Lob
	@Column(name = "CONTENT", columnDefinition = "ntext")
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	@Column(name="PARENT_ID",nullable=true)
	public Long getParentId() {
		return parentId;
	}
	
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	@Column(name="RELEVANCE_ID",nullable=false)
	public Long getRelevanceId() {
		return relevanceId;
	}
	
	public void setRelevanceId(Long relevanceId) {
		this.relevanceId = relevanceId;
	}
	
	@Column(name="MODULE_NAME",length=500)
	public String getModuleName() {
		return moduleName;
	}
	
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	@Column(name="USER_ID",nullable=false)
	public Long getUser() {
		return user;
	}

	public void setUser(Long user) {
		this.user = user;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TIME",updatable=false)
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
}
