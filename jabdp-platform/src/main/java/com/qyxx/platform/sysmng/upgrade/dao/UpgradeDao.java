/*
 * @(#)UpgradeDao.java
 * 2012-12-19 上午09:55:43
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.upgrade.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.upgrade.entity.Upgrade;


/**
 *  部署日志对象Dao
 *  @author gxj
 *  @version 1.0 2012-12-19 上午09:55:43
 *  @since jdk1.6 
 */
@Component
public class UpgradeDao extends HibernateDao<Upgrade, Long> {

}
