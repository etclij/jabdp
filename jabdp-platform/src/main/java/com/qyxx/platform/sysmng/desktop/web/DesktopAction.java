/*
 * @(#)DeskTopAction.java
 * 2012-9-6 上午08:36:32
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.desktop.web;

import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.gsc.utils.SystemParam;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.desktop.entity.Desktop;
import com.qyxx.platform.sysmng.desktop.entity.DesktopJson;
import com.qyxx.platform.sysmng.desktop.service.DesktopManager;
import com.qyxx.platform.sysmng.desktop.service.DesktopToUserManager;
import com.qyxx.platform.sysmng.utils.Constants;

/**
 * 系统个人桌面管理
 * 
 * @author ly
 * @version 1.0 2012-9-6 上午08:36:32
 * @since jdk1.6
 */
@Namespace("/sys/desktop")
@Results({ })
public class DesktopAction extends CrudActionSupport<Desktop> {
    private Long userId;
	private DesktopManager desktopManager;
	private DesktopToUserManager desktopToUserManager;
	private Long level=new Long(1);
	// 操作方法
	private String operMethod = CrudActionSupport.VIEW;
	private Long orgId;
	private Long roleId;
	private Long parentId;
	private SystemParam systemParam;
	
	public SystemParam getSystemParam() {
		return systemParam;
	}

	@Autowired
	public void setSystemParam(SystemParam systemParam) {
		this.systemParam = systemParam;
	}
	public Long getParentId() {
		return parentId;
	}
	
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public Long getRoleId() {
		return roleId;
	}

	
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}
	
	public DesktopManager getDesktopManager() {
		return desktopManager;
	}
	
	@Autowired
	public void setDesktopManager(DesktopManager desktopManager) {
		this.desktopManager = desktopManager;
	}
	
	public DesktopToUserManager getDesktopToUserManager() {
		return desktopToUserManager;
	}
	
	@Autowired
	public void setDesktopToUserManager(DesktopToUserManager desktopToUserManager) {
		this.desktopToUserManager = desktopToUserManager;
	}


	/**
	 * @return the operMethod
	 */
	public String getOperMethod() {
		return operMethod;
	}

	/**
	 * @param operMethod the operMethod to set
	 */
	public void setOperMethod(String operMethod) {
		this.operMethod = operMethod;
	}

	

	@JsonOutput
	public String queryList() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);

		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		pager = desktopManager.findDeskTopList(pager, filters);
		Struts2Utils.renderJson(pager);
		return null;

	}

	
	@JsonOutput
	public String desktopList() throws Exception {
		List<DesktopJson> list = desktopManager.getDesktopList(roleId);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	@Override
	public String list() throws Exception {
		return SUCCESS;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonOutput
	public String save() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		desktopToUserManager.saveDeskTop(entity, user.getId());
		formatMessage.setMsg(entity.getId());
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	/**
	 * 跳转到新增通知页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public String add() throws Exception {
		operMethod = CrudActionSupport.ADD;
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		orgId = user.getOrganization().getId();
		return VIEW;
	}

	/**
	 * 跳转到复制通知页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public String edit() throws Exception {
		operMethod=CrudActionSupport.EDIT;
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		orgId=user.getOrganization().getId();
		return VIEW;
	}
	
	/**
	 * 跳转到查看通知页面
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#view()
	 */
	@Override
	public String view() throws Exception {
		operMethod=CrudActionSupport.VIEW;
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		userId=user.getId();
		orgId=user.getOrganization().getId();
		return VIEW;
	}

	@Override
	@JsonOutput
	public String delete() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		desktopToUserManager.deleteDeskTops(ids,user.getId());
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	
	@Override
	protected void prepareModel() throws Exception {
		if (id != null) {
			entity = desktopToUserManager.findById(id);
		} else {
			entity = new Desktop();
		}
	}


	/**
	 * 根据id查询个人桌面
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryDesktop() throws Exception{
		entity=desktopToUserManager.findById(id);
		level=entity.getLevel();
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
		
	}

	@JsonOutput
	public String desktopTree() throws Exception{
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		List<Desktop> list=desktopManager.desktopTree(user.getId());
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	
	@JsonOutput
	public String queryLevel() throws Exception{
		Desktop desktop=desktopManager.findById(id);
		formatMessage.setMsg(desktop);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	/**
	 * 个人登入主页查询用户桌面（LEVEL=1）
	 */
	public String personalQueryLevel1Desktop()throws Exception{
		User user = (User) Struts2Utils
		.getSessionAttribute(Constants.USER_SESSION_ID);
		//查询用户个人桌面中LEVEL=1的集合 
		List<Desktop> list = desktopManager.findLevel1Desktop(user.getId());
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);	
		return null;	
	}
	/**
	 * 查询当前Level=1下所有的Level=2
	 * @return
	 * @throws Exception
	 */
  @JsonOutput
	public String personalQueryLevel2Desktop() throws Exception{
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		List<Desktop> dLevel2 = desktopManager
				.findLevel2DesktopByParentId(parentId,user.getId());
		formatMessage.setMsg(dLevel2);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
  
	/**
	 * 将desktop中的类容赋值给desktopToUser并返回该对象
	 * @return
	 */
	@JsonOutput
	public String queryDesktopSource(){
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		entity=desktopManager.queryDesktopSource(id, user);
		entity.setSystemPath(systemParam.getVirtualPicPath());
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
}
