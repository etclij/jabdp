/*
 * @(#)DictManager.java
 * 2011-5-22 下午10:52:28
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.dictmng.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.qyxx.platform.common.definition.DefinitionSource;
import com.qyxx.platform.common.definition.Definition.DefinitionEntity;
import com.qyxx.platform.sysmng.dictmng.dao.DictionaryDao;
import com.qyxx.platform.sysmng.dictmng.dao.DictionaryTypeDao;
import com.qyxx.platform.sysmng.dictmng.entity.Dictionary;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  数据字典管理类
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-22 下午10:52:28
 */
@Component
@Transactional
public class DictManager implements DefinitionSource {
	
	private static Logger logger = LoggerFactory.getLogger(DictManager.class);
	
	private DictionaryDao dictionaryDao;
	
	private DictionaryTypeDao dictionaryTypeDao;
	
	private static final String QUERY_DICT_SQL = "from Dictionary d where d.dictType.dictTypeKey = ? and d.dictLocale = ? and d.status = ? order by d.id asc";
	
	private static final String QUERY_DICT_SQL_NO_LOCALE = "from Dictionary d where d.dictType.dictTypeKey = ? and d.status = ? order by d.id asc";

	/**
	 * @param dictionaryDao
	 */
	@Autowired
	public void setDictionaryDao(DictionaryDao dictionaryDao) {
		this.dictionaryDao = dictionaryDao;
	}
	
	/**
	 * @param dictionaryTypeDao
	 */
	@Autowired
	public void setDictionaryTypeDao(DictionaryTypeDao dictionaryTypeDao) {
		this.dictionaryTypeDao = dictionaryTypeDao;
	}
	
	/**
	 * 根据条件查询数据字典
	 * 
	 * @param dictTypeKey 数据字典Key
	 * @param dictLocale 数据字典语言
	 * @param status 状态
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<Dictionary> findDictionary(String dictTypeKey, String dictLocale, String status) {
		return dictionaryDao.find(QUERY_DICT_SQL, dictTypeKey, dictLocale, status);
	}

	/**
	 * 根据条件查询数据字典，无语言环境
	 * 
	 * @param dictTypeKey 数据字典Key
	 * @param status 状态
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<Dictionary> findDictionary(String dictTypeKey, String status) {
		return dictionaryDao.find(QUERY_DICT_SQL_NO_LOCALE, dictTypeKey, status);
	}
	
	
	@Transactional(readOnly = true)
	@Override
	public List<DefinitionEntity> getDefinitions(String keyWord) {
		List<Dictionary> list = this.findDictionary(keyWord, Constants.ENABLED);
		List<DefinitionEntity> deList = Lists.newArrayList();
		for(Dictionary dt : list) {
			Map<String,Object> ev = new HashMap<String, Object>();
			ev.put(Constants.DESC, dt.getDictDesc());
			DefinitionEntity de = new DefinitionEntity(dt.getDictName(), dt.getDictValue(), keyWord,
					String.valueOf(dt.getId()), ev);
			deList.add(de);
		}
		return deList;
	}

	@Transactional(readOnly = true)
	@Override
	public List<DefinitionEntity> getDefinitions(String keyWord,
			String locale) {
		List<Dictionary> list = this.findDictionary(keyWord, locale, Constants.ENABLED);
		List<DefinitionEntity> deList = Lists.newArrayList();
		for(Dictionary dt : list) {
			Map<String,Object> ev = new HashMap<String, Object>();
			ev.put(Constants.LOCALE, locale);
			ev.put(Constants.DESC, dt.getDictDesc());
			DefinitionEntity de = new DefinitionEntity(dt.getDictName(), dt.getDictValue(), keyWord,
					String.valueOf(dt.getId()), ev);
			deList.add(de);
		}
		return deList;
	}

}
