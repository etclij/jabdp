/*
 * @(#)ResourceJson.java
 * 2011-6-11 下午10:23:31
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.google.common.collect.Lists;
import com.qyxx.platform.sysmng.accountmng.entity.Authority;
import com.qyxx.platform.sysmng.accountmng.entity.ResourceJson;


/**
 *  资源Json对象
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-6-11 下午10:23:31
 */
@JsonAutoDetect(getterVisibility=Visibility.ANY)
public class ResourceJson implements Serializable {
	   
		private static final long serialVersionUID = 7426213548750206072L;

		private Long id;
		private String resourceSign; 
		private String resourceName;
	    private String resourceType;
	    private Long parentId;
		private String resourceIcon;
		private String openType;
		private String resourceStatus;
		private String resourceUrl;
	    private String resourceLevel;
	    private String remark;
	    private Long resourceNo;
	    private  boolean appShow;

	    private List<ResourceJson> childs = Lists.newArrayList();
	    
	    private Boolean isParent;

	    
	    public ResourceJson(Authority al, String name) {
	    	this.id = al.getId();
	    	this.resourceSign = al.getName();
	    	this.resourceName = name;
	    	this.resourceType = al.getResourceType();
	    	if(null!=al.getAuthority()) {
	    		this.parentId = al.getAuthority().getId();
	    	}
	    	this.resourceIcon = al.getResourceIcon();
	    	this.openType = al.getOpenType();
	    	this.resourceStatus = al.getResourceStatus();
	    	this.resourceUrl = al.getResourceUrl();
	    	this.resourceLevel = al.getResourceLevel();
	    	this.remark  = al.getRemark();
	    	this.resourceNo = al.getResourceNo();	
	    	if(null!=al.getAppShow()){
	    		this.appShow = al.getAppShow(); 
	    	}
	    }
		
		/**
		 * @return id
		 */
	    @JsonProperty(value = "id")
		public Long getId() {
			return id;
		}

		
		/**
		 * @param id
		 */
		public void setId(Long id) {
			this.id = id;
		}

		
		/**
		 * @return resourceSign
		 */
		public String getResourceSign() {
			return resourceSign;
		}

		
		/**
		 * @param resourceSign
		 */
		public void setResourceSign(String resourceSign) {
			this.resourceSign = resourceSign;
		}

		
		/**
		 * @return resourceName
		 */
		@JsonProperty(value = "name")
		public String getResourceName() {
			return resourceName;
		}

		
		/**
		 * @param resourceName
		 */
		public void setResourceName(String resourceName) {
			this.resourceName = resourceName;
		}

		
		  public boolean isAppShow() {
				return appShow;
			}

			public void setAppShow(boolean appShow) {
				this.appShow = appShow;
			}
		
		/**
		 * @return resourceType
		 */
		public String getResourceType() {
			return resourceType;
		}

		
		/**
		 * @param resourceType
		 */
		public void setResourceType(String resourceType) {
			this.resourceType = resourceType;
		}

		
		/**
		 * @return parentId
		 */
		@JsonProperty(value = "pId")
		public Long getParentId() {
			return parentId;
		}

		
		/**
		 * @param parentId
		 */
		public void setParentId(Long parentId) {
			this.parentId = parentId;
		}

		
		/**
		 * @return resourceIcon
		 */
		@JsonProperty(value = "iconSkin")
		public String getResourceIcon() {
			return resourceIcon;
		}

		
		/**
		 * @param resourceIcon
		 */
		public void setResourceIcon(String resourceIcon) {
			this.resourceIcon = resourceIcon;
		}

		
		/**
		 * @return openType
		 */
		public String getOpenType() {
			return openType;
		}

		
		/**
		 * @param openType
		 */
		public void setOpenType(String openType) {
			this.openType = openType;
		}

		
		/**
		 * @return resourceStatus
		 */
		public String getResourceStatus() {
			return resourceStatus;
		}

		
		/**
		 * @param resourceStatus
		 */
		public void setResourceStatus(String resourceStatus) {
			this.resourceStatus = resourceStatus;
		}

		
		/**
		 * @return resourceUrl
		 */
		public String getResourceUrl() {
			return resourceUrl;
		}

		
		/**
		 * @param resourceUrl
		 */
		public void setResourceUrl(String resourceUrl) {
			this.resourceUrl = resourceUrl;
		}

		
		/**
		 * @return resourceLevel
		 */
		public String getResourceLevel() {
			return resourceLevel;
		}

		
		/**
		 * @param resourceLevel
		 */
		public void setResourceLevel(String resourceLevel) {
			this.resourceLevel = resourceLevel;
		}

		
		/**
		 * @return remark
		 */
		public String getRemark() {
			return remark;
		}

		
		/**
		 * @param remark
		 */
		public void setRemark(String remark) {
			this.remark = remark;
		}

		
		/**
		 * @return resourceNo
		 */
		public Long getResourceNo() {
			return resourceNo;
		}

		
		/**
		 * @param resourceNo
		 */
		public void setResourceNo(Long resourceNo) {
			this.resourceNo = resourceNo;
		}

		
		/**
		 * @return childs
		 */
		public List<ResourceJson> getChilds() {
			return childs;
		}

		
		/**
		 * @param childs
		 */
		public void setChilds(List<ResourceJson> childs) {
			this.childs = childs;
		}

		/**
		 * @return isParent
		 */
		public Boolean getIsParent() {
			if(childs != null && !childs.isEmpty()) return true;
			else return false;
		}

		
		/**
		 * @param isParent
		 */
		public void setIsParent(Boolean isParent) {
			this.isParent = isParent;
		}

		/** 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		/** 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ResourceJson other = (ResourceJson) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}

		
}
