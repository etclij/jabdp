package com.qyxx.platform.sysmng.memo.dao;

import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.memo.entity.Memo;

@Component
public class MemoDao extends HibernateDao<Memo, Long> {

}
