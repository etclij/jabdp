/*
 * @(#)DictConstants.java
 * 2014-1-13 下午10:10:04
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.utils;


/**
 *  数据字典常量类
 *  @author gxj
 *  @version 1.0 2014-1-13 下午10:10:04
 *  @since jdk1.6 
 */

public class DictConstants {
	
	/**
	 * 系统参数
	 */
	public static final String SYS_PARAMS_KEY = "SYS_PARAMS";
	
	/**
	 * 系统参数--用户默认密码key
	 */
	public static final String SYS_PARAMS_KEY_USER_DEFAULT_PASSWORD = "user.default.password";
	
	/**
	 * 默认用户密码
	 */
	public static final String DEFAULT_USER_PASSWORD = "123456";
	
	/**
	 * 系统参数--员工号默认位数
	 */
	public static final String SYS_PARAMS_KEY_USER_CODE_BIT = "user.default.code.bit";
	
	/**
	 * 默认员工号位数
	 */
	public static final String DEFAULT_USER_CODE_BIT = "4";
	
	/**
	 * 系统参数--员工号自动编号规则功能开启与否
	 */
	public static final String SYS_PARAMS_KEY_USER_AUTOCODE_ENABLE = "user.autocode.enable";
	
	/**
	 * 启用
	 */
	public static final String ENABLE = "true";
	
	/**
	 * 禁用
	 */
	public static final String DISABLE = "false";
	
}
