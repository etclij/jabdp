/*
 * @(#)UserConfigManager.java
 * 2014-5-29 下午00:00:27
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.userconfig.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.Page;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.ExceptionUtils;
import com.qyxx.platform.inf.adapter.AdapterException;
import com.qyxx.platform.inf.adapter.NeiTask;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.msg.dao.SmsMsgDao;
import com.qyxx.platform.sysmng.msg.entity.SmsMsg;
import com.qyxx.platform.sysmng.userconfig.dao.UserConfigDao;
import com.qyxx.platform.sysmng.userconfig.entity.UserConfig;


/**
 *  用户信息配置服务类
 *  @author tsd
 *  @version 1.0 2014-5-29 下午00:00:27
 *  @since jdk1.6 
 */
@Component
@Transactional
public class UserConfigManager {
	
	private static Logger logger = LoggerFactory.getLogger(UserConfigManager.class);
	
	private static final String FIND_UNSEND_HQL = "select a from SmsMsg a where a.status = ? and (a.regSendTime is null or a.regSendTime <= ?)";
	
	private UserConfigDao userConfigDao;

	private User user;

	/**
	 * @param userConfigDao
	 */
	@Autowired
	public void setUserConfigDao(UserConfigDao userConfigDao) {
		this.userConfigDao = userConfigDao;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * 获取当前用户的信息配置
	 */
	@Transactional(readOnly = true)
	public UserConfig get() {
		return userConfigDao.findUniqueBy("userId", user.getId());
	}
	
	/**
	 * 保存数据
	 * 如果已有该用户信息则更新当前用户信息配置
	 * @param entity
	 */
	public void save(UserConfig entity) {
		UserConfig userConfig = userConfigDao.findUniqueBy("userId", user.getId());
		entity.setUserId(user.getId());
		if(userConfig==null) {
			entity.setCreateTime(new Date());
			entity.setCreateUser(user.getId());
			userConfigDao.save(entity);
		} else {
			userConfig.setLastUpdateTime(new Date());
			userConfig.setLastUpdateUser(user.getId());
			userConfig.setDeskUrl(entity.getDeskUrl());
			userConfig.setShortcut(entity.getShortcut());
		}
	}
	
}
