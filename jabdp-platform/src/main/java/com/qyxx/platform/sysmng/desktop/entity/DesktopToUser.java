/*
 * @(#)DeskTopToUser.java
 * 2012-9-5 下午05:39:12
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.desktop.entity;

import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.collect.Lists;
import com.qyxx.platform.common.module.entity.BaseEntity;
import com.qyxx.platform.gsc.utils.SystemParam;

/**
 *  
 *  @author ly
 *  @version 1.0 2012-9-5 下午05:39:12
 *  @since jdk1.6 
 */

/**
 * 用户个人桌面表
 */
@Entity
@Table(name = "SYS_DESKTOP_TO_USER")
// 默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DesktopToUser extends BaseEntity {
	public static final long DESKTOPTOUSER_LEVEL1=1;
	public static final long DESKTOPTOUSER_LEVEL2=2;
	public static final long DESKTOPTOUSER_LEVEL3=3;
	public static final String STATUS_1="1";//显示
	public static final String STATUS_0="0";//隐藏
	private Long id;
	private Desktop desktop;
	private String title;
	private Long userId;
	private String layoutType;
	private String layoutVal;
	private Long displayNum;
	private Long height;
	private String options;//json格式数据：如{"showOwner":true,"showCreater":true}
	private Long fontNum;
	private Long level;//1--1级  2--2级  3--3级
	private Long parentId;
	private String status;//0.隐藏 1.显示
	private List<Map<String, Object>> list=Lists.newArrayList();
	private String systemPath; //存放新闻中上传图片的虚拟路径
	/**
	 * @return id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_DESKTOP_TO_USER_ID")
	@SequenceGenerator(name = "SEQ_SYS_DESKTOP_TO_USER_ID", sequenceName = "SEQ_SYS_DESKTOP_TO_USER_ID")
	public Long getId() {
		return id;
	}

	
	/**
	 * @return id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	
	/**
	 * @return desktop
	 */
	@ManyToOne(targetEntity = Desktop.class)
	@JoinColumn(name = "DESKTOP_ID")
	//@JsonIgnore
	public Desktop getDesktop() {
		return desktop;
	}

	
	/**
	 * @return desktop
	 */
	public void setDesktop(Desktop desktop) {
		this.desktop = desktop;
	}

	
	/**
	 * @return title
	 */
	@Column(name = "TITLE", length = 200)
	public String getTitle() {
		return title;
	}

	
	/**
	 * @return title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	
	/**
	 * @return userId
	 */
	@Column(name = "USER_ID")
	public Long getUserId() {
		return userId;
	}

	
	/**
	 * @return userId
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	
	/**
	 * @return layoutType
	 */
	@Column(name = "LAYOUT_TYPE", length = 100)
	public String getLayoutType() {
		return layoutType;
	}

	
	/**
	 * @return layoutType
	 */
	public void setLayoutType(String layoutType) {
		this.layoutType = layoutType;
	}

	
	/**
	 * @return layoutVal
	 */
	@Column(name = "LAYOUT_VAL", length = 2000)
	public String getLayoutVal() {
		return layoutVal;
	}

	
	/**
	 * @return layoutVal
	 */
	public void setLayoutVal(String layoutVal) {
		this.layoutVal = layoutVal;
	}

	
	/**
	 * @return displayNum
	 */
	@Column(name = "DISPLAY_NUM")
	public Long getDisplayNum() {
		return displayNum;
	}

	
	/**
	 * @return displayNum
	 */
	public void setDisplayNum(Long displayNum) {
		this.displayNum = displayNum;
	}

	
	/**
	 * @return height
	 */
	@Column(name = "HEIGHT")
	public Long getHeight() {
		return height;
	}

	
	/**
	 * @return height
	 */
	public void setHeight(Long height) {
		this.height = height;
	}

	
	/**
	 * @return options
	 */
	@Column(name = "OPTIONS", length = 1000)
	public String getOptions() {
		return options;
	}

	
	/**
	 * @return options
	 */
	public void setOptions(String options) {
		this.options = options;
	}

	
	/**
	 * @return fontNum
	 */
	@Column(name = "FONT_NUM")
	public Long getFontNum() {
		return fontNum;
	}

	
	/**
	 * @return fontNum
	 */
	public void setFontNum(Long fontNum) {
		this.fontNum = fontNum;
	}

	
	

	/**
	 * @return status
	 */
	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}

	
	/**
	 * @return status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	
	
	/**
	 * @return level
	 */
	@Column(name = "LEVEL_VAL")
	public Long getLevel() {
		return level;
	}

	
	/**
	 * @return level
	 */
	public void setLevel(Long level) {
		this.level = level;
	}



	
	/**
	 * @return parentId
	 */
	@Column(name = "PARENT_ID")
	public Long getParentId() {
		return parentId;
	}

	/**
	 * @return parentId
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}


	/**
	 * @return the list
	 */
	@Transient
	public List<Map<String, Object>> getList() {
		return list;
	}


	/**
	 * @param list the list to set
	 */
	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}

   @Transient
	public String getSystemPath() {
		return systemPath;
	}


	public void setSystemPath(String systemPath) {
		this.systemPath = systemPath;
	}
	
	

	
}
