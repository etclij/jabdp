package com.qyxx.platform.sysmng.attach.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qyxx.platform.common.module.entity.BaseEntity;

/**
 * 附件明细表
 */

@Entity
@Table(name = "SYS_ATTACH_DETAIL")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class AttachList extends BaseEntity {
	private Long id;
	private String fileName;//文件名
	private String filePath;//文件路径
	private Long fileSize;//文件大小
	private Long attachId;//关联附件ID
	

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_ATTACH_LIST_ID")
	@SequenceGenerator(name="SEQ_SYS_ATTACH_LIST_ID", sequenceName="SEQ_SYS_ATTACH_LIST_ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="FILE_NAME",length=500)
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	@Column(name="FILE_PATH",length=1000)
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	@Column(name="FILE_SIZE",nullable=false)
	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	@Column(name="ATTACH_ID",nullable=false)
	public Long getAttachId() {
		return attachId;
	}

	public void setAttachId(Long attachId) {
		this.attachId = attachId;
	}



}
