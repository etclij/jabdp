/*
 * @(#)ExpLogAction.java
 * 2011-5-31 下午09:34:04
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.logmng.web;

import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.service.ServiceException;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.logmng.entity.ExceptionLog;
import com.qyxx.platform.sysmng.logmng.service.ExceptionLogManager;


/**
 *  异常日志Action
 *  
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-31 下午09:34:04
 */
@Namespace("/sys/log")
public class ExpLogAction extends CrudActionSupport<ExceptionLog> {
	
	private static final long serialVersionUID = 6974441046917587058L;

	private ExceptionLogManager exceptionLogManager;
	
	//-- 页面属性 --//
	private Long id;
	private ExceptionLog entity;
	
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @param exceptionLogManager
	 */
	@Autowired
	public void setExceptionLogManager(
			ExceptionLogManager exceptionLogManager) {
		this.exceptionLogManager = exceptionLogManager;
	}

	@Override
	public ExceptionLog getModel() {
		return entity;
	}

	@Override
	public String list() throws Exception {
		return SUCCESS;
	}
	
	@JsonOutput
	public String queryList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter.buildFromHttpRequest(Struts2Utils.getRequest());
		pager = exceptionLogManager.searchExpLog(pager, filters);
		Struts2Utils.renderJson(pager);
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonOutput
	@Override
	public String view() throws Exception {
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		try {
			if (id != null) {
				entity = exceptionLogManager.getExpLog(id);
			} else {
				entity = new ExceptionLog();
			}	
		} catch(Exception e) {
			logger.error("", e);
		}
	}

}
