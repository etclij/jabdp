/*
 * @(#)NoticeAction.java
 * 2012-9-4 下午06:02:24
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.notice.web;

import java.util.Iterator;
import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.module.web.Messages;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.notice.entity.CommonType;
import com.qyxx.platform.sysmng.notice.entity.Notice;
import com.qyxx.platform.sysmng.notice.entity.NoticeToUser;
import com.qyxx.platform.sysmng.notice.service.CommonTypeManager;
import com.qyxx.platform.sysmng.notice.service.NoticeManager;
import com.qyxx.platform.sysmng.utils.Constants;

/**
 * 
 * @author ly
 * @version 1.0 2012-9-4 下午06:02:24
 * @since jdk1.6
 */
@Namespace("/sys/notice")
@Results({})
public class NoticeAction extends CrudActionSupport<Notice> {
	
	private static final long serialVersionUID = 3102345455894652143L;
	private NoticeManager noticeManager;
	private CommonTypeManager commonTypeManager;
	private String name;
	private Long typeId;
	private Long  atmId;
	private Long noticeToUserId;	
	private String style;
	private String status;
	private Boolean isReadAll = false;//一个人阅读，则表示所有人阅读通知
	
	public String getStatus() {
		return status;
	}



	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStyle() {
		return style;
	}


	
	public void setStyle(String style) {
		this.style = style;
	}


	public Long getNoticeToUserId() {
		return noticeToUserId;
	}



	
	public void setNoticeToUserId(Long noticeToUserId) {
		this.noticeToUserId = noticeToUserId;
	}



	public Long getAtmId() {
		return atmId;
	}

	
	public void setAtmId(Long atmId) {
		this.atmId = atmId;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return isReadAll
	 */
	public Boolean getIsReadAll() {
		return isReadAll;
	}

	
	/**
	 * @param isReadAll
	 */
	public void setIsReadAll(Boolean isReadAll) {
		this.isReadAll = isReadAll;
	}



	private Long orgId;
	
	public Long getOrgId() {
		return orgId;
	}

	
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	//操作方法
	private String operMethod = CrudActionSupport.VIEW;
	
	public String getOperMethod() {
		return operMethod;
	}
	
	public void setOperMethod(String operMethod) {
		this.operMethod = operMethod;
	}
	public NoticeManager getNoticeManager() {
		return noticeManager;
	}
	@Autowired
	public void setNoticeManager(NoticeManager noticeManager) {
		this.noticeManager = noticeManager;
	}
	
	public CommonTypeManager getCommonTypeManager() {
		return commonTypeManager;
	}

	@Autowired
	public void setCommonTypeManager(CommonTypeManager commonTypeManager) {
		this.commonTypeManager = commonTypeManager;
	}


	/**
	 * 查询通知列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryList() throws Exception {
			List<PropertyFilter> filters = PropertyFilter
					.buildFromHttpRequest(Struts2Utils.getRequest());
			User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
			if(!Constants.IS_SUPER_USERR.equals(user.getIsSuperAdmin())) {
				PropertyFilter filter = new PropertyFilter(
						"EQL_createUser", String.valueOf(user.getId()));
				filters.add(filter);
			}
			pager = noticeManager.findNoticeList(pager, filters);
	       // pager=noticeManager.findNoticeListByUserId(pager, user.getId());
			Struts2Utils.renderJson(pager);
			return null;
			
	}

	@Override
	public String list() throws Exception {
		return SUCCESS;
	}

	@Override
	public String input() throws Exception {
		return null;
	}
	
	/**
	 * 跳转到新增通知页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public String add() throws Exception{
		operMethod=CrudActionSupport.ADD;
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		orgId=user.getOrganization().getId();
		return VIEW;
	}
	
	/**
	 * 跳转到复制通知页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public String edit() throws Exception {
		operMethod=CrudActionSupport.EDIT;
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		orgId=user.getOrganization().getId();
		return VIEW;
	}

	/**
	 * 保存一条通知
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#save()
	 */
	@Override
	@JsonOutput
	public String save() throws Exception {
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		noticeManager.saveNotice(entity, user.getId());
		formatMessage.setMsg(entity.getId());
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 撤销一条通知
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String repeal() throws Exception{
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		noticeManager.repealNotice(id, user.getId(),status);
		formatMessage.setMsg(id);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 查询一条通知
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryNotice() throws Exception{
		entity=noticeManager.findById(id);
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	
	@JsonOutput
	public String queryNoticeDetail() throws Exception{
		List<NoticeToUser> list=noticeManager.findNoticeDetail(id);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 跳转到查看通知页面
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#view()
	 */
	@Override
	public String view() throws Exception {
		operMethod=CrudActionSupport.VIEW;
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		orgId=user.getOrganization().getId();
		return VIEW;
	}

	/**
	 * 删除通知
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#delete()
	 */
	@Override
	@JsonOutput
	public String delete() throws Exception {
		noticeManager.deleteNotices(ids);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		if (id != null) {
			entity = noticeManager.findById(id);
		} else {
			entity = new Notice();
		}
	}
	
	/**
	 * 更新一条新闻
	 * 
	 * @return
	 * @throws Exception
	 */
		@JsonOutput
		public String updateNotice() throws Exception {
			User user = (User) Struts2Utils
					.getSessionAttribute(Constants.USER_SESSION_ID);
			noticeManager.updateNotice(id,atmId);
			formatMessage.setMsg(true);
			Struts2Utils.renderJson(formatMessage);
			return null;

		}
		
	
	/**
	 * 查找当前用户的所有通知
	 * 
	 * @return
	 * @throws Exception
	 *//*
	@JsonOutput
	public String getCurrentUserNotices() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		List<Notice> list=noticeManager.findAllNoticeToUser(user.getId());
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
		
	}*/
	
	
	/**
	 * 查询通知列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryNoticeToUserList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		/*PropertyFilter filter = new PropertyFilter(
				"EQL_a.userId", String.valueOf(user.getId()));
		filters.add(filter);*/
		pager = noticeManager
				.findAllNoticeToUser(pager,user.getId(),filters);
		Struts2Utils.renderJson(pager);
         return null;
		
	}
	/**
	 * 当前用户未查看的通知数量
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryNoReadNoticeNum()throws Exception{
		User user = (User) Struts2Utils
			.getSessionAttribute(Constants.USER_SESSION_ID);
		Long num= noticeManager.findNoReadNoticeNumber(pager,user.getId());	
		formatMessage.setMsg(num);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 当前用户未查看的所有通知
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryNoReadNotice()throws Exception{
		List<PropertyFilter> filters = PropertyFilter
		.buildFromHttpRequest(Struts2Utils.getRequest());
		User user = (User) Struts2Utils
			.getSessionAttribute(Constants.USER_SESSION_ID);
		pager = noticeManager.findNoReadNotice(pager,filters,user.getId());
		Struts2Utils.renderJson(pager);
        return null;
	}

	@JsonOutput
	public String queryNoReadNoticeList()throws Exception{
		List<PropertyFilter> filters = PropertyFilter
		.buildFromHttpRequest(Struts2Utils.getRequest());
		User user = (User) Struts2Utils
			.getSessionAttribute(Constants.USER_SESSION_ID);
		pager = noticeManager.findNoReadNotice(pager,filters,user.getId());
		List<Notice> list=pager.getResult();
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	/**
	 * 跳转到复制通知页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public String viewNoticeToUser() throws Exception {
		return "query-view";
	}

	
	public String noticeToUserList() throws Exception {
		return "query";
	}
	
	/**
	 * 保存一条NoticeToUser
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#save()
	 */
	@JsonOutput
	public String doRead() throws Exception {
		//NoticeToUser ntu=noticeManager.findNtuById(noticeToUserId);	
    	//noticeManager.saveNoticeToUser(ntu);
		noticeManager.readNotice(noticeToUserId);
    	formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 读通知
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String doReadNotice() throws Exception {
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
    	noticeManager.readNotice(id, user.getId(), isReadAll);
    	formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	

	/**
	 * 修改通知状态为已读
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String updateNoticeState() throws Exception{
		//entity=noticeManager.findById(id);
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		entity = noticeManager.findNoticeByIdAndUserId(id, user.getId());
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	
	/**
	 * 查询分类类型
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryNoticeType() throws Exception{
		List<CommonType> list=commonTypeManager.findNoticeType();
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 增加公用类型表中的通知类型
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
    public String addNoticeType() throws Exception{
    	User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
    	//判断数据库中是否存在该类型,不存在才能添加，存在就提示不能添加重复类型
    	List<CommonType> list=commonTypeManager.findNoticeTypeByName(name);
    	if(list==null || list.isEmpty()){
    		CommonType ct=commonTypeManager.addNoticeType(name, user.getId(),id);
        	formatMessage.setMsg(ct.getId());
    		Struts2Utils.renderJson(formatMessage);
    	}else{
    		formatMessage.setFlag(Messages.FAIL);
    		formatMessage.setMsg("该类型已存在!");
    		Struts2Utils.renderJson(formatMessage);
    	}
    	
		return null;
    }
	
	/**
	 * 删除公用类型表中的通知类型
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String deleteNoticeType()throws Exception{
		    commonTypeManager.deleteCommonType(ids);
			formatMessage.setMsg(true);
			Struts2Utils.renderJson(formatMessage);
			return null;
	}
	
}
