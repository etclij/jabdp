/*
 * @(#)UserConfigAction.java
 * 2014-5-29 下午00:00:27
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.userconfig.web;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.msg.service.SmsManager;
import com.qyxx.platform.sysmng.userconfig.dao.UserConfigDao;
import com.qyxx.platform.sysmng.userconfig.entity.UserConfig;
import com.qyxx.platform.sysmng.userconfig.service.UserConfigManager;
import com.qyxx.platform.sysmng.utils.Constants;

/**
 *  用户信息配置action
 *  @author tsd
 *  @version 1.0 2014-5-29 下午00:00:27
 *  @since jdk1.6 
 */
@Namespace("/sys/userconfig")
@Results({})
public class UserConfigAction extends CrudActionSupport<UserConfig>{

	private static final long serialVersionUID = -1402277195690659504L;

	private UserConfigManager userConfigManager;
	
	/**
	 * @param userConfigManager
	 */
	@Autowired
	public void setUserConfigManager(UserConfigManager userConfigManager) {
		this.userConfigManager = userConfigManager;
	}

	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@JsonOutput
	public String save() throws Exception {
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		userConfigManager.setUser(user);
		userConfigManager.save(entity);
		return null;
	}

	@Override
	@JsonOutput
	public String view() throws Exception {
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		userConfigManager.setUser(user);
		UserConfig userConfig = userConfigManager.get();
		formatMessage.setMsg(userConfig);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		entity = new UserConfig();
	}

}
