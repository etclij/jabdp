/*
 * @(#)DeskTopDao.java
 * 2012-9-5 下午06:15:54
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.desktop.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.sysmng.desktop.entity.Desktop;



/**
 *  
 *  @author ly
 *  @version 1.0 2012-9-5 下午06:15:54
 *  @since jdk1.6 
 */

/**
 * 系统个人桌面Dao类
 */
@Component
public class DesktopDao extends HibernateDao<Desktop, Long>{
	private static final String INSERT_DESKTOP_TO_ROLE = "INSERT INTO SYS_DESKTOP_TO_ROLE(DESKTOP_ID, ROLE_ID) VALUES(?, ?)";
	private static final String DELETE_DESKTOP_ID = "DELETE FROM SYS_DESKTOP_TO_ROLE WHERE DESKTOP_ID = ?";
	private static final String DELETE_DESKTOP_ROLEID = "DELETE FROM SYS_DESKTOP_TO_ROLE WHERE ROLE_ID = ?";
	private static final String DELETE_DESKTOPTOUSER="DELETE FROM SYS_DESKTOP_TO_USER WHERE DESKTOP_ID = ?";
    private static final String DELETE_DESKTOP_BY_PARENTID="DELETE FROM SYS_DESKTOP WHERE A.PARENTID = ?";
	/**
	 * 往关系表中添加关联关系
	 * @param desktopId
	 * @param roleId
	 */
	public void saveDesktopRoles(Long desktopId,Long roleId){
		 getSession().createSQLQuery(INSERT_DESKTOP_TO_ROLE).setLong(0, desktopId).setLong(1,roleId).executeUpdate();
	}
	
	
	/**
	 * 根据DesktopId删除关系表中的记录
	 * @param DesktopId
	 */
	
	public void deleteDesktopRoles(Long id){
		 getSession().createSQLQuery(DELETE_DESKTOP_ID).setLong(0, id).executeUpdate();
		 
	}
	/**
	 * 根据DesktopId删除关系表中的记录
	 * @param RoleId
	 */
	
	public void deleteDesktopId(Long rid){
		 getSession().createSQLQuery(DELETE_DESKTOP_ROLEID).setLong(0, rid).executeUpdate();
		 
	}
	
	
	/**
	 * 删除余系统个人桌面关联的用户个人桌面
	 * @param id
	 */
	public void deleteDesktopToUsers(Long id){
		getSession().createSQLQuery(DELETE_DESKTOPTOUSER).setLong(0, id).executeUpdate();
	}

	
	/**
	 * 根据父id删除系统个人桌面
	 * @param parentId
	 */
	public void deleteDesktopByParentId(Long parentId){
		getSession().createSQLQuery(DELETE_DESKTOP_BY_PARENTID).setLong(0, parentId).executeUpdate();
	}
	/**
	 * 根据sql语句及参数查询指定数量的记录
	 * 
	 * @param sql
	 * @param pageSize
	 * @param param
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> findDataList(String sql, int pageSize, Map<String, Object> param) {
		SQLQuery sq = getSession().createSQLQuery(sql);
		if(null!=param) {
			sq.setProperties(param);
		}
		sq.setFirstResult(0);
		sq.setMaxResults(pageSize);
		sq.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		return sq.list();
	}
}
