/*
 * @(#)IndexAction.java
 * 2011-5-16 下午11:15:06
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.web;

import java.util.List;


import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.ResourceJson;
import com.qyxx.platform.sysmng.accountmng.entity.Role;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.accountmng.service.AuthorityManager;
import com.qyxx.platform.sysmng.accountmng.service.ResourceCacheService;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  首页action
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-16 下午11:15:06
 */
@Namespace("/")
@Results({
		@Result(name = CrudActionSupport.RELOAD, location = "index!authInfo.action", type = "redirect"),
		@Result(name = "authInfo", location = "/WEB-INF/content/licCheck.jsp")
		})
public class IndexAction extends CrudActionSupport<Role> {

	private static final long serialVersionUID = 4138098258119351907L;
	
	private AuthorityManager authorityManager;
	
	private ResourceCacheService resourceCacheService;

	/**
	 * @param authorityManager
	 */
	@Autowired
	public void setAuthorityManager(AuthorityManager authorityManager) {
		this.authorityManager = authorityManager;
	}
	
	@Autowired
	public void setResourceCacheService(
			ResourceCacheService resourceCacheService) {
		this.resourceCacheService = resourceCacheService;
	}

	@Override
	public Role getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {
		return SUCCESS;
	}


	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonOutput
	@Override
	public String view() throws Exception {
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		String locale = (String)Struts2Utils.getSessionAttribute(Constants.LOCALE_SESSION_ID);
		List<ResourceJson> rjList = authorityManager.findMenuResource(user.getId(), locale);
		formatMessage.setMsg(rjList);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 刷新缓存数据
	 * 
	 * @return
	 */
	@JsonOutput
	public String refreshCacheData() {
		resourceCacheService.refreshResource();
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	
	@JsonOutput
	public String findAppMenuResource() throws Exception {
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		String locale = (String)Struts2Utils.getSessionAttribute(Constants.LOCALE_SESSION_ID);
		List<ResourceJson> rjList = authorityManager.findAppMenuResource(user.getId(), locale);
		formatMessage.setMsg(rjList);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	@JsonOutput
	public  String  findAppResource() throws Exception {
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		List<String> rsUrlList = authorityManager.findAppResourceUrl(user.getId());
		formatMessage.setMsg(rsUrlList);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

}
