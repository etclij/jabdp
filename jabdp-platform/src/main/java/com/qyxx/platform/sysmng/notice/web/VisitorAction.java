package com.qyxx.platform.sysmng.notice.web;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.notice.entity.Visitor;
import com.qyxx.platform.sysmng.notice.service.NewsManager;
import com.qyxx.platform.sysmng.utils.Constants;

@Namespace("/sys/visitor")
@Results({})
public class VisitorAction extends CrudActionSupport<Visitor>{

	/**
	 * long
	 */
	private static final long serialVersionUID = 303066786931742380L;

	private NewsManager newsManager;
	
	private Integer size; // 最近size条访问者
	
	@Autowired
	public void setNewsManager(NewsManager newsManager) {
		this.newsManager = newsManager;
	}
	
	public void setSize(Integer size) {
		this.size = size;
	}
	
	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 记录访问者
	 * @return
	 */
	@JsonOutput
	public void addVisitor() {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		newsManager.addVisitor(id, user.getId());
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
	}
	
	/**
	 * 根据关联ID查询近期访问者列表
	 * @return
	 */
	@JsonOutput
	public void queryLateVisitors() {
		List<Map<String, Object>> visitors = newsManager.findVisitorsList(id, size);
		formatMessage.setMsg(visitors);
		Struts2Utils.renderJson(formatMessage);
	}
	
	/**
	 * 根据关联ID查询访问者列表
	 * @return
	 */
	@JsonOutput
	public void queryVisitors() {
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		PropertyFilter filter = new PropertyFilter("EQL_relevanceId",
				String.valueOf(id));
		filters.add(filter);
		pager = newsManager.findVisitorsList(pager, filters);
		Struts2Utils.renderJson(pager);
	}

}
