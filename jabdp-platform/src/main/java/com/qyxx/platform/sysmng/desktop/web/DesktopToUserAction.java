package com.qyxx.platform.sysmng.desktop.web;

import java.util.List;


import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.gsc.utils.SystemParam;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.desktop.entity.Desktop;
import com.qyxx.platform.sysmng.desktop.entity.DesktopToUser;
import com.qyxx.platform.sysmng.desktop.service.DesktopToUserManager;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 * 用户个人桌面管理
 * 
 * @author ly
 * @version 1.0 2012-9-6 上午08:38:38
 * @since jdk1.6
 */
@Namespace("/sys/desktop")
@Results({
		@Result(name = "portal", location = "/WEB-INF/content/sys/portal/portal.jsp")})
public class DesktopToUserAction extends CrudActionSupport<DesktopToUser> {

	private DesktopToUserManager desktopToUserManager;
	private List<DesktopToUser> list;
	private Long parentId;
	private String strType;
	private String layoutVal;
	private SystemParam systemParam;
	private Long desktopId;
	//操作方法
	private String operMethod = CrudActionSupport.VIEW;
	
	
	
	
	public Long getDesktopId() {
		return desktopId;
	}

	
	public void setDesktopId(Long desktopId) {
		this.desktopId = desktopId;
	}

	public SystemParam getSystemParam() {
		return systemParam;
	}

	@Autowired
	public void setSystemParam(SystemParam systemParam) {
		this.systemParam = systemParam;
	}

	public String getLayoutVal() {
		return layoutVal;
	}

	public void setLayoutVal(String layoutVal) {
		this.layoutVal = layoutVal;
	}

	
	public String getStrType() {
		return strType;
	}

	public void setStrType(String strType) {
		this.strType = strType;
	}

	
	public String getOperMethod() {
		return operMethod;
	}

	public void setOperMethod(String operMethod) {
		this.operMethod = operMethod;
	}

	public Long getParentId() {
		return parentId;
	}
	
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public List<DesktopToUser> getList() {
		return list;
	}

	public void setList(List<DesktopToUser> list) {
		this.list = list;
	}

	@Autowired
	public void setDesktopToUserManager(
			DesktopToUserManager desktopToUserManager) {
		this.desktopToUserManager = desktopToUserManager;
	}
	

	
	/**
	 * 根据id查找detktopToUser
	 * @return
	 */
	@JsonOutput
	public String queryDesktopToUserById(){
		entity=desktopToUserManager.findDtuById(id);
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	/**
	 * 查询用户个人桌面（LEVEL=1）
	 * 
	 * @return
	 * @throws Exception
	 */

	public String queryLevel1DesktopToUser()throws Exception{
		User user = (User) Struts2Utils
		.getSessionAttribute(Constants.USER_SESSION_ID);
		//查询用户个人桌面中LEVEL=1的集合 
		list=desktopToUserManager.findLevel1DesktopToUser(user.getId(),null);
		return "portal";	
	}
	
	/**
	 * 根据桌面ID查询用户个人桌面（LEVEL=1）
	 * 
	 * @return
	 * @throws Exception
	 */

	public String refreshByDesktopId()throws Exception{
		User user = (User) Struts2Utils
		.getSessionAttribute(Constants.USER_SESSION_ID);
		//根据指定桌面ID查询用户个人桌面中LEVEL=1的集合 
		list =desktopToUserManager.findLevel1DesktopToUser(user.getId(),desktopId);
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);	
		return null;	
	}
	
	/**
	 * 查询当前用户下所有Level=1的桌面
	 * @return
	 * @throws Exception
	 */
	  @JsonOutput
		public String queryLevel1Desktop() throws Exception{
			User user = (User) Struts2Utils
					.getSessionAttribute(Constants.USER_SESSION_ID);
			List<Desktop> dLevel1 = desktopToUserManager
					.findLevel1DesktopByUser(user);
			formatMessage.setMsg(dLevel1);
			Struts2Utils.renderJson(formatMessage);
			return null;
		}
	/**
	 * 查询当前Level=1下所有的Level=2
	 * @return
	 * @throws Exception
	 */
  @JsonOutput
	public String queryLevel2DesktopToUser() throws Exception{
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		List<DesktopToUser> dtuLevel2 = desktopToUserManager
				.findLevel2DesktopToUserByParentId(parentId, user.getId());
		formatMessage.setMsg(dtuLevel2);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
  
  /**
   * 查询系统个人桌面中每一个LEVEL1下的LEVEL2
   * @return
   * @throws Exception
   */
	public String queryLevel2Desktop() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		List<Desktop> list = desktopToUserManager.findLevelDesktopByParentId(parentId,user.getId());
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	
	/**
	 * 根据desktopId查找desktopToUser对象
	 * @return
	 * @throws Exception
	 */
	public String queryDesktopToUserByDesktopId() throws Exception{
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		entity=desktopToUserManager.findDesktopToUser(parentId, user.getId()).get(0);
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
		
	}
	
	/**
	 * 修改系统个人桌面布局
	 * @return
	 * @throws Exception
	 */
	public String updateLayout() throws Exception{
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
	    entity=desktopToUserManager.updateDesktopLayout(parentId, user.getId(), strType);
	    formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 修改列宽
	 * @return
	 * @throws Exception
	 */
	public String updateColsHeight() throws Exception{
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
	    DesktopToUser dtu=desktopToUserManager.updateCols(parentId,user.getId(),strType);
	    formatMessage.setMsg(dtu);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 修改desktopToUser的实际布局
	 * @return
	 * @throws Exception
	 */
	public String updateDesktopLayoutVal() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		desktopToUserManager.saveDesktopLayout(id,user.getId(), layoutVal);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	
	public void prepareUpdateDesktopDisplayNum() throws Exception{
		 entity=new DesktopToUser();
	}
	
	@JsonOutput
	public String updateDesktopDisplayNum() throws Exception{
		desktopToUserManager.doUpdate(entity);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 添加用户自定义个人桌面
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String addPortal() throws Exception{
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		entity=desktopToUserManager.addDesktopToPortal(parentId,ids,user.getId());
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
		
	}
	/**
	 * 添加用户自定义tab页
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String addTabs() throws Exception{
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		list = desktopToUserManager.addDesktopToTabs(ids,user.getId());
		formatMessage.setMsg(list);
		Struts2Utils.renderJson(formatMessage);
		return null;
		
	}
	/**
	 * 用户个人桌面列表
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		PropertyFilter filter = new PropertyFilter("EQL_createUser",
				String.valueOf(user.getId()));
		pager = desktopToUserManager.findDeskTopToUserList(pager, filters);

		Struts2Utils.renderJson(pager);
		return null;
	}

	@Override
	public String list() throws Exception {
		return SUCCESS;
	}
	


	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 保存一条记录
	 */
	@Override
	@JsonOutput
	public String save() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		desktopToUserManager.saveDeskTopToUser(entity,user.getId());
		formatMessage.setMsg(entity.getId());
		Struts2Utils.renderJson(formatMessage);
		// TODO Auto-generated method stub
		return null;
	}


	
	/**
	 * 跳转到查看页面
	 */
	@Override
	public String view() throws Exception {
		
		return VIEW;
	}

	
	@Override
	@JsonOutput
	public String delete() throws Exception {
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		desktopToUserManager.deleteDeskTopToUsers(id,user.getId());
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		if (id != null) {
			entity = desktopToUserManager.findDesktopToUserById(id);
		} else {
			entity = new DesktopToUser();
		}
	}

	/**
	 * 将desktop中的类容赋值给desktopToUser并返回该对象
	 * @return
	 */
	@JsonOutput
	public String queryDesktopSource(){
		User user = (User) Struts2Utils
				.getSessionAttribute(Constants.USER_SESSION_ID);
		entity=desktopToUserManager.queryDesktopSource(id, user);
		entity.setSystemPath(systemParam.getVirtualPicPath());
		formatMessage.setMsg(entity);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	
	

}
