/*
 * @(#)Theme.java
 * 2011-5-29 下午03:49:50
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * 系统主题样式表
 * 
 * @author YB
 * @version 1.0
 * @since 1.6 2011-5-29 下午03:49:50
 */
@Entity
//表名与类名不相同时重新定义表名.
@Table(name = "SYS_THEME")
//默认的缓存策略.
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Theme implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String themeName;
	private String themeCssPath;
	private String themeImage;
	private String themeStatus;
	private String themeRemark;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_SYS_THEME_ID")
	@SequenceGenerator(name="SEQ_SYS_THEME_ID", sequenceName="SEQ_SYS_THEME_ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public String getThemeCssPath() {
		return themeCssPath;
	}

	public void setThemeCssPath(String themeCssPath) {
		this.themeCssPath = themeCssPath;
	}

	public String getThemeImage() {
		return themeImage;
	}

	public void setThemeImage(String themeImage) {
		this.themeImage = themeImage;
	}

	public String getThemeStatus() {
		return themeStatus;
	}

	public void setThemeStatus(String themeStatus) {
		this.themeStatus = themeStatus;
	}

	public String getThemeRemark() {
		return themeRemark;
	}

	public void setThemeRemark(String themeRemark) {
		this.themeRemark = themeRemark;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
