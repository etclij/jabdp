package com.qyxx.platform.sysmng.memo.web;

import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.memo.entity.Memo;
import com.qyxx.platform.sysmng.memo.entity.MemoToUser;
import com.qyxx.platform.sysmng.memo.service.MemoManager;
import com.qyxx.platform.sysmng.utils.Constants;

@Namespace("/sys/memo")
@Results({})
public class MemoAction extends CrudActionSupport<Memo> {
	
	private static final long serialVersionUID = -1818069671709449025L;

	private MemoManager memoManager;
	
	private Long memoId;
	private Long[] memoIds;
	private String memoStatus;
	private Long mtuId;
	
	public void setMtuId(Long mtuId) {
		this.mtuId = mtuId;
	}

	public void setMemoStatus(String memoStatus) {
		this.memoStatus = memoStatus;
	}

	public void setMemoId(Long memoId) {
		this.memoId = memoId;
	}
	
	public void setMemoIds(Long[] memoIds) {
		this.memoIds = memoIds;
	}

	@Autowired
	public void setMemoManager(MemoManager memoManager) {
		this.memoManager = memoManager;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 查询备忘录列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String queryMemoToUserList() throws Exception {
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		pager = memoManager.findAllMemoToUser(pager,user.getId(),filters);
		Struts2Utils.renderJson(pager);
		return null;
	}
	
	/**
	 * 管理备忘录列表
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public void queryList() throws Exception {
		// TODO Auto-generated method stub
		List<PropertyFilter> filters = PropertyFilter
				.buildFromHttpRequest(Struts2Utils.getRequest());
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		if(!Constants.IS_SUPER_USERR.equals(user.getIsSuperAdmin())) {
			PropertyFilter filter = new PropertyFilter(
					"EQL_createUser", String.valueOf(user.getId()));
			filters.add(filter);
		}
		pager = memoManager.findMemoList(pager, filters);
		Struts2Utils.renderJson(pager);
	}
	
	@JsonOutput
	public void queryView() throws Exception {
		Memo memo = memoManager.findMemoByMtuId(mtuId);
		formatMessage.setMsg(memo);
		Struts2Utils.renderJson(formatMessage);
	}
	
	//修改备忘录提醒状态为过期提醒
	@JsonOutput
	public void doRead() {
		MemoToUser memoToUser = memoManager.updataMemoToUserStatusAfterRead(mtuId);
		formatMessage.setMsg(memoToUser);
		Struts2Utils.renderJson(formatMessage);
	}
	
	//查询用户被共享的所有到期备忘录
	@JsonOutput
	public void queryExpireMemo() {
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		Struts2Utils.renderJson(memoManager.findAllExpireMemoByUserId(user.getId()));
	}
	
	/**
	 * 修改备忘录状态
	 * 
	 * @return
	 * @throws Exception
	 */
	@JsonOutput
	public String updateStatus() throws Exception{
		User user = (User) Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		Long id = memoManager.updateStatus(memoId, memoStatus, user.getId());
		formatMessage.setMsg(id);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;
	}
	
	@Override
	@JsonOutput
	public String save() throws Exception {
		// TODO Auto-generated method stub
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		memoManager.saveMemo(entity, user.getId());
		formatMessage.setMsg(entity.getId());
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	@JsonOutput
	public String view() throws Exception {
		// TODO Auto-generated method stub
		Memo memo = memoManager.findById(memoId);
		formatMessage.setMsg(memo);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	@JsonOutput
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		memoManager.deleteMemos(memoIds);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		if (id != null) {
			entity = memoManager.findById(id);
		} else {
			entity = new Memo();
		}
	}
	
}
