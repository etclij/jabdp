/*
 * @(#)GsException.java
 * 2011-5-28 下午09:59:13
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.exception;

import com.qyxx.platform.common.module.web.Messages;


/**
 *  Gs异常
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-5-28 下午09:59:13
 */

public class GsException extends RuntimeException {

	private static final long serialVersionUID = 8578915982385558057L;
	
	private String flag = Messages.ERROR;
	
	public GsException(String msg) {
		super(msg);
	}
	
	public GsException(String msg, Throwable t) {
		super(msg, t);
	}

	public GsException(Throwable t) {
		super(t);
	}
	
	/**
	 * @return flag
	 */
	public String getFlag() {
		return flag;
	}

	
	/**
	 * @param flag
	 */
	public void setFlag(String flag) {
		this.flag = flag;
	}

}
