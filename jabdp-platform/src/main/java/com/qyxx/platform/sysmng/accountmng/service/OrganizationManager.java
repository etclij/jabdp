/*
 * @(#)OrganizationManager.java
 * 2011-5-22 下午04:57:19
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.sysmng.accountmng.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qyxx.platform.common.orm.PropertyFilter;
import com.qyxx.platform.sysmng.accountmng.dao.OrganizationDao;
import com.qyxx.platform.sysmng.accountmng.entity.Organization;
import com.qyxx.platform.sysmng.accountmng.entity.OrganizationJson;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.notice.dao.NewsDao;
import com.qyxx.platform.sysmng.notice.entity.News;

/**
 * 组织管理
 * 
 * @author YB
 * @version 1.0
 * @since 1.6 2011-5-22 下午04:57:19
 */
@Component
@Transactional
public class OrganizationManager {

	private static final String FIND_MAX_ORGCODE_HQL = "select max(organizationCode)"
			+ " from Organization a where a.organization.id = ?";
	private static final String FIND_ORGCODE_HQL = "select organizationCode"
			+ " from Organization a where a.id = ?";
	private OrganizationDao organizationDao;
	
	protected User user;
	
	private NewsDao newsDao;
   
	public void setUser(User user) {
		this.user = user;
	}   
    @Autowired
    public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}
	@Autowired
	public void setOrganizationDao(OrganizationDao organizationDao) {
		this.organizationDao = organizationDao;
	}

	/**
	 * 保存用户组织
	 * 
	 * @param entity
	 */
	public void saveOrganization(Organization entity, Long parentId) {
		Organization parentOrg = null;
		String pcode = null;
		if (parentId != null) {
			parentOrg = getOrganizationById(parentId);
			pcode = parentOrg.getOrganizationCode();
		}
		if (entity.getId() == null) {//新增
			/*
			 * String maxCode = organizationDao.findUnique(
			 * FIND_MAX_ORGCODE_HQL, id); if
			 * (StringUtils.isNotBlank(maxCode)) { //
			 * 找到被点击节点的所有子节点中最大的orgnazationCode String endThreeCode =
			 * maxCode.substring( maxCode.length() - 3, maxCode.length());
			 * String startCode = maxCode.substring(0, maxCode.length() -
			 * 3); Integer endCode = Integer.parseInt(endThreeCode); String
			 * organizationCode = ""; if (endCode < 9) { organizationCode =
			 * startCode + "00" + (Integer.parseInt(endThreeCode) + 1); }
			 * else if (endCode >= 9 && endCode < 99) { organizationCode =
			 * startCode + "0" + (Integer.parseInt(endThreeCode) + 1); }
			 * else if (endCode >= 99 && endCode < 999) { organizationCode =
			 * startCode + (Integer.parseInt(endThreeCode) + 1); }
			 * entity.setOrganizationCode(organizationCode);
			 * entity.setParentOrganizationCode(startCode); } else { //
			 * 没有子节点，找到该节点的orgnazationCode String code =
			 * organizationDao.findUnique( FIND_ORGCODE_HQL, id);
			 * entity.setOrganizationCode(code + "001");
			 * entity.setParentOrganizationCode(code); }
			 */
			String code = organizationDao.getAutoTreeCode(
					"Organization", "organizationCode", Organization.CODE_BIT_LEN,
					"organization.id", parentId, "id");
			entity.setOrganizationCode(code);
			entity.setParentOrganizationCode(pcode);
			entity.setCreateUser(user.getId());
			entity.setCreateTime(new Date());
		} else {//修改
			if (!StringUtils.equals(pcode, 
					entity.getParentOrganizationCode())) {// 父级组织编码不一致，需要重新生成编码
				String code = organizationDao.getAutoTreeCode(
						"Organization", "organizationCode", Organization.CODE_BIT_LEN,
						"organization.id", parentId, "id");
				entity.setOrganizationCode(code);
				entity.setParentOrganizationCode(pcode);
			}
			entity.setLastUpdateUser(user.getId());
			entity.setLastUpdateTime(new Date());
		}
		entity.setOrganization(parentOrg);
		organizationDao.save(entity);
	}

	/**
	 * 删除用户组织
	 * 
	 * @param id
	 */
	public void removeOrganization(Long id) {
		organizationDao.delete(id);
	}

	/**
	 * 获得用户组织实体
	 * 
	 * @param id
	 */
	@Transactional(readOnly = true)
	public Organization getOrganizationById(Long id) {
		Organization organization = organizationDao.get(id);
		return organization;
	}

	/**
	 * 获得用户组织实体列表
	 * 
	 * @param id
	 */
	public List<Organization> getOrganizationList(
			List<PropertyFilter> filters) {
		List<Organization> list = organizationDao.find(filters);
		return list;
	}

	/**
	 * 获得OrganizationJson组织实体列表
	  * @param id 其他关系对象ID
	 */
	public List<OrganizationJson> getOrganizationJsonList(Long id) {
		List<Organization> list = organizationDao.getAll();
		List<OrganizationJson> listJson = new ArrayList<OrganizationJson>();

		for (int j = 0, i = list.size(); j < i; j++) {
			Organization o = list.get(j);
			Long oid = o.getId();
			OrganizationJson oj = new OrganizationJson(oid, o);
			listJson.add(oj);// 将组织添加到集合中
		}
		
		if (id != null) {
			News news = newsDao.get(id);
			List<Organization> selectList = news.getOrganizationList();
			for (int i = 0; i < selectList.size(); i++) {
				Long b  = selectList.get(i).getId();
				for (int k = 0; k < listJson.size(); k++) {
					if (b == listJson.get(k).getId()) {
						listJson.get(k).setChecked(true);
					}
				}
			}
			return listJson;
		}
		return listJson;
	}
	
	/**
	 * 根据组织编码查询子组织
	 * 
	 * @param orgCode
	 * @return
	 */
	public List<OrganizationJson> getOrgChildList(String orgCode) {
		List<Organization> list = organizationDao.findOrgList(orgCode);
		List<OrganizationJson> listJson = new ArrayList<OrganizationJson>();

		for (int j = 0, len = list.size(); j < len; j++) {
			Organization o = list.get(j);
			Long oid = o.getId();
			OrganizationJson oj = new OrganizationJson(oid, o);
			listJson.add(oj);// 将组织添加到集合中
		}
		return listJson;
	}


}
