package com.qyxx.platform.gsmng.common.dao;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.qyxx.platform.common.orm.hibernate.DynamicHibernateDao;
import com.qyxx.platform.common.orm.hibernate.HibernateDao;
import com.qyxx.platform.gsmng.common.entity.TaskInstance;

/**
 * 流程实例管理
 */
@Component
public class TaskInstanceDao extends HibernateDao<TaskInstance, Long> {

 
}
