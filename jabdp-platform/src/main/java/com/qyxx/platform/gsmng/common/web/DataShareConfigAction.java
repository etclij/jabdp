/*
 * @(#)DataShareConfigAction.java
 * 2015-3-11 下午02:59:29
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsmng.common.web;

import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.gsmng.common.entity.DataShareConfig;
import com.qyxx.platform.gsmng.common.service.DataShareConfigManager;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  数据共享设置
 *  @author gxj
 *  @version 1.0 2015-3-11 下午02:59:29
 *  @since jdk1.6 
 */
@Namespace("/gs")
@Results({})
public class DataShareConfigAction extends CrudActionSupport<DataShareConfig>{

	/**
	 * long
	 */
	private static final long serialVersionUID = -5810404010162341892L;
	
	private DataShareConfigManager dataShareConfigManager;
	
	private String entityName;
	
	private String[] entityIds;
	
	private String entityId;
	
	private String log;

	/**
	 * @param dataShareConfigManager
	 */
	@Autowired
	public void setDataShareConfigManager(
			DataShareConfigManager dataShareConfigManager) {
		this.dataShareConfigManager = dataShareConfigManager;
	}

	/**
	 * @return entityName
	 */
	public String getEntityName() {
		return entityName;
	}

	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * @return entityIds
	 */
	public String[] getEntityIds() {
		return entityIds;
	}

	/**
	 * @param entityIds
	 */
	public void setEntityIds(String[] entityIds) {
		this.entityIds = entityIds;
	}
	
	/**
	 * @return entityId
	 */
	public String getEntityId() {
		return entityId;
	}

	
	/**
	 * @param entityId
	 */
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	/**
	 * @return log
	 */
	public String getLog() {
		return log;
	}

	
	/**
	 * @param log
	 */
	public void setLog(String log) {
		this.log = log;
	}

	@Override
	public String list() throws Exception {
		
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 保存数据共享配置
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#save()
	 */
	@JsonOutput
	@Override
	public String save() throws Exception {
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		dataShareConfigManager.saveDataShareConfig(entityName, user.getId(), entityIds, ids, log);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	/**
	 * 查找已设置共享用户
	 * @see com.qyxx.platform.common.module.web.CrudActionSupport#view()
	 */
	@JsonOutput
	@Override
	public String view() throws Exception {
		List<Long> shareUserIdList = dataShareConfigManager.find(entityName, entityId);
		formatMessage.setMsg(shareUserIdList);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
