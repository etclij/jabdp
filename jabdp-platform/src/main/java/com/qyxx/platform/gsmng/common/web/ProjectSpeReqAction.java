/*
 * @(#)ProjectSpeReqAction.java
 * 2014-4-24 上午09:23:23
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsmng.common.web;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.qyxx.platform.common.json.JsonOutput;
import com.qyxx.platform.common.module.web.CrudActionSupport;
import com.qyxx.platform.common.utils.encode.JsonBinder;
import com.qyxx.platform.common.utils.web.struts2.Struts2Utils;
import com.qyxx.platform.gsmng.common.service.ProjectSpeReqManager;
import com.qyxx.platform.sysmng.accountmng.entity.User;
import com.qyxx.platform.sysmng.utils.Constants;


/**
 *  项目定制化需求action
 *  @author gxj
 *  @version 1.0 2014-4-24 上午09:23:23
 *  @since jdk1.6 
 */
@Namespace("/gs")
@Results({})
public class ProjectSpeReqAction extends CrudActionSupport<Map<String, Object>>{

	/**
	 * long
	 */
	private static final long serialVersionUID = 724050260310961647L;
	
	private ProjectSpeReqManager projectSpeReqManager;
	
	/* 实体名（模块名.实体对象名） */
	private String entityName;
	
	/* Json格式数据 */
	private String jsonSaveData;
	
	private JsonBinder jsonBinder = JsonBinder.getAlwaysMapper();
	
	/**
	 * @param projectSpeReqManager
	 */
	@Autowired
	public void setProjectSpeReqManager(
			ProjectSpeReqManager projectSpeReqManager) {
		this.projectSpeReqManager = projectSpeReqManager;
	}

	/**
	 * @param entityName
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * @param jsonSaveData
	 */
	public void setJsonSaveData(String jsonSaveData) {
		this.jsonSaveData = jsonSaveData;
	}

	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String input() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String view() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 科百特项目生成产品方法
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@JsonOutput
	public String genProductForCbt() throws Exception {
		User user = (User)Struts2Utils.getSessionAttribute(Constants.USER_SESSION_ID);
		projectSpeReqManager.setUser(user);
		projectSpeReqManager.setEntityName(entityName);
		Map<String, Object> map = jsonBinder.fromJson(jsonSaveData, HashMap.class);
		projectSpeReqManager.genProductForCbtSilent(map);
		formatMessage.setMsg(true);
		Struts2Utils.renderJson(formatMessage);
		return null;
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
