/*
 * @(#)ProcessInfo.java
 * 2012-9-13 上午11:01:38
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.gsmng.common.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 *  流程流转模块信息
 *  
 *  @author xk
 *  @version 1.0 2013-06-18 下午15:26:38
 *  @since jdk1.6
 */
@Entity
@Table(name = "SYS_PROCESS_MODEL_INFO")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ProcessModelInfo implements Serializable {

	/**
	 * long
	 */
	private static final long serialVersionUID = -1010247935451714904L;
	private Long id;
	private Long processModelId;// 启动流程单据的ID
	private Long masterId;// 生成单据的主表ID	
	private Long createUser;//单据创建者登入名
	private String modelKey;//生成 单据对应的模块key
	private String processInstanceId;//流程实例ID
	private String businessKey;//流程的业务key
	private String  executionId;//对应的活动ID
	private Date createTime;//创建时间
	 
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SYS_PROCESS_MODEL_INFO_ID")
	@SequenceGenerator(name = "SEQ_SYS_PROCESS_MODEL_INFO_ID", sequenceName = "SEQ_SYS_PROCESS_MODEL_INFO_ID")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
		
	@Column(name="PROCESS_MODEL_ID")
	public Long getProcessModelId() {
		return processModelId;
	}

	
	public void setProcessModelId(Long processModelId) {
		this.processModelId = processModelId;
	}

	@Column(name="CREATE_USER")
	public Long getCreateUser() {
		return createUser;
	}

	
	public void setCreateUser(Long createUser) {
		this.createUser = createUser;
	}

	@Column(name="MODEL_KEY", length=300)
	public String getModelKey() {
		return modelKey;
	}

	
	public void setModelKey(String modelKey) {
		this.modelKey = modelKey;
	}

	@Column(name="MASTER_ID")
	public Long getMasterId() {
		return masterId;
	}

	
	public void setMasterId(Long masterId) {
		this.masterId = masterId;
	}

	@Column(name="BUSINESS_KEY")
	public String getBusinessKey() {
		return businessKey;
	}

	
	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	@Column(name="PROCESS_INSTANCE_ID", length=300)
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	
	@Column(name="EXECUTION_ID", length=300)
	public String getExecutionId() {
		return executionId;
	}

	
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATE_TIME")
	public Date getCreateTime() {
		return createTime;
	}

	
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


}
