/*
 * @(#)NewWritableNumberArrayMethod.java
 * 2012-7-5 上午10:59:14
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.freemarker;

import java.util.List;

import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateNumberModel;


/**
 *  定义可修改数字序列方法
 *  @author gxj
 *  @version 1.0 2012-7-5 上午10:59:14
 *  @since jdk1.6 
 */

public class NewWritableNumberArrayMethod implements TemplateMethodModelEx {

	@SuppressWarnings("rawtypes")
	@Override
	public Object exec(List arguments) throws TemplateModelException {
		if (arguments.size() == 1) {
			Object arg = arguments.get(0);
			if(arg instanceof TemplateNumberModel) {
				int size = ((TemplateNumberModel) arg).getAsNumber().intValue();
				return new NumberArraySequence(size);
			} else {
				throw new TemplateModelException(
                        "The argument to newWritableNumberArray(size) must be "
                        + "a numberical value.");
			}
        } else {
            throw new TemplateModelException(
                    "The newWritableNumberArray method needs 1 argument, not "
                    + arguments.size() + ".");
        }
	}

}
