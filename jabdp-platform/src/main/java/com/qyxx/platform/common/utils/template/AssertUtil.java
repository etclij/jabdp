/*
 * @(#)的.java
 * 2011-8-24 下午04:45:05
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.common.utils.template;

import java.util.Collection;
import java.util.Map;

/**
 *  
 *  @author YB
 *  @version 1.0
 *  @since 1.6 2011-8-24 下午04:45:05
 */

public class AssertUtil {
    
    public static boolean notEmpty(Object[] array) {
        return (array != null) && (array.length > 0);
    }
    
    public static boolean notEmpty(Collection<?> collection) {
        return (collection != null) && (!collection.isEmpty());
    }
    
    public static boolean notEmpty(Map<?,?> map) {
        return (map != null) && (!map.isEmpty());
    }
    
    public static void main(String args[]){
        
    }

}



