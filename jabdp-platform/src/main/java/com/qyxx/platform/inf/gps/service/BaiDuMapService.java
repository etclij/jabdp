/*
 * @(#)BaiDuMapService.java
 * 2017年12月29日 下午4:26:38
 * 
 *
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.platform.inf.gps.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Maps;
import com.qyxx.platform.common.utils.encode.JsonBinder;
import com.qyxx.platform.common.utils.httpclient.HttpClientUtils;


/**
 *  百度地图接口服务
 *  @author gxj
 *  @version 1.0 2017年12月29日 下午4:26:38
 *  @since jdk1.6 
 */

public class BaiDuMapService {
	//访问应用秘钥
	public static final String AK = "fo4dLDbjoRkaFTaNMCfQjmQsVs44lUKs";
	//百度地图API地址
	public static final String API_URL = "http://api.map.baidu.com/geocoder/v2/";
	
	private static JsonBinder jsonBinder = JsonBinder.getAlwaysMapper();
	
	/**
	 * 根据地址查询对应的经纬度
	 * 
	 * @param address
	 * @param coordtype--坐标系  gcj02ll（国测局坐标）、bd09mc（百度墨卡托坐标）、bd09ll（百度经纬度坐标）
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getGpsByAddress(String address, String coordtype) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("ak", AK);
		params.put("output", "json");
		params.put("ret_coordtype", StringUtils.defaultIfEmpty(coordtype, "gcj02ll"));
		params.put("address", address);
		String result = HttpClientUtils.get(API_URL, params);
		//System.out.println(result);
		Map<String, Object> map = jsonBinder.fromJson(result, HashMap.class);
		return map;
	}
	
	/**
	 * 根据地址解析成经度纬度
	 * 
	 * @param address
	 * @param coordtype
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, String> getGpsByAddressResult(String address, String coordtype) {
		Map<String, Object> result = getGpsByAddress(address, coordtype);
		Map<String, String> map = Maps.newHashMap();
		String status = String.valueOf(result.get("status"));
		map.put("status", status);
		if("0".equals(status)) {
			Map<String, Object> rs = (Map)result.get("result");
			Map<String, Object> location = (Map)rs.get("location");
			map.put("lat", String.valueOf(location.get("lat")));
			map.put("lng", String.valueOf(location.get("lng")));
		}
		return map;
	}


	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(getGpsByAddressResult("杭州科百特过滤器材有限公司", null));
	}

}
