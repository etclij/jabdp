package com.qyxx.platform.gsc.utils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by sy on 2019/8/26.
 */
public class CallBat {
    public static void  callCmd(String locationCmd){
        try {
            Process child = Runtime.getRuntime().exec(locationCmd);
            InputStream in = child.getInputStream();
            int c;
            while ((c = in.read()) != -1) {
                System.out.print((char)c);
            }
            in.close();
            try {
                child.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("zip  success!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
