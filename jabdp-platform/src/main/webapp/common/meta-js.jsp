<%@ include file="/common/meta-html5-v2.jsp"%>
<!-- jquery js -->
<script src="${ctx}/js/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery/jquery-migrate-1.4.1.min.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/js/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="${ctx}/js/bootstrap/js/bootstrap3-typeahead.js" type="text/javascript"></script>
<script src="${ctx}/js/form-validator/jquery.form-validator.min.js" type="text/javascript"></script>
<c:if test="${locale != 'en'}">
<script src="${ctx}/js/form-validator/lang/${locale}.js" type="text/javascript"></script>
</c:if>
<script src="${ctx}/js/form-validator/es-validate.js" type="text/javascript"></script>
<script src="${ctx}/js/layer/layer.js" type="text/javascript"></script>
<%-- 
<!-- easyui -->
<script type="text/javascript" src="${ctx}/js/easyui-1.4/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.4/locale/easyui-lang-${locale}.js"></script>

<script type="text/javascript" src="${ctx}/js/easyui-1.4/easyui.extend.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.4/datagrid-detailview.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.4/jquery.edatagrid.js"></script>

<!--common i18n -->
<script type="text/javascript" src="${ctx}/js/common/scripts/locale/data-${locale}.js"></script>
<!-- jquery form -->
<script type="text/javascript" src="${ctx}/js/form/scripts/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/js/form/scripts/json2form.js"></script>
<!-- jquery loadmask -->
<script type="text/javascript" src="${ctx}/js/loadmask/jquery.loadmask.min.js"></script>
<!-- common.js -->
<script type="text/javascript" src="${ctx}/js/common/scripts/common.js?v20140808"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/json2.js?v20140808"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/jwpf.js?v20140720"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/focus_tips.js"></script>

<!-- ztree -->
<script src="${ctx}/js/ztree/scripts/jquery.ztree.all-3.2.min.js" type="text/javascript"></script>

<!-- datepicker -->
<script type="text/javascript" src="${ctx}/js/datepicker/${locale}_WdatePicker.js"></script>

<!-- theme.js -->
<script type="text/javascript" src="${ctx}/js/common/scripts/theme.js"></script>
<!-- theme.js -->
<script type="text/javascript" src="${ctx}/js/common/scripts/gscommon.js"></script>
<script type="text/javascript" src="${ctx}/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript"
        src="${ctx}/js/ckeditor/adapters/jquery.js"></script>
        
<!-- hotkey -->
<script type="text/javascript" src="${ctx}/js/hotkey/jquery.hotkeys.js"></script>    
<!-- imgpreview -->
<script type="text/javascript" src="${ctx}/js/imgpreview/scripts/jquery.gzoom.js"></script>
<script type="text/javascript" src="${ctx}/js/imgpreview/scripts/ui.core.min.js"></script>
<script type="text/javascript" src="${ctx}/js/imgpreview/scripts/ui.slider.min.js"></script>
<!-- uploadify -->
<script type="text/javascript"
	src="${ctx}/js/uploadify/jquery.uploadify-3.1.min.js"></script>
<!-- XDate -->
<script type="text/javascript" src="${ctx}/js/common/scripts/xdate.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/accounting.js"></script>
<!-- qtip2 -->
<script type="text/javascript" src="${ctx}/js/qtip2/jquery.qtip.min.js"></script>
<script type="text/javascript" src="${ctx}/js/qtip2/imagesloaded.pkgd.min.js"></script>
<!-- ZeroClipboard -->
<script src="${ctx}/js/zeroclipboard/ZeroClipboard.min.js"></script>
<script type="text/javascript" src="${ctx}/js/webuploader/webuploader.min.js"></script>
<script type="text/javascript" src="${ctx}/js/webuploader/es.filebox.js"></script>
<!-- toastr -->
<script src="${ctx}/js/toastr/toastr.min.js"></script>--%>

<script type="text/javascript" src="${ctx}/js/datepicker/${locale}_WdatePicker.js"></script>
<script type="text/javascript" src="${ctx}/js/dist/${jsVersion}/app-v2.min.js"></script>
<script type="text/javascript" src="${ctx}/js/easyui-1.4/locale/easyui-lang-${locale}.js"></script>
<!--common i18n -->
<script type="text/javascript" src="${ctx}/js/common/scripts/locale/data-${locale}.js"></script>
<script type="text/javascript" src="${ctx}/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="${ctx}/js/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="${ctx}/js/dist/${jsVersion}/app.min.js"></script>
<c:if test="${sessionScope.THEME_VERSION != 'bootstrap'}">  
<script>
$(document).ready(function() {
	var c = readCookie('style');
	if(c) switchStylestyle(c);
});
</script>
</c:if>
