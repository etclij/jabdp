$.jwpf = {
	system : {
		alertinfo : {
			errorTitle : "錯誤",
			errorInfo : "對不起,出現錯誤!",
			titlet : "消息框",
			info : "請稍等...",
			titleInfo : "提示信息",
			file : "文件",
			uploadFailed : "上傳失敗:",
			tryAgain : "請稍後重試！",
			serverException : "服務器出現異常，請稍後重試",
			failedInfo : "上傳失敗，錯誤信息如下",
			set : "請選擇",
			addType : "--增加類型--",
			confirmTitle:"確認信息",
			operDialog:"操作對話框",
			expTitle:"異常提示：",
			loadDataError:"加載數據出現異常：",
			getDataError:"獲取數據出現異常：",
			currPage:"當頁：",
			selPage:"選中："
		},
		attach : {
			attachList : "附件列表"
		},
		process : {
			processStartWin : "流程啓動窗口",
			startSuccess : "流程啓動成功"
		},
		button : {
			expand : "全部展開",
			contract : "全部收縮",
			deleteImage:"刪除圖片",
			add :"新增",
			edit:"編輯",
			view:"查看",
			save:"保存",
			saveNew:"保存並新增",
			close:"關閉",
			quickAddEdit:"快速新增或修改",
			quickSelect:"快速選擇",
			ok:"確定",
			cancel:"取消"
		},
		confirm:{
			selectOnlyOneRow:"請選擇且只能選擇1條數據！",
			sureDeleteSelRow:"確認要刪除選中的數據？",
			selectRow:"請先選擇數據！",
			nodata:"暫無數據",
			exportAllData:"您沒有選擇數據，確認要導出全部數據嗎？",
			selectInsertRow:"請選擇插入行的位置"
		},
		module : {
			status : {
				"00":"初始化",
				"10":"草稿",
				"20":"審批中",
				"30":"審批結束",
				"31":"審批通过",
				"32":"審批不通过",
				"35":"審核通过",
				"40":"作廢"
			},
			statusSub : {
				"10":"啟用",
				"40":"禁用"
			},
			statusList:[
						{"id":'00,10',"text":'草稿'},
						{"id":'20',"text":'審批中'},
						{"id":'31,35',"text":'審批通过'},
						{"id":'32',"text":'審批不通过'},
						{"id":'40',"text":'已作廢'}          
			],
			reviseLog:"修訂日誌"
		},
		notice : {
			style:{
				"01":"普通通知",
				"02":"业务提醒",
				"03":"待办事宜",
				"04":"业务确认",
				"05":"提醒通知",
				"06":"业务办理"
			}
		}
	}
};