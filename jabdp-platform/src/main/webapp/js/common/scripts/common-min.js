/**
 * 封装ajax方法，作统一异常错误处理
 * @param options
 */
function fnFormAjaxWithJson(options){
	var tmpFun = options.success;
	var callBackFun = function(data){
		if(typeof tmpFun==='function'&&tmpFun){
			if(data) {
				if(data.flag=="1" || data.rows || $.isArray(data)) {
					tmpFun(data);
				} else if(data.flag=="3") {
					alert("异常提示："+data.msg);
					doReLogin();
				} else {
					alert("异常提示："+data.msg);
				}
			}
		}
	};
	options.success = undefined;
	options = $.extend({
		url: false,
		type : 'post',
		dataType : 'json',
		success:callBackFun,
		beforeSend:function(xhr) {
		},
		error:function(xhr, ts, errorThrown) {
			alert("异常提示：服务器出现异常");
		},
		complete: function (XMLHttpRequest, textStatus) {
			//$.messager.progress('close');
		}
	},options);
	$.ajax(options);
}
function doLogout() {
	window.location.replace(__ctx + "/mb/logout.jsp");
}

function doReLogin() {
	window.top.location.replace(__ctx + "/mb/login.jsp?timeout=true");
}

/**
 * 根据sqlkey查询数据
 * @param sqlKey
 * @param filterParam
 * @returns
 */
function getFilterData(sqlKey,filterParam){				 
	 	var  param = {
	 			"entityName" :	sqlKey
	 	};
	 	$.extend(param,filterParam);
	  	var jsonData=null;
		var opt = {
				data:param,
				async:false,
				url:__ctx+"/gs/gs-mng!dataFilter.action",
				success:function(data){
					jsonData=data.msg;
				}
		};
	  	fnFormAjaxWithJson(opt, true);
		return jsonData;
}
/**
 * 根据sqlkey查询参数映射关系
 * @param sqlKey
 * @returns
 */
function getFieldsByKey(sqlKey){				 
	 	var  param = {
	 			"entityName" :	sqlKey
	 	};
	  	var jsonData=null;
		var opt = {
				data:param,
				async:false,
				url:__ctx+"/gs/gs-mng!getFieldsBySqlKey.action",
				success:function(data){
					jsonData=data.msg;
				}
		};
	  	fnFormAjaxWithJson(opt, true);
		return jsonData;
	}
/**
 * 根据sqlkey和fieldKey查询单个值
 * @param sqlKey
 * @param filterParam
 * @param fieldKey
 * @returns
 */
function getFieldValBySql(sqlKey,filterParam, fieldKey) {
		var data = jwpf.getFilterData(sqlKey,filterParam);
		var fk = fieldKey || "key";
		if(data && data.list && data.list.length) {
			return data.list[0][fk];
		} else {
			return "";
		}
	}

var $MsgUtil = {
		alert:function(opt, method) {
			var op = method || "warning";
			top.toastr.options = {
				"closeButton":true,
				"positionClass": "toast-top-center"
			}
			top.toastr[op](opt);
		},
		info:function(opt) {
			$MsgUtil.alert(opt, "info");
		}
};

	window.alert = function(msg) {
		$MsgUtil.alert(msg);
	};