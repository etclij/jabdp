jwpf={
		messageStyle :{
			"COMMON" : "01",//普通通知
			"WARNING" : "02",//预警通知
			"TODO" : "03",//待办事宜通知
			"BUSINESS" : "04",//业务确认通知
			"REMIND" : "05",//提醒通知
			"BUSINESS_DEAL" : "06"//业务办理通知
		},
		getFormVal:function(fieldKey){
			var values;
			var object = $("#" + fieldKey);
			var className = object.attr("class");
			if(className && className.indexOf("easyui") >=0){// 判断字符中是否包含"easyui"
				if(className.indexOf("combo") >=0){
					if(object.combo("options").multiple){// 判断是否为多选
						values = object.combo("getValues");
					} else {
						values = object.combo("getValue");
					}
				} else if(className.indexOf("numberbox") >=0) {
					values = object.numberbox("getValue");
				} else if(className.indexOf("processbarSpinner") >= 0) {
					values = object.processbarSpinner("getValue");
			    } else if(className.indexOf("dialoguewindow") >=0) {
			    	values = object.dialoguewindow("getValue");
			    } else if(className.indexOf("textbox") >=0) { 
			    	values = object.textbox("getValue");
			    } else {
					values = object.val();
				}
			} else if(className && className.indexOf("combobox") >=0) {
				values = $ES.getComboBoxVal(object);
			} else if(className && className.indexOf("checkbox-radio") >=0) {
				values = $ES.getCheckBoxVal(object);
			} else if(className && className.indexOf("combotree") >=0) {
				values = $ES.getSelectTreeVal(object);
			} else {// 非easyui选框取值
				values = object.val();
			}
			return values;
		},//isTriggerSelectEvent--是否触发选中事件
		setFormVal:function(fieldKey,value,isTriggerSelectEvent){
			var object = $("#" + fieldKey);
			var className = object.attr("class");
			if(className && className.indexOf("easyui") >=0){// 判断字符中是否包含"easyui"
				if(className.indexOf("combo") >=0){
					if(className.indexOf("combobox") >=0){
						var opt = object.combobox("options");
						if(opt.multiple){// 判断是否为多选
							object.combobox("setValues",value);					
						} else {
							object.combobox("setValue",value);
						}
						var selectFlag = true;
						if(opt.url) {
							var val = (opt.multiple)?object.combobox("getValues"):[object.combobox("getValue")];
							var txt = object.combobox("getText");
							if(val.join(",") == txt) {
								if(isTriggerSelectEvent) {
									opt.onLoadSuccess = function() {
										if(opt.onSelect) {
											var target = object[0];
											opt.onSelect.call(target, opt.finder.getRow(target, value));
										}
										opt.onLoadSuccess = null;
									};
								}
								selectFlag = false;//不需要重新触发onselect事件
								object.combobox("reload");
							}
						}
						if(selectFlag && isTriggerSelectEvent && opt.onSelect) {
							var target = object[0];
							opt.onSelect.call(target, opt.finder.getRow(target, value));
						}
					} else if(className.indexOf("combotree") >=0){
						if(object.combotree("options").multiple){// 判断是否为多选
							object.combotree("setValues",value);					
						} else {
							object.combotree("setValue",value);
						}
					} else if(className.indexOf("combogrid") >=0){
						if(object.combogrid("options").multiple){// 判断是否为多选
							object.combogrid("setValues",value);					
						} else {
							object.combogrid("setValue",value);
						}
					} else if(className.indexOf("comboradio") >=0) {
						object.comboradio("setValue",value);
					} else if(className.indexOf("combocheck") >=0) {
						object.combocheck("setValue",value);
					} else if(className.indexOf("newradiobox") >=0) {
				    	object.newradiobox("setValue", value);
					} else if(className.indexOf("newcheckbox") >=0) {
				    	if(value!=undefined && value!=null) {
				    		object.newcheckbox("setValues", value.split(","));
				    	}
					} else{
						if(object.combo("options").multiple){// 判断是否为多选
							object.combo("setValues",value);					
						} else {
							object.combo("setValue",value);
						}
					}				
				} else if(className.indexOf("numberbox") >=0) {
					object.numberbox("setValue",value);
				} else if(className.indexOf("processbarSpinner") >= 0) {
					object.processbarSpinner("setValue", value);
			    } else if(className.indexOf("dialoguewindow") >=0) {
			    	object.dialoguewindow("setValue", value);
			    } else if(className.indexOf("textbox") >=0) { 
			    	object.textbox("setValue", value);
			    } else {
					object.val(value);
				}
			} else if(className && className.indexOf("hyperlink") >=0) {//超链接
				object.val(value);
				if(value) $("#hl_"+fieldKey).text(value);
		    } else if(className && className.indexOf("combobox") >=0) {
		    	$ES.setComboBoxVal(object,value);
			} else if(className && className.indexOf("checkbox-radio") >=0) {
				$ES.setCheckBoxVal(object, value);
			} else if(className && className.indexOf("combotree") >=0) {
				$ES.setSelectTreeVal(object, value);
			}  else if(className && className.indexOf("imagebox") >=0) {
				object.imagebox("setValue", value);
			} else {// 非easyui选框
				object.val(value);
			}
		},//获取表单combo类字段显示名称
		getFormComboFieldCaption:function(fieldKey){
			var values = null;
			var object = $("#" + fieldKey);
			var className = object.attr("class");
			if(className && className.indexOf("easyui") >=0){// 判断字符中是否包含"easyui"
				if(className.indexOf("combo") >=0){
					if(object.combo("options").multiple){// 判断是否为多选
						values = object.combo("getText");
					} else {
						values = object.combo("getText");
					}
				}
			} else if(className && className.indexOf("combobox") >=0) {
				values = $ES.getComboBoxVal(object,"text");
			} else if(className && className.indexOf("checkbox-radio") >=0) {
				values = $ES.getCheckBoxVal(object,"text");
			} else if(className && className.indexOf("combotree") >=0) {
				values = $ES.getSelectTreeVal(object,"text");
			}
			return values;
		},
		//设置form字段编辑与否
		setFormFieldReadonly:function(fieldKey, isReadonly){
			var object = $("#" + fieldKey);
			var className = object.attr("class");
			if(className) {
				if(className.indexOf("easyui") >=0){// 判断字符中是否包含"easyui"
					if(className.indexOf("newradiobox") >=0) {
				    	if(isReadonly) {
							object.newradiobox("disable");
						} else {
							object.newradiobox('enable');
						}
					} else if(className.indexOf("newcheckbox") >=0) {
				    	if(isReadonly) {
							object.newcheckbox("disable");
						} else {
							object.newcheckbox('enable');
						}
					} else if(className.indexOf("combo") >=0){
						if(isReadonly) {
							object.combo('readonly', true);
						} else {
							object.combo('readonly', false);
						}
					} else if(className.indexOf("numberbox") >=0) {
						if(isReadonly) {
							object.numberbox('readonly', true);
						} else {
							object.numberbox('readonly', false);
						}
					} else if(className.indexOf("processbarSpinner") >= 0) {
						if(isReadonly) {
							object.processbarSpinner("disable");
						} else {
							object.processbarSpinner("enable");
						}
				    } else if(className.indexOf("textbox") >=0) { 
				    	if(isReadonly) {
							object.textbox('readonly', true);
						} else {
							object.textbox('readonly', false);
						}
				    } else {
				    	if(isReadonly) {
							object.attr("readonly", "readonly");
						} else {
							object.removeAttr("readonly");
						}
					}
				} else if(className.indexOf("Idate") >=0) {//日期控件
					if(isReadonly) {
						object.attr("readonly", "readonly");
						var onfocusStr = object.attr("onfocus");
						if(onfocusStr) {
							object.data("focusEvent",onfocusStr);
							object.removeAttr("onfocus");
						}
					} else {
						object.removeAttr("readonly");
						object.removeAttr("disabled");
						var onfocusStr = object.data("focusEvent");
						if(onfocusStr) {
							object.attr("onfocus", onfocusStr);
							object.removeData("focusEvent");
						}
					}
				} else if(className.indexOf("jquery_ckeditor") >=0) {//富文本框控件
					var ro = isReadonly || false;
					try {
						object.ckeditorGet().setReadOnly(ro);
					} catch(e) {
						object.ckeditorGet().setReadOnly(ro);
					}
				} else if(className.indexOf("dialoguewindow") >=0) {//对话框控件
					if(isReadonly) {
						object.dialoguewindow("disable");
					} else {
						object.dialoguewindow("enable");
					}
				} else if(className.indexOf("combobox") >=0) {
					if(isReadonly) {
						object.prop("disabled", true);
					} else {
						object.removeProp("disabled");
					}
				} else if(className.indexOf("checkbox-radio") >=0) {
					if(isReadonly) {
						$ES.disableCheckBox(object);
					} else {
						$ES.enableCheckBox(object);
					}
				} else if(className.indexOf("combotree") >=0) {
					if(isReadonly) {
						object.selectTree("disable");
					} else {
						object.selectTree("enable");
					}
				} else {
					if(isReadonly) {
						object.attr("readonly", "readonly");
					} else {
						object.removeAttr("readonly");
					}
				}
			} else {
				if(isReadonly) {
					object.attr("readonly", "readonly");
				} else {
					object.removeAttr("readonly");
				}
			}
		},
		isEmptyRow:function(row) {
			   var flag=true;
			   $.each(row,function(k,v){
				   if(k!="isNewRecord" && k!="autoParentCode" && k!="autoCode" &&k!="_parentId"&& k!="state"){
					   //为空v=false
					   //不为空v=true
				      flag=flag&&!v;
				   }
			   });
			   return flag;
		},
		getTableVal:function(tableKey,rowIndex,fieldKey){
			var child = $("#"+tableKey);
			if(!child.length || !child.data("datagrid")) return null;
			if($.isNumeric(rowIndex)){// rowIndex为数字，返回未选的记录
				var allrows = child.datagrid("getRows");
				if(fieldKey){// 返回未选的记录对象指定字段的数值
				  if(allrows[rowIndex])
					return allrows[rowIndex][fieldKey];
				}else{// 返回未选的记录对象
					return allrows[rowIndex];
				}
			} else if(rowIndex === true){// 非数字,返回已选的记录
				if(fieldKey){// 已选的记录，指定字段，返回字段数值
					var row = child.datagrid('getChecked');
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v[fieldKey]);
						}
					});			
					return rows;
				}else{// 已选的记录，未指定字段，返回记录对象
					var row = child.datagrid('getChecked');	
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v);
						}
					});
					return rows;
				}
			} else {// 其他情况，为false或者null、undefined值，返回所有的记录
				if(fieldKey){// 返回所有记录对象指定字段的数值
					var row = child.datagrid('getRows');
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v[fieldKey]);
						}
					});			
					return rows;
				}else{// 返回所有记录对象
					var row = child.datagrid('getRows');
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v);
						}
					});
					return rows;
				}			
			}
		},
		/**
		 * 设置列表值
		 * @param tableKey
		 * @param rowIndex
		 * @param rowData 包含部分列字段及值，如：{"fieldKey":"fieldVal"}
		 * @param isAddRow 是否添加新行
		 * @param isNotAutoExp 不自动计算
		 */
		setTableVal:function(tableKey,rowIndex,rowData,isAddRow,isNotAutoExp){
			rowIndex = parseInt(rowIndex);
			if(rowData) {
				var tb = $("#"+tableKey);
				//先结束可编辑控件
				var opts = tb.edatagrid('options');
				var index = opts.editIndex;
				var field = opts.editCol;
				if((index ||index==0) && field) {
					var editOpt={"index":index,"field":field};
					tb.edatagrid('endCellEdit', editOpt);
				}
				//再更新行数据
				tb.edatagrid("updateRow", {index:rowIndex,row:rowData});
				if(!isNotAutoExp) {
					setTimeout(function() {
						  jwpf.doSubExpression(tableKey,rowIndex);
						  jwpf.doFormExpression(tableKey);
					},0);
				}
				if(isAddRow) {
					tb.edatagrid("addRow");
				}
			}
		},
		/**
		 * 公式设置子表值
		 */
		setTableValExp:function(tableKey,rowIndex,rowData,isAddRow){
			if(rowData) {
				var tb = $("#"+tableKey);
				//updateRow方法会导致tr.html（）被刷新。正在编辑的编辑器会被动销毁。而子表两个字段需要运算，
				//故简单的对datagrid的panel附加了两次相同的keyup执行函数。(此处反复执行相同事件函数，还有优化余地)
				//当第二次事件执行完毕，input其实已受到updateRow的影响销毁了，
				//e.targer之中取不到data，从而产生报错
				//因updateRow在datagrid内部大量调用，不方便修改。
				//tb.edatagrid("updateRow", {index:rowIndex,row:rowData});
				var row = tb.edatagrid("getRows")[rowIndex];
				tb.edatagrid("getPanel").panel("panel").find("tr[datagrid-row-index="+rowIndex+"] td").each(function(){
					var _this = $(this);
					$.each(rowData, function(k, v) {
						if(_this.attr("field")==k){
							if(_this.hasClass("datagrid-cell-editing")) {//编辑条件下
								//_this.find("input.numberbox-f").numberbox("setValue", v);
							} else {
								_this.children("div.datagrid-cell").html(v);//显示下
							}
							row[k] = v;//没有对计算后的值回调edit的验证器进行验证，不过观察datagrid的updatarow也不进行验证的
							//自动计算关联属性
							setTimeout(function() {
								  jwpf.doSubExpression(tableKey,rowIndex, k);
								  jwpf.doFormExpression(tableKey, k);
							},0);
							return false;
						}
					});
				});
				if(isAddRow) {
					tb.edatagrid("addRow");
				}//重新计算footer值
				jwpf.reloadDataGridFooter(tb);
			}
		},	
		getTreeTableVal:function(tableKey,rowCode,fieldKey){
			var child = $("#"+tableKey);
			var allRows=child.edatagrid("getData");
			if($.type(rowCode) === "string"){// rowCode为字符串，返回指定行记录对象的指定字段的数值			
				var row = child.treegrid("find",rowCode);
				if(fieldKey){
					/*var rows = [];
					if(!jwpf.isEmptyRow(row)) {
						rows.push(row[fieldKey]);
					}*/	
					return row[fieldKey];
				}else{// 返回指定行记录
					return row;
				}
			} else if($.type(rowCode) === "boolean" && rowCode === true){// 非数字,返回已选的记录
				if(fieldKey){// 已选的记录，指定字段，返回字段数值
					var row = child.edatagrid('getChecked');
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v[fieldKey]);
						}
					});			
					return rows;
				}else{// 已选的记录，未指定字段，返回记录对象
					var row = child.edatagrid('getChecked');	
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v);
						}
					});
					return rows;
				}
			} else {// 其他情况，为false或者null、undefined值，返回所有的记录
				if(fieldKey){// 返回所有记录对象指定字段的数值
					var row = allRows;
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v[fieldKey]);
						}
					});			
					return rows;
				}else{// 返回所有记录对象
					var row = allRows;
					var rows = [];
					$.each(row, function(k,v) {
						if(!jwpf.isEmptyRow(v)) {
							rows.push(v);
						}
					});
					return rows;
				}			
			}
		},
		setTreeTableVal:function(tableKey,rowCode,rowData,isAddRow){
			if(rowData) {
				var tb = $("#"+tableKey);
				//先结束可编辑控件
				var opts = tb.edatagrid('options');
				var index = opts.editIndex;
				var field = opts.editCol;
				if((index ||index==0) && field) {
					var editOpt={"index":index,"field":field};
					tb.datagrid('endCellEdit', editOpt);
				}
				//再更新行数据
				tb.edatagrid("update",  {
					id: rowCode,
					update:true,
					row : rowData
				});
				if(isAddRow) {
					tb.edatagrid("addRow");
				}
			}
		},	
		/**
		 * key-统计字段，caption-显示名称，displayKey-显示字段，dataType-数据类型
		 * type-统计类别：total、average、other，value-统计规则
		 * @param tableId
		 * @param rules
		 * @param isInit--初始化
		 */
		setTableTotalVal : function(tableId, rules, isInit) {
			 if(rules && rules.length > 0) {
				 var grid = $("#"+tableId);
				 var map = {};
				 $.each(rules, function(k, v) {
					 var name = v.caption + "===" + v.displayKey;
					 if(map[name]) {
						 map[name].push(v);
					 } else {
						 map[name] = [v];
					 }
				 });
				 grid.data("ruleMap", map);//缓存统计规则
				 if(isInit) {
					 jwpf.reloadDataGridFooter(grid);
				 } /*else {
					 grid.datagrid('getPanel').panel('panel').bind('keyup', function (e) {
				        	 try {
				        		if(jwpf.isNumber(e)) {
				        			var input =  $(e.target);
						        	var td = input.closest("td[field]");
						        	var field = td.attr("field");
						        	if(field && map[field]) {
						        		var tr=td.parent();
										var index = tr.attr("datagrid-row-index");
										index = parseInt(index);
						        		var allrows = grid.datagrid("getRows");
						        		allrows[index][field] = input.val();
						        		var fl = map[field];
						        		for(var i=0,len=fl.length;i<len;i++) {
						        			var val = 0;
						        			if(fl[i].type=="total") {
						        				val = jwpf.getTotalVal(allrows, field, fl[i].dataType);
						        			} else if(fl[i].type=="average") {
						        				val = jwpf.getAvgVal(allrows, field, fl[i].dataType);
						        			} else if(fl[i].type=="max") {
												val = jwpf.getMaxVal(allrows, field, fl[i].dataType);
											} else if(fl[i].type=="min") {
												val = jwpf.getMinVal(allrows, field, fl[i].dataType);
											}
						        			var footAllrows =  grid.datagrid("getFooterRows");
											if(footAllrows && footAllrows.length > 0) {
												for(var j=0,jlen=footAllrows.length;j<jlen;j++) {
													if(fl[i].caption == footAllrows[j][fl[i].displayKey]) {
														footAllrows[j][field] = val;
														break;
													}
												}
											}
						        		}
										grid.datagrid('reloadFooter');
						        	}
				        		}	
				         } catch(e1) {alert("执行汇总统计时出现异常：" + e1);}	
					  }); 
				 } */
			 }
		},//重新加载footer，用于自动计算值变化，isCheckedFirst-- 选中优先计算
		reloadDataGridFooter:function(grid, isCheckedFirst) {
			var ruleMap = grid.data("ruleMap");
			if(ruleMap) {
				 var map = ruleMap;
				 var list = [];
				 var allrows = [];
				 if(isCheckedFirst) {
					 allrows = grid.datagrid("getChecked");//选中数据
					 if(allrows && allrows.length == 0) {
						 allrows = grid.datagrid("getRows");
					 }
				 } else {
					 allrows = grid.datagrid("getRows");
				 }
				 $.each(map, function(k,v) {
					 var frObj = {};
					 var names = k.split("===");
					 var caption = names[0];
					 var disKey = names[1];
					 frObj[disKey] = caption;
					 frObj["_isFooter_"] = true;
					 if(v.length > 0) {
						 $.each(v, function(i,o) {
							 var field = o.key;
							 var val = "";
							 if(o.type=="total") {
								 val = jwpf.getTotalVal(allrows, field, o.dataType);
							 } else if(o.type=="average") {
								 val = jwpf.getAvgVal(allrows, field, o.dataType);
							 } else if(o.type=="max") {
								 val = jwpf.getMaxVal(allrows, field, o.dataType);
							 } else if(o.type=="min") {
								 val = jwpf.getMinVal(allrows, field, o.dataType);
							 }
							 frObj[field] = val;
						 });
					 }
					 list.push(frObj);
				 });
				 grid.datagrid('reloadFooter', list);
			}
		},
		getTotalVal:function(allrows, field, dataType) {
			var totalVal = "";
			var num = allrows.length;
			if(num == 0) {
				return totalVal;
			}
			totalVal = 0;
			for(var i =0;i<num;i++){
				var flVal = null;
				if(field) {
					flVal = allrows[i][field];
				} else {
					flVal = allrows[i];
				}
				if(!flVal) {
					flVal = 0;
				}
				if(dataType=="dtLong"){
					var val = parseInt(flVal);					
					//totalVal = val+totalVal ;
					totalVal = Calc.add(val, totalVal);
				}else if(dataType=="dtDouble"){
					var val = parseFloat(flVal);				
					//totalVal = val+totalVal ;
					totalVal = Calc.add(val, totalVal);
				}else {
					var val = parseFloat(flVal);				
					//totalVal = val+totalVal ;
					totalVal = Calc.add(val, totalVal);
				}
			}
			return totalVal;
		},//求和函数，对于对象数组与数字数组求和
		getSumVal:function(data, field, dataType) {
			var totalVal = 0;
			if(data && data.length) {
				var num = data.length;
				for(var i =0;i<num;i++){
					var flVal = null;
					if(field) {
						flVal = data[i][field];
					} else {
						flVal = data[i];
					}
					if(!flVal) {
						flVal = 0;
					}
					if(dataType=="dtLong"){
						var val = parseInt(flVal);					
						//totalVal = val+totalVal ;
						totalVal = Calc.add(val, totalVal);
					} else if(dataType=="dtDouble"){
						var val = parseFloat(flVal);				
						//totalVal = val+totalVal ;
						totalVal = Calc.add(val, totalVal);
					} else {
						var val = parseInt(flVal);					
						//totalVal = val+totalVal;
						totalVal = Calc.add(val, totalVal);
					}
				}
			}
			return totalVal;
		},
		getAvgVal:function(allrows, field, dataType, fixNum) {
			var num = allrows.length;
			if(num == 0) {
				return "";
			}
			var totalVal = jwpf.getTotalVal(allrows, field, dataType);
			//var averageVal = totalVal/num;
			//return averageVal.toFixed(2);
			var ps = ((fixNum === 0)?0:(fixNum || 2));
			return Calc.div(totalVal, num, ps);
		},
		getMaxVal:function(allrows, field, dataType) {
			var num = allrows.length;
			if(num == 0) {
				return "";
			}
			var numArr = [];
			for(var i =0;i<num;i++){
				var flVal = allrows[i][field];
				if(flVal) {
					if(dataType=="dtLong"){
						var val = parseInt(flVal);		
						numArr.push(val);
					}else if(dataType=="dtDouble"){
						var val = parseFloat(flVal);			
						numArr.push(val);
					}
				}
			}
			var maxVal = "";
			if(numArr.length > 0) {
				var maxVal = Math.max.apply(null, numArr);
			}
			return maxVal;
		},
		getMinVal:function(allrows, field, dataType) {
			var num = allrows.length;
			if(num == 0) {
				return "";
			}
			var numArr = [];
			for(var i =0;i<num;i++){
				var flVal = allrows[i][field];
				if(flVal) {
					if(dataType=="dtLong"){
						var val = parseInt(flVal);					
						numArr.push(val);
					}else if(dataType=="dtDouble"){
						var val = parseFloat(flVal);				
						numArr.push(val);
					}
				}
			}
			var minVal = "";
			if(numArr.length > 0) {
				minVal = Math.min.apply(null, numArr);
			}
			return minVal;
		},
		setFormTotalVal : function(tableId,formKey,type) {
			 var grid = $("#"+tableId);
			    grid.datagrid('getPanel').panel('panel').bind('keyup', function (e) {
		        	 try {
		        		if(jwpf.isNumber(e)){
		        			var input =  $(e.target);
				        	var td = input.closest("td[field]");
				        	var field = td.attr("field");
							var allrows = grid.datagrid("getRows");
							var totalVal = 0;
							var num =allrows.length;
							for(var i =0;i<num;i++){
								if(type=="dtLong"){
									var val = parseInt(allrows[i][field]);							
									//totalVal = val+totalVal ;
									totalVal = Calc.add(val, totalVal);
								}else if(type=="dtDouble"){
									var val = parseFloat(allrows[i][field]);							
									//totalVal = val+totalVal ;
									totalVal = Calc.add(val, totalVal);
								}
							}
							$("#"+formKey).val(totalVal);	        
		        		}	
		         } catch(e1) {alert("执行setFormTotalVal求和计算时出现异常："+e1);}	
			  });  
		},
		updateField:function(moduleTableKey,fieldKey,id,val, callFunc){
			var ids = [];
			if($.isArray(id)) {
				ids = id;
			} else {
				ids.push(id);
			}
		   	var options = {
		             url : __ctx+'/gs/gs-mng!updateTableField.action',
		             async : false,
                     traditional:true,
		             data : {
		            	 "ids" : ids,
		            	 "entityName":moduleTableKey,
		            	 "field":fieldKey,
		            	 "value":val
		             },
		             success : function(data) {
		            	 if(data.msg) {
		            		 if(callFunc) { 
		            			 callFunc();
		            	     }
						 }
		             }
		        };
		        fnFormAjaxWithJson(options, true);
		   },
		getId:function(){
			var id = $("#id").val();
			return id;
		},

	uniqueArr : function(arr) {//去除数组中的重复数据
			var i = 0;
			while (i < arr.length) {
				var current = arr[i];
				for (k = arr.length; k > i; k--) {
					if (arr[k] === current) {
						arr.splice(k, 1);
					}
				}
				i++;
			}
			 return arr.sort();
		},
	unbindFormKey : function(expression) {//去除公式中字段的监听(主要用于主表字段)
		var regex = /([^\[]+?)(?=\])/gi;
		var fieldKeys = expression.match(regex);
		fieldKeys = fieldKeys?fieldKeys:[];
		fieldKeys = jwpf.uniqueArr(fieldKeys);
		for(var i =0;i<fieldKeys.length;i++){
			$("#"+fieldKeys[i]).unbind('keyup');
		}
	},
	
 	//页面加载时添加公式监听方法
	  changeTableFormValue:function(key,expression){
		 try {
			 var exp =  expression;
			var regex = /([^\[]+?)(?=\])/gi;
			var fieldKeys = expression.match(regex);
			fieldKeys = fieldKeys?fieldKeys:[];
			fieldKeys = jwpf.uniqueArr(fieldKeys);
			var keys = key.split(".");
			if(keys.length > 1) { // 目标字段为---子表
				
				for(var i =0;i<fieldKeys.length;i++){
					if(fieldKeys[i] && fieldKeys[i]!=""){
					   var fks = fieldKeys[i].split(".");
					   if(fks.length > 1) { //公式可能 ：子+子...=子  或 子+主...=子
						   //1.子表的字段添加监听 //2.计算剩余对象的值
						   
						   //保存子表公式到内存数据中
						  	var expAry = $("#"+fks[0]).data("subExpArray");						  	
						  	var expAll = key + "=" +exp;
						   	if(expAry && expAry.length){						   		
						   		if($.inArray(expAll, expAry)<0){
						   			expAry.push(expAll);
						   			$("#"+fks[0]).data("subExpArray",expAry);
						   		} 
						   	}else{
						   		var ary = [];
						   		ary.push(expAll);
						   		$("#"+fks[0]).data("subExpArray",ary);
						   	}
						   
						   	var newArray = [];
						   	for(var j =0;j<fieldKeys.length;j++){
							   	if(fieldKeys[j]!=fieldKeys[i]){
								   	newArray.push(fieldKeys[j]);
							   		}
						   		}	  
						   	jwpf.changeTableValueOther(fks[0],fks[1],newArray,exp,keys);
						
					   } else { //公式 ：主+...=子 
						 //1.公式中主表的字段添加监听 //2.计算剩余对象的值
						   if(fieldKeys[i].split("_").length>1){//不對其監聽
								 
						   }else{
							   jwpf.changeTableValue(fieldKeys[i],fieldKeys,exp,keys);
						   }
					   }
					}
				}
			} else { // 目标字段为---主表
				for(var i =0;i<fieldKeys.length;i++){
					if(fieldKeys[i] && fieldKeys[i]!=""){
					   var fks = fieldKeys[i].split(".");
					   if(fks.length > 1) { //公式可能 ：子+主... =主  或 子+子...=主 			 					   												  
						   //1.子表字段添加监听 //2.计算剩余对象的值
						   
						   //保存公式到内存数据中
						  	var expAry = $("#"+fks[0]).data("expArray");						  	
						  	var expAll = key + "=" +expression;
						   	if(expAry && expAry.length){						   		
						   		if($.inArray(expAll, expAry)<0){
						   			expAry.push(expAll);
						   			$("#"+fks[0]).data("expArray",expAry);
						   		} 
						   	}else{
						   		var ary = [];
						   		ary.push(expAll);
						   		$("#"+fks[0]).data("expArray",ary);
						   	}
						   	
						   if(fks.length==2){
							jwpf.changeFormValueOther(fks[0],fks[1],fieldKeys,exp,key);
						   }else if(fks.length==3){//这种公式为了计算子表的合计
							 	var newArray = [];
							   	for(var j =0;j<fieldKeys.length;j++){
								   	if(fieldKeys[j]!=fieldKeys[i]){
									   	newArray.push(fieldKeys[j]);
								   		}
							   		}
							 jwpf.changeFormValueOnce(fks[0],fks[1],fks[2],newArray,exp,key);
						   }						 						 
					   } else { //公式 ：主+主....=主 或主+子....=主 
						 //1.主表字段添加监听 //2.计算剩余对象的值					
						 jwpf.changeFormValue(fieldKeys[i],fieldKeys,exp,key);
					   }
					}
				}
			} 
		}catch(e) {alert("为" + key + "添加自动计算公式时出现异常：" + e);} 
	  },
	//事件中动态添加公式监听
	  newChangeTableFormValue : function(key, expression, rowIndex,flag) {
		var exp = expression;
		var regex = /([^\[]+?)(?=\])/gi;
		var fieldKeys = expression.match(regex);
		fieldKeys = fieldKeys ? fieldKeys : [];
		fieldKeys = jwpf.uniqueArr(fieldKeys);
		var keys = key.split(".");
		if (keys.length > 1) { // 子表字段
			if(flag){
				var grid = $("#" + keys[0]);//去除当前行的监听
				var pl = grid.edatagrid('getPanel').panel('panel');
				var tr = pl.find("div.datagrid-body").find(
					"tr[datagrid-row-index=" + rowIndex + "]");
				tr.unbind('keyup');
				}
			for ( var i = 0; i < fieldKeys.length; i++) {
				if (fieldKeys[i] && fieldKeys[i] != "") {
					var fks = fieldKeys[i].split(".");
					if (fks.length > 1) { // 公式可能 ：子+子...=子 或 子+主...=子
						// 1.子表字段添加监听 //2.计算剩余对象的值
						var newArray = [];
						for ( var j = 0; j < fieldKeys.length; j++) {
							if (fieldKeys[j] != fieldKeys[i]) {
								newArray.push(fieldKeys[j]);
							}
						}
						jwpf.newChangeTableValueOther(fks[0], fks[1], newArray,
								exp, keys, rowIndex);

					} else { // 公式 ：主+...=子
						// 1.主表字段添加监听 //2.计算剩余对象的值
						// jwpf.changeTableValue(fieldKeys[i],fieldKeys,exp,keys);
					}
				}
			}
		} else { //主表字段
			//保存公式到内存数据中
			   var expressionData = $("#"+key).data("expressionData");
			   if(expressionData){
				   jwpf.unbindFormKey(expressionData);
			   }
			  $("#"+key).data("expressionData",exp);
			for(var i =0;i<fieldKeys.length;i++){
				if(fieldKeys[i] && fieldKeys[i]!=""){
				   var fks = fieldKeys[i].split(".");
				   if(fks.length > 1) { //公式可能 ：子+主... =主  或 子+子...=主 			 					   												  
					   //1.子表字段添加监听 //2.计算剩余对象的值
						if(flag){//去除这行当前的监听事件
							var grid = $("#" + keys[0]);
							var pl = grid.edatagrid('getPanel').panel('panel');
							var tr = pl.find("div.datagrid-body").find(
								"tr[datagrid-row-index=" + rowIndex + "]");
							tr.unbind('keyup');
							}
						
						  //保存动态公式到内存数据中
					  	var expAry = $("#"+fks[0]).data("expArray");						  	
					  	var expAll = key + "=" +expression;
					   	if(expAry && expAry.length){						   		
					   		if($.inArray(expAll, expAry)<0){
					   			expAry.push(expAll);
					   			$("#"+fks[0]).data("expArray",expAry);
					   		} 
					   	}else{
					   		var ary = [];
					   		ary.push(expAll);
					   		$("#"+fks[0]).data("expArray",ary);
					   	}
					   if(fks.length==2){					
						jwpf.newChangeFormValueOther(fks[0],fks[1],fieldKeys,exp,key,rowIndex);
					   }else if(fks.length==3){
						 	var newArray = [];
						   	for(var j =0;j<fieldKeys.length;j++){
							   	if(fieldKeys[j]!=fieldKeys[i]){
								   	newArray.push(fieldKeys[j]);
							   		}
						   		}
						 //jwpf.newchangeFormValueTotal(fks[0],fks[1],fks[2],newArray,exp,key,rowIndex);
					   }				 						 
				   } else { //公式 ：主+主....=主 或主+子....=主 
					 //1.主表字段添加监听 //2.计算剩余对象的值						   
					 jwpf.changeFormValue(fieldKeys[i],fieldKeys,exp,key);
				   }
				}
			}
		} 
	},
	//自动处理子表数据
	autoHandleTableFieldValue:function(dgObj, fieldKey, val) {
		var opt = dgObj.datagrid("getColumnOption",fieldKey);
		if(opt && opt.editor ){
			var precision = opt.editor.options.precision;
			var newVal = jwpf.dataHandling(val,precision);
			return newVal;
		} else {
			return val;
		}
	},
	  changeTableValueOther:function(tableKey,targetField,newArray,expression,key){
			 var grid = $("#" + tableKey);
			 if(!grid.length || !grid.data("datagrid")) return null;
			 var exp =  expression;
			 /*var eventName = "input";
			 if(jwpf.getIEVersion() != -1) {
				 eventName = "keyup";
			 }*/
			 var eventName = "keyup";
			 //console.log(eventName);
			 grid.edatagrid('getPanel').panel('panel').on(eventName, function(e) {
				if (jwpf.isNumber(e)) {
					//console.log("1:" + e.timeStamp);
					var input = $(e.target);
					var td = input.closest("td[field]");
					var currentField = td.attr("field");				
					if (currentField == targetField) {
						var tr = td.parent();
						var allrows = grid.datagrid("getRows");
						var index = tr.attr("datagrid-row-index");
						index = parseInt(index);
						var value = input.val();
						if(!value){
							value=0;
						}else{
							value = jwpf.autoHandleTableFieldValue(grid, targetField, value);
							//console.log(input.data('events').keyup);
							//input.val(value);
							//console.log("2:" + e.timeStamp);
						}
						exp = expression.replace(eval("/\\["+tableKey+"."+targetField+"\\]/g"),value);
						allrows[index][targetField] = value;
						jwpf.commonChangeTableValue(tableKey,index,newArray,exp,key);

					}
				}
				exp = expression;
			});
		  },  
		  
		  commonChangeTableValue :function(tableKey,rowIndex,newArray,exp,key){
			  var grid = $("#" + tableKey);
				for ( var h = 0; h < newArray.length; h++) {
					var fields = newArray[h].split(".");
					if (fields.length > 1) { // 子+子...=子
						var allrows = $("#" + fields[0]).edatagrid("getRows");
						var value = allrows[rowIndex][fields[1]];
						if(!value){//处理空值
							value=0;
						}else if(value<0){//处理负数
							value = "("+value+")";
						}
						exp = exp.replace(eval("/\\[" + newArray[h] + "\\]/g"), value);
					} else {// 子+主...=子
						var master = newArray[h].split("_");
						var value = 0;
						if(master.length>1){
							value = $("#" + master[0]).val();
						}else{
							value = $("#" + newArray[h]).val();
						}
						exp = exp.replace(eval("/\\[" + newArray[h] + "\\]/g"), value);
					}
				}
				
				try {
					var newVal = eval(exp);
					var opt = grid.datagrid("getColumnOption",key[1]);
					if(opt && opt.editor ){
						var precision = opt.editor.options.precision;
						newVal = jwpf.dataHandling(newVal,precision);
						var obj = {};
						obj[key[1]] = newVal;
						jwpf.setTableValExp(tableKey,rowIndex,obj);
					}
				} catch(e) {}
	},

	newChangeTableValueOther : function(tableKey, targetField, newArray,
			expression, key, rowIndex) {
		var grid = $("#" + tableKey);
		if(!grid.length || !grid.data("datagrid")) return null;
		var exp = expression;
		var pl = grid.edatagrid('getPanel').panel('panel');
		var tr = pl.find("div.datagrid-body").find(
				"tr[datagrid-row-index=" + rowIndex + "]");
		tr.bind('keyup', function(e) {
			if (jwpf.isNumber(e)) {

				var input = $(e.target);
				var td = input.closest("td[field]");
				var currentField = td.attr("field");
				if (currentField == targetField) {
					var tr = td.parent();
					var allrows = grid.datagrid("getRows");
					var index = tr.attr("datagrid-row-index");
					index = parseInt(index);
					var value = input.val();
					if(!value){
						value=0;
					}else{
						value = jwpf.autoHandleTableFieldValue(grid, targetField, value);
						input.val(value);
					}
					exp = expression.replace(eval("/\\["+tableKey+"."+targetField+"\\]/g"),value);
					for ( var h = 0; h < newArray.length; h++) {
						var fields = newArray[h].split(".");
						if (fields.length > 1) { // 子+子...=子
							var allrows = $("#" + fields[0]).edatagrid(
									"getRows");
							var value = allrows[index][fields[1]];
							if (!value) {
								value = 0;
							}
							exp = exp.replace(eval("/\\[" + newArray[h] + "\\]/g"), value);
						} else {// 子+主...=子
							var value = $("#" + newArray[h]).val();
							if (!value) {
								value = 0;
							}
							exp = exp.replace(eval("/\\[" + newArray[h] + "\\]/g"), value);
						}
					}
					var newVal = eval(exp);
					var opt = grid.datagrid("getColumnOption",key[1]);
					var precision = opt.editor.options.precision;
					newVal = jwpf.dataHandling(newVal,precision);
					var obj = {};
					obj[key[1]] = newVal;
					jwpf.setTableValExp(tableKey, index, obj);
		
				}
			}
			exp = expression;
			//return false;
		});
	},  
		  changeTableValue:function(formKey,fieldList,expression,key){
			  //$("#"+formKey).bind('keyup', function(e) {
			  /*$("#"+formKey).numberbox*/
			  jwpf.bindTextBoxEvent($("#"+formKey), {onChange:function(newValue,oldValue){
			        var exp =  expression;
					//var value = $("#"+formKey).val();
					var value = newValue;
			        if(!value){
						value=0;
					}
					var f ={};
					f[formKey]=value;
					var newArray = [];
					var allrows= [];
					   for(var j =0;j<fieldList.length;j++){
						   if(fieldList[j]!=formKey){
							   newArray.push(fieldList[j]);
						   }
					   }
					   for(var h =0;h<newArray.length;h++){
							  var fields = newArray[h].split(".");
							   if(fields.length>1){	//主+子....=子								
								   allrows =$("#"+fields[0]).edatagrid("getRows");
								   f[newArray[h]] = [];
								   for(var g =0;g<allrows.length;g++){
									   	var value = allrows[g][fields[1]];
									   	if(!value){
											value=0;
										}
									   	f[newArray[h]].push(value);												 											   	
								   }
							   }else{//主+主....=子
								  var value = $("#"+newArray[h]).val();
								  if(!value){
										value=0;
									}
								  f[newArray[h]]=value; 
							   }
						   }
					   
					var m =[];
					for(var t =0;t<fieldList.length;t++){
						if($.isArray(f[fieldList[t]])){
							if(m.length==0){
								m= new Array(f[fieldList[t]].length);
							}
							var values = f[fieldList[t]];
							for(var v =0;v<values.length;v++){
								if(m[v]){
									m[v] = m[v].replace(eval("/\\["+fieldList[t]+"\\]/g"),values[v]);
								}else{
									m[v] = exp.replace(eval("/\\["+fieldList[t]+"\\]/g"),values[v]);
								}								
							}																				
						}else{
							if(m.length==0) {//只有主...的情况
								exp = exp.replace(eval("/\\["+fieldList[t]+"\\]/g"),f[fieldList[t]]);
							} else {
								for(var mi=0;mi<m.length;mi++) {
									if(m[mi]) {
										m[mi] = m[mi].replace(eval("/\\["+fieldList[t]+"\\]/g"),f[fieldList[t]]);
										
									}
								}
							}
						}									
					}
				
					if(m.length==0){
						var newVal = eval(exp);
						var opt = $("#"+key[0]).datagrid("getColumnOption",key[1]);
						var precision = opt.editor.options.precision;
						newVal = jwpf.dataHandling(newVal,precision);
						allrows =$("#"+key[0]).edatagrid("getRows");
						for(var e=0;e<allrows.length;e++){
							var obj = {};
							obj[key[1]] = newVal;
							jwpf.setTableValExp(key[0],e, obj);
							//$("#"+key[0]).edatagrid("refreshRow",e);
						}
					}else{
						var opt =$("#"+key[0]).edatagrid("getColumnOption",key[1]);
						var precision = opt.editor.options.precision;
						allrows =$("#"+key[0]).edatagrid("getRows");
						for(var e=0;e<m.length;e++){							
							var newVal = eval(m[e]);
							newVal = jwpf.dataHandling(newVal,precision);
							var obj = {};
							obj[key[1]] = newVal;
							jwpf.setTableValExp(key[0],e, obj);
							//$("#"+key[0]).edatagrid("refreshRow",e);
						}
					}
					
					setTimeout(function() {
						  jwpf.doSubExpression(key[0]);
						  jwpf.doFormExpression(key[0]);
					},0);
					
					exp = expression;
			}}, "dtDouble");  
			//});	
		  },
	  changeFormValueOther : function(tableKey, targetField, fieldList,
			expression, key) {
		var exp = expression;
		$("#" + tableKey).edatagrid('getPanel').panel('panel').bind(
				'keyup',
				function(e) {
					var newArray = [];
					var fd = tableKey + "." + targetField;
					for ( var j = 0; j < fieldList.length; j++) {
						if (fieldList[j] != fd) {
							newArray.push(fieldList[j]);
						}
					}
					if (jwpf.isNumber(e)) {
						var input = $(e.target);
						var td = input.closest("td[field]");
						var currentField = td.attr("field");
						if (currentField == targetField) {
							var tr=td.parent();	
							var index = tr.attr("datagrid-row-index");
							index=parseInt(index);	
							var allrows = $("#" + tableKey)
									.edatagrid("getRows");
							var opt = $("#"+tableKey).datagrid("getColumnOption",currentField);
							var precision = opt.editor.options.precision;
							var f = {};
							f[fd] = [];
							for ( var k = 0; k < allrows.length; k++) {
								var value = 0;
								if(k==index){
									value = jwpf.dataHandling(input.val(),precision);
								}else{
									value = allrows[k][currentField];
								}
								if (!value) {
									value = 0;
								}
								f[fd].push(value);
								
							}
							jwpf.commonChangeFormVal(key,newArray,fieldList,exp,f);							
						}
					}
					exp = expression;
				});
	},
	commonChangeFormVal : function (formKey,expFields,allFileds,expression,dataObj){//计算的子表每一行
		var exp = expression;
		var f = dataObj?dataObj:{};
		for ( var h = 0; h < expFields.length; h++) {
			var fields = expFields[h].split(".");
			if (fields.length > 1) { //子+子....=主
				allrows = $("#" + fields[0]).edatagrid(
						"getRows");
				f[expFields[h]] = [];
				for ( var g = 0; g < allrows.length; g++) {
					var value = allrows[g][fields[1]];
					if (!value) {
						value = 0;
					}
					f[expFields[h]].push(value);
				}

			} else {// 子+主... =主
				var value = $("#" + expFields[h]).val();
				if (!value) {
					value = 0;
				}
				f[expFields[h]] = value;
			}
		}
		var m = [];
		for ( var t = 0; t < allFileds.length; t++) {
			if ($.isArray(f[allFileds[t]])) {
				if (m.length == 0) {
					m = new Array(f[allFileds[t]].length);
				}
				var values = f[allFileds[t]];
				for ( var v = 0; v < values.length; v++) {
					if (m[v]) {
						m[v] = m[v].replace(eval("/\\["+allFileds[t]+"\\]/g"),values[v]);
					} else {
						m[v] =exp.replace(eval("/\\["+allFileds[t]+"\\]/g"),values[v]);
					}
				}
			} else {
				if (m.length == 0) {// 主...的情况
					exp = exp.replace(eval("/\\["+allFileds[t]+"\\]/g"),f[allFileds[t]]);
				} else {
					for ( var mi = 0; mi < m.length; mi++) {
						if (m[mi]) {
					m[mi] = m[mi].replace(eval("/\\["+allFileds[t]+"\\]/g"),f[allFileds[t]]);	
						}
					}
				}
			}
		}
		var total = 0;
		
		for ( var e = 0; e < m.length; e++) {
			var newVal = eval(m[e]);
			total = total + newVal;
		}
		/*var opt =  $('#'+formKey).numberbox("options");
		var precision = opt.precision;*/
		total = jwpf.dataHandling(total,jwpf.getPrecision(formKey));
		jwpf.setFormVal(formKey,total);
		jwpf.triggerInputEvent(formKey);
	},
	triggerInputEvent:function(fieldKey) {
		$("#"+fieldKey).trigger("input");
	},
	 newChangeFormValueOther : function(tableKey, targetField, fieldList,
				expression, key, rowIndex) {
			var grid = $("#" + tableKey);
			var exp = expression;
			var pl = grid.edatagrid('getPanel').panel('panel');
			var tr = pl.find("div.datagrid-body").find(
					"tr[datagrid-row-index=" + rowIndex + "]");
			tr.bind('keyup', function(e) {
						var newArray = [];
						var fd = tableKey + "." + targetField;
						for ( var j = 0; j < fieldList.length; j++) {
							if (fieldList[j] != fd) {
								newArray.push(fieldList[j]);
							}
						}
						if (jwpf.isNumber(e)) {
							var input = $(e.target);
							var td = input.closest("td[field]");
							var currentField = td.attr("field");
							if (currentField == targetField) {
								var tr=td.parent();	
								var index = tr.attr("datagrid-row-index");
								index=parseInt(index);	
								var allrows = $("#" + tableKey)
										.edatagrid("getRows");
								var f = {};
								f[fd] = [];
								for ( var k = 0; k < allrows.length; k++) {
									var value = 0;
									if(k==index){
										value = input.val();	
									}else{
										value = allrows[k][currentField];
									}
									if (!value) {
										value = 0;
									}
									f[fd].push(value);
									
								}
								jwpf.commonChangeFormVal(key,newArray,fieldList,exp,f);
								
							}
						}
						exp = expression;
					});
		},
		 newChangeFormValueTotal : function(tableKey, targetField, dataType,newArray,
					expression, key, rowIndex) {
				var grid = $("#" + tableKey);
				var exp = expression;
				var pl = grid.edatagrid('getPanel').panel('panel');
				var tr = pl.find("div.datagrid-body").find(
						"tr[datagrid-row-index=" + rowIndex + "]");
				tr.bind('keyup', function(e) {																							
							if (jwpf.isNumber(e)) {
									var index =	0;											
									var input = $(e.target);
									var td = input.closest("td[field]");
									var currentField = td.attr("field");
									if (currentField == targetField) {
										var tr = td.parent();
										var allrows = grid.datagrid("getRows");
										index =  parseInt(tr.attr("datagrid-row-index"));					
										var value = input.val();
										if(!value){
											value=0;
										}else{
											value = jwpf.autoHandleTableFieldValue(grid, targetField, value);
											input.val(value);
										}										
									
									exp = expression.replace(eval("/\\["+tableKey+"."+targetField+"."+dataType+"\\]/g"),value);
									
									for ( var h = 0; h < newArray.length; h++) {
										var fields = newArray[h].split(".");
										if (fields.length > 1 && fields.length==3) { // 子+子....=主
											var allrows = grid.datagrid("getRows");
											var value = allrows[index][fields[1]];
												if (!value) {
													value = 0;
												}
											exp = expression.replace(eval("/\\["+newArray[h]+"\\]/g"),value);											
										} else {// 子+主... =主
											var value = $("#" + newArray[h]).val();
											if (!value) {
												value = 0;
											}
											exp = expression.replace(eval("/\\["+newArray[h]+"\\]/g"),value);
										}
									}
						
									var newVal = eval(exp);
									newVal = jwpf.dataHandling(newVal);
									var obj = {};
									obj[dataType] = newVal;
									jwpf.setTableValExp(tableKey, index, obj);
									var allrows = grid.datagrid("getRows");
									var total = jwpf.getTotalVal(allrows,dataType,dtDouble);
									jwpf.setFormVal(key,total);
								}
							}
							exp = expression;
						});
			},
	changeFormValueOnce : function(tableKey, targetField,dataType,fieldList,
			expression, key) {
		var exp = expression;
		var grid = $("#" + tableKey);
		grid.edatagrid('getPanel').panel('panel').bind(
				'keyup',
				function(e) {
					var newArray = [];
					var fd = tableKey + "." + targetField;
					for ( var j = 0; j < fieldList.length; j++) {
						if (fieldList[j] != fd) {
							newArray.push(fieldList[j]);
						}
					}
					if (jwpf.isNumber(e)) {
						var input = $(e.target);
						var td = input.closest("td[field]");
						var currentField = td.attr("field");
						if (currentField == targetField) {
							var tr=td.parent();	
							var index = tr.attr("datagrid-row-index");
							index=parseInt(index);	
							var allrows = $("#" + tableKey)
									.edatagrid("getRows");
							var total = 0;
							for ( var k = 0; k < allrows.length; k++) {
								var value = 0;							
								if(k==index){
									value = input.val();	
									value = jwpf.autoHandleTableFieldValue(grid, targetField, value);
									input.val(value);
								}else{
									value = allrows[k][currentField];
								}
								if (!value) {
									value = 0;
								}
								if(dataType=="dtLong"){
									value = parseInt(value);					
								}else if(dataType=="dtDouble"){
									value = parseFloat(value);				
								}
								total = total + value;
							}
							exp = expression.replace(eval("/\\["+tableKey+"."+targetField+"."+dataType+"\\]/g"),total);
							
							jwpf.commonChangeFormValOther(key,newArray,exp);
																
						}
					}
					//exp = expression;
				});
	},
	
	commonChangeFormValOther: function(formKey,expFields,expression){//计算的子表一列的总和
		var exp = expression;
		for ( var h = 0; h < expFields.length; h++) {
			var fields =expFields[h].split(".");
			if (fields.length > 1) { // 主+子....=子
				allrows = $("#" + fields[0]).edatagrid(
						"getRows");
				var total = 0;
				for ( var k = 0; k < allrows.length; k++) {
					var value = allrows[k][fields[1]];
					if (!value) {
						value = 0;
					}
					if(fields[2]=="dtLong"){
						value = parseInt(value);					
					}else if(fields[2]=="dtDouble"){
						value = parseFloat(value);				
					}
					total = total + value;									
					}
				exp = exp.replace(eval("/\\[" + expFields[h] + "\\]/g"), total);
			} else {// 子+主... =主
				var value = $("#" + expFields[h]).val();
				if (!value) {
					value = 0;
				}
				exp = exp.replace(eval("/\\[" + expFields[h] + "\\]/g"), value);
			}
		}
		var newVal = eval(exp);	
		/*var opt =  $('#'+formKey).numberbox("options");
		var precision = opt.precision;*/
		newVal = jwpf.dataHandling(newVal,jwpf.getPrecision(formKey));
		jwpf.setFormVal(formKey,newVal);
	},
	  changeFormValue : function(formKey,fieldList,expression,key){
		  var newArray = [];																
			   for(var j =0;j<fieldList.length;j++){
				   if(fieldList[j]!=formKey){
					   newArray.push(fieldList[j]);
				   }
			   }
		  for(var h =0;h<newArray.length;h++){
			  var fields = newArray[h].split(".");
			   if(fields.length==1){//主+主....=主
					/*$("#"+newArray[h]).numberbox({
							onChange :function(newValue,oldValue){
								 jwpf.formToformValue(formKey,fieldList,newArray,expression,key);
							}
					});*/
				   jwpf.bindTextBoxEvent($("#"+newArray[h]), {
						onChange :function(newValue,oldValue){
							 jwpf.formToformValue(formKey,fieldList,newArray,expression,key);
						}
				   }, "dtDouble");
				   /*$("#"+newArray[h]).numberbox("textbox").bind("keyup", function(e) {
					    console.log(e);
						jwpf.formToformValue(formKey,fieldList,newArray,expression,key);
				   });*/
			   }
		   }
		  $("#"+formKey).bind('keyup', function(e) {
			  jwpf.formToformValue(formKey,fieldList,newArray,expression,key);
		  });	
	  },
	  formToformValue :function(formKey,fieldList,newArray,expression,key){
		  var exp =  expression;
			//var value = $("#"+formKey).val();// 2.主表的
		    var value = jwpf.getFormVal(formKey);
			  if(!value){
					value=0;
				}
			var f ={};
			f[formKey]=value;
			var allrows = [];
			   for(var h =0;h<newArray.length;h++){
					  var fields = newArray[h].split(".");
					   if(fields.length==2){	//主+子(当前该列的每一行计算)....=主								
						   allrows =$("#"+fields[0]).edatagrid("getRows");							   
						   f[newArray[h]] = [];
						   for(var g =0;g<allrows.length;g++){
							   	var value = allrows[g][fields[1]];
							   	if(!value){
									value=0;
								}
							   	f[newArray[h]].push(value);												 											   	
						   }
					   }if(fields.length==3){//主+子(当前这一列的总和)....=主
						   allrows =$("#"+fields[0]).edatagrid("getRows");
						   var totalVal = jwpf.getTotalVal(allrows,fields[1],fields[2]);
						   f[newArray[h]] = [];
						   f[newArray[h]].push(totalVal );						   
					   }else{//主+主....=主
						   //var value = $("#"+newArray[h]).val();
						   var value = jwpf.getFormVal(newArray[h]);
						   if(!value){
									value=0;
								}
							  f[newArray[h]]=value;						
					   }
				   }
				var m = [];
				for ( var t = 0; t < fieldList.length; t++) {
					if ($.isArray(f[fieldList[t]])) {
						if (m.length == 0) {
							m = new Array(f[fieldList[t]].length);
						}
						var values = f[fieldList[t]];
						for ( var v = 0; v < values.length; v++) {
							if (m[v]) {
								m[v] = m[v].replace(eval("/\\["+fieldList[t]+"\\]/g"),values[v]);
							} else {
								m[v] =exp.replace(eval("/\\["+fieldList[t]+"\\]/g"),values[v]);
							}
						}
					} else {
						if (m.length == 0) {// 主...的情况
							exp = exp.replace(eval("/\\["+fieldList[t]+"\\]/g"),f[fieldList[t]]);
						} else {
							for ( var mi = 0; mi < m.length; mi++) {
								if (m[mi]) {
									m[mi] = m[mi].replace(eval("/\\["+fieldList[t]+"\\]/g"),f[fieldList[t]]);
								}
							}
						}
					}
				}
				
				if(m.length==0){
					var newVal = eval(exp);
					/*var opt =  $('#'+key).numberbox("options");
					var precision = opt.precision;*/
					newVal = jwpf.dataHandling(newVal,jwpf.getPrecision(key));
					jwpf.setFormVal(key,newVal);
					jwpf.triggerInputEvent(key);
				}else{
					var total = 0;
					for ( var e = 0; e < m.length; e++) {
						var newVal = eval(m[e]);
						total = total + newVal;
					}
					/*var opt =  $('#'+key).numberbox("options");
					var precision = opt.precision;*/
					total = jwpf.dataHandling(total,jwpf.getPrecision(key));
					jwpf.setFormVal(key,total);	
					jwpf.triggerInputEvent(key);
				}	
				//$("#"+key).trigger("input");//手动触发input事件
				exp = expression;	  
	  },
	  getPrecision:function(key) {
		  var jqObj = $('#'+key);
		  if(jqObj.hasClass("numberbox-f")) {
			  var opt =  jqObj.numberbox("options");
			  return opt.precision;
		  } else {
			  return jqObj.attr("options");
		  }
	  },//子表公式自动计算
	  doSubExpression: function (tableKey,rowIndex, fieldKey){
			//执行该子表中相关的公式（子+....=子）
			var expAry = $("#"+tableKey).data("subExpArray");
		  	if(expAry && expAry.length){	
		  		for ( var j = 0; j <expAry.length; j++) {
		  			var  expression = [];				
					expression.push(expAry[j].slice(0,expAry[j].indexOf("=")));
					expression.push(expAry[j].slice(expAry[j].indexOf("=")+1));		  			
					var regex = /([^\[]+?)(?=\])/gi;
					var allfieldKeys =(expression[1]).match(regex);
					allfieldKeys = allfieldKeys?allfieldKeys:[];
					allfieldKeys = jwpf.uniqueArr(allfieldKeys);
					//只自动计算fieldKey相关的字段
					if(!fieldKey || (fieldKey && allfieldKeys.indexOf(tableKey+"."+fieldKey) >= 0)) {
						var keys = expression[0].split(".");
						if(rowIndex){
							jwpf.commonChangeTableValue(tableKey,rowIndex,allfieldKeys,expression[1],keys);	
						}else{
							var allrows = $("#" + tableKey).edatagrid("getRows");
							for(var i = 0; i <allrows.length; i++){
								jwpf.commonChangeTableValue(tableKey,i,allfieldKeys,expression[1],keys);	
							}
						}
					}
				}
		  	}
	  },//主表公式自动计算
	  doFormExpression: function (tableKey, fieldKey){
		  //执行该子表中相关的公式（子+....=主）
		var expAry = $("#"+tableKey).data("expArray");
	  	if(expAry && expAry.length){		
	  		for ( var j = 0; j <expAry.length; j++) {
	  			var  expression = [];				
				expression.push(expAry[j].slice(0,expAry[j].indexOf("=")));
				expression.push(expAry[j].slice(expAry[j].indexOf("=")+1));				  			
				var regex = /([^\[]+?)(?=\])/gi;
				var allfieldKeys =(expression[1]).match(regex);
				allfieldKeys = allfieldKeys?allfieldKeys:[];
				allfieldKeys = jwpf.uniqueArr(allfieldKeys);
				//只自动计算fieldKey相关的字段
				if(!fieldKey || (fieldKey && allfieldKeys.indexOf(tableKey+"."+fieldKey) >= 0)) {
					var flag = false;
					for ( var k = 0; k <allfieldKeys.length; k++) {
						var fields = allfieldKeys[k].split(".");
						if (fields.length > 1 && fields.length==3){
							flag = true;
							break;
						}
					}				
					if(flag){
						//计算子表字段的总和的赋值给主表字段
						jwpf.commonChangeFormValOther(expression[0],allfieldKeys,expression[1]);
					}else{
						jwpf.commonChangeFormVal(expression[0],allfieldKeys,allfieldKeys,expression[1]);
					}															
				}
			}
	  	}
	  },
	  //判断输入的是否为数字
	  isNumber :function(e){		
		  if(e.which==45 || e.which==46){
			  return true;
		  }else if(e.which>=96 && e.which<=105){
			  return true;
		  }
		  if(e.which==46){
			  return true;
		  }else{
			  if((e.which>=48&&e.which<=57&&e.ctrlKey==false&&e.shiftKey==false)||e.which==0||e.which==8){
				  return true;;
			  }else{
				  if(e.ctrlKey==true&&(e.which==99||e.which==118)){
					  return true;
				  }else{
					  return false;
				  }
			  }
		  }
	  },
	  bindEvent:function(id, eventObj, editType, dataType) {
		try {
		  var obj = $("#" + id);
		  switch(editType) {
			  case "TextBox":
				  jwpf.bindTextBoxEvent(obj, eventObj, dataType);
				  break;
			  case "ComboBox":
				  if(obj.hasClass("combobox-f")) {
					  obj.combobox(eventObj);
				  } else if(obj.hasClass("autocomplete")) {
					  $ES.bindAutoCompleteEvent(obj, eventObj);
				  } else {
					  $ES.bindComboBoxEvent(obj, eventObj);
				  }
				  break;
			  case "ComboBoxSearch":
				  if(obj.hasClass("combobox-f")) {
					  obj.comboboxsearch(eventObj);
				  } else if(obj.hasClass("autocomplete")) {
					  $ES.bindAutoCompleteEvent(obj, eventObj);
				  } else {
					  $ES.bindComboBoxEvent(obj, eventObj);
				  }
				  break;	  
			  case "ComboTree":
				  obj.combotree(eventObj);
				  break;
			  case "RadioBox":
			  case "CheckBox":	
				  if(obj.hasClass("combo-f")) {
					  $.extend(obj.combo("options"),eventObj);
				  } else {
					  $ES.bindCheckBoxEvent(obj, eventObj);
				  }
				  break;
			  case "DataGrid":
				  obj.datagrid(eventObj);
				  break;
			  case "TreeGrid":
				  obj.treegrid(eventObj);
				  break;
			  case "Tree":
				  obj.tree(eventObj);
				  break;
			  case "ComboGrid":
				  obj.combogrid(eventObj);
				  break;
			  case "Button":	
				  obj = $("#bt_" + id);
				  jwpf.bindDefaultEvent(obj, eventObj);
				  break;
			  case "HyperLink":
				  obj = $("#hl_" + id);
				  jwpf.bindDefaultEvent(obj, eventObj);
				  break;
			  case "zTree":
				  var treeObj = $.fn.zTree.getZTreeObj(id);
				  var set = treeObj.setting;
				  set.callback = eventObj;
				  break;	  
			  case "DialogueWindow":
				  $.extend(obj.dialoguewindow("options"),eventObj);
				  break;	
			  case "ImageBox":
				  $.extend(obj.imagebox("options"),eventObj);
				  break;	  
			  default:
				  jwpf.bindDefaultEvent(obj, eventObj);
		  }
		} catch(e) {alert("为" + id + "绑定事件时出现异常：" + e);}
	  },
	  bindDefaultEvent:function(obj, eventObj) {
		  $.each(eventObj, function(k, v) {
			  if(k) {
				  var eventName = k;
				  if(eventName.indexOf("on") >= 0) {
					  eventName = eventName.substr(2);
				  }
				  //console.debug(k);
				  //obj.unbind(eventName, v);
				  obj.bind(eventName, v);
			  }
		  });
	  },
	  bindTextBoxEvent:function(obj, eventObj, dataType) {
		  if(dataType=="dtLong" || dataType=="dtInteger" || dataType=="dtDouble") {
			  if(obj.hasClass("numberbox-f")) {
				  obj.numberbox(eventObj); 
				  if(eventObj["onfocus"] || eventObj["onblur"]) {
					  jwpf.bindDefaultEvent(obj, eventObj);
				  }
			  } else {
				  if(eventObj["onChange"]) {
					  eventObj["oninput"] = function(event) {
						  var newVal = event.delegateTarget.value, oldVal = null;
						  eventObj["onChange"].call(obj, newVal, oldVal);
					  };
				  }
				  jwpf.bindDefaultEvent(obj, eventObj);
			  }
		  } else {
			  if(obj.hasClass("textbox-f")) {
				  if(eventObj["onChange"]) {
					  obj.textbox(eventObj);
				  }
				  jwpf.bindDefaultEvent(obj.textbox("textbox"), eventObj);
			  } else {
				  if(eventObj["onChange"]) {
					  eventObj["oninput"] = function(event) {
						  var newVal = event.delegateTarget.value, oldVal = null;
						  eventObj["onChange"].call(obj, newVal, oldVal);
					  };
				  }
				  jwpf.bindDefaultEvent(obj, eventObj);
			  }
		  }
	  },
	  findPreOrNextEditTd : function(td, isPre) {
				var nextTd = null;
				if(isPre) {
					nextTd = td.prev();
				} else {
					nextTd = td.next();
				}
				if(nextTd.length) {
					return jwpf.findEditInput(nextTd, isPre);
				} else {
					//找下一行
					var tr = td.closest("tr");
					var nextTr = null;
					if(isPre) {
						nextTr = tr.prev();
					} else {
						nextTr = tr.next();
					}
					if(nextTr.length) {
						if(isPre) {
							nextTd = nextTr.find("td:last");
						} else {
							nextTd = nextTr.find("td:first");
						}
						return jwpf.findEditInput(nextTd, isPre);
					} else {
						return null;
					}
				}
	  },		
	  findEditInput:function(nextTd, isPre) {
		    var nextInputs = nextTd.find("input,select,textarea");
			if(nextInputs.length) {
				var nextInput = nextInputs[0];
				if($(nextInput).hasClass("combo-f")) {
					return nextInput;
				} else {
					if($(nextInput).is(":visible")) {
						if($(nextInput).attr("readonly") || $(nextInput).attr("disabled")
								|| $(nextInput).attr("type") == "button") {
							return jwpf.findPreOrNextEditTd(nextTd, isPre);
						} else {
							return nextInput;
						}
					} else {
						return jwpf.findPreOrNextEditTd(nextTd, isPre);
					}
				}
			} else {
				return jwpf.findPreOrNextEditTd(nextTd, isPre);
			}
	  },
	  switchEditInput:function(input, isPre) {
		  var td = input.closest("td");
		  var nextInput = jwpf.findPreOrNextEditTd(td, isPre);
		  if(nextInput){
			  /*现在combo已经在显示input上取消了独立样式，继承了text-box*/
			    /*if (input.hasClass("combo-text")) {
					$(td.find("input,select")[0])
						.combo('hidePanel');
				}*/
			  td.find("input.combo-f,select.combo-f").combo('hidePanel');
				if ($(nextInput).hasClass("combo-f")) {
					$(nextInput).combo('showPanel');
				}
				$(nextInput).focusEnd();
		  }
	  },//向上或向下查找可编辑控件
	  findUpDownEditInput:function(nextTd, isUp) {
		    var curTr = nextTd.parent();
		    var tb = nextTd.parent().parent();
		    var trIndex = $("tr",tb).index(curTr);
		    //var tdIndex = $("td", curTr).index(nextTd);
		    var left = nextTd.offset().left;
		    var trs = $("tr", tb);
		    if(isUp) {
		    	trIndex--;
				var nextInput = null;
		    	for(var i=trIndex ;i>=0 ; i--) {
		    		$("td", trs[i]).each(function(j){
			    		if($(this).offset().left + $(this).width() > left) {
			    			var nextInputs = $(this).find("input,select,textarea");
			    			if(nextInputs.length) {
			    				nextInput = nextInputs[0];
			    				if(!$(nextInput).hasClass("combo-f")) {
			    					if($(nextInput).is(":visible")) {
			    						if($(nextInput).attr("readonly") || $(nextInput).attr("disabled")
			    								|| $(nextInput).attr("type") == "button") {
			    							nextInput = null;
			    						}
			    					} else {
			    						nextInput = null;
			    					}
			    				}
			    			} else {
			    				nextInput = null;
			    			}
			    			return false;
			    		}
			    	});
			    	if(nextInput !== null) return nextInput;
			    }
		    } else {
		    	trIndex++;
		    	var nextInput = null;
		    	for(var i=trIndex ;i<trs.length ; i++) {
			    	$("td", trs[i]).each(function(j){
			    		if($(this).offset().left + $(this).width() > left) {
			    			var nextInputs = $(this).find("input,select,textarea");
			    			if(nextInputs.length) {
			    				nextInput = nextInputs[0];
			    				if(!$(nextInput).hasClass("combo-f")) {
			    					if($(nextInput).is(":visible")) {
			    						if($(nextInput).attr("readonly") || $(nextInput).attr("disabled")
			    								|| $(nextInput).attr("type") == "button") {
			    							nextInput = null;
			    						}
			    					} else {
			    						nextInput = null;
			    					}
			    				}
			    			} else {
			    				nextInput = null;
			    			}
			    			return false;
			    		}
			    	});
			    	if(nextInput !== null) return nextInput;
			    }
		    }
		    /*var nTd = $("td",$("tr:eq("+trIndex+")", tb)).eq(tdIndex);
		    if(nTd.length == 0) return null;
		    var nextInputs = nTd.find("input,select,textarea");
			if(nextInputs.length) {
				var nextInput = nextInputs[0];
				if($(nextInput).hasClass("combo-f")) {
					return nextInput;
				} else {
					if($(nextInput).is(":visible")) {
						if($(nextInput).attr("readonly") || $(nextInput).attr("disabled")
								|| $(nextInput).attr("type") == "button") {
							return jwpf.findUpDownEditInput(nTd, isUp);
						} else {
							return nextInput;
						}
					} else {
						return jwpf.findUpDownEditInput(nTd, isUp);
					}
				}
			} else {
				return jwpf.findUpDownEditInput(nTd, isUp);
			}*/
	  },
	  switchUpDownEditInput:function(input, isUp) {
		  var td = input.closest("td");
		  var nextInput = jwpf.findUpDownEditInput(td, isUp);
		  if(nextInput){
			    if (input.hasClass("combo-text")) {
					$(td.find("input,select")[0])
						.combo('hidePanel');
				}
				if ($(nextInput).hasClass("combo-f")) {
					$(nextInput).combo('showPanel');
				}
				$(nextInput).focusEnd();
		  }
	  },
	  bindKeydownOnForm : function(id) {
		$("#" + id)
				.bind(
						'keydown',
						function(e) {
							var input = $(e.target);
							switch (e.keyCode) {
							case 38: // up
								jwpf.switchUpDownEditInput(input, true);
								break;
							case 40: // down	
								jwpf.switchUpDownEditInput(input);
							    break;
							case 37: // left
								if(input.getSelectionStart() == 0) {
									jwpf.switchEditInput(input, true);
								}
								break;
							case 39: // right
								var valLength = input.val().length;
								if(input.getSelectionStart() == valLength) {
									jwpf.switchEditInput(input);
								}
								break;
							case 13: // 回车
								if(e.ctrlKey && input[0].type=="textarea"){
									/*var content = input.val();
									content = content+'\n';
									input.val(content);*/
									input.insert({"text":"\n"});
								}else{
									jwpf.switchEditInput(input);
								}
								break;
							}
						});
	},
	focusFirstInputOnFormTabs : function(id) {
		var tab = $("#" + id).tabs('getSelected');
		/*避免选到主表控件*/
		var tbl = tab.find("table[id]:not([grid_widget])");
		if(tbl && tbl.length>0){
 			var childId = $(tbl[0]).attr("id");
			var opts = $("#"+childId).edatagrid('options');    
			var editIndex = opts.editIndex;
			var editCol =opts.editCol;
			if(editIndex && editCol){
				$("#"+childId).edatagrid('editCol',{"index":editIndex,"field":editCol});
				$("#"+childId).focusEditor(editCol);
			}else{
				$("#"+childId).edatagrid('addRow');
			}
		} else {
			var body = tab.panel('body');
			var input = body.find("input:visible:not([readonly]),select:visible:not([readonly]),textarea:not([readonly])");
			if (input && input.length > 0) {
				if ($(input[0]).hasClass("combo-f")) {
					$(input[0]).combo('showPanel');
				} else if($(input[0]).hasClass("jquery_ckeditor")) {
					var ckId = $(input[0]).attr("id");
					CKEDITOR.instances[ckId].focus();
					return;
				}
				$(input[0]).focusEnd();
			}
		}
	},
	focusFirstInputOnForm : function(id) {
		var input = $("#" + id).find("input:visible:not([readonly]),select:visible:not([readonly]),textarea:not([readonly])");
		if (input && input.length > 0) {
			if ($(input[0]).hasClass("combo-f")) {
				$(input[0]).combo('showPanel');
			} else if($(input[0]).hasClass("jquery_ckeditor")) {
				var ckId = $(input[0]).attr("id");
				CKEDITOR.instances[ckId].focus();
				return;
			}
			$(input[0]).focusEnd();
		}
	},//发送业务通知
	doSendBusinessMessage : function(options){
		//		var options = {
		//				"userId" : ,
		//				"content" :,
		//				"title" :,
		//				"moduleDataId" :,
		//				"moduleKey" :,
		//				"fields" : []
		//		}
		jwpf.sendMessage(options,jwpf.messageStyle.BUSINESS);
	},
	//发送业务预警通知
	doSendWarningMessage : function(options){
		jwpf.sendMessage(options,jwpf.messageStyle.WARNING);
	},
	//发送待办事宜通知
	doSendTodoMessage : function(options){
		jwpf.sendMessage(options,jwpf.messageStyle.TODO);
	},
	//发送普通通知
	doSendCommonMessage : function(options){
		jwpf.sendMessage(options,jwpf.messageStyle.COMMON);
	},//发送业务办理通知
	doSendBusinessDealMessage : function(options){		
		jwpf.sendMessage(options,jwpf.messageStyle.BUSINESS_DEAL);
	},
	sendMessage : function(opt,style){
		var sendJsondata = JSON.stringify(opt);
	   	var options = {
	             url : __ctx+'/gs/gs-mng!sendMessage.action',
	             data : {
	            	 "jsonSenddata" : sendJsondata,
	            	 "style":style
	             },
	             success : function(data) {
	            	 
	             }
	        };
	        fnFormAjaxWithJson(options);
	},
	bindHotKeysOnListPage : function(){
        jQuery(document).bind('keydown', 'Ctrl+E',function (evt){evt.preventDefault();if($("#bt_view").is(":visible")){$("#bt_view").trigger('click');} return false; });//查看
        jQuery(document).bind('keydown', 'Ctrl+G',function (evt){evt.preventDefault();if($("#bt_copy").is(":visible")){$("#bt_copy").trigger('click');} return false; });//复制
        jQuery(document).bind('keydown', 'Ctrl+I',function (evt){evt.preventDefault();if($("#bt_add").is(":visible")){$("#bt_add").trigger('click');} return false; });//新增
        jQuery(document).bind('keydown', 'Ctrl+Y',function (evt){evt.preventDefault();if($("#bt_accessory").is(":visible")){$("#bt_accessory").trigger('click');} return false; });//附件
        jQuery(document).bind('keydown', 'Ctrl+R',function (evt){evt.preventDefault();if($("#bt_report").is(":visible")){$("#bt_report").trigger('click');} return false; });//报表
        jQuery(document).bind('keydown', 'Ctrl+D',function (evt){evt.preventDefault();if($("#bt_del").is(":visible")){$("#bt_del").trigger('click');} return false; });//删除
  
	},
	bindHotKeysOnViewPage : function(){
		jQuery("input,textarea").live('keydown','Ctrl+Q',function (evt){
        	evt.preventDefault();
        	if($("#tclose").is(":visible")){
        		$(this).blur();
        		$("#tclose").trigger('click');
        		} 
        	evt.stopPropagation();
        	return false; 
        	});//关闭
        jQuery(document).bind('keydown', 'Ctrl+Q',function (evt){evt.preventDefault();if($("#tclose").is(":visible")){$("#tclose").trigger('click');} return false; });//关闭
        jQuery("input,textarea").live('keydown','Ctrl+U',function (evt){
        	evt.preventDefault();
        	if($("#tcancel").is(":visible")){
        		$(this).blur();
        		$("#tcancel").trigger('click');
        		}
        	evt.stopPropagation();
        	return false;
        	});//取消
        jQuery(document).bind('keydown', 'Ctrl+U',function (evt){evt.preventDefault();if($("#tcancel").is(":visible")){$("#tcancel").trigger('click');} return false; });//取消
        jQuery("input,textarea").live('keydown', 'Ctrl+S',function (evt){       	     	
        	evt.preventDefault();
        	if($("#tsave").is(":visible")){
        		$(this).blur();
        		doSaveObj();
        	}
        	evt.stopPropagation();
        	return false; 
        });//保存
        jQuery(document).bind('keydown', 'Ctrl+S',function (evt){
        	evt.preventDefault();
        	if($("#tsave").is(":visible")){
        		doSaveObj();
        	} 
        	return false; 
        });//保存
        jQuery(document).bind('keydown', 'Ctrl+M',function (evt){evt.preventDefault();if($("#tedit").is(":visible")){$("#tedit").trigger('click');} return false; });//修改
        jQuery(document).bind('keydown', 'Ctrl+G',function (evt){evt.preventDefault();if($("#tcopy").is(":visible")){$("#tcopy").trigger('click');} return false; });//复制
        jQuery(document).bind('keydown', 'Ctrl+I',function (evt){evt.preventDefault();if($("#tadd").is(":visible")){$("#tadd").trigger('click');} return false; });//新增
        jQuery(document).bind('keydown', 'Ctrl+Y',function (evt){evt.preventDefault();if($("#tattach").is(":visible")){$("#tattach").trigger('click');} return false; });//附件
        jQuery(document).bind('keydown', 'Ctrl+R',function (evt){evt.preventDefault();if($("#bt_report").is(":visible")){$("#treport").trigger('click');} return false; });//报表
        jQuery(document).bind('keydown', 'Ctrl+F',function (evt){evt.preventDefault();if($("#tfirst").is(":visible")){$("#tfirst").trigger('click');} return false; });//第一条
        jQuery(document).bind('keydown', 'Ctrl+right',function (evt){evt.preventDefault();if($("#tnext").is(":visible")){$("#tnext").trigger('click');} return false; });//下一条
        jQuery(document).bind('keydown', 'Ctrl+left',function (evt){evt.preventDefault();if($("#tprev").is(":visible")){$("#tprev").trigger('click');} return false; });//上一条
        jQuery(document).bind('keydown', 'Ctrl+L',function (evt){evt.preventDefault();if($("#tlast").is(":visible")){$("#tlast").trigger('click');}return false; });//最后一条
        jQuery(document).bind('keydown', 'Ctrl+P',function (evt){evt.preventDefault();if($("#tprocess").is(":visible")){$("#tprocess").trigger('click');}return false; });//启动流程
        jQuery(document).bind('keydown', 'Ctrl+E',function (evt){evt.preventDefault();if($("#tviewprocess").is(":visible")){$("#tviewprocess").trigger('click');} return false; });//查看流程
        jQuery(document).bind('keydown', 'Ctrl+J',function (evt){evt.preventDefault();if($("#tcancelprocess").is(":visible")){$("#tcancelprocess").trigger('click');}return false; });//取消审核
        jQuery(document).bind('keydown', 'Ctrl+D',function (evt){evt.preventDefault();if($("#tdel").is(":visible")){$("#tdel").trigger('click');} return false; });//删除
      /*  jQuery(document).bind('keydown', 'Ctrl+J',function (evt){evt.preventDefault();if($("#stInsert").is(":visible")){$("#stInsert").trigger('click');} return false; });//插入 insert
        jQuery(document).bind('keydown', 'Ctrl+H',function (evt){evt.preventDefault();if($("#stCopy").is(":visible")){$("#stCopy").trigger('click');} return false; });//复制  copy
        jQuery(document).bind('keydown', 'Ctrl+Y',function (evt){evt.preventDefault();if($("#stMoveUp").is(":visible")){$("#stMoveUp").trigger('click');} return false; });//上移 moveUp
        jQuery(document).bind('keydown', 'Ctrl+X',function (evt){evt.preventDefault();if($("#stMoveDown").is(":visible")){$("#stMoveDown").trigger('click');} return false; });//下移 moveDown
        jQuery(document).bind('keydown', 'Ctrl+D',function (evt){evt.preventDefault();if($("#stDelete").is(":visible")){$("#stDelete").trigger('click');} return false; });//删除  delete
       */	
        //绑定双击复制事件    
        $("body").append('<button id="_copy_text_bt_" type="button" class="validInView" style="display:none;"></button>');
        var clipboard = new ClipboardJS("#_copy_text_bt_", {
		    text: function(trigger) {
		    	return $("#_copy_text_bt_").data("text");
		    }
		});

		clipboard.on('success', function(e) {
			//console.log(e);
			alert("已复制到剪切板！");
		});

		clipboard.on('error', function(e) {
		    console.log(e);
		    alert("复制失败，请尝试重新复制！");
		});
        
	    $("input,textarea,span.select2-selection").on("dblclick", function(event) {
	        	var obj = this;
	        	if($(obj).is("span")) {
	        		var txt = $(obj).text() || "";
	        		txt = txt.slice(1).replace(/×/g," ");
	        		$("#_copy_text_bt_").data("text", txt);
		    	} else {
		    		$("#_copy_text_bt_").data("text", $(obj).val());
		    	}
	        	$("#_copy_text_bt_").trigger("click");
	    });
        },
        getFilterData : function(sqlKey,filterParam){				 
      		 	var  param = {
      		 			"entityName" :	sqlKey
      		 	};
      		 	$.extend(param,filterParam);
      		  	var jsonData=null;
      			var opt = {
      					data:param,
      					async:false,
      					url:__ctx+"/gs/gs-mng!dataFilter.action",
      					success:function(data){
      						jsonData=data.msg;
      					}
      			};
      		  	fnFormAjaxWithJson(opt, true);
      			return jsonData;
      	},
      	getFilterDataAsync : function(sqlKey,filterParam,callFunc){				 
  		 	var  param = {
  		 			"entityName" :	sqlKey
  		 	};
  		 	$.extend(param,filterParam);
  		  	var jsonData=null;
  			var opt = {
  					data:param,
  					async:false,
  					url:__ctx+"/gs/gs-mng!dataFilter.action",
  					success:function(data){
  						if(callFunc) callFunc(data.msg);
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
      	},
      	//根据sqlKey查找查询字段列表
      	getFieldsByKey : function(sqlKey){				 
  		 	var  param = {
  		 			"entityName" :	sqlKey
  		 	};
  		  	var jsonData=null;
  			var opt = {
  					data:param,
  					async:false,
  					url:__ctx+"/gs/gs-mng!getFieldsBySqlKey.action",
  					success:function(data){
  						jsonData=data.msg;
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
  			return jsonData;
      	},
      	getFieldValBySql : function(sqlKey,filterParam, fieldKey) {
      		var data = jwpf.getFilterData(sqlKey,filterParam);
      		var fk = fieldKey;
      		if(data && data.list && data.list.length) {
      			return fk ? data.list[0][fk] : data.list[0];
      		} else {
      			return "";
      		}
      	},
      	getFieldValBySqlAsync : function(sqlKey,filterParam, fieldKey, callFunc) {
      		var fk = fieldKey, cf = callFunc;
      		jwpf.getFilterDataAsync(sqlKey,filterParam, function(data) {
          		var result = "";
      			if(data && data.list && data.list.length) {
      				result = (fk ? data.list[0][fk] : data.list[0]);
          		}
      			if(cf) {
      				cf(result);
      			}
      		});
      	},
        getQueryData : function(sqlKey,filterParam,event){				 
  		 	var  param = {
  		 			"entityName" :	sqlKey
  		 	};
  		 	$.extend(param,filterParam);
  		  	var jsonData=null;
  			var opt = {
  					data:param,
  					async:false,
  					url:__ctx+"/gs/gs-mng!dataFilter.action",
  					success:function(data){
  						jsonData=data.msg;
  						var list = jsonData['list'];
  						var opt = {
  								"top" : 0,
  								"left" : 0,
  								"width" : 0,
  								"height" : 0
  						};
  						opt.top = $(event.target).position().top+$(event.target).height()+5;
  						opt.left = $(event.target).position().left;
  						opt.width = $(event.target).width();
  						opt.height = 150;
  						opt.object = $(event.target);
  						jwpf.createPanel(list,opt);
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
  			return jsonData;
        },
  		createPanel : function (list,opts){		
				var len=list.length;
				if(len>0){
				var arr=[];
				for(var i=0;i<len;i++){//拼装内容
					var dataObject=list[i];
					 var str =
							"<li><span>"
							+dataObject
							+"</span></li>";
					 arr.push(str); 
				}
				var content=arr.join("");
				var color = opts.color || "red";
				var dp = $('#_query_pl_').length;
				var divs = "<div id='_query_content_' style='color:" + color + "'>"+content+"</div>";
				if(!dp){	
					var options = {
							"top" : 0,
							"left" : 0,
							"width" : 150,
							"height" : 150
					};
					$.extend(options,opts);
					var top = options.top;
					var left = options.left;
					var width = options.width;
					var height = options.height;
					var pl = "<div id='_query_pl_' style='position: absolute; top:"+top+"px;left:"+left+"px;z-index: 9000;width:"+width+"px;height:"+height+"px;padding:10px;background:#fafafa;'> ";
					var obj = options.object;
					if(obj){
						obj.after(pl);
					}else{
						$("body").append(pl);
					}
					
					$('#_query_pl_').panel({
						"content" : divs
					});
				}else{
					$('#_query_pl_').show();							
					$('#_query_content_').html(content);
				}
				window.onmousedown = function(){
					$('#_query_pl_').hide();
				};
			}
  	},
      	doOpenDataFilterWin : function(sqlKey,filterParam,childTableKey,winTitle,onAfterSure, width, height){
      		var title = winTitle || "数据筛选";
      		var param = JSON.stringify(filterParam);
      		var wid = width || 600;
      		var hei = height || 400;
      		var url = __ctx+"/gs/gs-mng!dataFilterWin.action?sqlKey="+sqlKey+
      													"&filterParam="+encodeURI(param);
      		var callFunc = function() {
      			var flag = saveFilterData__(childTableKey);
      			if(flag) {
      				if(onAfterSure) onAfterSure();
      				doCloseModuleOperDialog();
      			}
      		};
      		doOpenModuleOperDialog(url, title, wid, hei, callFunc);
      	},
      	createProc : function(content){
    	   	var options = {
    	             url : __ctx+'/gs/gs-mng!createProcedure.action',
    	             data : {
    	            	 "procContent":content
    	             },
    	             success : function(data) {
    	            	 if(data.msg){
    	            		 alert("创建成功");
    	            	 }
    	             }
    	        };
    	        fnFormAjaxWithJson(options,true);
    	},
    	callProc : function(key,filterParam, callFunc, isShowProgress){
    		var isSP = isShowProgress || false;
    		var  param = {
  		 			"procKey" :	key
  		 	};
  		 	$.extend(param,filterParam);
    	   	var options = {
    	   			data:param,
  					async:isSP,
    	            url : __ctx+'/gs/gs-mng!callProcedure.action',
    	            success : function(data) {
    	            	 if(data.msg){
    	            		if(callFunc) {
								callFunc(data.msg);
							}
    	            	 }
    	             }
    	        };
    	    fnFormAjaxWithJson(options,!isSP);
    	},//更新子表combo控件类型编辑属性，不支持combogrid、combotree
    	updateTableComboFieldOption : function(tableKey, fieldKey, data, isNeedReEdit) {
    		var dgId = "#" + tableKey;
    		var opt = $(dgId).data("edatagrid");
    		var paramObj = {"key":"key","caption":"caption"};
	     	if(opt) {
		     	var fieldOpt = $(dgId).edatagrid("getColumnOption", fieldKey);
		     	fieldOpt.editor.options.data = data;
		     	fieldOpt.editor.options.valueField = paramObj.key;
		     	fieldOpt.editor.options.textField = paramObj.caption;
	     	}
	     	var obj = {"data":data,"obj":paramObj};
	     	eval("sub_" + tableKey + "_" + fieldKey + "Json = obj;");
	    	if(isNeedReEdit && opt) {
	    		var opts = opt.options;
				if (opts.editIndex >= 0 && opts.editCol==fieldKey) {
					var eIndex = opts.editIndex;
					var editOpt = {
							"index" : opts.editIndex,
							"field" : fieldKey
						};
					$(dgId).edatagrid('endCellEdit', editOpt);
					$(dgId).edatagrid('beginCellEdit',editOpt);
					opts.editIndex = eIndex;
					opts.editCol = fieldKey;
				}
	    	}
    	},//更新子表combo控件类型数据属性，不支持combogrid、combotree
    	updateTableComboFieldDataOption : function(tableKey, fieldKey, data) {
    		if(tableKey) {
    			var dgId = "#" + tableKey;
        		var opt = $(dgId).data("edatagrid");
    	     	if(opt) {
    		     	var fieldOpt = $(dgId).edatagrid("getColumnOption", fieldKey);
    		     	fieldOpt.editor.options.data = data;
    	     	}
    		} else {
    			var object = $("#" + fieldKey);
    			var className = object.attr("class");
    			if(className){
    				if(className.indexOf("easyui") >=0 && className.indexOf("combobox") >=0) {//老控件
    					object.combobox({
    						data:data,
    						valueField:"id",
    						textField:"text"
    					});
    				} else if(className.indexOf("combobox") >=0) {//新控件
    					object.empty();
						for(var i in data) {
							var row = data[i];
							var $opt = $('<option value="'+ row.id +'" >'+ (row.text) +'</option>');
							$opt.data("data", row);
							jqObj.append($opt);
						}
						object.trigger("change");
    				}
    			}
    		}
    	},
    	//打开数据项列表，横向展示
    	doOpenItemListWin : function(winParam){
    		var defaultParam = {"title":"数据筛选",
    							"width":600,
    							"height":400,
    							"sqlKey":"",
    							"filterParam":{},
    							"onAfterSure":null};
      		var param = $.extend(defaultParam, winParam);
      		var filterParam = JSON.stringify(param.filterParam);
      		var url = __ctx+"/sys/common/item-list.action?sqlKey="+param.sqlKey+
      													"&filterParam="+encodeURI(filterParam);
      		var callFunc = function() {
      			var items = _getItemList_();
      			if(items) {
	      			var flag = false;
	      			if(param.onAfterSure) flag = param.onAfterSure(items);
	      			if(flag) {
	      				doCloseModuleOperDialog();
	      			}
      			}
      		};
      		doOpenModuleOperDialog(url, param.title, param.width, param.height, callFunc);
      	},
      	//设置子表数据，isAppend表示是否追加
      	setTableData : function(tableKey, rowsData, isAppend, isAutoAddEmptyRow, isCopyCol) {
			var isAddRow = !(isAutoAddEmptyRow === false);//默认自动加行 
      		if(rowsData) {
      			var dg = $('#'+tableKey);
      			var oldRows = dg.edatagrid("getRows");
      			if(!isAppend) {
          			//覆盖更新，先删除原有历史数据
      				if(oldRows && oldRows.length) {
      					for(var i=oldRows.length-1;i>=0;i--) {
      						dg.edatagrid("deleteRow", i);
      					}
      				}
          		} else {
          			//删除最后一个空行或默认行
      				if(oldRows && oldRows.length) {
      					var rowIndex = oldRows.length - 1;
      					if(dg.edatagrid("validEmptyOrDefaultRow", oldRows[rowIndex])) {
      						dg.edatagrid("deleteRow", rowIndex);
      					}
      				}
          		}
      			//追加记录
      			if(isCopyCol) {
	  				for(var i=0,len=rowsData.length;i<len;i++) {
						rowsData[i]["_isAutoFocus_"] = false;//不自动聚焦
						if(oldRows && oldRows.length > i) {
							dg.edatagrid("updateRow",{index : i,row : rowsData[i]});
						} else {
							dg.edatagrid("addRow",rowsData[i]);
						}
	  				}
      			} else {
      				for(var i=0,len=rowsData.length;i<len;i++) {
						rowsData[i]["_isAutoFocus_"] = false;//不自动聚焦
						dg.edatagrid("addRow",rowsData[i]);
	  				}
      			}
		    setTimeout(function() {
  			  jwpf.doSubExpression(tableKey);
  			  jwpf.doFormExpression(tableKey);
			 }, 0);
  				//自动加入空行或默认行
  			  if(isAddRow) dg.edatagrid("addRow");
      		}
      	},
        //下推选中数据
      	pushDownTableData : function(fromTableKey, toTableKey, duplicateKeys, isPushDownFunc, noticeFunc) {
			var noticeArr = [];
			var dpKeys = duplicateKeys || "";
			var dpKeyArr = dpKeys.split(",");
			var fromDg = $('#'+fromTableKey);
			var toDg = $('#'+toTableKey);
			var rowsData = jwpf.getTableVal(fromTableKey, true);
			var toData = jwpf.getTableVal(toTableKey, false);
      		if(rowsData && rowsData.length) {
  				for(var i=0,len=rowsData.length;i<len;i++) {
  					var row = rowsData[i];
  					var flag = true;
					if(isPushDownFunc && !isPushDownFunc(row, toData)) {
						noticeArr.push(row);
						flag = false;
					}
					if(flag) {
						if(!jwpf.checkRowInRows(row, toData, dpKeyArr)) {//检查重复项
							row["_isAutoFocus_"] = false;//不自动聚焦
		  					toDg.edatagrid("addRow",row);
						}
					}
  				}
      		}//不需要下推的数据进行提醒
      		if(noticeFunc) {
      			noticeFunc(noticeArr);
      		}
      		fromDg.edatagrid("clearSelections");
      	}, //根据重复检测属性值，检测row是否在row中
      	checkRowInRows:function(row, rows, duplicateKeyArr) {
      		if(rows && rows.length 
      				&& duplicateKeyArr && duplicateKeyArr.length) {
      			for(var i=0,len=rows.length; i<len; i++) {
      				var r = rows[i];
      				var flag = true;
      				for(var j=0,jLen=duplicateKeyArr.length;j<jLen;j++) {
      					var key = duplicateKeyArr[j];
      					flag = flag && (row[key] == r[key]); 
      				}
      				if(flag) {
      					return true;
      				}
      			}
      		}
      		return false;
      	},
		//设置树形子表数据
      	setTreeTableData : function(tableKey, rowsData, isAppend, isAutoAddEmptyRow) {
      		var isAddRow = !(isAutoAddEmptyRow === false);//默认自动加行 
      		if(rowsData) {
      			var dg = $('#'+tableKey);
      			if(!isAppend) { //覆盖数据
      				dg.edatagrid("removeAllNodes");//删除数据
      			}
      			//追加记录
  				for(var i=0,len=rowsData.length;i<len;i++) {
					rowsData[i]["_isAutoFocus_"] = false;//不自动聚焦
  					dg.edatagrid("addRow",rowsData[i]);
  				}
				setTimeout(function() {
  			      jwpf.doSubExpression(tableKey);
  			      jwpf.doFormExpression(tableKey);
				},0);
  			    //自动加入空行或默认行
  			    if(isAddRow) dg.edatagrid("addRow");
      		}
      	},
        //打开Datagrid数据列表，对于数据进行处理
    	doOpenDataGridWin : function(winParam){
    		var defaultParam = {"title":"数据列表",
    							"width":600,
    							"height":400,
    							"sqlKey":"",
    							"filterParam":{},
    							"onAfterSure":null};
      		var param = $.extend(defaultParam, winParam);
      		var filterParam = JSON.stringify(param.filterParam);
      		var url = "/gs/gs-mng!dataFilterWin.action?sqlKey="+param.sqlKey+
      													"&filterParam="+encodeURIComponent(filterParam);
      		param["url"] = url;
      		openFormWin(param);
      		/*var callFunc = function() {
      			var flag = false;
	      		if(param.onAfterSure) flag = param.onAfterSure();
	      		if(flag) {
	      			doCloseModuleOperDialog();
	      		}
      		};
      		doOpenModuleOperDialog(url, param.title, param.width, param.height, callFunc);*/
      	},
		//更新主表combo类控件数据
		updateFormComboFieldOption : function(fieldKey, data, paramObj, subCall, initFunc) {
    		if(initFunc) {
				initFunc(data, fieldKey, paramObj, subCall);
			} else {
				selectinit(data, fieldKey, paramObj, subCall);
			}
    	},//执行sql语句
    	execSql : function(sqlKey, filterParam){				 
  		 	var  param = {
  		 		"entityName" :	sqlKey
  		 	};
  		 	$.extend(param,filterParam);
  		 	var result = 0;
  			var opt = {
  					data:param,
  					async:false,
  					url:__ctx+"/gs/gs-mng!execSql.action",
  					success:function(data){
  						result=data.msg;
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
  			return result;
    	},//异步执行sql语句
    	execSqlAsync : function(sqlKey, filterParam, callFunc){				 
  		 	var  param = {
  		 		"entityName" :	sqlKey
  		 	};
  		 	$.extend(param,filterParam);
  		 	var result = 0;
  			var opt = {
  					data:param,
  					url:__ctx+"/gs/gs-mng!execSql.action",
  					success:function(data){
  						if(callFunc) {
  							callFunc(data.msg);
  						}
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
    	},//批量执行sql，params格式为[{"sqlKey":{param}}, {"sqlKey2":{param2}}]
    	execBatchSql : function(params){				 
  		 	var result = 0;
  			var opt = {
  					data:{"jsonSenddata":JSON.stringify(params)},
  					async:false,
  					url:__ctx+"/gs/gs-mng!execBatchSql.action",
  					success:function(data){
  						result=data.msg;
  					}
  			};
  		  	fnFormAjaxWithJson(opt, true);
  			return result;
    	},//显示表格行
    	showTableRow : function(rowIndex, tableIndex) {
    		if($.isNumeric(rowIndex)) {
    			var tableObj = null;
    			if($.isNumeric(tableIndex)) {
    				tableObj = $("table.module_form").eq(tableIndex);
    			} else {
    				tableObj = $("table.module_form").first();
    			}
    			tableObj.find("tr").eq(rowIndex).show();
    		} else {
    			alert("rowIndex必须为数字");
    		}
    	},//隐藏表格行
    	hideTableRow : function(rowIndex, tableIndex) {
    		if($.isNumeric(rowIndex)) {
    			var tableObj = null;
    			if($.isNumeric(tableIndex)) {
    				tableObj = $("table.module_form").eq(tableIndex);
    			} else {
    				tableObj = $("table.module_form").first();
    			}
    			tableObj.find("tr").eq(rowIndex).hide();
    		} else {
    			alert("rowIndex必须为数字");
    		}
    	},//根据json对象获取主表字段映射值
    	getMappedVal:function(fieldKey, jsonMap) {
    		var fieldVal = jwpf.getFormVal(fieldKey);
    		if($.isNumeric(fieldVal)) {
    			return jsonMap[fieldVal.toString()];
    		} else {
    			if(fieldVal) {
    				return jsonMap[fieldVal];
    			} else {
    				return "";
    			}
    		}
    	},//当前日期时间
    	now:function(dp) {
    		var p = dp || "yyyy-MM-dd";
    		return new XDate().toString(p);
    	},
    	showTab : function(tabIndex,title){
        	var tb = $('#form_tabs_'+tabIndex).tabs('getTab',title);
    		tb.panel("options").tab.show();
    		$('#form_tabs_'+tabIndex).tabs('select',title);
    	},
    	hideTab : function(tabIndex,title){
    		var tb = $('#form_tabs_'+tabIndex).tabs('getTab',title);
    		tb.panel("options").tab.hide();
    	},
    	dataHandling : function(newVal,precision){
	    	if(isNaN(newVal)){
				newVal=0;
			}else{
				if(parseInt(newVal)!=newVal){
					if(precision){
						precision = parseInt(precision);
						//newVal = newVal.toFixed(precision);
						newVal = accounting.toFixed(newVal, precision);
					}else{
						//newVal = newVal.toFixed(2);
						newVal = accounting.toFixed(newVal, 2);
					}
				}
			}
	    	return newVal;
    	},//设置子表字段只读
    	setTableFieldReadonly:function(tableKey, fieldKey, isReadonly){
			var object = $("#" + tableKey);
			if(object.length) {
				var fieldOpt = object.edatagrid("getColumnOption", fieldKey);
				if(fieldOpt && fieldOpt.editor) {
					var editor = fieldOpt.editor;
					var readOnlyAttr = "readonly";
					switch(editor.type) {
						case "newcheckbox":
						case "newradiobox":
							readOnlyAttr = "disabled";
							break;
						default:
							readOnlyAttr = "readonly";
							break;
					}
					if(editor.options) {
						if(isReadonly) {
							editor.options[readOnlyAttr] = true;
						} else {
							delete editor.options[readOnlyAttr];
						}
					}
				}
			}
		},//打开URL链接地址
		doOpenUrl:function(title, url) {
			if(top.addTab) {
				top.addTab(title, url);
			} else {
				window.open(url, title);
			}
		},//执行动态函数体
		execFunction:function(fBody, param) {
			var callFunc = new Function("param",fBody);
			return callFunc(param);
		},//保存变更日志
		doSaveChangeLog:function(param) {
			var dp = {
				"moduleNameCn" : "",//模块中文名
				"moduleNameEn" : "",//模块英文名
				"entityName" : "",//实体名
				"entityId" : "",//实体ID
				"subEntityId":"",//子实体ID
				"tableNameCn":"",//表中文名
				"tableNameEn":"",//表英文名
				"fieldNameCn" : "",//变更字段
				"fieldNameEn" : "",//变更字段英文
				"beforeVal" : "",//变更前值
				"afterVal" : "",//变更后值
				"changeDesc" : "",//变更详情
				"onAfterSave":null //保存日志后调用方法
			};
			$.extend(dp, param);
			var opt = {
				data : dp,
				async : false,
				url : __ctx + "/sys/log/change-log!save.action",
				success : function(data) {
					if(data.msg && dp.onAfterSave) {
						dp.onAfterSave(data.msg);
					}
				}
			};
			fnFormAjaxWithJson(opt, true);
		},//获取本地接口返回数据 param-url,param-callFunc
		doGetLocalLinkResult:function(param, callFunc) {
			$(document.body).mask("Please Waiting...正在努力读取数据中");
			var dp = $.extend(true, {}, param);
			var url = dp.url || "http://127.0.0.1:8310";
			var opt = {
					type:"post",
					data : dp,
					dataType:'jsonp',
					async : false,//jsonp模式下，同步异步无效
					url : url,
					success : function(data) {
						$(document.body).unmask();
						if(data.flag == "1") { //成功返回结果
							if(callFunc) callFunc(data.msg);
						} else {
							alert("获取数据出错了："+data.msg);
						}
					},
					error:function(XMLHttpRequest, textStatus, errorThrown) {
						$(document.body).unmask();
						alert("出错了！" + errorThrown);
					}
			};
			$.ajax(opt);
		},//初始化
		doInitDatagrid:function(dgId, jsonData) {
			var data = jsonData["list"];	
			var mapping = jsonData["map"];
		  	var fields = [];
		   $.each(mapping,function(i,n){
			   var field = {width:n.width||100};
			   field["field"] = n.key;
			   field["title"] = n.caption;
			   if(n.formatter) {
				   field["formatter"] = n.formatter;
			   }
			   if(n.order == "hidden" || n.hidden) {
				   field["hidden"] = true;
			   }
			   if(n.align) {
				   field["align"] = n.align;
			   }
			   fields.push(field);
		   });	   
		   var fColumns=[];
		   var field={};
		   field["field"]="ck";
		   field["checkbox"]=true;
		   fColumns.push(field);		   
		   var dataList={
	    		   "total":data.length,                                                      
				   "rows":data
	       };
		   var dgList = $("#"+dgId);
		   dgList.datagrid({  
			    nowrap: false,
				striped: true,	
				rownumbers:true,
				//idField:'key',  
				frozenColumns:[fColumns],
				columns:[fields]					 							
		   });  
		   dgList.datagrid("loadData",dataList);
		},//打开datagrid窗口，展示列表数据
		doOpenDataGridWinNew:function(dgParam) {
			var defaultParam = {
			"title":"结果列表",
			"width":800,
			"height":600,
			"isMaxWin":false,
			"data":{"list":[],"map":[]},
			"onAfterSure":null};
			var param = $.extend(defaultParam, dgParam);
			var win = '<div style="text-align: center;" class="easyui-layout" fit="true"><div region="center" border="false"><table id="__resultList__"></table></div></div>';
			var winTitle = param.title;
			var winWid = param.width;
			var winHei = param.height;
			var winMax = param.isMaxWin;
			var callFunc = param.onAfterSure;
			var p = $("<div style='overflow: auto;'/>").appendTo("body");
			p.dialog({
				id:'_datagrid_win_',
				title:winTitle,
				content: win,
				width:winWid,
				height:winHei,
				closable:true,
				collapsible:true,
				maximizable:true,
				minimizable:false,
				maximized:winMax,
				resizable:true,
				modal: true,
				shadow: false,
				cache:false,
				closed:true,
				onOpen:function() {
					jwpf.doInitDatagrid("__resultList__",param.data);
				},
				onClose:function() {
					$("#_datagrid_win_").dialog("destroy");
				},
				buttons:[{
					text:'确定',
					iconCls:'icon-ok',
					handler:function(){
						if(callFunc) {
							var rows = $("#__resultList__").datagrid('getChecked');
							var result = callFunc(rows);
							if(result) $("#_datagrid_win_").dialog("close");
						}
					}
				},{
					text:'取消',
					iconCls:'icon-cancel',
					handler:function(){
						$("#_datagrid_win_").dialog("close");
					}
				}]
			});
			$("#_datagrid_win_").dialog("open");
		},//触发下移事件
		triggerDatagridDownEvent:function(obj) {
			var e = jQuery.Event("keydown");
			e.keyCode = 40;
			$(obj).trigger(e);
			return false;
		},//保存子表数据，在主表数据存在情况下
		doSaveSubObj: function(tableKey, entityName, type, callFunc, saveIds, masterId) {
		 		var jsons = {"id":masterId || parseInt(jwpf.getId())};
		 		var rows = false;
		 		if(type == "TreeGrid") {
		 			rows = cSaveObjTree(tableKey);
		 		} else {
		 			rows = cSaveObj(tableKey);
		 		}
		        if(rows === false) {return;}
		        if(saveIds && saveIds.length) {
		        	var cr = rows["updated"];
		        	var upRows = [];
		        	if(cr && cr.length) {
		        		for(var i=0,len=cr.length;i<len;i++) {
		        			var obj = cr[i];
		        			if($.inArray(obj["id"], saveIds)>=0) {
		        				upRows.push(obj);
		        			}
		        		}
		        	}
		        	var newRows = {"updated":upRows};
		        	jsons['data'] = newRows;
		        	jsons["saveType"] = "saveModify";//只保存修改数据的模式
		        } else {
		        	jsons['data'] = rows;//全部数据保存
		        }
				var jsonSavedata = JSON.stringify(jsons);
				var options = {
					url : __ctx + '/gs/gs-mng!saveEntity.action?entityName=' + entityName,
					async : false,
					data : {
						"jsonSaveData" : jsonSavedata
					},
					success : function(data) {
						$("#" + tableKey).edatagrid("load");//刷新子表
						if(callFunc) {
							callFunc();
						}
					}
				};
				fnFormAjaxWithJson(options);
		},//开启指定条件行的可编辑性
		enableEditRow:function(tableKey, attrKey, attrVal, isAutoFocus, focusField) {
			var dg = $("#"+tableKey);
			var param = {
				"attrKey":attrKey,
				"attrVal":attrVal,
				"isAutoFocus":isAutoFocus,
				"field":focusField
			};
			dg.edatagrid("enableEditRowByAttr",param);
		},//随机数，minVal为下限，maxVal为上限，scale为需要保留的小数位
		getRandom:function(minVal,maxVal,scale){
		    var t1 = minVal,t2 = maxVal, t3 = scale;
		    if(!$.isNumeric(t1)) {t1=0;}
		    if(!$.isNumeric(t2)) {t2=1;}
		    if(!$.isNumeric(t3)) {t3=0;}
		    t3 = t3>15?15:t3; // 小数位不能大于15位
		    var ra = Math.random() * (t2-t1)+t1,du=Math.pow(10,t3);
		    ra = Math.round(ra * du)/du;
		    return ra;
		},//启用超链接
		enableLinkButton: function(jqObj) {
			var jqElements = jqObj || [];
            if (jqElements.length > 0) {
                jqElements.each(function(){
                    if ($(this).hasClass('link-disabled')) {
                    	var backStore = $(this).data("$backup$");
                    	var itemData = backStore || {};
                        if(itemData.target == this){
                            //恢复超链接
                            if (itemData.href) {
                                $(this).attr("href", itemData.href);
                            }
                            //回复点击事件
                            if (itemData.onclicks) {
                                for (var j = 0; j < itemData.onclicks.length; j++) {
                                    $(this).bind('click', itemData.onclicks[j]);
                                }
                            }
                            //设置target为null，清空存储的事件处理程序
                            itemData.target = null;
                            itemData.onclicks = [];
                            $(this).removeClass('link-disabled');
                        }
                    }
                });
            }
		},//禁用超链接
		disableLinkButton: function(jqObj) {
			var jqElements = jqObj || [];
            if (jqElements.length > 0) {
                jqElements.each(function(){
                    if (!$(this).hasClass('link-disabled')) {
                        var backStore = {};
                        backStore.target = this;
                        backStore.onclicks = [];
                        //处理超链接
                        var strHref = $(this).attr("href");
                        if (strHref) {
                            backStore.href = strHref;
                            $(this).attr("href", "javascript:void(0)");
                        }
                        //处理直接耦合绑定到onclick属性上的事件
                        var onclickStr = $(this).attr("onclick");
                        if (onclickStr && onclickStr != "") {
                            backStore.onclicks[backStore.onclicks.length] = new Function(onclickStr);
                            $(this).attr("onclick", "");
                        }
                        //处理使用jquery绑定的事件
                        var eventDatas = $(this).data("events") || $._data(this, 'events');
                        if (eventDatas && eventDatas["click"]) {
                            var eventData = eventDatas["click"];
                            for (var i = 0; i < eventData.length; i++) {
                                backStore.onclicks[backStore.onclicks.length] = eventData[i]["handler"];
                                $(this).unbind('click', eventData[i]["handler"]);
                                i--;
                            }
                        }
                        $(this).data("$backup$", backStore);
                        $(this).addClass('link-disabled');
                    }
                });
            }
		},//数字转大写
		toDigitUppercase : function(n) {
		    var fraction = ['角', '分'];
		    var digit = [
		        '零', '壹', '贰', '叁', '肆',
		        '伍', '陆', '柒', '捌', '玖'
		    ];
		    var unit = [
		        ['元', '万', '亿'],
		        ['', '拾', '佰', '仟']
		    ];
		    var head = n < 0? '欠': '';
		    n = Math.abs(n);
		    var s = '';
		    for (var i = 0; i < fraction.length; i++) {
		        s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i]).replace(/零./, '');
		    }
		    s = s || '整';
		    n = Math.floor(n);
		    for (var i = 0; i < unit[0].length && n > 0; i++) {
		        var p = '';
		        for (var j = 0; j < unit[1].length && n > 0; j++) {
		            p = digit[n % 10] + unit[1][j] + p;
		            n = Math.floor(n / 10);
		        }
		        s = p.replace(/(零.)*零$/, '')
		             .replace(/^$/, '零')
		          + unit[0][i] + s;
		    }
		    return head + s.replace(/(零.)*零元/, '元')
		                   .replace(/(零.)+/g, '零')
		                   .replace(/^整$/, '零元整');
		},//审核通过或取消审核
		doStartOrCancelApprove:function(entityName, id, approveFlag,callFunc){
			var ids = [];
			if($.isArray(id)) {
				ids = id;
			} else {
				ids.push(id);
			}
		   	var options = {
		             url : __ctx+'/gs/process!'+approveFlag+'.action?entityName='+entityName,
		             async : false,
                     traditional:true,
		             data : {
		            	 "ids" : ids
		             },
		             success : function(data) {
		            	 if(data.msg) {
		            		 if(callFunc) { 
		            			 callFunc();
		            	     }
						 }
		             }
		        };
		        fnFormAjaxWithJson(options, true);
		 },//读通知
		 doReadNotice:function(noticeId, isReadAll, callFunc) {
			 var options = {
						url : __ctx + '/sys/notice/notice!doReadNotice.action',
						async : false,
						data : {
							"id" : noticeId,
							"isReadAll": isReadAll || false
						},
						success : function(data) {
							if(data.msg) {
			            		 if(callFunc) { 
			            			 callFunc();
			            	     }
							}
						}
			 };
			 fnFormAjaxWithJson(options, true); 
		 },//刷新portal页面panel方法
		 doRefreshPortalPanel:function(title) {
			 var ifr = window.top.$("#ifr_portal");
			 if(ifr.length) {
				 var win = ifr[0].contentWindow.window;
				 win.$("div.panel-title:contains('"+title+"')").next().find("a.icon-reload").trigger("click");
			 }
		 },//数字转科学计数法
		 toExponential:function(num, bitNum) {
			 var n = parseFloat(num);
			 var e = n.toExponential(bitNum);
			 return e.toString().toUpperCase();
		 },//返回截取字符串
		 getFormatSpanText:function(content, len, suffix) {
			 if(content) {
				 if(content.length > len) {
					 var str = content.substring(0, len) + (suffix || "...");
					 return "<span title=\"" + content + "\">" + fnFormatText(str) + "</span>";
				 } else {
					 return fnFormatText(content);
				 }
			 } else {
				 return "";
			 }
		 },//查看修订日志
		 doViewReviseLog:function(id, mn, en, fn, sid) {
			 var strUrl = "/sys/log/revise-log.action?mn=";
			 strUrl += mn+"&id=" + id +"&en=" +(en?en:"")+"&fn="+(fn?fn:"")+"&sid="+(sid?sid:"");
			 openFormWin({
				 "title":"修订日志",
				 "width":800,
				 "height":400,
				 "url":strUrl,
				 "showTools":false
			 });
		 },	//将主表控件设置成可修订状态
		 doSetFormToRevise:function(entityName, moduleEn, moduleCn, jqSelector){
			    var jqs = jqSelector || ".jwpf-revise";
		    	$(jqs).each(function(i) {
		    		var cid = $(this).attr("id");
		    		var isTextField = $(this).hasClass("easyui-dialoguewindow") || false;//是否包含text字段
		    		var btn = $('<a href="javascript:void(0);" class="easyui-linkbutton bt-extra validInView" plain="true" iconCls="icon-edit" title="修订" forId="'+ cid +'"></a>');
		    		var obja = $("a[forId="+cid+"]");
		    		if(obja.length) obja.remove();
		    		var parentObj = $(this).parent();
		    		if(parentObj.hasClass("input-group")) {parentObj = parentObj.parent();}
		    		btn.appendTo(parentObj);
		    		btn.linkbutton({});
		    		btn.toggle(function() { //启用修订
		    			var bid = $(this).attr("forId");
		    			//$("#"+bid).removeAttr("disabled");
		    			jwpf.setFormFieldReadonly(bid, false);
		    			var oldVal = jwpf.getFormVal(bid);
		    			var oldCap = "";
		    			if($.isPlainObject(oldVal)) {
		    				oldCap = oldVal["text"];
		    				oldVal = oldVal["id"];
		    			} else {
		    				oldCap = jwpf.getFormComboFieldCaption(bid);
		    			}
		    			$(this).data("oldVal",oldVal);
		    			$(this).data("oldCap",oldCap);
		    			$(this).attr("title","保存");
		    			$(this).linkbutton({iconCls:"icon-save"});
		    		},function() { //保存修订
		    			var _thibtn_ = $(this);
		    			var bid = _thibtn_.attr("forId");
	    			    var oldVal = _thibtn_.data("oldVal");
	    			    var oldCap = _thibtn_.data("oldCap");
	    				var newVal = jwpf.getFormVal(bid);
		    			var newCap = "";
		    			if($.isPlainObject(newVal)) {
		    				newCap = newVal["text"];
		    				newVal = newVal["id"];
		    			} else {
		    				newCap = jwpf.getFormComboFieldCaption(bid);
		    			}
		    			var dataType = $("#"+bid).attr("jwDataType");
		    			var fnc = $("#"+bid).attr("jwTitle");
		    			var field = "EQ"+dataType+"_" + bid;
		    			var jwEn = $("#"+bid).attr("jwEn");
		    			var sid = jwpf.getId();
		    			if(jwEn) {
		    				entityName = jwEn;
		    				var tableKey = jwEn.split(".")[1];
		    				sid = jwpf.getFormVal(tableKey+"_id");
		    			}
		    			var beforeFunc = window["onBeforeReviseFor_form_"+bid];
						if(beforeFunc && beforeFunc(newVal, oldVal)=== false) {//修订前事件
							return false;
						}
		    			jwpf.updateField(entityName, field, sid, newVal, function() {
		    				if(isTextField) {
		    					jwpf.updateField(entityName, "EQS_" + bid + "text", sid, newCap);//更新text字段
		    				}
		    				var bv = oldVal;
		    				var av = newVal;
		    				if(oldCap) bv = oldVal + "-" + oldCap;
			    			if(newCap) av = newVal + "-" + newCap;
			    			//var fnc = $("label[for="+bid+"]").text();
			    			var tnc = _thibtn_.closest("div.easyui-tabs").find("div.tabs-header li.tabs-selected span.tabs-title").text();
		    				var opt = {
									"moduleNameCn" : moduleCn,
									"moduleNameEn":moduleEn, 
									"entityName" : entityName, 
									"entityId" : jwpf.getId(),
									"tableNameCn": tnc,
									"fieldNameCn" : fnc,
									"fieldNameEn" : bid,
									"beforeVal" : bv,
									"afterVal" : av,
									"changeDesc" : "修订操作"
							};
							jwpf.doSaveChangeLog(opt);
							
							jwpf.setFormFieldReadonly(bid, true);
							//$("#"+bid).attr("disabled","disabled");
							_thibtn_.removeData("oldVal");
							_thibtn_.removeData("oldCapt");
							_thibtn_.attr("title","修订");
							_thibtn_.linkbutton({iconCls:"icon-edit"});
							
							var eventKey = "onAfterReviseFor_form_"+bid;
							var func = window[eventKey];
							if(func) {
								func(newVal, oldVal, newCap, oldCap);
							}
		    			});
		    		});
		    	});
		    },//子表字段启用修订
		    doSetTableToRevise:function(tableKey, entityName, moduleEn, moduleCn, reviseFields) {
		    	$('div.datagrid-body td[field]', $("#"+tableKey).parent()).each(function() {
		    		var fn = $(this).attr("field");
		    		var fdOpt = $("#"+tableKey).edatagrid("getColumnOption",fn);
		    		if((!reviseFields && fdOpt.jwRevise) || (reviseFields && $.inArray(fn,reviseFields)>=0)) {
						if(fdOpt.editor && fdOpt.editor.options) {
							var eOpt = fdOpt.editor.options;
							if(eOpt.readonly) {
								eOpt.readonly = false;
							}
							if(eOpt.disabled) {
								eOpt.disabled = false;
							}
						}
		    			var tnc = $(this).closest("div.easyui-tabs").find("div.tabs-header li.tabs-selected span.tabs-title").text();
		    			var that = $(this);
		    			$(this).qtip({
						    content: '<a href="javascript:void(0)" class="validInView l-btn l-btn-plain" plain="true" title="修订" ><span class="l-btn-left l-btn-icon-left"><span class="l-btn-text">修订</span><span class="l-btn-icon icon-edit">&nbsp;</span></span></a>',
						    position: {
						    	my: 'bottom left',
						        at:"top center"
						    },
						    hide: {
						        delay: 1000
						    },
						    events: {
						      	show: function(event, api) {
									var btn = api.elements.content;
									var target = api.elements.target;
									//var target = that;
									var rowIndex = target.closest("tr.datagrid-row").attr("datagrid-row-index");
									var spBtn = $("span>span.l-btn-text", btn);
									if(target.hasClass("datagrid-cell-editing")) {//可编辑，保存操作
									    spBtn.text("保存");
									    //btn.attr("保存");
									    spBtn.next().removeClass("icon-edit").addClass("icon-save");
										btn.unbind("click").click(function() {
											$("#"+tableKey).edatagrid("endCellEdit",{"index":rowIndex,"field":fn});
											var oldCap = btn.data("oldCap");
											var oldVal = btn.data("oldVal");
											//var newCap = target.text();
											var newCap = $("#"+tableKey).parent().find("tr[datagrid-row-index="+rowIndex+"] td[field=" + fn + "]").text();
											var rowData = jwpf.getTableVal(tableKey,rowIndex);
											var newVal = rowData[fn];
											var beforeFunc = window["onBeforeReviseFor_"+tableKey+"_"+fn];
											if(beforeFunc && beforeFunc(newVal, oldVal, rowIndex, tableKey, rowData)=== false) {//修订前事件
												return false;
											}
											var sid = rowData["id"];
											var dataType = fdOpt.jwDataType;
							    			var field = "EQ"+dataType+"_" + fn;
							    			var isTextField = (fdOpt.editor.type=="dialoguewindow") || false;//是否包含text字段
							    			jwpf.updateField(entityName, field, sid, newVal, function() {
							    				if(isTextField){
							    					jwpf.updateField(entityName, "EQS_" + fn + "text", sid, newCap);//更新text字段
							    				}
							    				var bv = oldVal;
							    				var av = newVal;
							    				if(oldCap != oldVal) bv = oldVal + "-" + oldCap;
								    			if(newCap != newVal) av = newVal + "-" + newCap;
								    			var fnc = fdOpt.title;
								    			tnc = tnc + "第" + (parseInt(rowIndex)+1) + "行记录";
												var opt = {
														"moduleNameCn" : moduleCn,
														"moduleNameEn":moduleEn,
														"entityName" : entityName,
														"entityId" : jwpf.getId(),
														"subEntityId": sid,
														"tableNameCn": tnc,
														"fieldNameCn" : fnc,
														"fieldNameEn" : fn,
														"beforeVal" : bv,
														"afterVal" : av,
														"changeDesc" : "修订操作"
												};
												jwpf.doSaveChangeLog(opt);
												
												var eventKey = "onAfterReviseFor_"+tableKey+"_"+fn;
												var func = window[eventKey];
												if(func) {
													func(newVal, oldVal, newCap, oldCap, rowIndex, tableKey, rowData);
												}
												
												jwpf.doSetTableToRevise(tableKey, entityName, moduleEn, moduleCn, reviseFields);
							    			});
										});
									} else{ //修订操作
									    spBtn.text("修订");
									    //btn.attr("修订");
									    spBtn.next().removeClass("icon-save").addClass("icon-edit");
										btn.unbind("click").click(function() {
											var oldCap = target.text();
											btn.data("oldCap", oldCap);
											var oldVal = jwpf.getTableVal(tableKey,rowIndex,fn);
											btn.data("oldVal", oldVal);
											$("#"+tableKey).edatagrid("beginCellEdit",{"index":rowIndex,"field":fn});
										});
									}
						        }
						    }
						});	
		    		}
		    	});
		    },//退出修订模式
		    doCancelSetFormToRevise:function(jqSelector) {
		    	var jqs = jqSelector || ".jwpf-revise";
		    	$(jqs).each(function(i) {
		    		var cid = $(this).attr("id");
		    		var obja = $("a[forId="+cid+"]");
		    		if(obja.length) obja.remove();
		    	});
		    },//初始化修订查看
		    doInitReviseView:function(entityName, moduleEn, jqSelector) {
		    	var jqs = jqSelector || ".jwpf-revise";
		    	$(jqs).each(function(i) {
		    		var cid = $(this).attr("id");
		    		var btn = $('<a href="javascript:void(0);" class="easyui-linkbutton bt-extra validInView" plain="true" iconCls="icon-new-doc-ico" title="修订日志" forViewId="'+ cid +'"></a>');
		    		var obja = $("a[forViewId="+cid+"]");
		    		if(obja.length) obja.remove();
		    		var parentObj = $(this).parent();
		    		if(parentObj.hasClass("input-group")) {parentObj = parentObj.parent();}
		    		btn.appendTo(parentObj);
		    		//btn.appendTo($(this).parent());
		    		btn.linkbutton({});
		    		btn.click(function() {
		    			var bid = $(this).attr("forViewId");
		    			var en = $("#"+bid).attr("jwEn") || entityName;
		    			jwpf.doViewReviseLog(jwpf.getId(),moduleEn,en, bid);
		    		}); 
		    	});
		    },//取消修订查看按钮
		    doCancelReviseView:function(jqSelector) {
		    	var jqs = jqSelector || ".jwpf-revise";
		    	$(jqs).each(function(i) {
		    		var cid = $(this).attr("id");
		    		var obja = $("a[forViewId="+cid+"]");
		    		if(obja.length) obja.remove();
		    	});
		    },//批量启动流程
		    doBatchStartProcess:function(ids, entityName, userIds, callFunc, isForSingleUser) {
		    	var param = isForSingleUser?"ForSingleUser":"";
		    	var options = {
						url : __ctx+'/gs/process!batchStartProcess'+param+'.action',
						traditional:true,
						data : {
							"entityName": entityName,
							"ids" : ids,
							"userIds": (userIds||"")
						},
						async : false,
						success : function(data) {
							if(data.msg) {
								var attUrl = "/sys/workflow/agree-process.action?roles="+encodeURIComponent(data.msg) 
										+ "&taskIds=" + ids.join(",");
								top.openFormWin({
									"title":"选择下个环节用户",
									 "width":500,
									 "height":450,
									 "url":attUrl,
									 "onAfterSure":function(win) {
										 if(win.getIds) {
											 var uids = win.getIds();
											 if(uids) {
												 jwpf.doBatchStartProcess(ids, entityName, uids, callFunc);
												 return true; 
											 }
										 }
									 }
								});
							} else {
								$.messager.alert("提示信息", "批量操作成功！",'info');
								if(callFunc) { setTimeout(callFunc, 2000);}
							}
						}
			    	};
			    	fnFormAjaxWithJson(options, true);
		    },//批量同意
		    doBatchAgreeProcess:function(taskIds, userIds, callFunc, isForSingleUser) {
		    	var param = isForSingleUser?"ForSingleUser":"";
		    	var options = {
					url : __ctx+'/gs/process!batchAgree'+param+'.action',
					traditional:true,
					data : {
						"taskIds" : taskIds,
						"userIds": (userIds||"")
					},
					async : false,
					success : function(data) {
						if(data.msg) {
							var attUrl = "/sys/workflow/agree-process.action?roles="+encodeURIComponent(data.msg) 
									+ "&taskIds=" + taskIds.join(",");
							top.openFormWin({
								"title":"选择下个环节用户",
								 "width":500,
								 "height":450,
								 "url":attUrl,
								 "onAfterSure":function(win) {
									 if(win.getIds) {
										 var uids = win.getIds();
										 if(uids) {
											 jwpf.doBatchAgreeProcess(taskIds, uids, callFunc);
											 return true;
										 }
									 }
								 }
							});
						} else {
							$.messager.alert("提示信息", "批量操作成功！",'info');
							if(callFunc) { setTimeout(callFunc, 2000);}
						}
					}
		    	};
		    	fnFormAjaxWithJson(options, true);
		    },//批量不同意
		    doBatchDisAgreeProcess:function(taskIds, userIds, callFunc, isForSingleUser) {
		    	var param = isForSingleUser?"ForSingleUser":"";
		    	var options = {
					url : __ctx+'/gs/process!batchDisAgree'+param+'.action',
					traditional:true,
					data : {
						"taskIds" : taskIds,
						"userIds": (userIds||"")
					},
					async : false,
					success : function(data) {
						if(data.msg) {
							var attUrl = "/sys/workflow/agree-process.action?roles="+encodeURIComponent(data.msg) 
									+ "&taskIds=" + taskIds.join(",");
							top.openFormWin({
								"title":"选择下个环节用户",
								 "width":500,
								 "height":450,
								 "url":attUrl,
								 "onAfterSure":function(win) {
									 if(win.getIds) {
										 var uids = win.getIds();
										 if(uids) {
											 jwpf.doBatchAgreeProcess(taskIds, uids, callFunc);
											 return true;
										 }
									 }
								 }
							});
						} else {
							$.messager.alert("提示信息", "批量操作成功！",'info');
							if(callFunc) { setTimeout(callFunc, 2000);}
						}
					}
		    	};
		    	fnFormAjaxWithJson(options, true);
		    },//检查是否内网地址，true-是，false-不是，null-获取出错
	    	checkInnerIp : function() {
	  		 	var result = null;
	  			var opt = {
	  					async:false,
	  					url:__ctx+"/sys/account/online-user!checkInnerIp.action",
	  					success:function(data){
	  						result=data.msg;
	  					}
	  			};
	  		  	fnFormAjaxWithJson(opt, true);
	  			return result;
	    	},//存储过程返回值
	    	callProcWithResult : function(key, filterParam, callFunc){
	    		var result = null;
	    		var isSP = false;
	    		var  param = {
	  		 			"procKey" :	key
	  		 	};
	  		 	$.extend(param,filterParam);
	    	   	var options = {
	    	   			data:param,
	  					async:false,
	    	            url : __ctx+'/gs/gs-mng!callProcedureWithResult.action',
	    	            success : function(data) {
	    	            	 result = data.msg;
	    	            	 if(callFunc) {
								callFunc(data.msg);
							 }
	    	             }
	    	    };
	    	    fnFormAjaxWithJson(options,!isSP);
	    	    return result;
	    	},//设置数据共享用户
	    	doSetDataShareUser:function(entityName, ids, onAfterSetSuccess) {
	    		if(ids && ids.length) {
	    			var attUrl = "/sys/authority/data-share-config.action?id="+ids[0]
							   + "&entityName=" + entityName;
					openFormWin({
						 "title":"设置数据共享用户",
						 "width":500,
						 "height":500,
						 "url":attUrl,
						 "onAfterSure":function(win) {
							 var dataObj = {
								 "entityName":entityName,
								 "entityIds":ids
							 };
							 var flag = win.saveDsc(dataObj);
							 if(flag) {
								 if(onAfterSetSuccess) onAfterSetSuccess();
								 if(window.doQuery) doQuery();
								 return true;
							 }
						 }
					});
	    		} else {
	    			alert("请选择要操作的数据！");
	    		}
	    	},//在查看状态下新增行数据及保存
	    	doAddRowInViewAndSave:function(tableKey, entityName, btObj, saveTitle, onAfterSave, onBeforeSave) {
	    		var oldTitle = $("span.l-btn-text", btObj).text();
	    		if(oldTitle == saveTitle) {//保存
	    			if(onBeforeSave && onBeforeSave() === false) {
	    				return false;
					}
	    			jwpf.doSaveSubObj(tableKey, entityName, null, function() {
	    				var inde = $(btObj).data("index");
	    				$("#"+tableKey).edatagrid("disableEditRow", inde);
	    				if(onAfterSave) {
	    					onAfterSave();
	    				}
	    			});
	    			$("span.l-btn-text", btObj).text($(btObj).data("title"));
	    		} else {//新增
	    			$(btObj).data("title",oldTitle);
	    			$("span.l-btn-text", btObj).text(saveTitle);
		    		var rows = jwpf.getTableVal(tableKey,false);
		    		var index = rows.length;
		    		$(btObj).data("index",index);
		    		$("#"+tableKey).edatagrid("addRow");
		    		$("#"+tableKey).edatagrid("enableEditRow",{index:index});
	    		}
	    	},//在查看状态下修改行数据及保存
	    	doModifyRowInViewAndSave:function(tableKey, entityName, btObj, saveTitle, onAfterSave, onBeforeSave) {
	    		var oldTitle = $("span.l-btn-text", btObj).text();
	    		if(oldTitle == saveTitle) {//保存
					if(onBeforeSave && onBeforeSave() === false) {
						return false;
					}
	    			jwpf.doSaveSubObj(tableKey, entityName, null, function() {
	    				var inde = $(btObj).data("index");
	    				$("#"+tableKey).edatagrid("disableEditRow", inde);
	    				if(onAfterSave) {
	    					onAfterSave();
	    				}
	    			});
	    			$("span.l-btn-text", btObj).text($(btObj).data("title"));
	    		} else {//修改
	    			var rows = jwpf.getTableVal(tableKey,true);
	    			if(rows.length != 1) {
	    				alert("请选择1条记录修改！");
	    				return;
	    			}
	    			$(btObj).data("title",oldTitle);
	    			$("span.l-btn-text", btObj).text(saveTitle);
		    		var index = $("#"+tableKey).edatagrid("getRowIndex", rows[0]);
		    		$(btObj).data("index",index);
		    		$("#"+tableKey).edatagrid("enableEditRow",{index:index});
	    		}
	    	},//平行样计算
	    	doCalcPingXingYang:function(tableKey, pxyNum, calcField, setField, calcFunc, fixNum, preFunc, filterFunc) {
	    		var rows = jwpf.getTableVal(tableKey, false);
	    		if(filterFunc) {
	    			rows = $.grep(rows, function(row,i){
	    				return filterFunc(row);
	    			});
	    		}
	    		var calcValues = [];
	    		var startIndex = 0;
	    		if(rows.length) {
	    			startIndex = $("#"+tableKey).edatagrid("getRowIndex", rows[0]);
	    		}
	    		for(var i=0,len=rows.length; i<len; i++) {
	    			var row = rows[i];
	    			calcValues.push(row);
	    			if((i+1)%pxyNum == 0 || i == (len-1)) {//平行样数或最后一个
	    				var result = 0;
	    				if(calcFunc) {
	    					try {
	    						result = calcFunc(calcValues);
	    					} catch(e) {alert("自定义计算公式出现异常，请检查计算函数和数据！");}
	    				} else {//默认平均值计算
	    					result = jwpf.getAvgVal(calcValues,calcField,null,fixNum);
	    				}
	    				var rowData = {};
	    				rowData[setField] = (preFunc ? preFunc(result) : result);
	    				for(var j=startIndex;j<=startIndex+i;j++) {
	    					jwpf.setTableVal(tableKey,j,rowData, false);
	    				}
	    				calcValues = [];
	    				startIndex = i+1;
	    			}
	    		}
	    	}, //更新表格列标题
	    	doUpdateTableFieldTitle: function(tableKey, fieldKey, title) {
	    		$("#"+tableKey).edatagrid("getPanel")
	    			.find("div.datagrid-header td[field=\"" + fieldKey + "\"] span:first")
	    			.text(title);
	    	},
	    	doDisplayUppercaseNum:function(fieldKey) {
	    		var formatter = function(val) {
	    			return jwpf.toDigitUppercase(val);
	    		};
	    		jwpf.doDisplayTipMsg($("#"+fieldKey), formatter);
	    	},
	    	doDisplayTipMsg:function(jqObj, formatter) {
	    		jqObj.each(function() {
	    			if($(this).attr("disabled")) {
	    				if($(this).hasClass("easyui-numberbox")) {
	    					$(this).numberbox("enable");
	    				} else {
	    					$(this).removeAttr("disabled");
	    				}
	    				if(!$(this).attr("readonly")) {
	    					$(this).attr("readonly", true);
	    				}
	    			}
	    			$(this).qtip({
					    content: '<span></span>',
					    position: {
					    	my: 'bottom left',
					        at:"top center"
					    },
					    hide: {
					        delay: 500
					    },
					    events: {
					      	show: function(event, api) {
								var spanContent = api.elements.content;
								var target = api.elements.target;
								var ct = "";
								var val = target.val();
				    			if(formatter) {
				    				ct = formatter(val);
				    			}
				    			spanContent.text(ct);
					      	}
					    }
					});	
	    		});
	    	},//下顺丰订单
	    	sendSfOrder : function(querySqlKey,filterParam,updateSqlKey, updateErrSqlKey) {				 
      		 	var  param = {
      		 		"querySqlKey" :	querySqlKey,
      		 		"updateSqlKey" : updateSqlKey,
      		 		"updateErrSqlKey": updateErrSqlKey
      		 	};
      		 	$.extend(param,filterParam);
      		  	var jsonData=null;
      			var opt = {
      					data:param,
      					async:false,
      					url:__ctx+"/yk/api/yk-wu-liu!input.action",
      					success:function(data){
      						jsonData=data.msg;
      					}
      			};
      		  	fnFormAjaxWithJson(opt, true);
      			return jsonData;
	    	},//日期字符串转日期对象
			dateStrToDate:function(dateStr) {
				return new Date(dateStr.replace(/-/g, "\/"));
			},//计算时间差，返回分钟数,time2-time1
			getTimeDiff:function(time1, time2) {
				return parseInt(time2.slice(0,2))*60+parseInt(time2.slice(3,5))-(parseInt(time1.slice(0,2))*60+parseInt(time1.slice(3,5)));
			},
			//计算两个日期间的工作时间（小时）
			getWorkHours:function(opt){
				var startDate = opt.startDate, endDate = opt.endDate, startTimeAm = opt.startTimeAm,
								endTimeAm = opt.endTimeAm, startTimePm = opt.startTimePm, endTimePm = opt.endTimePm,
								workHours1Day = opt.workHours1Day;
			    var datetime1 = this.dateStrToDate(startDate), datetime2 = this.dateStrToDate(endDate);

			    var date1 = this.dateStrToDate(startDate.slice(0, 10));  //开始日期，毫秒表示
			    var date2 = this.dateStrToDate(endDate.slice(0, 10));  //结束日期，毫秒表示

			    var time1 = startDate.slice(11); //开始时间
			    var time2 = endDate.slice(11); //结束时间
			    
			    var travelHours = 0;    //保存请假分钟数
			    //开始结束时间均在一天
			    if(date1.getTime() == date2.getTime()){
			        //1.开始时间8-12点
			        if(time1 >= startTimeAm && time1 <= endTimeAm){
			            //1.1 结束时间在8-12点
			            if(time2 >= startTimeAm && time2 <= endTimeAm){
			                travelHours += this.getTimeDiff(time1, time2);
			            }
			            //1.2  结束时间在13-17点
			            if(time2 >= startTimePm && time2 <= endTimePm){
			                travelHours += this.getTimeDiff(time1, endTimeAm) + this.getTimeDiff(startTimePm, time2);
			            }

			            //1.3
			            if(time2 > endTimePm){
			                alert("结束时间不能大于" + endTimePm);
			                return 0;
			            }
			        }
			        //如果开始时间小于8点
			        if(time1 < startTimeAm){
			            alert("开始时间不能小于" + startTimeAm);
			            return 0;
			        }
			        //如果开始时间大于17点，
			        if(time1 > endTimePm){
			        	alert("开始时间不能大于" + endTimePm);
			            return 0;
			        }
			        //2.开始时间在13-17点
			        if(time1 >= startTimePm && time1 <= endTimePm){
			            //1.2  结束时间在13-17点
			            if(time2 >= startTimePm && time2 <= endTimePm){
			                travelHours += this.getTimeDiff(time1, time2);
			            }
			            //2.2 如果结束时间在17点之后，则工时累计到17点截止
			            if(time2 > endTimePm){
			                travelHours += this.getTimeDiff(time1, endTimePm);
			            }
			        }

			    } else {
			        //如果开始时间小于8点，则计算工时的时候从8点开始计算
			        if(time1 < startTimeAm){
			        	alert("开始时间不能小于" + startTimeAm);
			            return 0;
			        }
			        //如果开始时间大于17点，
			        if(time1 > endTimePm){
			        	alert("开始时间不能大于" + endTimePm);
			            return 0;
			        }

			        if(time2 < startTimeAm){
			        	alert("结束时间不能小于" + startTimeAm);
			            return 0;
			        }
			        //1.3
			        if(time2 > endTimePm){
			        	alert("结束时间不能大于" + endTimePm);
			            return 0;
			        }
			        //计算开始时间当天工时
			        if(time1>= startTimeAm && time1<= endTimeAm){
			            travelHours+= this.getTimeDiff(time1, endTimePm) - this.getTimeDiff(endTimeAm, startTimePm);
			        }
			        if(time1>= startTimePm && time1<= endTimePm){
			            travelHours+= this.getTimeDiff(time1, endTimePm);
			        }
			        //计算结束时间当天工时
			        if(time2>=startTimeAm && time2<=endTimeAm){
			            travelHours+= this.getTimeDiff(startTimeAm, time2);
			        }
			        if(time2>=startTimePm && time2<=endTimePm){
			            travelHours+= this.getTimeDiff(startTimeAm, time2) - this.getTimeDiff(endTimeAm, startTimePm);
			        }
			        //计算开始时间和结束时间相差天数  开始时间和结束时间不算进去，只计算相差的时间
			        var dd = (date2 - date1) / 1000 / 60 / 60 / 24 - 1;
			        if(dd > 0){
			            travelHours+= dd*workHours1Day*60;
			        }
			        //计算开始时间和结束时间之中有几个周六周末,然后去除这个时间
			        var wkDays = this.getWeekendDays(date1,date2);
			        if(wkDays > 0){
			            travelHours -= wkDays*workHours1Day*60;
			        }
			    }
			    return travelHours;
			},//计算日期间的双休日天数
			getWeekendDays: function(dtStart, dtEnd) {
			    if (typeof dtEnd == 'string' )
			        dtEnd = this.dateStrToDate(dtEnd);
			    if (typeof dtStart == 'string' )
			        dtStart = this.dateStrToDate(dtStart);

			    var delta = (dtEnd - dtStart) / (1000 * 60 * 60 * 24) + 1; 
			    var date1 = dtStart;
			    
			    var weekEnds = 0; 
			    for(var i = 0; i < delta; i++) { 
			        if(date1.getDay() == 0 || date1.getDay() == 6) weekEnds ++; 
			        date1 = date1.valueOf(); 
			        date1 += 1000 * 60 * 60 * 24; 
			        date1 = new Date(date1); 
			    } 
			    return weekEnds;  
			},//自定义表单显示视图{index:1,row:row,container:container,formKey:formKey}
			setFormDetailView:function(param) {
				var id = param.row["id"];
				var vUrl = __ctx + "/gs/forms/" + param.formKey +".action?notShowQuery=1&queryParams=" 
							+ encodeURI(JSON.stringify({"fix_EQL_id":id, "rowIndex":param.index}));
				var ifrKey = "ifr_" + id;
				var str = '<iframe scrolling="auto" id="' + ifrKey + '" frameborder="0" src="' + vUrl + '" style="width:100%;height:200px;overflow:hidden;"></iframe>';
				param.container.html(str);
			},//父iframe自适应高度
			changeFdvParentHeight:function() {
			       var height = $("div.datagrid-body")[0].scrollHeight+$("div.datagrid-header").height() + 10;
			       var params = getFormQueryParams();
			       var id = params["fix_EQL_id"];
			       var rowIndex = params["rowIndex"];
				   var parentIfr = window.parent.$("#ifr_" + id);
			       if(parentIfr.length) {
			       	   parentIfr.css("height", height);
			       	   window.parent.$("#queryList").datagrid('fixDetailRowHeight', rowIndex);

			       }
			}//保存数据，
			,doSaveData: function(entityName, data, referId, callFunc) {
			 		var rows = data;
					var options = {};
					if(referId) {
						var jsons = {"id":parseInt(referId)};
						var newRows = {"updated":rows};
			        	jsons['data'] = newRows;
			        	jsons["saveType"] = "saveModify";//只保存修改数据的模式
						var jsonSavedata = JSON.stringify(jsons);
						options = {
								url : __ctx + '/gs/gs-mng!saveEntity.action?entityName=' + entityName,
								async : false,
								data : {
									"jsonSaveData" : jsonSavedata
								},
								success : function(data) {
									if(callFunc) {
										callFunc(data);
									}
								}
						};
					} else {
						var jsonSavedata = JSON.stringify(data);
						options = {
								url : __ctx + '/gs/gs-mng!saveList.action?entityName=' + entityName,
								async : false,
								data : {
									"jsonSaveData" : jsonSavedata,
									"field":"id",
									"value":"true"
								},
								success : function(data) {
									if(callFunc) {
										callFunc(data);
									}
								}
						};
					}
					fnFormAjaxWithJson(options);
			},//获取表单值，form表单转成json对象
			getFormData:function(formId) {
		      	var fields = $("#"+(formId||"viewForm")).serializeArray();
		      	var jsonObj = {};
		      	$.each(fields, function(k, v) {
		      		 if (jsonObj[v.name]) {
		      			 if(v.value) {
		      				jsonObj[v.name] = jsonObj[v.name] + "," + v.value;
		      			 }
		      		 } else { 
		      			 jsonObj[v.name] = v.value || ''; 
		      		 } 
		      	}); 
		      	return jsonObj;
		    },//设置表单值
		    setFormData:function(formId, data) {
		    	var fid = formId || "viewForm";
		    	$('#' + fid).form('load', data);
		    },
		    setPageValue:function(key, value) {
				this.getOpenerWin().PageStorage.write(key, value);
			},
			getPageValue:function(key) {
				return this.getOpenerWin().PageStorage.read(key);
			},
			removePageValue:function(key) {
				this.getOpenerWin().PageStorage.remove(key);
			},
			getOpenerWin:function() {
				if(window.opener) {
					return window.opener.top;
				} else {
					return window.top;
				}
			},//判断所有函数是否一致，一致返回true
			isAllEqual:function(array){
			    if(array.length>0){
			       return !array.some(function(value,index){
			         return value !== array[0];
			       });   
			    }else{
			        return true;
			    }
			},//打开新增页面
			doOpenAddPage:function(title, entityName, addParam) {
				var ckdUrl = __ctx + "/gs/gs-mng!add.action?entityName=" + entityName;
				if(addParam) ckdUrl += "&addParams=" + encodeURIComponent(JSON.stringify(addParam));
				jwpf.doOpenUrl(title, ckdUrl);
			},//根据属性名查询实体ID
			getEntityIdByAttr:function(entityName, fieldName, fieldValue) {
				var id = "";
				if($ES.isNotBlank(fieldValue)) {
					var url = __ctx+"/gs/gs-mng!queryEntityIdByAttr.action?entityName=" + entityName;
					var opt = {
							data:{
								"field":fieldName,
								"value":fieldValue
							},
							async:false,
							url:url,
							success:function(data){
								id = data.msg;
							}
					};
					fnFormAjaxWithJson(opt);
				}
				return id;
			},//打开查看页面
			doOpenViewPage:function(title, entityName, fieldName, fieldValue) {
				if($ES.isNotBlank(fieldValue)) {
					var id = (!fieldName || fieldName === "id") ? fieldValue : this.getEntityIdByAttr(entityName, fieldName, fieldValue);
					var ckdUrl = __ctx + "/gs/gs-mng!view.action?entityName=" + entityName + "&id=" + id;
					jwpf.doOpenUrl(title + "-" + id, ckdUrl);
				}
			},
			getStatus : function() {
				return jwpf.getFormVal("status");
			},
			/**
			 * 修订数据
			 * @reviseFields-- ["module.bb.name","module.bb.ss.1"]
			 * 
			 * {"aa.bb":{"id":"1", "version":11, "checkVersion":true, "isRevise":"10", "fields": [{"name":{"id":"","text":""}},{"ss":"11"}]}}
			 */
			reviseData : function(reviseFields, callBack, checkVersion) {
				var data = {};
				var checkVer = !(checkVersion === false);
				var isRevise = jwpf.getStatus();//单据状态
				var formData = jwpf.getFormData("viewForm");
				for(var i in reviseFields) {
					var fieldKey = reviseFields[i];
					if(fieldKey) {
						var fs = fieldKey.split(".");
						if(fs.length >= 3) {
							var isMaster = (fs.length == 3)?true:false;
							var tableKey = fs[1];
							var entityName = fs[0]+"."+fs[1];
							var fkey = fs[2];
							var fkeytext = fkey + "text";
							var map = {};
							if(isMaster) {
								map = formData;
							} else {
								var rowIndex = parseInt(fs[3]);
								map = jwpf.getTableVal(tableKey, rowIndex);
							}
							if(!data[entityName]) {
								data[entityName] = {"id":parseInt(map["id"]), "version":map["version"], "checkVersion": checkVer, "isRevise":isRevise, "fields":[]};
							}
							var field = {};
							if(map[fkeytext] === undefined) {
								field[fkey] = map[fkey];
							} else {
								field[fkey] = {"id":map[fkey], "text":map[fkeytext]};
							}
							data[entityName]["fields"].push(field);
						}
					}
				}
				var jsonSavedata = JSON.stringify(data);
				var options = {
						url : __ctx + '/gs/gs-mng!revise.action?1=1&entityName=' + reviseFields[0],
						//async : false,
						data : {
							"jsonSaveData" : jsonSavedata
						},
						success : function(data) {
							var msg = data.msg;
							if(callBack) {
								callBack(data.msg);
							}
						}
				};
				fnFormAjaxWithJson(options);
			},
			getIEVersion: function() {
	            var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串  
	            var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1; //判断是否IE<11浏览器  
	            var isEdge = userAgent.indexOf("Edge") > -1 && !isIE; //判断是否IE的Edge浏览器  
	            var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf("rv:11.0") > -1;
	            if(isIE) {
	                var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
	                reIE.test(userAgent);
	                var fIEVersion = parseFloat(RegExp["$1"]);
	                if(fIEVersion == 7) {
	                    return 7;
	                } else if(fIEVersion == 8) {
	                    return 8;
	                } else if(fIEVersion == 9) {
	                    return 9;
	                } else if(fIEVersion == 10) {
	                    return 10;
	                } else {
	                    return 6;//IE版本<=7
	                }   
	            } else if(isEdge) {
	                return 'edge';//edge
	            } else if(isIE11) {
	                return 11; //IE11  
	            }else{
	                return -1;//不是ie浏览器
	            }
	        },//根据word模板导出word文档
			exportWordByTemplateAsync: function(dataSqlKey, dataFilters, fileSqlKey, fileFilters,
												updateSqlKey, updateFilters, callBack) {
				var param = {
					"data":{
						"sqlKey":dataSqlKey,
						"filters":dataFilters
					},
					"file": {
						"sqlKey":fileSqlKey,
						"filters":fileFilters
					},
					"update": {
						"sqlKey":updateSqlKey,
						"filters":updateFilters
					}
				};
				var jsonData = JSON.stringify(param);
				var options = {
					url : __ctx + '/gs/gs-mng!exportWordByTemplateAsync.action',
					data : {
						"jsonSenddata" : jsonData
					},
					success : function(data) {
						if(callBack) {
							callBack(data);
						}
					}
				};
				fnFormAjaxWithJson(options);
			},
			exportWordByTemplate: function(dataSqlKey, dataFilters, fileSqlKey, fileFilters) {
				var param = {
					"data":{
						"sqlKey":dataSqlKey,
						"filters":dataFilters
					},
					"file": {
						"sqlKey":fileSqlKey,
						"filters":fileFilters
					}
				};
				var jsonData = JSON.stringify(param);
				var strUrl = __ctx+'/gs/gs-mng!exportWordByTemplate.action?jsonSenddata='
					+ (encodeURIComponent(jsonData)||"").replace(/\'/,"%27");
				window.open(strUrl);
			},
			exportWord: function(fileSqlKey, fileFilters) {
				var param = {
					"file": {
						"sqlKey":fileSqlKey,
						"filters":fileFilters
					}
				};
				var jsonData = JSON.stringify(param);
				var strUrl = __ctx+'/gs/gs-mng!exportWord.action?jsonSenddata='
					+ (encodeURIComponent(jsonData)||"").replace(/\'/,"%27");
				window.open(strUrl);
			},//{onAfterUpload:function(){},
			uploadFile: function(opt) {
				var param = $.extend(true, {}, opt);
				param.winId = "simple_file_box_win";
				param.message = '<div id="_simple_file_box_" style="margin-top:10px;"></div>';
				param.showTools = false;
				param.isIframe = false;
				param.maxFileNum = param.maxFileNum || 50;
				param.maxFileSize = param.maxFileSize || 500 * 1024 * 1024;
				param.maxSingleFileSize = param.maxSingleFileSize || 10 * 1024 * 1024;
				param.chunked = param.chunked || true;
				param.onInit = function(index) {
					$("#_simple_file_box_").filebox({
						baseUrl:__ctx,
						maxFileNum: param.maxFileNum,
						maxFileSize: param.maxFileSize,
						maxSingleFileSize: param.maxSingleFileSize,
						chunked: param.chunked,
						value:null,
						onAfterUploadFinished:function(val, oldVal, data) {
							if(param.onAfterUpload) {
								param.onAfterUpload(val, index, oldVal, data);
							}
						}
					});
				};
				openFormWinV2(param);
			}
};