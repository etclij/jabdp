define("pages/borrow/loaninfo", ["jquery", "widgets/widgets"],
function(a) {
    function b() {
        d("form#borrowForm3").hide();
        d("form#borrowForm2").hide();
        d("form#borrowForm1").show();
        var a = d("#productIdul");
//        d.ajax({
//            url: "/borrow/getProductsByUser.action",
//            cache: !1,
//            dataType: "JSON",
//            success: function(b) {
//                a.empty();
//                var c = b;
//                c.vos.length <= 1 ? a.parents(".ui-form-item").hide() : a.parents(".ui-form-item").show(),
//                d("#productId").attr("value", c.vos[0].id),
//                d("#productId").parent("div").find(".J_txt").text(c.vos[0].productName),
//                d.each(c.vos,
//                function(b, c) {
//                    g.push({
//                        id: c.id,
//                        name: c.productName,
//                        loanType: c.loanType,
//                        monthInte: d.parseJSON(c.monthlyMinInterest)
//                    }),
//                    d("<li datavalue='" + c.id + "'><span>" + c.productName + "</span></li>").appendTo(a)
//                });
//                var e = d("#productId").val();
//                if (e) {
//                    for (var f = 0; f < g.length; f++) if (e == g[f].id) {
//                        d("#loanType").html(g[f].loanType);
//                        for (var h = g[f].monthInte, i = "", k = 0, l = h.length; l > k; k++) i += " <li datavalue='" + h[k].month + "'><span>" + h[k].month + "个月</span></li>";
//                        d("#repayTimeul").html(i),
//                        d("#repayTime").attr("value", h[0].month),
//                        d("#repayTime").parent("div").find(".J_txt").text(h[0].month + "个月");
//                        break
//                    }
//                    j()
//                }
//            }
//        })
    }
    function c(a) {
        jQuery.validator.addMethod("isBorrowAmount",
        function(a, b) {
            return this.optional(b) || a >= parseInt(k[0], 10) && a <= parseInt(k[1], 10) && a % 50 === 0 && /^(([1-9]{1}\d*)|([0]{1}))?$/.test(a)
        },
        "借款金额范围" + k[0] + "-" + k[1] + "，且为50的倍数"),
        jQuery.validator.addMethod("isRateOver",
        function(a) {
            var b = parseFloat(Number(d("#minRate").html().replace("%", ""))),
            c = parseFloat(Number(d("#maxRate").html().replace("%", "")));
            return a = parseFloat(a),
            a >= b && c >= a
        },
        a.err.isRateOver)
    }
    var d = a("jquery"),
    e = a("widgets/widgets"),
    f = e.Form,
    g = [],
    h = function() {
        var a, b, c, e, f = {};
        a = d.trim(d("#borrowAmount").val()),
        b = d.trim(d("#apr").val()),
        b = "" === b ? "": parseFloat(b),
        c = d("#repayTime").val(),
        e = d("#productId").val(),
        a && b && c && e && (f = {
            amount: a,
            apr: b,
            repayTime: c,
            productId: e
        },
        d.ajax({
            url: "/update/yong-kuan-shen-qing!list.action",
            data: d.param(f),
            type: "POST",
            dataType: "json",
            success: function(a) {
                d("#monthRepayMoney").html("￥" + a.monthlyRepay.toFixed(2)),
                d("#managerFee").html("￥" + a.manageFee.toFixed(2))
            }
        }))
    },
    i = function() {
        var a, b, c, e;
        b = d("#ratetable tr"),
        a = d.trim(d("#borrowAmount").val()),
        c = b.eq(1).find("td").filter(function() {
            return "服务费率" != d(this).text()
        }),
        e = b.eq(2).find("td").filter(function() {
            return "服务费" != d(this).text()
        }),
        !a || a % 50 !== 0 || a > 1e6 ? d.each(c,
        function(a) {
            e.eq(a).html("")
        }) : d.each(c,
        function(b) {
            var c = parseFloat(d(this).text().replace("%", ""));
            e.eq(b).html((c * a * .01).toFixed(0))
        })
    };
    d("#borrowAmount,#apr").keyup(function() {
        h(),
        j(),
        i()
    }),
    d("#repayTime").change(function() {
        h(),
        j()
    });
    var j = function() {
        for (var a = d("#productId").val(), b = d("#repayTime").val(), c = 0; c < g.length; c++) if (a == g[c].id) {
            for (var e = g[c].monthInte, f = 0, h = e.length; h > f; f++) b == e[f].month && (d("#minRate").html(e[f].minInterest + "%"), d("#maxRate").html(e[f].maxInterest + "%"));
            break
        }
    };
    b();
    var k = d("#borrowAmount").data("range").split(","),
    e = a("widgets/widgets").Form,
    l = f.validate({
    	//target: jQuery("form#borrowForm1").css("display")=="block" ? "#borrowForm2" : "#borrowForm2",
        showTip: !0,
        tip: {
            borrowTitle: "不超过14字"
        },
        before: function(a) {
            c(a)
        },
        validateData: {
            submitHandler: function(a) {
            	if(jQuery("form#borrowForm1").css("display")=="block") {
                	jQuery(".ui-step.ui-step-5 li").each(function(i){
             		   if(i==1) {
             			   jQuery(this).addClass("ui-step-start ui-step-active")
             			   .find(".iconfont").append("<span></span>");
             		   } else if(i===0) {
             			   jQuery(this).removeClass("ui-step-start ui-step-active")
             			   .find(".iconfont").remove("span");
             		   }
             	    });
             	    jQuery("form#borrowForm2").show();
             	    jQuery("form#borrowForm1").hide();
             	    jQuery("#borrowForm2 input[name='jiekuanbiaoti']").val(jQuery("#borrowForm1 input#borrowTitle").val());
                   	jQuery("#borrowForm2 input[name='yongkuanjine']").val(jQuery("#borrowForm1 input#borrowAmount").val());
                   	jQuery("#borrowForm2 input[name='yongkuanqixian']").val(jQuery("#borrowForm1 input#repayTime").val());
                   	jQuery("#borrowForm2 input[name='gongganlv']").val(jQuery("#borrowForm1 input#apr").val());
                   	jQuery("#borrowForm2 input[name='jiekuanmiaoshu']").val(jQuery("#borrowForm1 textarea").val());
            	} else {
                    e.ajaxSubmit(jQuery("form#borrowForm2"), {
	                	dataType:"json",
	                    success: function(a) {
                    		jQuery(".ui-step.ui-step-5 li").each(function(i){
                			   if(i==2) {
                				   jQuery(this).addClass("ui-step-start ui-step-active")
                				   .find(".iconfont").append("<span></span>");
                			   } else if(i==1) {
                				   jQuery(this).removeClass("ui-step-start ui-step-active")
                				   .find(".iconfont").remove("span");
                			   }
                		    });
                    		jQuery("form#borrowForm3").show();
                     	    jQuery("form#borrowForm2").hide();
	                    }
	                })
                }
            }
        }
    });
//    d("form").submit(function() {
//        return l.valid() ? "true" != d("#agree_contract").attr("value") ? (d("#J_error_agree").css("display", "block"), !1) : (this.submit(), d(this).find(":submit").attr("disabled", "disabled"), !1) : !1
//    }),
    d(document).click(function(a) {
        var b = a.target;
        d(b).parent("span").hasClass("arrow") || d(b).parent("div").hasClass("J_select_btn") || d(".J_popBox").css("display", "none")
    }),
    d(".J_select_btn").click(function(a) {
        d(".J_popBox").css("display", "none");
        var b = d(a.currentTarget).parent().find("ul");
        "block" == b.css("display") ? b.css("display", "none") : b.css("display", "block")
    }),
    d(".J_popBox").delegate("li", "mouseover",
    function(a) {
        d(a.currentTarget).attr("class", "selected")
    }),
    d(".J_popBox").delegate("li", "mouseleave",
    function(a) {
        d(a.currentTarget).attr("class", "")
    }),
    d(".J_popBox").delegate("li", "click",
    function(a) {
        var b = d(a.currentTarget).attr("datavalue"),
        c = d(a.currentTarget).find("span").text(),
        e = d(a.currentTarget).parent().parent();
        if (e.find("input").attr("value", b), e.find(".J_txt").text(c), ("RRGXD" == b || "RRSYD" == b || "RRWSD" == b) && (location.href = "/borrow/calculator.action?prodType=" + b), "repayTimeul" == d(a.currentTarget).parent("ul").attr("id")) switch (b) {
        case "3":
            d("#apr").val("10");
            break;
        case "6":
            d("#apr").val("11");
            break;
        case "9":
            d("#apr").val("12");
            break;
        case "12":
            d("#apr").val("12");
            break;
        case "15":
            d("#apr").val("13");
            break;
        case "18":
            d("#apr").val("13");
            break;
        case "24":
            d("#apr").val("13");
            break;
        case "36":
            d("#apr").val("13")
        }
        h(),
        j()
    }),
    d(".J_ui_checkbox").bind("click",
    function(a) {
        var b = d(a.currentTarget);
        b.hasClass("uncheck") ? (b.removeClass("uncheck"), b.addClass("check"), d("#agree_contract").attr("value", "true"), d("#J_error_agree").css("display", "none")) : (b.removeClass("check"), b.addClass("uncheck"), d("#agree_contract").attr("value", "false"), d("#J_error_agree").css("display", "block"))
    }),
    d("#apr").on("focus",
    function() {
        d("#J_yearrate_pop").css("display", "block")
    }),
    d("#apr").on("blur",
    function() {
        d("#J_yearrate_pop").css("display", "none")
    }),
    d("#borrowAmount").on("focus",
    function(a) {
        d("#J_amount_range").text("借款金额范围:" + d(a.currentTarget).attr("data-range").replace(",", "-") + ",且为50的倍数"),
        d("#J_borrowamount_pop").css("display", "block")
    }),
    d("#borrowAmount").on("blur",
    function() {
        d("#J_borrowamount_pop").css("display", "none")
    }),
    d(".extra-div-a").on("mouseenter",
    function() {
        d(".ui-poptip-bx").css("display", "block")
    }),
    d(".extra-div-a").on("mouseleave",
    function() {
        d(".ui-poptip-bx").css("display", "none")
    })
});