/**
 * 扩展form-validator
 * 增加ajax校验 
 * @bobgao 2016-04-11
 */
(function($, window) {

    'use strict';
	
	//覆盖方法，修复bootstrap显示位置bug
	$.formUtils.dialogs.getParentContainer = function ($elem) {
      if ($elem.valAttr('error-msg-container')) {
        return $($elem.valAttr('error-msg-container'));
      } else {
        var $parent = $elem.parent();
		if($parent.hasClass('input-group')) {
			$parent = $parent.parent();
		}
        /*if (!$parent.hasClass('form-group') && !$parent.closest('form').hasClass('form-horizontal')) {
          var $formGroup = $parent.closest('.form-group');
          if ($formGroup.length) {
            return $formGroup.eq(0);
          }
        }*/
        return $parent;
      }
    };
	
    var requestServer = function(serverURL, $element, val, conf, callback) {
		var reqParams = $element.valAttr('req-params') || $element.data('validation-req-params') || {},
            handleResponse = function(response, callback) {
              if(response.valid) {
                $element.valAttr('backend-valid', 'true');
              }
              else {
                $element.valAttr('backend-invalid', 'true');
                if (response.message) {
                  $element.attr(conf.validationErrorMsgAttribute, response.message);
                }
              }

              if( !$element.valAttr('has-keyup-event') ) {
                $element
                  .valAttr('has-keyup-event', '1')
                  .bind('keyup change', function(evt) {
                    if( evt.keyCode !== 9 && evt.keyCode !== 16 ) {
                      $(this)
                        .valAttr('backend-valid', false)
                        .valAttr('backend-invalid', false);
                    }
                  });
              }

              callback();
            };

        if ( !reqParams ) {
            reqParams = {};
        }
        if ( typeof reqParams === 'string' ) {
            reqParams = eval("("+reqParams+")");
        }
        reqParams[$element.valAttr('param-name') || $element.attr('name')] = val;

        $.ajax({
            url : serverURL,
            type : 'POST',
            cache : false,
            data : reqParams,
            dataType : 'json',
            error : function(error) {
              handleResponse({valid: false, message:'Connection failed with status: ' + error.statusText}, callback);
              return false;
            },
            success : function(response) {
              handleResponse(response, callback);
            }
        });
    },
    disableFormSubmit = function() {
        return false;
    };

    /*
     * Server validation
     * Flow (form submission):
     *  1) Check if the value already has been validated on the server. If so, display the validation
     *     result and continue the validation process, otherwise continue to step 2
     *  2) Return false as if the value is invalid and set $.formUtils.haltValidation to true
     *  3) Disable form submission on the form being validated
     *  4) Request the server with value and input name and add class 'validating-server-side' to the form
     *  5) When the server responds an attribute will be added to the element
     *      telling the validator that the input has a valid/invalid value and enable form submission
     *  6) Run form submission again (back to step 1)
     */
    $.formUtils.addValidator({
        name : 'ajax',
        validatorFunction : function(val, $el, conf, lang, $form) {
            var backendValid = $el.valAttr('backend-valid'),
                backendInvalid = $el.valAttr('backend-invalid'),
                serverURL = document.location.href;

            if($el.valAttr('url')) {
                serverURL = $el.valAttr('url');
            } else if('serverURL' in conf) {
                serverURL = conf.backendUrl;
            }

            if (backendValid) {
                return true;
            }
            else if (backendInvalid) {
                return false;
            }
            else if($.formUtils.eventType === 'keyup' && !$.formUtils.isValidatingEntireForm) {
                return null;
            }
			//判断为空值，不做校验
			if(!$.isNumeric(val) && !val) {
				return true;
			}

            if ($.formUtils.isValidatingEntireForm) {
                $form
                    .bind('submit', disableFormSubmit)
                    .addClass('validating-server-side')
                    .addClass('on-blur');

                $el.addClass('validating-server-side');
                $.formUtils.haltValidation = true;

                requestServer(serverURL, $el, val, conf, function() {

                    $form
                        .removeClass('validating-server-side')
                        .removeClass('on-blur')
                        .get(0).onsubmit = function() {};

                    $form.unbind('submit', disableFormSubmit);
                    $el.removeClass('validating-server-side');

                    $el.valAttr('value-length', val.length);

                    // fire submission again!
                    $.formUtils.haltValidation = false;
                    //$form.trigger('submit');
                });

                return null;

            } else {
                // validation on blur
                $form.addClass('validating-server-side');
                $el.addClass('validating-server-side');
                requestServer(serverURL, $el, val, conf, function() {
                    $form.removeClass('validating-server-side');
                    $el.removeClass('validating-server-side');
                    $el.trigger('blur');
                });
                return null;
            }
        },
        errorMessage : '',
        errorMessageKey: 'badBackend',
        validateOnKeyUp : false
    });


})(jQuery, window);
