//任务栏设置
myLib.NS("desktop.taskBar");
myLib.desktop.taskBar={
	timer:function(obj){
		 var curDaytime=new Date().toLocaleString().split(" ");
		 obj.innerHTML=curDaytime[1];
		 obj.title=curDaytime[0];
		 setInterval(function(){obj.innerHTML=new Date().toLocaleString().split(" ")[1];},1000);
		},
	upTaskWidth:function(){
		var myData=myLib.desktop.getMydata()
		    ,$task_bar=myData.panel.taskBar['_this'];
		var maxHdTabNum=Math.floor($(window).width()/100);
		    //计算任务栏宽度
		    $task_bar.width(maxHdTabNum*100);	
			//存储活动任务栏tab默认组数
			$('body').data("maxHdTabNum",maxHdTabNum-2);
		},	
	init:function(){
		//读取元素对象数据
		var myData=myLib.desktop.getMydata();
 		var $task_lb=myData.panel.taskBar['task_lb']
		    ,$task_bar=myData.panel.taskBar['_this']
			,wh=myData.winWh;
		 var _this=this;
		 _this.upTaskWidth();
		 //当改变浏览器窗口大小时，重新计算任务栏宽度
		 $(window).wresize(function(){
						_this.upTaskWidth();   
								   });
 		},
	contextMenu:function(tab,id){
		var _this=this;
		 //初始化任务栏Tab右键菜单
		 var data=[
			[{
			text:"最大化",
			func:function(){
				if(!tab.hasClass("selectTab")){
					tab.trigger('click');
				}
				else if(tab.data("options").index!=$("#applemenu img.desktop_icon_over_b").data("index")){
					tab.trigger('click');
				}
				$("#myWin_"+tab.data('win')).find('a.winMaximize').trigger('click');
				}
			},{
			text:"最小化",
			func:function(){
				if(tab.hasClass("selectTab")){
					myLib.desktop.win.minimize($("#myWin_"+tab.data('win')));
				}
				}
			}],[{
			  text:"关闭",
			  func:function(){
				  $("#smartMenu_taskTab_menu"+id).remove();
				  //console.log(tab);
				  //console.log($("#myWin_"+tab.data('win')));
				  myLib.desktop.win.closeWin($("#myWin_"+tab.data('win')));
				  //if()
				  } 
			 }]
		];
		myLib.desktop.contextMenu(tab,data,"taskTab_menu"+id,10);
	},
	addWinTab:function(text,id,imgclass,index){
		var myData=myLib.desktop.getMydata();
 		var $task_lb=myData.panel.taskBar['task_lb']
		    ,$task_bar=myData.panel.taskBar['_this']
			,$navBar=myData.panel.navBar
 			,$navTab=$navBar.find("a")
		    ,tid="myWinTab_"+id
			,allTab=$task_lb.find('a')
			,curTabNum=allTab.size();
 		
 			var title = text;
 			if(text.replace(/[^\x00-\xff]/g, '__').length > 12) {
 				text = text.substring(0,5) + '...';
 			}
		    var docHtml="<a href='#' id='" + tid + "' title='" + title + "'>"
		    + "<img src='js/desktop/themes/default/images/null.png' style='margin-bottom:-3px;height:16px' class='" 
		    + imgclass + "' /> " + text + "</a>";
 		
			//添加新的tab
		    $task_lb.append($(docHtml));
			var $newTab=$("#"+tid);
			$newTab.data("options",{"index":index,"title":title});
			//右键菜单
			this.contextMenu($newTab,id);
			$task_lb
			.find('a.selectTab')
			.removeClass('selectTab')
			.addClass('defaultTab');
			$newTab
			.data('win',id)
			.addClass('selectTab')
			.click(function(){
				//console.log(this.id);
				//if(this.id)
				var win=$("#myWin_"+$(this).data('win')),
				    tabId=this.id,
					iconId=tabId.split("_")[1],
					desk=null;
				
				//console.log(index);
				//console.log($("#applemenu img.desktop_icon_over_b").data("index"));
				if(index==$("#applemenu img.desktop_icon_over_b").data("index")){
					if(win.is(".hideWin")){
						//win.show();
						win.css({"left":win.position().left+10000,"visibility":"visible"}).removeClass("hideWin");
						$(this).removeClass('defaultTab').addClass('selectTab');//当只有一个窗口时
						myLib.desktop.win.switchZindex(win);
					}else{
						if($(this).hasClass('selectTab')){
							myLib.desktop.win.minimize(win);
						}else{
							myLib.desktop.win.switchZindex(win);
						} 
					}
			    //如果不在当前窗口			  
			    }else{
			    	//先转动至当前页
				    var myData=myLib.desktop.getMydata(),
					$navBar=myData.panel.navBar,
					$innerPanel=myData.panel.desktopPanel.innerPanel,
					$navTab=$navBar.find("a"),
					$deskIcon=myData.panel.desktopPanel['deskIcon'],
					desktopWidth=$deskIcon.width(),
					lBarWidth=myData.panel.lrBar["_this"].outerWidth();
					myLib.desktop.deskIcon.desktopMove($innerPanel,$deskIcon,$navTab,500,desktopWidth+lBarWidth,index);
					if(win.is(".hideWin")){
							//win.show();
						win.css({"left":win.position().left+10000,"visibility":"visible"}).removeClass("hideWin");
 						$(this).removeClass('defaultTab').addClass('selectTab');//当只有一个窗口时
						myLib.desktop.win.switchZindex(win);
	  				}else{
	  					$(this).removeClass('defaultTab').addClass('selectTab');//当只有一个窗口时
						myLib.desktop.win.switchZindex(win);
	  				}
					//$navTab.eq(i).trigger("click");
				}
// 							}
			});
			$('body').data("topWinTab",$newTab);
			//当任务栏活动窗口数超出时
			if(curTabNum>myData.maxHdTabNum-1){
				var LeftBtn=$('#leftBtn')
				    ,rightBtn=$('#rightBtn')
					,bH;
                LeftBtn
				.show()
				.find("a")
				.click(function(){
							        var pos=$task_lb.position();
									if(pos.top<0){
										$task_lb.animate({
                                                  "top":pos.top+40
                                                      }, 50);
										}
									 });
				rightBtn
				.show()
				.find("a")
				.click(function(){
									var pos=$task_lb.position(),h=$task_lb.height(),row=h/40;
									if(pos.top>(row-1)*(-40)){
									$task_lb.animate({
                                                  "top": pos.top-40
                                                      }, 50); 
									}
									   });
				
				$task_lb.parent().css("margin","0 100");
				}
	 
		},
	delWinTab:function(wObj){
		var myData=myLib.desktop.getMydata()
 		    ,$task_lb=myData.panel.taskBar['task_lb']
			,$task_bar=myData.panel.taskBar['_this']
			,LeftBtn=$('#leftBtn')
			,rightBtn=$('#rightBtn')
		    ,pos=$task_lb.position();
			
		
		this.findWinTab(wObj).remove();
		
		var newH=$task_lb.height();
		if(Math.abs(pos.top)==newH){
			LeftBtn.find('a').trigger("click");
			}
		if(newH==40){
			LeftBtn.hide();
			rightBtn.hide();
			$task_lb.parent().css("margin",0);
			}	
		},
	findWinTab:function(wObj){
		var myData=myLib.desktop.getMydata(),
		    $task_lb=myData.panel.taskBar['task_lb'],
		    objTab;
		    $task_lb.find('a').each(function(index){
				var id="#myWin_"+$(this).data("win");		 
				
				if($(id).is(wObj)){
					//console.log("wObj");
					//console.log($(id));
					objTab=$(this);
				}		 
			 });
		    return objTab;
		}	
	};