<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.springframework.security.web.WebAttributes" %>
<%@ page import="org.springframework.security.web.authentication.session.SessionAuthenticationException" %>
<%@ page import="org.springframework.security.core.AuthenticationException" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CRM登录</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="抗癌 健康"/>
    <meta name="description" content="抗癌健康网"/>
    <meta name="viewport" content="width=device-width"/>
    <link href="memreg.css" rel="stylesheet" />
</head>
<body>
<div class="m-login-top"> <a>CRM客户登陆界面</a> </div>
<div class="m-login-box">
      <form id="loginForm" action="${ctx}/j_spring_security_check" method="post" data-ajax="false">   
      		<input type="hidden" name="spring-security-redirect" value="/mb/index.jsp"></input> 
                <dl><dt>登录账户：</dt><dd><span><input value="" type="text" name="j_username" id="j_username" autofocus="autofocus" required="required" placeholder="请输入登录账户"/></span></dd></dl>
                <dl><dt>登录密码：</dt><dd><span><input value="" type="password" name="j_password" id="j_password" required="required" placeholder="请输入登录密码"/></span></dd></dl>
              <div class="dlu-p">
                 <dl class="dlu"><dd><span><button type="submit">登录</button></span></dd></dl>
              </div>
      </form>           
</div>
<div class="user_login_msg">
					<c:if test="${param.error!=null}">
						<span>用户名或密码错误，请重试。</span>&nbsp;&nbsp;
					</c:if>
					<c:if test="${param.timeout!=null}">
						<span>会话超时，请重新登录。</span>&nbsp;&nbsp;
					</c:if>
</div>
</div>
<!--底部-->
<div class="denglu1"></div>
  <script type="text/javascript">
  var __ctx='${ctx}';
  var $curUserId$ = "${sessionScope.USER.id}";
  </script>
<script src="${ctx}/js/jquery/jquery-1.7.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/common-min.js"></script>
<script type="text/javascript">
function doCheckUserIsLogin(userName, userPass) {
	var flag = false;
	var options = {
			async:false,
			url : '${ctx}/sys/account/online-user!checkUserIsLogin.action',
			data : {
				"userName" : userName,
				"userPass" : userPass
			},
			success : function(data) {
				var fm = data.msg;
				if(fm) {
					switch(fm.flag) {
						case "0":
							var cMsg = "" + fm.msg + "。是否确认将其强制下线？";
								if (confirm(cMsg)){
									doRemoveOnlineUser(userName);
								} else {
									$("#j_username").val("");
									$("#j_password").val("");
									$("#j_username").focus();
								}
							break;
						case "2":
							$(".user_login_msg").text(fm.msg);
							break;
						default:
							flag = true;
					}
				}
			}
	};
	fnFormAjaxWithJson(options);
	return flag;
}

function doRemoveOnlineUser(userName) {
	var options = {
			async:false,
			url : '${ctx}/sys/account/online-user!removeLoginUser.action',
			data : {
				"userName" : userName
			},
			success : function(data) {
				$("#loginForm").unbind("submit");
				$("#loginForm").submit();
			}
	};
	fnFormAjaxWithJson(options);
}
$(function() {
<%
	AuthenticationException authException = (AuthenticationException)session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	if(authException!=null && authException instanceof SessionAuthenticationException) {
		String errorMsg = authException.getMessage();
%>
		$(".user_login_msg").text("<%=errorMsg%>");
<%
	}
%>
$("#loginForm").submit(function() {
	var userName = $("#j_username").val();
	var pass = $("#j_password").val();
	return doCheckUserIsLogin(userName, pass);
});
});    
</script>
</body>
</html>