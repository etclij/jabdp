<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.qyxx.platform.sysmng.accountmng.entity.User" %>
<%@ include file="/common/taglibs.jsp" %>
<%
User user = (User)session.getAttribute("USER");
if(null==user) {
	response.sendRedirect(request.getContextPath() + "/mb/login.jsp");
}
%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>来电记录</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="抗癌 健康"/>
    <meta name="description" content="抗癌健康网"/>
    <meta name="viewport" content="width=device-width"/>
    <link href="memreg.css" rel="stylesheet" />
</head>
<body>
<div class="m-login-top"><a class="dl" id="cur_time_a"></a></div>
<div class="mob-zuche">
   <table width="100%" border="1" >
	<thead>
	<tr class="biaoti">
	    <td>序列号</td>
	    <td>手机号码</td>
	    <td>拨打</td>
	    <td>时间</td>
  	</tr>
	</thead>
	<tbody id="dhjl_body"></tbody>
</table>
</div>
<div class="m-login-box">
<div class="dlu-p">
    <dl class="dlu"><dd><span><button type="button" onclick="doLogout()">退出</button></span></dd></dl>
</div>
</div>
<!--底部-->
<div class="denglu1"></div>

  <script type="text/javascript">
  var __ctx='${ctx}';
  var $curUserId$ = "${sessionScope.USER.id}";
  </script>
<script src="${ctx}/js/jquery/jquery-1.7.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/common-min.js"></script>
<script type="text/javascript" src="${ctx}/js/common/scripts/xdate.js"></script>
<script type="text/javascript">
$(function() {
	var now = new XDate();
	$("#cur_time_a").text(now.toString("yyyy年MM月dd日"));
	var data = getFilterData("dianhuajilu.DianHuaJiLuZhuBiao.laidianhaoma.sql1", {"filter_EQL_uId":$curUserId$,"filter_EQS_ldsj":now.toString("yyyy-MM-dd")});
	if(data && data.list && data.list.length) {
		var list = data.list;
		var htmlList = [];
		for(var i=0,len=list.length;i<len;i++) {
			var obj = list[i];
			htmlList.push("<tr><td>");
			htmlList.push(i+1);
			htmlList.push("</td><td>");
			htmlList.push(obj.ldhm);
			htmlList.push("</td><td>");
			htmlList.push("<a href='tel:");
			htmlList.push(obj.ldhm);
			htmlList.push("'><button type='button'>拨号</button></a>");
			htmlList.push("</td><td>");
			htmlList.push(obj.ldsj);
			htmlList.push("</td></tr>");
		}
		$("#dhjl_body").html(htmlList.join(""));
	}
});
</script>
</body>
</html>
