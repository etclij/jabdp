<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.qyxx.platform.common.utils.spring.SpringContextHolder" %>
<%@ page import="com.qyxx.platform.gsc.utils.SystemParam" %>
<%@ page import="com.qyxx.platform.sysmng.accountmng.entity.User" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
SystemParam systemParam = SpringContextHolder.getBean("systemParam");
String portalUrl = "";
String systemPortalUrl = systemParam.getSystemPortalUrl();
if(StringUtils.isNotBlank(systemPortalUrl)) {
	portalUrl = systemPortalUrl;
}
User user = (User)session.getAttribute("USER");
if(null!=user) {
	String rolePortalUrl = user.getPortalUrl();
	if(StringUtils.isNotBlank(rolePortalUrl)) {
		portalUrl = rolePortalUrl;
	}
} else {
	response.sendRedirect(request.getContextPath() + "/login.action");
}
if(StringUtils.isNotBlank(portalUrl)) {
	portalUrl = request.getContextPath() + "/" + portalUrl;
}
request.setAttribute("portalUrl", portalUrl);
%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<title><%=systemParam.getSystemTitle()%></title>
	<%@ include file="/common/meta-css.jsp" %>
	<link rel="stylesheet" type="text/css" href="${ctx}/js/desktop/themes/default/css/desktop.css" />
	<link rel="stylesheet" type="text/css" href="${ctx}/js/desktop/jsLib/jquery-ui-1.8.18.custom.css" />
	<link rel="stylesheet" type="text/css" href="${ctx}/js/desktop/jsLib/jquery-smartMenu/css/smartMenu.css" />
	<style type="text/css">
		#applemenu{position:absolute;width:100%;bottom:0;text-align:center;height:70px;}
		#applemenu a{width:72px}
		.desktop_icon_over_a{border-style:solid;border-width:2px}
		.desktop_icon_over_b{border-style:solid;border-width:2px;width:72px;height:72px}
		.desktop_icon_over_c{width:48px;height:48px}
	</style> 
</head>

<body>

<div id="wallpapers"></div>

<!-- <div id="navBar">
  <a href="#" class="currTab" title="桌面1"></a>
  <a href="#" title="桌面2"></a>
  <a href="#" title="桌面3"></a>
  <a href="#" title="桌面4"></a>
</div> -->

<!-- 桌面二级模块图标 -->
<div id="desktopPanel"></div>

<div id="taskBarWrap">
<div id="taskBar">
  <div id="leftBtn"><a href="#" class="upBtn"></a></div>
  <div id="rightBtn"><a href="#" class="downBtn"></a></div>
  <div id="task_lb_wrap"><div id="task_lb"></div></div>
</div>
</div>

<!-- 侧边栏 -->
<div id="lr_bar">
  <ul id="default_app">
  </ul>
  <div id="default_tools">
    <span id="bt_showZm" title="全屏"></span>
    <span id="bt_favorite" title="收藏"></span>
    <span id="bt_homepage" title="切换到经典风格"></span>
    <span id="bt_refresh" title="刷新"></span>
    <span id="bt_notice" title="通知管理"></span>
  </div>
  <div id="start_block">
	<a title="开始" id="start_btn"></a>
	<div id="start_item">
      <ul class="item admin">
        <li><span class="adminImg"></span><s:property value="#session.USER.nickname" /></li>
      </ul>
      <ul class="item">
        <li id="bt_update"><span class="sitting_btn"></span>个人信息</li>
        <li id="bt_help"><span class="help_btn"></span>关于</li>
        <!-- <li><span class="about_btn"></span>关于我们</li> -->
        <li id="bt_logout"><span class="logout_btn"></span>退出系统</li>
      </ul>
	</div>
  </div>
</div>

<%--<div style="position:absolute;left:10px;top:8px;">
	当前时间：<span id="_span_time_"></span>
</div>

<div style="position:absolute;left:10px;top:22px;font-size:14px;">
	
	<s:text name="system.index.welcome.title">
		<s:param><s:property value="#session.USER.realName" /></s:param>
		<s:param><s:property value="#session.USER.organizationName" /></s:param>
	</s:text>
	<s:text name="system.sysmng.user.name.title"/>：<s:property value="#session.USER.realName" />&nbsp;&nbsp;&nbsp;&nbsp;
	<s:if test="%{#session.USER.nickname != null && #session.USER.nickname != ''}"><s:text name="system.sysmng.user.nickname.title"/>：<s:property value="#session.USER.nickname" />&nbsp;&nbsp;&nbsp;&nbsp;</s:if>
	<s:text name="system.sysmng.role.organization.title"/>：<s:property value="#session.USER.organizationName" />
</div>
 --%>
 
<div id="applemenu"></div>

<div style="display:none;">
<div id="aboutWin" class="easyui-window" closed="true" minimizable="false" modal="true" title="About V1.0.0" style="width:450px;height:100px;">
	<div style="text-align:center;padding:10px 10px;"><s:text name="system.index.copyright.title"/></div>
	<div style="text-align:center;padding:2px 2px;"><a href="javascript:void(0);" onclick="$('#authWin').window('open');"><s:text name="system.index.license.info.title"/></a></div>
</div>

<div id="authWin" class="easyui-window" closed="true" minimizable="false" href="${ctx}/index!authInfo.action" modal="true" title="<s:text name="system.index.license.info.title"/>" style="width:400px;height:210px;padding:10px 10px;font-size:18px;">
</div>

<div id="aboutPass" class="easyui-window" closed="true" minimizable="false" modal="true" title="<s:text name="system.login.changepasswords.title"/>" style="width:380px;height:260px;">
	<div style="text-align:center;padding:10px 10px;" class="easyui-layout" fit="true">
		<div region="center" border="false">
			<div id="userInfo" class="easyui-tabs"  fit="true">
				<div title="<s:text name="system.login.userInfo.title" />"  style="padding:5px;">
				<form action="" name="userInfoForm" id="userInfoForm" method="post" style="margin: 0px;">
					  <input type="hidden" name="id" id="_u_id" value="<s:property value="#session.USER.id" />" />
					<table align="center" border="0" cellpadding="0" cellspacing="1"
						   class="table_form">
					  <tbody>
					  	<tr>						  		
					  		<th><label for="realName"><s:text name="system.sysmng.user.name.title" />:</label></th>
							<td align="left"><input class="easyui-validatebox"  required="true" type="text" name="realName" id="realName" value="<s:property value="#session.USER.realName" />" ></input>&nbsp;<font color="red">*</font></td>
						</tr>
						<tr>
					  		<th><label for="nickname"><s:text name="system.sysmng.user.nickname.title" />:</label></th>
							<td align="left"><input  type="text" name="nickname" id="nickname" value="<s:property value="#session.USER.nickname" />" ></input></td>
						</tr>
						<tr>
					  		<th><label for="mobilePhone"><s:text name="system.sysmng.user.mobilephone.title" />:</label></th>
							<td align="left"><input class="easyui-validatebox" required="true" value="<s:property value="#session.USER.mobilePhone" />" type="text" name="mobilePhone" id="mobilePhone" validType="mobile" invalidMessage="请输入正确的11位手机号码.格式:13120002221"></input>&nbsp;<font color="red">*</font></td>
						</tr>
						<tr>
					  		<th><label for="email"><s:text name="system.sysmng.user.email.title" />:</label></th>
							<td align="left"><input class="easyui-validatebox" type="text" value="<s:property value="#session.USER.email" />" name="email" id="email"  validType="email"></input></td>
						</tr>							
					</tbody>
				   </table>
					</form> 
					</div>
						<div title="<s:text name="system.login.changepasswords.title" />"  style="padding:5px;">
				<form action="" name="passWordForm" id="passWordForm" method="post" style="margin: 0px;">
					<table align="center" border="0" cellpadding="0" cellspacing="1" class="table_form">
					  <tbody>								 				
						<tr>
							<th><label for="oldPass"><s:text name="system.login.oldpassword.title" /></label></th>
							<td align="left"><input type="password" name="oldPass" id="oldPass" class="easyui-validatebox" required="true" validtype="length[6,18]" invalidMessage="<s:text name="system.login.passwordLength.title"/>"/>&nbsp;<font color="red">*</font></td>
						  </tr>
						  <tr>
							<th><label for="newPass"><s:text name="system.login.newpassword.title"/></label></th>
							<td align="left"><input type="password" name="newPass" id="newPass" class="easyui-validatebox" required="true" validtype="length[6,18]" invalidMessage="<s:text name="system.login.passwordLength.title"/>"/>&nbsp;<font color="red">*</font></td>
						  </tr>
						  <tr>
							<th><label for="confPass"><s:text name="system.login.confirmpassword.title" /></label></th>
							<td align="left"><input type="password" name="rePass" id="rePass" class="easyui-validatebox" required="true" validType="equalTo['#newPass']" invalidMessage="<s:text name="system.login.passwordError.title"/>"/>&nbsp;<font color="red">*</font></td>
						  </tr>
					</tbody>
				   </table>
					</form> 
					</div>
				</div>
			</div>
			<div region="south" border="false" style="text-align: right; ">
				 <a id="mm" class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0);" onclick="doUpdate();"><s:text
					name="system.button.ok.title" /> </a> 
				 <a class="easyui-linkbutton"
					iconCls="icon-cancel" href="javascript:void(0);"
					onclick="doCanel();"><s:text
					name="system.button.cancel.title" /> </a>
		  	</div>
	</div>
</div>
<div id="aboutDoAction"  closed="true" minimizable="false" modal="true" resizable="false" title="<s:text name='system.top.message.title'/>" style="width:600px;height:400px;">
	<div class="easyui-layout" fit="true">
	<div region="north" title="" border="false" style="height: 37px; padding: 1px;">
		<form action="" name="toDoForm" id="toDoForm">
			<table border="0" cellpadding="0" cellspacing="1" class="table_form">
				<tbody>
					<tr>
						<th><label for="keyWords"><s:text name="关键字" />:</label></th>
						<td><input type="text" name="keyWords" id="keyWords"></input></td>						
						<td><button type="button" id="_bt_query_"><s:text name="system.search.button.title" /></button>
						&nbsp;&nbsp;
						<button type="button" id="_bt_reset_"><s:text name="system.search.reset.title" /></button></td>								
					</tr>
				</tbody>
			</table>
		</form>
	</div>
	<div region="center" title="" border="false" style="padding: 1px; overflow: hidden;">
		<div id="tnotice" class="easyui-tabs" fit="true" border="false" tools="#about_do_tab-tools">
			<div selected="true"
				title="<s:text name="system.sysmng.notice.todoSomething.title"/>">
				<table id="toDoList" border="false"></table>
			</div>
			<div title="<s:text name="system.sysmng.notice.list.title"/>">
				<table id="noReadNoticeDetail" border="false"></table>
			</div>
		</div>
	</div>
	<div region="south" border="false" align="right" style="padding:1px;height:25px">
		自动弹出通知间隔时间设置
		<select id="aboutDoActionConfig" style="padding:1px;width:60px">
			<option>不弹出</option>
			<option>1</option>
			<option>2</option>
			<option>5</option>
			<option>10</option>
			<option>30</option>
			<option>60</option>
		</select>
		分钟
		<input type="button" value="设置" style="height:22px" onClick="aboutDoActionConfig()"></input>
	</div>
</div>
</div>
<div id="myNotice"  style="width:100px;height:0px;">
</div>
</div>
<div id="gerenxinxi" title="个人信息"></div>
<div id="gerenxinxi_menu" class="easyui-menu">
	<div><s:text name='system.sysmng.user.name.title'/>：<s:property value='#session.USER.realName' /></div>
	<div><s:if test="%{#session.USER.nickname != null && #session.USER.nickname != ''}"><s:text name="system.sysmng.user.nickname.title"/>：<s:property value="#session.USER.nickname" />&nbsp;&nbsp;&nbsp;&nbsp;</s:if></div>
	<div><s:text name='system.sysmng.role.organization.title'/>：<s:property value='#session.USER.organizationName' /></div>
</div>
<div id="about_do_tab-tools">			
	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-scheduled_Tasks" onclick="addTab('待办历史','${ctx}/sys/workflow/process-history.action');$('#aboutDoAction').window('close');" title="待办历史">待办历史</a>	
	<a href="#" class="easyui-linkbutton" plain="true" iconCls="icon-chat-ico" onclick="addTab('通知历史','${ctx}/sys/notice/notice!noticeToUserList.action');$('#aboutDoAction').window('close');" title="通知历史">通知历史</a>		
</div>
	<%@ include file="/common/meta-js.jsp" %>
	<script type="text/javascript" src="${ctx}/js/desktop/desktop_api.js"></script>
	<script type="text/javascript" src="${ctx}/js/desktop/jsLib/jquery.winResize.js"></script>
	<script type="text/javascript" src="${ctx}/js/desktop/jsLib/jquery-smartMenu/js/mini/jquery-smartMenu-min.js"></script>
	<script type="text/javascript">
	var aboutDoActionConfigID = null;
	$(window).load(function(){
		
		//desktop("${ctx}");
		myLib.desktop.ctx('${ctx}');
		myLib.desktop.winWH();
		
		//初始化页面
		initMenu();
		
		_userList = findAllUser(); 
		_userLoginNameList = getUserLoginNameList(_userList);
		
		$("#bt_favorite").click(function() {
			var ctrl = (navigator.userAgent.toLowerCase()).indexOf('mac') != -1 ? 'Command/Cmd': 'CTRL';
			var url = window.location.href;
			if (document.all) {
				window.external.addFavorite(url, '<s:text name="system.index.title"/>');
			} else if (window.sidebar) {
				window.sidebar.addPanel('<s:text name="system.index.title"/>', url, "");
			} else {
				alert('您可以尝试通过快捷键' + ctrl + ' + D 加入到收藏夹~');
			}
		});
		
		$("#bt_homepage").click(function() {
			window.location.href = "${ctx}/";
		});
		
		$("#bt_help").click(function() {
			//alert();
			//$("#aboutWin").window("open");
			$("#aboutWin").dialog("open");
		});
		
		//修改当前用户密码
		$("#bt_update").click(function(){
			$("#aboutPass").window("open");
			$("#realName").focus();
			
		});
		
		$("#bt_refresh").click(function(){
			var options = {
					url:'${ctx}/index!refreshCacheData.action',
					success:function(data) {
						$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>', '<s:text name="system.top.synchronizationData.title"/>');
					}
			};
			fnFormAjaxWithJson(options);
		});
		
		$("#bt_logout").click(function() {
			$.messager.confirm('<s:text name="system.logout.title"/>', '<s:text name="system.logout.confirm.title"/>', function(r){
				if (r){
					//$("#logoutForm").submit();
					window.location.replace("${ctx}/logout.action");
					//window.location.replace("${ctx}/logout.action");
				}
			});
		});
		
		$("#bt_notice").click(function() {
			//FIXME
			//$("#myNotice").window("open");
			$("#aboutDoAction").window("open");
		});
		var notices = getNoticesNum();
		var process = getToDoNum(); 
		var no = 0;
		var po = 0;
		if(notices){
			no = parseInt(notices);
		}
		if(process){
			po = parseInt(process);
		}
		var total = no + po;
		$("#bt_notice").append("<div id='total' style='color:red;position:relative;left:21px;top:-1px;'>"+total+"</div>");
		setInterval(function(){
			var notices = getNoticesNum();
			var process = getToDoNum(); 
			var no = 0;
			var po = 0;
			if(notices){
				no = parseInt(notices);
			}
			if(process){
				po = parseInt(process);
			}
			var total = no + po;
			//console.log(total);
			$("#bt_notice div#total").text(total);
		},5000);
		
		setCurrentTime();
		
		windowInit();
		
		initgerenxinxi();
		
		//刷新时间
		setInterval(function(){
			$("#gerenxinxitime").text(new XDate().toString("HH:mm"));
		}, 5000);
		
		/* for(var i = 0; i < imgs.length; i++) {
			aWidth.push(imgs[i].offsetWidth);
			var width = parseInt(imgs[i].offsetWidth / 2);
			imgs[i].css({width:width});
			//imgs[i].width = parseInt(imgs[i].offsetWidth / 2) ;
		} */
		
		/* $("body").live("mousemove", function(event) {
			var event = event || window.event;
			$("#applemenu img").each(function(j) {
				var a = event.clientX - imgs[j].offsetLeft - imgs[j].offsetWidth / 2;
				var b = event.clientY - imgs[j].offsetTop - oMenu.offsetTop - imgs[j].offsetHeight / 2;
				var iScale = 1 - Math.sqrt(a * a + b * b) / 300;
				if(iScale < 0.5) {
					iScale = 0.5;
				}
				var width = aWidth[j] * iScale;
				$(this).css({width:width});
			})
		}); */
		
		/* for(var i = 0; i < imgs.length; i++) {
			aWidth.push(imgs[i].offsetWidth);
			imgs[i].width = parseInt(imgs[i].offsetWidth / 2) ;
			
			console.log(imgs[i]);
			
			
			imgs[i].onmousemove = function(event) {
				var event = event || window.event; 
				for (var j = 0; i < imgs.length; i++) { 
					var a = event.clientX - imgs[j].offsetLeft - imgs[j].offsetWidth / 2; 
					var b = event.clientY - imgs[j].offsetTop - oMenu.offsetTop - imgs[j].offsetHeight / 2; 
					var iScale = 1 - Math.sqrt(a * a + b * b) / 300; 
					if(iScale < 0.5) {
						iScale = 0.5; 
					}
					imgs[j].width = aWidth[j] * iScale 
				} 
			}; 
		} */

	});			
	
	
	/* $(function(){
	//将当前窗口宽度和高度数据存储在body元素上
	myLib.desktop.winWH();
	//存储桌面布局元素的jquery对象
	myLib.desktop.desktopPanel();
	//myLib.desktop.wallpaper.init("${ctx}/js/desktop/themes/default/images/blue_glow.jpg");
	myLib.desktop.navBar.init();
	var lrBarIconData = null;
	myLib.desktop.lrBar.init(lrBarIconData);
	
	myLib.desktop.taskBar.init();
	
});
*/

function getUserLoginNameList(ulist) {
	var uMap = {};
	$.each(ulist, function(k,uObj) {
		var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
		uMap[uObj.loginName] = uObj.realName + "[" + uObj.loginName + "]" + nickName;
	});
	return uMap;
}

//保存快捷方式
function shortcutSave(shortcutid) {
	fnFormAjaxWithJson({
		url:'${ctx}/sys/userconfig/user-config!save.action',
		data:{"shortcut":JSON.stringify({"id":shortcutid})}
	});
}

function addTab(title, url) {
	//console.log($('#task_lb a:contains("title")').length);
	if ($("#task_lb a[title='"+title+"']").length){
		$("#task_lb a[title='"+title+"']").trigger("click");
	} else {
		var raId = getRandomNum(1, 1000);
		//console.log(title);
		myLib.desktop.win.newWin({
			WindowTitle:title,
			iframSrc:url,
			WindowsId:"teskbartab_"+raId,
			WindowAnimation:'none'
			/* WindowWidth:$(window).width()-80,
			WindowHeight:$(window).height()-80 */
		});
	}
}

function closeTab(title) {
	//alert();
	//console.log(title);
	/* var tabWin = getTabDataWin(title);
	if(tabWin && tabWin.checkIsSaveBeforeClose) {
		return tabWin.checkIsSaveBeforeClose();
	} */
	//tabWin.checkIsSaveBeforeClose();
	$("#task_lb_wrap a").each(function(i) {
		if($(this).data(options).title==title){
			//console.log(title);
			myLib.desktop.win.closeWin($("#myWin_"+$(this).data('win')));
		}
		/* var abc = $("#task_lb a:contains('"+title+"')").data("win");
		console.log(abc);
		myLib.desktop.win.closeWin($("#myWin_"+tab.data('win'))); */
	});
}

function closeCurrentTab() {//alert();
	//console.log($("#taskBarWrap a.selectTab"));
	
	
	//console.log(tab);
	tab = $("#task_lb_wrap a.selectTab");
	if(tab.length) {
		//console.log($("#myWin_"+tab.data("win")));
		
		myLib.desktop.win.closeWin($("#myWin_"+tab.data('win')));
		
		//$("#myWin_"+tab.data("options").title).remove();
	}
}

function getCurrentTabTitle() {
	//var tab = $("#main_tabs_div").tabs('getSelected');
	var title = "";
	var tab = $("#task_lb_wrap a.selectTab");
	//console.log(tab);
	if(tab.length) {
		//console.log(tab.id);
		title=tab.data("options").title;
	}
	return title;
}

function doModifyTabTitle(title) {
	var tab = $("#task_lb_wrap a.selectTab");
	if(tab.length) {
		tab.data("options").title = title;
		var text = title;
		if(title.replace(/[^\x00-\xff]/g, '__').length > 12) {
			text = title.substring(0,5) + '...';
		}
		var c = tab.find("img").attr("class");
		var str = "<img src='js/desktop/themes/default/images/null.png' style='margin-bottom:-3px;height:16px' class='" 
	    + c + "' /> " + text;
		//str = tab.text().replace(title,text);
		tab.text('');
		tab.append(str);
	}
	var mywin = $("#myWin_"+tab.data('win'));
	if(mywin.length){
		mywin.find("b").text(title);
	}
}

function refreshTabData(title) {
	var win = getTabDataWin(title);
	if(win){
		win.doRefreshDataGrid();
	}
	//getTabDataWin(title).doRefreshDataGrid();
}

function getTabDataWin(title) {
	var obj = null;
	var tb = null;
	//console.log(title);
	
	$("#task_lb_wrap a").each(function(i){
		//console.log(title);
		//console.log($(this).data("options"));
		if($(this).data("options").title==title){
			tb = $(this);
		}
	});
	//var tb = $('#main_tabs_div').tabs('getTab',title);
	if(tb) {
		if(tb.length) {
			var tbby = $("#myWin_"+tb.data('win'));
			if(tbby) {
				var tb_ifr = tbby.find("iframe");
				if(tb_ifr) {
					var win = tb_ifr[0].contentWindow.window;
					obj = win;
				}
			}
		}
	}
	return obj;
}

function getRandomNum(min,max) {
	var range = max - min;
	var rand = Math.random();
	return(min + Math.round(rand * range));
}

function doCanel(){
	$("#aboutPass").window("close");
}

function doUpdate(){
	var tab = $('#userInfo').tabs('getSelected');
	var tl = tab.panel('options').title;
	if(tl=='<s:text name="system.login.userInfo.title" />'){
		var options = {
				url:'${ctx}/sys/account/user!updateUserInfo.action',  
			    success:function(data){  
			    	doCanel();
			    	window.location.replace("${ctx}/index-qq.action");
			    }  
			};
		fnAjaxSubmitWithJson("userInfoForm",options);						
	}else{
		var options = {
				url:'${ctx}/sys/account/user!updatePassWord.action',
			    success:function(data){  
			    	$.messager.alert('<s:text name="system.javascript.alertinfo.titleInfo"/>', '密码已修改成功，请重新登录', "info");
			    	setTimeout(function(){
			  			window.location.replace("${ctx}/logout.action");
			  		},1000);
			    	
			    }  
			};
		fnAjaxSubmitWithJson("passWordForm",options);							
	}				
}

function setCurrentTime() {
	$("#_span_time_").text(new XDate().toString("yyyy年MM月dd日  HH:mm"));
}

function initMenu() {
	var options = {
		url:'${ctx}/index!view.action',
		success:function(data) {
			var msg = data.msg;
			myLib.desktop.desktopPanel();
			//初始化桌面背景
			myLib.desktop.wallpaper.init("${ctx}/js/desktop/themes/default/images/blue_glow.jpg");
			
			//初始化一级模块
			initapplemenu(msg);
			
			//初始化桌面中的二级模块
			myLib.desktop.deskIcon.init(msg);
			
			//initdesktopInnerPanel(msg);

			//初始化任务栏
			myLib.desktop.taskBar.init();
			
			//初始化桌面
			//initdesk(msg);
			
			//初始化桌面导航栏
			//myLib.desktop.navBar.init();

			//初始化快捷方式
			initshortcut(msg);
			
			//初始化侧边栏
			//myLib.desktop.lrBar.init(lrBarIconData);
			
			//initdropmenu(msg);
			/* $('#mm').menu('show',{left:100,top:200});
			console.log($("#1188")); */
			
			//初始化子模块菜单
			myLib.desktop.childMenu.init(msg);
			
			//存储桌面布局元素的jquery对象
			myLib.desktop.desktopPanel();
			
			$.parser.parse();
			
		}
	};
	fnFormAjaxWithJson(options);
}

//自动生成快捷方式栏
function initshortcut(data) {
	fnFormAjaxWithJson({
		url:'${ctx}/sys/userconfig/user-config!view.action',
		success:function(shortcut){
			var shortcutid = [];
			var str = "";
			var mark = 0;//标记当前用户是否有权限变化
			var iconDatas = [];
			var k = 0;//计算快捷模块的绝对位置
			if(shortcut.msg != null){
				shortcutid = JSON.parse(shortcut.msg.shortcut).id;
			}
			for(var i=0;i<shortcutid.length;i++){
				var str_ = "";
				for(var j=0;j<data.length;j++){
					var adata = data[j].childs;
					for(var l=0;l<adata.length;l++){
						if(parseInt(shortcutid[i])==adata[l].id){
							//生成的数据将保存在元素的data中
							var absolute = {};
							var iconData = {
								'id':adata[l].id,
								'iconSkin':adata[l].iconSkin,
								'resourceUrl':'${ctx}' + adata[l].resourceUrl,
								'name':adata[l].name,
								'childsLength':adata[l].childs.length,//是否有下拉菜单
								'root':'${ctx}',
								'absoluteSite':absolute
							};
							k++;
							iconDatas.push(iconData);
							str_ = myLib.desktop.lrBar.newShortcutHtml(iconData);
							break;
						}
					}
					if(str_ != ""){
						break;
					}
				}
				//如果在桌面没有找到相应的快捷方式模块，说明当前用户权限有变化
				if(str_ == ""){
					mark = 1;
				}
				else{
					str = str + str_;
				}
			}
			//添加我的桌面
			var iconData = {
				'id':'mydesk',
				'iconSkin':'icon-desktop',
				'resourceUrl':'${ctx}/personal-index-qq.action',
				'name':'我的桌面',
				'childsLength':0,
				'root':'${ctx}',
				'absoluteSite':{}
			};
			iconDatas.push(iconData);
			str = str + myLib.desktop.lrBar.newShortcutHtml(iconData);
			//加入模块快捷方式
			$("#default_app").append(str);
			//为每个元素添加数据
			$("#default_app li")
			.each(function(i){
				$(this).data('iconData',iconDatas[i]);
			});
			myLib.desktop.lrBar.init();
			$("#default_app li#lrBardeskIconmydesk").trigger("click");
		}
	});
}
		
//自动初始化桌面图标JSON
function initdesk(data) {
	var deskIconData = {};
	for(var i = 0; i<data.length; i++) {
		var adata = data[i].childs;
		for(var j = 0; j<adata.length; j++) {
			if(adata[j].resourceUrl=="") {
				deskIconData['deskIcon'+adata[j].id] = {
						'title':adata[j].name,
						'url':""
						/* 'winWidth':1150,
						'winHeight':750 */
					}
			}
			else{
				deskIconData['deskIcon'+adata[j].id] = {
						'title':adata[j].name,
						'url':"${ctx}" + adata[j].resourceUrl
						/* 'winWidth':1150,
						'winHeight':750 */
					}
			}
		}
	}
	myLib.desktop.deskIcon.init(deskIconData);
}
		
//自动生成applemenu
function initapplemenu(data) {
	var str = "<div id = 'applemenu' style = 'z-index:90'><div style = 'width:"+ data.length*128 +"px; margin:auto; '>";
	for(var i = 0; i<data.length; i++) {
		str = str + "<a style='display:block; position:absolute; bottom:5px; left:"
		+ ((i-data.length/2)*84+$(window).width()/2) +"px'><img src='${ctx}/js/desktop/themes/default/images/null.png' id = '"
		+ data[i].id + "' class = 'imgIcon " + data[i].iconSkin
		+ "-auto' style = 'z-index:9000' /><br/><span>" + data[i].name + "</span></a>";
	}
	str = str + "</div></div>";
	$("#applemenu").replaceWith(str);
	var oMenu = document.getElementById("applemenu");
	var imgs = oMenu.getElementsByTagName("img");
	var aWidth = [];
	var offsetLeft = [];
	//为applemenu添加点击事件
	$("#applemenu img").each(function(i) {
		$(this).bind("click", function() {
			//console.log($(this));
			var myData=myLib.desktop.getMydata(),
			$navBar=myData.panel.navBar,
			$innerPanel=myData.panel.desktopPanel.innerPanel,
			$navTab=$navBar.find("a"),
			$deskIcon=myData.panel.desktopPanel['deskIcon'],
			desktopWidth=$deskIcon.width(),
			lBarWidth=myData.panel.lrBar["_this"].outerWidth();
			//event.preventDefault();
			//event.stopPropagation();
			//console.log($navBar);
			//console.log($navTab);
			myLib.desktop.deskIcon.desktopMove($innerPanel,$deskIcon,$navTab,500,desktopWidth+lBarWidth,i);
		});
		$(this).data("index",i);
	});
	//添加特效
	/* var oMenu = document.getElementById("applemenu");
	var imgs = oMenu.getElementsByTagName("img");
	var aWidth = [];
	var offsetLeft = []; */
	$(oMenu).find("img").hover(function(){
		$(this).addClass("desktop_icon_over_a");
	}, function(){
		$(this).removeClass("desktop_icon_over_a");
	});
	$(imgs[0]).addClass("desktop_icon_over_b");
	//var offsetTop = [];
	for(var i = 1; i < imgs.length; i++) {
		//aWidth.push(imgs[i].offsetWidth);
		$(imgs[i]).addClass("desktop_icon_over_c");
		//var width = parseInt(imgs[i].offsetWidth / 2);
		//$(imgs[i]).css({width:width});
		/* console.log($(imgs[i]).context.x);
		console.log(imgs[i].offsetLeft); */
		//offsetLeft.push($(imgs[i]).context.x-imgs[i].offsetLeft);
		//offsetTop.push($(imgs[i]).context.y-imgs[i].offsetTop);
	}

}

//自动生成deskIcon
function initdesktopInnerPanel(data) {
	var str = "<div id='desktopInnerPanel'>";
	//需要注册事件的deskIconID
	var deskIconID = [];
	for(var i = 0; i < data.length; i++) {
		var adata = data[i].childs;
		//console.log(adata);
		if(i===0) {
			str = str + "<ul id='" + i + "' class='deskIcon currDesktop'>";
		}
		else {
			str = str + "<ul id='" + i + "' class='deskIcon'>";
		}
		for(var j = 0; j < adata.length; j++) {
			if(adata[j].childs.length) {
				str = str + "<li iconSkin='imgIcon " + adata[j].iconSkin + "' class='desktop_icon' id='deskIcon" + adata[j].id
				//+ "' onclick=\"javascript:$('#menu" + adata[j].id + "').menu('show',{left:100,top:200})\""
				//+ "' onclick=\"javascript:$('menua11').menu('show',{left:100,top:200})\""
				+ "'><span class='icon'><img src='${ctx}/js/desktop/themes/default/images/null.png' class = 'imgIcon "
				+ adata[j].iconSkin + "-auto' /></span><div class='text'>"
				+ adata[j].name + "<s></s></div></li>";
				//console.log(adata[j]);
				var menustr = "<div id='menu"+ adata[j].id +"' class='easyui-menu easyui-panel' style='width:220px;height:250px'>";
				//var menustr = "<div id='a11' class='easyui-menu' style='width:150px;'>";
				for(var l=0;l<adata[j].childs.length;l++){
					menustr = menustr + initdropmenu(adata[j].childs[l],adata[j].id);
				}
				menustr = menustr + "</div>";
				$("#applemenu").after(menustr);
				deskIconID.push(adata[j].id);
			}
			else {
				str = str + "<li myiframSrc='${ctx}"+adata[j].resourceUrl+"' iconSkin='imgIcon " + adata[j].iconSkin + "' class='desktop_icon' id='deskIcon" + adata[j].id
				+ "'><span class='icon'><img src='${ctx}/js/desktop/themes/default/images/null.png' class = 'imgIcon "
				+ adata[j].iconSkin + "-auto' /></span><div class='text'>"
				+ adata[j].name + "<s></s></div></li>";
			}
		}
		str = str + "</ul>";
	}
	str = str + "<ul class='deskIcon'><iframe src='${ctx}/personal-index-qq.action' style='height:" 
	+ ($(window).height()-52)+"px;width:" + ($(window).width()-75)+"px;border-width:0px;position:relative;left:-30px;top:-20px;'></iframe></ul></div>";
	$("#desktopInnerPanel").replaceWith(str);
	//为下拉的图标注册事件
	//FIXME
	for(i=0;i<deskIconID.length;i++) {
		//console.log(i);
		//console.log("#deskIcon"+deskIconID[i]);
		/* var deskIconid="#deskIcon"+deskIconID[i];
		var menuid="#menu"+deskIconID[i]; */
		//console.log(menuid);
		$("#deskIcon"+deskIconID[i]).live('click', function(e) {
			//console.log($(deskIconid)[0].offsetLeft);
			//console.log(obj = $(e.currentTarget));
			//console.log($(e.currentTarget)[0].id.replace(/deskIcon/,"menu"));
			var menuid="#" + $(e.currentTarget)[0].id.replace(/deskIcon/,"menu_");
			var deskIconid="#" + $(e.currentTarget)[0].id;
			//console.log($(deskIconid).css("top"));
			var left = $(deskIconid)[0].offsetLeft + 157;
			var top = $(deskIconid)[0].offsetTop + 80;
			$(menuid).menu('show',{left:left,top:top});
			//console.log(xx);
			//console.log($("#deskIcon"+deskIconID[i]));
			//console.log($(deskIconmenuid));
		});
	}
}

/* //自动生成下拉菜单
function initdropmenu(node) {
	//console.log(node);
	//console.log("node");
	//console.log(node.resourceUrl);
	if(node.childs.length==0) {
		console.log(node);
		var str = "<li class='mymark'><a onclick=\"myLib.desktop.win.newWin({"
			+"WindowTitle:'"+node.name+"',"
			+"iframSrc:'${ctx}"+node.resourceUrl+"',"
			+"WindowsId:'"+node.id+"',"
			+"WindowAnimation:'none',"
			+"WindowWidth:"+1150+","
			+"WindowHeight:"+750+","
			+"imgclass:$(this).attr('iconSkin')"
		+ "});\">"
		//var str = "<li><a href='javascript:;'>"
		
		+ "<img src='${ctx}/js/desktop/themes/default/images/null.png' width='16px' class='imgIcon " + node.iconSkin + "' />"
		+ "  " + node.name +"</a></li>";
		return str;
	}
	else{
		var str = "<li class='dropdown-submenu mymark'><a href='javascript:;'>"
		+ "<img src='${ctx}/js/desktop/themes/default/images/null.png' width='16px' class='imgIcon " + node.iconSkin + "' />"
		+ "  " + node.name + "</a><ul class='dropdown-menu'>";
		//console.log(str);
		for(var i = 0; i<node.childs.length; i++) {
			str = str + initdropmenu(node.childs[i]);
		}
		str = str + "</ul></li>";
		return str;
	} */
	
	/* function initdropmenuclick(){
		alert("A");
		var data=$(this).data("iconData"),id=this.id;
		if(data.url!=""){   
			myLib.desktop.win.newWin({
				WindowTitle:data.title,
				iframSrc:data.url,
				WindowsId:id,
				WindowAnimation:'none',
				WindowWidth:data.winWidth,
				WindowHeight:data.winHeight,
				imgclass:$(this).attr('iconSkin')
			});	
		}
	} */
	
//}

//自动生成下拉菜单
function initdropmenu(node,id) {
	//console.log(node);
	//console.log("node");
	//console.log(node.resourceUrl);
	if(node.childs.length==0) {
		//console.log(id);
		var str = "<div myparentid='"+ id +"' id='menu"+node.id+"' iconSkin='imgIcon "+ node.iconSkin +"' onclick=\"myLib.desktop.win.newWin({"
			+"WindowTitle:'"+node.name+"',"
			+"iframSrc:'${ctx}"+node.resourceUrl+"',"
			+"WindowsId:'menu"+node.id+"',"
			+"WindowAnimation:'none',"
			/* +"WindowWidth:"+1150+","
			+"WindowHeight:"+750+"," */
			+"imgclass:$(this).attr('iconSkin')"
		+ "});\" iconCls='imgIcon "+ node.iconSkin +"'><span>"
		+ node.name + "</span></div>";
		return str;
	}
	else{
		var str = "<div iconCls='imgIcon "+ node.iconSkin +"'><span>"
		+ node.name + "</span><div style='width:150px'>";
		for(var i = 0; i<node.childs.length; i++) {
			str = str + initdropmenu(node.childs[i],id);
		}
		str = str + "</div></div>";
		return str;
	}
}

/* 通知消息提示 */
function showMyNotice(data){
	var jsonMore=[{"href":"${ctx}/sys/notice/notice!noticeToUserList.action","title":"<s:text name='system.sysmng.notice.query.title'/>"}];
		var jsonData=data.msg.length;
		if(jsonData>0){
			 $.messager.show({
				    width:200,  
					height:100,  
					title:'<s:text name="system.sysmng.notice.my.title"/>',
					msg: "<a href='javascript:void(0);' onclick='openImageNews(\""+jsonMore[0].href+"\",\""+jsonMore[0].title+"\")'>您有"+jsonData+"条通知请查收!</a>",
					timeout:60000,
					//showType:'slide'
					showType:'fade'
				}); 
		}
	} 
 function getNotices(flag){
	var  notices = getNoticesNum();
	var process = getToDoNum(); 
	if(notices && !process){
		/* $("#aboutDoAction").window("open");
		$("#aboutDoAction").window({
		    title:'右边倒三角按钮设置是否弹出',
		    //toolbar:[{text:'增加',iconCls:'icon-add',handler:function(){alert("a");}}],
			tools: [{
		    iconCls:'icon-edit',
		    handler:function(){$("#aboutDoActionConfig").window("open");alert("a")}
		  	}]
		}); */
		$("#aboutDoAction").window("open");
		$("#tnotice").tabs('select','<s:text name="system.sysmng.notice.list.title"/>');
		}
	else if(notices || process || flag){
		$("#aboutDoAction").window("open");	
		}
	$("#keyWords").val("");
 	}

 
 function aboutDoActionConfig() {
	 var value = $("#aboutDoActionConfig").combobox("getText");
	 var zhengzhengshu = /^[1-9]\d*$/;//正整数
	 if(zhengzhengshu.test(value)&&parseInt(value)!==0) {
		 var time = parseInt(value) * 60000;
		 //console.log(value);
		 createCookie('aboutDoActionConfigTime',value,365);
		 //console.log(document.cookie);
		 if(aboutDoActionConfigID!=null){
			 clearInterval(aboutDoActionConfigID);
		 }
		 aboutDoActionConfigID = setInterval(getNotices,time);
		 alert("成功设置弹出时间间隔为"+ value +"分钟");
	 }
	 else if(value=="不弹出"||value=='0') {
		 createCookie('aboutDoActionConfigTime',0,365);
		 if(aboutDoActionConfigID!=null){
			 clearInterval(aboutDoActionConfigID);
		 }
		 $("#aboutDoActionConfig").combobox("setText","不弹出");
		 alert("成功设置为不自动弹出");
	 }
	 else {
		 $("#aboutDoActionConfig").combobox("setText","");
		 alert("请输入整数或从下拉列表中选值");
	 }
 }
 function windowInit(){
	//打开待办事宜窗口时，初始化化消息和待办事宜列表
	$("#aboutDoActionConfig").combobox({
         /* onChange:function(newValue,oldValue){
        	 //alert(newValue);
        	 //egg(newValue);
         }, */
         /* onHidePanel:function() {
        	 egg();
         }, */
         panelHeight:"150"
	 });
 $("#aboutDoAction").window({
	 minimizable :false,
	 collapsible :false,
	 onClose :function(){
		var  notices = getNoticesNum();
		var process = getToDoNum(); 
		var no = 0;
		var po = 0;
		if(notices){
			no = parseInt(notices);
		}
		if(process){
			po = parseInt(process);
		}
		var total = no+po;
		var str = '<s:text name="system.top.message.title"/>(<span style="color:red;">'+total+'</span>)';
		 //$("#myNotice").window("setTitle",str);
		 //$("#myNotice").window("open");
	 	},
	 onOpen: function(){
	 		$("#myNotice").window("close");
	 		$("#toDoList").datagrid(getToDoList());
			$("#noReadNoticeDetail").datagrid(getCurrentUserNotices());
		}
	});
 $("#myNotice").window({
	 title :'<s:text name="system.top.message.title"/>',			
	 collapsible:false,		
	 minimizable:false,		
	 maximizable:true,		
	 closable:false,
	 closed:true,
	 resizable:false,
	 inline:false,
	 onOpen:function(){
		 //FIXME
		var notices = getNoticesNum();
		var process = getToDoNum(); 
		var no = 0;
		var po = 0;
		if(notices){
			no = parseInt(notices);
		}
		if(process){
			po = parseInt(process);
		}
		var total = no + po;
		var str = '<s:text name="system.top.message.title"/>(<span style="color:red;">'+total+'</span>)';
		$("#myNotice").window("setTitle",str);
		myNoticeID = setInterval(function(){
			var notices = getNoticesNum();
			var process = getToDoNum(); 
			var no = 0;
			var po = 0;
			if(notices){
				no = parseInt(notices);
			}
			if(process){
				po = parseInt(process);
			}
			var total = no+po;
			var str = '<s:text name="system.top.message.title"/>(<span style="color:red;">'+total+'</span>)';
			$("#myNotice").window("setTitle",str);
		},5000);
			$(this).window("move", {
				top:5,
				left:300
			});				 
		},
	onMaximize:function() {
		$("#myNotice").window("restore");
		$("#myNotice").window("close");
		if(myNoticeID!=null){
			clearInterval(myNoticeID);
		}
		getNotices(true);
		}
 	});
 }
	 function openImageNews(url,title){
			parent.addTab(title,url);
		 }		 
	 function doSignin(id,name){
			window.parent.addTab(name+'_'+id,'${ctx}/gs/process!getProcess.action?taskId='+id);
			 $("#aboutDoAction").window("close");
		}	 
	//查询当前用户所有的待办事宜数量		
	function getToDoNum(){
		 var flag = false;
	       	var options = {
	       		async :false,
	             url : '${ctx}/gs/process!toDoNumber.action',
	             success : function(data) {
	            	 if(data.msg>0){
	            		 flag =  data.msg;		         
						}			
	             }
	        };
	        fnFormAjaxWithJson(options,true);  
	       return flag;
	}
	 //查询当前用户所有的待办事宜
	 function getToDoList(){
		   return {
	        		width : 'auto',
		height : 'auto',
		url : '${ctx}/gs/process!toDoList.action',
		fitColumns : true,
		nowrap : false,
		fit: true,
		pagination : true,
		rownumbers : true,
		remoteSort: true,
		striped : true,
		pageSize: 10,
		pageList: [10,20,30,50,100],
		sortName : 'createTime',
	             sortOrder : 'desc',
	         /* 	rowStyler :  function(rowIndex,rowData) {
	         		var date =rowData.createTime;
	         		var d2 =new Date();
	         		var d1= new Date(Date.parse(date.replace(/-/g, "/")));  
	         		var day = d1.dateDiff(d2, 'd');
	         		day= Math.abs(day);
	         		if(day>=3){            			
	         			return 'background:pink';
	         		}
		}, */
	             columns :[[
			{field:'moduleName',title:'<s:text name="system.sysmng.process.moduleName.title"/>',width:80, sortable:false},
			{field:'name',title:'<s:text name="system.sysmng.process.taskName.title"/>',width:120, sortable:false,
				formatter :  function(value, rowData, rowIndex) {	
					var newName = value;
					if(rowData.field) {
						newName = value+'-'+rowData.field;
					}
					return newName;
				}
			},
			{field:'createTime',title:'<s:text name="system.sysmng.desktop.createTime.title"/>',width:130, sortable:false},
			{field:'startUser',title:'<s:text name="system.sysmng.process.startUser.title"/>',width:100,sortable:false,
			 formatter:function(value, rowData, rowIndex) {
				 var val = _userLoginNameList[value];
				 if(val) {
					 return val;
				 } else {
					 return value;
				 }
			 }},
			{
					field : 'oper',
					title : '<s:text name="system.button.oper.title"/>',
					width : 80,
					align : 'left',
					formatter :  function(value, rowData, rowIndex) {	
						var strArr = [];
						if(rowData.assignee==null){
						strArr.push('<a href="javascript:void(0);" onclick="doSignin(\'');
						strArr.push(rowData.id+'\',\''+rowData.name);
						strArr.push('\')"><s:text name="system.sysmng.process.signFor.title"/></a>');
						}else if(rowData.assignee!=null){
							strArr.push('<a href="javascript:void(0);" onclick="doSignin(\'');
							strArr.push(rowData.id+'\',\''+rowData.name);
							strArr.push('\')"><s:text name="system.sysmng.process.transact.title"/></a>');
						}
						return strArr.join("");
					}
			}
				]]
	         		};
			   
	   }
	 
	 //查询当前用户所有通知的数量
	 function getNoticesNum() {
		 var flag = false;
	       	var options = {
	       		async :false,
	             url : '${ctx}/sys/notice/notice!queryNoReadNoticeNum.action',
	             success : function(data) {
	            	 if(data.msg>0){
	            		 flag =  data.msg;	            		 		            	
					}			
	             }
	        };
	        fnFormAjaxWithJson(options,true);  
	       return flag;
	   }
 //查询当前用户所有通知queryNoReadNoticeNum
	 /* function getCurrentUserNotices() {
		 var flag = false;
	       	var options = {
	       		async :false,
	             url : '${ctx}/sys/notice/notice!queryNoReadNotice.action',
	             success : function(data) {
	            	 transToDatagridJson(data);
	            	 var dt=data.msg;
	            	if(dt.length){
	            		for(var i=0;i<dt.length;i++){
	            			var style=dt[i][0];
	            			var count=dt[i][1];
	            			alert(style+":"+count);
	            		}
	            	} 
	            	 if(data.msg && data.msg.length>0){
	            		 flag =  true;
						}			
	            	 //showMyNotice(data);
	             }
	        };
	        fnFormAjaxWithJson(options,true);  
	        return flag;
	   } */
	 		 		 
	//初始化当前用户未读的消息通知datagrid
		function getCurrentUserNotices(){
		return{
				width : 'auto',
				height : 'auto',
				url : '${ctx}/sys/notice/notice!queryNoReadNotice.action',
				fitColumns : true,
				nowrap : false,
				fit: true,
				pagination : true,
				rownumbers : true,
				pageSize: 10,
				pageList: [10,20,30,50,100],
				remoteSort: true,
				striped : true,
				columns : [ [ {
                    field : 'title',
                    title : '<s:text name="system.sysmng.desktop.modelTitle.title"/>',
                    width : 160,
                    sortable : false
                } ,
               {
					field : 'style',
					title : '<s:text name="system.sysmng.notice.style.title"/>',
					width : 80,
					sortable : false,
				    sortOrder : 'asc',
					formatter : function(value, rowData, rowIndex) {
						if(value=="01"){
							return "<s:text name='system.sysmng.notice.common.title'/>";
						}else if(value=="02"){
							return "<s:text name='system.sysmng.notice.warning.title'/>";
						}else if(value=="03"){
							return "<s:text name='system.sysmng.notice.todo.title'/>";
						}else if(value=="04"){
							return "<s:text name='业务确认通知'/>";
						}else if(value=="05"){
							return "<s:text name='提醒通知'/>";
						}else if(value=="06"){
							return "<s:text name='业务办理通知'/>";
						}

					},styler:function(value,row,index){
						if (value=="01"){
							return 'color:blue;';
						}else if(value=="02"){
							return 'color:red;';
						}else if(value=="03"){
						}
					}
				},{
                    field : 'createUser',
                    title : '<s:text name="system.sysmng.desktop.createUser.title"/>',
                    width : 100,
                    sortable : false,
                	formatter : function(value, rowData, rowIndex){	
                        var uObj=_userList[value];
                        if(uObj){
                        	var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
							return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                        }else{
                            return value;
                        }
    				}
                } ,
                {
                    field : 'createTime',
                    title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
                    width : 120,
                    sortable : false             
                }] ],
				onClickRow:function(rowIndex, rowData){
                	 $("#aboutDoAction").window("close");
                	 if(rowData.style=="01" ||rowData.style=="04"){
                		doDblView(rowData.id,rowData.ntuId,rowData.style);
					}else if(rowData.style=="03" ){//待办通知(办理任务)
						doSignin(rowData.moduleDataId,rowData.moduleKey);
						doRead(rowData.ntuId);
					}else if(rowData.style=="05" ){//提醒通知(查看历史)
						parent.addTab('<s:text name="system.sysmng.process.dealHistory.title"/>-'+rowData.moduleDataId, 
						'${ctx}/gs/process!getProcess.action?processInstanceId=' + rowData.moduleDataId +'&rptData='+(rowData.moduleFields ? rowData.moduleFields : ""));
						doRead(rowData.ntuId);
					}else if(rowData.style=="06"){
						if(rowData.moduleFields=="true"){
							parent.addTab('<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'+rowData.moduleDataId, '${ctx}/gs/gs-mng!view.action?entityName='+rowData.moduleKey+'&id=' + rowData.moduleDataId);
						}else if(rowData.moduleFields=="fasle"){
							parent.addTab('<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'+rowData.moduleDataId, '${ctx}/gs/gs-mng!view.action?entityName='+rowData.moduleKey+'&id=' + rowData.moduleDataId+'&isEdit=2');	
						}							
						doRead(rowData.ntuId);
					}else if(rowData.style=="02") {
						parent.addTab('<s:text name="system.button.view.title"/><s:text name="业务办理通知"/>-'+rowData.moduleDataId, '${ctx}/gs/gs-mng!view.action?entityName='+rowData.moduleKey+'&id=' + rowData.moduleDataId);
						doRead(rowData.ntuId);
					}
                	$('#queryList').datagrid('unselectRow',rowIndex);
                }
			};

		}
	   var _userList={};
	   var _userLoginNameList = {};
		  function doDblView(id,ntuId,style) {
			  parent.addTab('<s:text name="system.sysmng.notice.view.title"/>-'+id, '${ctx}/sys/notice/notice!viewNoticeToUser.action?id=' + id+"&nTouId="+ntuId+"&style="+style);            	
           }
		function doRead(nTouId){
				var options = {
						url : '${ctx}/sys/notice/notice!doRead.action',
						data : {
							"noticeToUserId" : nTouId
						},
						success : function(data) {
							if(data.msg){
						
							}
						}
					};
					fnFormAjaxWithJson(options); 						
			}
		//快速查询消息或待办事宜
		function queryDoList(){
			var selectPanel = $("#tnotice").tabs("getSelected");
			var selectTitle = selectPanel.panel('options').title;
			var queryValue = $("#keyWords").val();
			if(selectTitle == '<s:text name="system.sysmng.notice.todoSomething.title"/>'){
				$("#toDoList").datagrid("clearSelections");
				var data = $("#toDoList").datagrid("getData");
				var rowData = data.rows;
				 var len=rowData.length;
            	 for(var i=0;i<len;i++){
            		 var taskName = "";
            		 if(rowData[i].field) {
            			 taskName = rowData[i].name+'-'+rowData[i].field;
     				}else{
     					taskName = rowData[i].name;
     				}
            			var isSelect =  taskName.indexOf(queryValue);
                    	if(isSelect>-1){
                    		$("#toDoList").datagrid("selectRow",i);
                    	}
            	 }
			}else if(selectTitle == '<s:text name="system.sysmng.notice.list.title"/>'){
				$("#noReadNoticeDetail").datagrid("clearSelections");
				var data = $("#noReadNoticeDetail").datagrid("getData");
		 		var rowData = data.rows;
				 var len=rowData.length;
            	 for(var i=0;i<len;i++){
            		var taskName = rowData[i].title;	     			
            		var isSelect =  taskName.indexOf(queryValue);
                    if(isSelect>-1){
                    		$("#noReadNoticeDetail").datagrid("selectRow",i);
                    }
            	 } 
			}       
		}
		//初始化个人信息
		function initgerenxinxi(){
			$("#gerenxinxi").css({width:110,height:40,position:'absolute',left:5,top:5,zIndex:9999});
			var str = "<div style='padding-left:10px;padding-top:4px'>" + new XDate().toString('yyyy-MM-dd') + "</div><div id='gerenxinxitime' style='padding-left:27px;'>"
			+ new XDate().toString("HH:mm") + "</div>";
			$("#gerenxinxi").append(str);
			$("#gerenxinxi").mouseover(function(event){
				$("#gerenxinxi_menu").menu('show',{left:$(this).offset().left+5,top:$(this).offset().top+35});
			}).mouseout(function(event){
				$("#gerenxinxi_menu").menu('hide');
			});
			$("#gerenxinxi_menu").css({width:300});
			
			//$("#gerenxinxi").css("background-color","#FFF");
			//$("#gerenxinxi").css({background-color:"red",position:'absolute',left:30,top:20});
		}
	</script>
</body>
</html>

