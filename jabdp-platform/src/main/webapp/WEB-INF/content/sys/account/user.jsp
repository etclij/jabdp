<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title" />
</title>
<%@ include file="/common/meta-gs.jsp"%>
<script type="text/javascript">

		
		var setting = {
				data : {
					simpleData : {
						enable : true
					}
				},
				view : {
					dblClickExpand : false
				},
				callback : {
					onRightClick : OnRightClick,
					onClick : onClick
				}
			};
			
		function initMenu(){
			var options = {
				url : '${ctx}/sys/account/organization!ztreeList.action',
				success : function(data) {
					if (data.msg) {
						$.fn.zTree.init($("#organizationTree"), setting, data.msg);
						zTree = $.fn.zTree.getZTreeObj("organizationTree");
						var rootNodes = zTree.getNodes();
						zTree.expandNode(rootNodes[0], true, false, true);

					}
				}
			};
			fnFormAjaxWithJson(options,true);
		}
			
	$(function() {
		$("#userFrame")[0].src = "${ctx}/sys/account/user!init.action?orgId=1";
		$("#win_message").window({
			width:500,
			height:200,
			onOpen: function() {
				$(this).window("move", {
					top:($(window).height()-200)*0.5,
					left:($(window).width()-500)*0.5
				});
			},
			onClose:function() {
				$("#saveForm")[0].reset();
			}
		});
	    

	});
	
	function onClick(){
	var nodes = zTree.getSelectedNodes();
	var org_id = nodes[0].id;
	
	userFrame.location.href = "${ctx}/sys/account/user!init.action?orgId="
			+ org_id;
	roleFrame.location.href = "${ctx}/sys/account/role!init.action?orgId="
		+ org_id;
	}
	
	function OnRightClick(event, treeId, treeNode) {
		event.preventDefault();
		zTree.selectNode(treeNode);
			$('#mm').menu('show', {
				left : event.pageX,
				top : event.pageY
			});
	}

	function doAppend() {
		var nodes = zTree.getSelectedNodes();
		var id = nodes[0].id;
		var name = nodes[0].name;
		$('#orgMethod').val("add");
		/*$("#parentIds").combotree('reload');
		$("#parentIds").combotree('setValue', id);
		$("#parentIds").combotree('disable');*/
		//$("#parentIds").val(name);
		//$("#parentId").val(id);
		$("#parentIds").selectTree('setValue', id);
		$("#parentIds").selectTree("enable");
		$("#win_message").window("open");
		$("#organizationName").focus();

	}
	//取消
	function cancel() {
		$('#saveForm')[0].reset();
		$("#id").val("");
		$("#parentId").val("");
		$('#win_message').window('close');
		//$("#parentIds").combotree('reload');
	}
	function saveOrg() {
		var options = {
			url : '${ctx}/sys/account/organization!save.action',
			success : function(data) {
				cancel();
				initMenu();
				//$("#parentIds").combotree('reload');
			}
		};
		fnAjaxSubmitWithJson("saveForm", options);

	}

	//删除一个组织
	function doRemove() {
		$.messager
				.confirm(
						'<s:text name="system.javascript.alertinfo.title"/>',
						'<s:text name="system.javascript.alertinfo.info"/>',
						function(r) {
							if (r) {
								var nodes = zTree.getSelectedNodes();
								var id = nodes[0].id;
								var options = {
									url : '${ctx}/sys/account/organization!delete.action',
									data : {
										"parentId" : id
									},
									success : function(data) {
										if (data.msg) {
											initMenu();
										}
									}
								};
								fnFormAjaxWithJson(options);

							}
						});

	}
	function doModify() {
		var nodes = zTree.getSelectedNodes();
		var id = nodes[0].id;
		var options = {
			url : '${ctx}/sys/account/organization!modifyInfo.action',
			data : {
				"id" : id
			},
			success : function(data) {
				if (data.msg) {
					var json2fromdata = data.msg;
					var pId = json2fromdata['parentId'];
					$("#parentIds").selectTree('setValue', pId);
					$("#parentIds").selectTree("enable");
					//$("#parentIds").combotree('setValue', pId);
					//$("#parentIds").combotree('disable');
					$('#saveForm').form('load', json2fromdata);
					$('#orgMethod').val("modify");
					$("#win_message")
							.window("setTitle",
									"<s:text name="system.sysmng.organization.modify.title"/>");
					$("#win_message").window("open");
				}
			}
		};
		fnFormAjaxWithJson(options);
	}
	
	//选中或取消选中
	function doCheckNodesOnTree(treeId, param) {
		var treeObj = $.fn.zTree.getZTreeObj(treeId);
		treeObj.cancelSelectedNode();
		if(!param) {
			return;
		}
		var nodes = treeObj.getNodesByParamFuzzy("name", param, null);
		//for (var i=0, l=nodes.length; i < l; i++) {
		for (var i=nodes.length-1; i >= 0; i--) {	
			treeObj.selectNode(nodes[i],true);
		}
	}
		
	function initOrgTree() {
		$("#parentIds").selectTree({
			url:"${ctx}/sys/account/organization!ztreeList.action",
			queryParams:{},
			filterUrl:"${ctx}/sys/account/organization!findOrgList.action",
			filterParam:"filter_INL_id",
			pIdKey:"pId"
		});
	}
	
	function _doDictTreeQuery_() {
		var val = $("#_dictTree_qd_").val();
		doCheckNodesOnTree("organizationTree", val);
	}
	
	$(document).ready(function() {
		initMenu();
		initOrgTree();
		$("#_dictTree_qd_").bind('keydown', 'return',function (evt){evt.preventDefault(); _doDictTreeQuery_(); return false; });
	});
</script>
</head>
<body class="easyui-layout">
	<div region="west" split="true" border="false"
		title="<s:text name="system.sysmng.user.organization.title"/>"
		style="width: 230px; padding: 5px; overflow: auto;">
		<input type="hidden" name="authorityId" id="addId"/>
		<div><input type="text" id="_dictTree_qd_" style="width:130px" class="Itext"/>&nbsp;&nbsp;<button type="button" class="button_small" onclick="_doDictTreeQuery_()">树查询</button></div>
		<ul id="organizationTree" class="ztree"></ul>
	</div>

	<div region="center" style="overflow: hidden;" border="false">
		<div class="easyui-tabs" fit="true" border="false">
			<div title="<s:text name="system.sysmng.user.manager.title"/>"
				closable="false" style="overflow: hidden;">
				<iframe name="userFrame" id="userFrame" scrolling="auto"
					frameborder="0" src="" style="width: 100%; height: 100%;"></iframe>
			</div>
			<div title="<s:text name="system.sysmng.role.manager.title"/>"
				closable="false" style="overflow: hidden;">
				<iframe scrolling="auto" name="roleFrame" id="roleFrame"
					frameborder="0" src="${ctx}/sys/account/role!init.action?orgId=1"
					style="width: 100%; height: 100%;"></iframe>
			</div>
		</div>

	</div>





	<!--右键菜单  -->
	<div style="display:none;">
	
	<div id="mm" class="easyui-menu" style="width: 120px;">
		<div onclick="doAppend()" iconCls="icon-add">
			<s:text name="system.sysmng.user.add.title" />
		</div>
		<div onclick="doModify()" iconCls="icon-edit">
			<s:text name="system.sysmng.user.modify.title" />
		</div>
		<div onclick="doRemove()" iconCls="icon-remove">
			<s:text name="system.sysmng.user.delete.title" />
		</div>
	</div>
	</div>
	<!--右键菜单  -->
	<!--win弹出框  新增 -->
	<div style="display:none;">
	<div id="win_message" class="easyui-window" closed="true" modal="true"
		title="<s:text name="system.sysmng.organization.add.title"/>"
		iconCls="icon-save"
		style="width: 500px; height: 200px; padding: 0px; background: #fafafa;">
		<div class="easyui-layout" fit="true">
			<div region="center" border="false"
				style="padding: 2px; background: #fff; border: 1px solid #ccc; overflow: hidden;">
				<form action="${ctx}/sys/account/organization!save.action"
					name="saveForm" id="saveForm" method="post">
					<input type="hidden" name="method" id="orgMethod"/>
					<input type="hidden" id="id" name="id" />
					<input type="hidden" id="organizationCode" name="organizationCode" />
					<input type="hidden" id="parentOrganizationCode" name="parentOrganizationCode" />
					<input type="hidden" id="status" name="status" value="1" />
					<table align="center" border="0" cellpadding="0" cellspacing="1"
						class="table_form">
						<tbody>
							<tr>
								<th><label for="parentIds"><s:text
											name="system.sysmng.organization.parent.title" />:</label>
								</th>
								<td><!-- <div id="parentName"></div>  -->
								<%-- <input type="hidden"
									name="parentId" id="parentId"></input>  --%>
								<input  id="parentIds"
									name="parentId" style="width: 200px"></input></td>
									</td>
							</tr>
							<tr>
								<th><label for="organizationName"><s:text
											name="system.sysmng.organization.name.title" />:</label>
								</th>
								<td><input type="text" name="organizationName"
									id="organizationName" class="Itext easyui-validatebox" required="true" style="width: 200px"></input>
								</td>
							</tr>
							<tr>
								<th><label for="organizationNo"><s:text
											name="system.sysmng.organization.no.title" />:</label>
								</th>
								<td><input type="text" name="organizationNo" class="easyui-numberbox" 
									id="organizationNo" style="width: 200px"></input>
								</td>
							</tr>
							<tr>
								<th><label for="organizationDesc"><s:text
											name="system.sysmng.organization.desc.title" />:</label>
								</th>
								<td><input type="text" name="organizationDesc" class="Itext"
									id="organizationDesc" style="width: 200px"></input>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
			<div region="south" border="false"
				style="text-align: right; height: 30px; line-height: 30px;">
				<a class="easyui-linkbutton" iconCls="icon-ok"
					href="javascript:void(0)" onclick="saveOrg();"><s:text
						name="system.button.save.title" /> </a> <a class="easyui-linkbutton"
					iconCls="icon-cancel" href="javascript:void(0)" onclick="cancel();"><s:text
						name="system.button.cancel.title" /> </a>
			</div>
		</div>
	</div>
	</div>

</body>
</html>

