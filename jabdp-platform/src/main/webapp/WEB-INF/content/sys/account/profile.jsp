<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.modifyprofile.title"/></title>
<%@ include file="/common/meta-css.jsp" %>
<style>
			.pwdcheck {
				/*margin:50px auto;
				width:700px;
				height:50px;*/
			}
			.pwdcheck p.pwdInput input.pwd{
				width:150px;
				height:20px;
			}
			.pwdcheck p.pwdInput label {
				font-size:12px;
			}
			.pwdcheck p.pwdInput label.tips{
				color:red;
			}
			.pwdcheck p.pwdInput label span {
				color:#f00;
				padding:0 5px;
			}
			.pwdcheck p.pwdColor {
				width:150px;
				height:5px;
				margin-top:6px;
				margin-left:6px;
			}
			.pwdcheck p.pwdColor span {
				display:inline-block;
				width:40px;
				height:4px;
				float:left;
				margin-right:10px;
				background:#ccc;
			}
			
			.pwdcheck p.pwdText {
				margin-left:6px;
			}
			.pwdcheck p.pwdText span{
				margin-right:20px;
				margin-left:10px;
				font-size:12px;
				color:#ddd;
			}

			/* 密码强度改变的样式 */

			.pwdcheck p.pwdColor span.co1 {
				background:#f00;
			}
			.pwdcheck p.pwdColor span.co2 {
				background:#00ff66;
			}

			.pwdcheck p.pwdColor span.co3 {
				background:#0033cc;
			}		
</style>
</head>
<body>
<div class="easyui-layout" fit="true">
					<div region="center" border="false">
						<div id="userInfo" class="easyui-tabs"  fit="true">
							<div title="<s:text name="system.login.userInfo.title" />"  style="padding:5px;">
							<form action="" name="userInfoForm" id="userInfoForm" method="post" style="margin: 0px;">
								  <input type="hidden" name="id" id="_u_id" value="<s:property value="#session.USER.id" />" />
								<table align="center" border="0" cellpadding="0" cellspacing="1"
									   class="table_form">
								  <tbody>
								  	<tr>						  		
								  		<th><label for="realName"><s:text name="system.sysmng.user.name.title" />:</label></th>
										<td align="left"><input class="easyui-validatebox Itext"  required="true" type="text" name="realName" id="realName" value="<s:property value="#session.USER.realName" />" ></input>&nbsp;<font color="red">*</font></td>
									</tr>
									<tr>
								  		<th><label for="nickname"><s:text name="system.sysmng.user.nickname.title" />:</label></th>
										<td align="left"><input  type="text" class="Itext" name="nickname" id="nickname" value="<s:property value="#session.USER.nickname" />" ></input></td>
									</tr>
									<tr>
								  		<th><label for="signature"><s:text name="英文名" />:</label></th>
										<td align="left"><input  type="text" class="Itext" name="signature" id="signature" value="<s:property value="#session.USER.signature" />" ></input></td>
									</tr>
									<tr>
								  		<th><label for="mobilePhone"><s:text name="system.sysmng.user.mobilephone.title" />:</label></th>
										<td align="left"><input class="easyui-validatebox Itext" required="true" value="<s:property value="#session.USER.mobilePhone" />" type="text" name="mobilePhone" id="mobilePhone" validType="mobile" invalidMessage="请输入正确的11位手机号码.格式:13120002221"></input>&nbsp;<font color="red">*</font></td>
									</tr>
									<tr>
								  		<th><label for="fax"><s:text name="固定电话" />:</label></th>
										<td align="left"><input class="easyui-validatebox Itext" value="<s:property value="#session.USER.fax" />" type="text" name="fax" id="fax"></input></td>
									</tr>
									<tr>
								  		<th><label for="email"><s:text name="system.sysmng.user.email.title" />:</label></th>
										<td align="left"><input class="easyui-validatebox Itext" type="text" value="<s:property value="#session.USER.email" />" name="email" id="email"  validType="email"></input></td>
									</tr>							
								</tbody>
							   </table>
								</form> 
								</div>
									<div title="<s:text name="system.login.changepasswords.title" />"  style="padding:5px;">
							<form action="" name="passWordForm" id="passWordForm" method="post" style="margin: 0px;">
								<table align="center" border="0" cellpadding="0" cellspacing="1" class="table_form">
								  <tbody>								 				
									<tr>
										<th><label for="oldPass"><s:text name="system.login.oldpassword.title" /></label></th>
										<td align="left"><input type="password" name="oldPass" id="oldPass" class="easyui-validatebox Itext" required="true" validtype="length[6,18]" invalidMessage="<s:text name="system.login.passwordLength.title"/>"/>&nbsp;<font color="red">*</font></td>
									  </tr>
									  <tr>
										<th valign="top"><label for="newPass"><s:text name="system.login.newpassword.title"/></label></th>
										<td align="left">
											<div class="pwdcheck">
												<p class="pwdInput">
												<input type="password" class="pwd easyui-validatebox Itext" id="newPass" name="newPass" required="true" maxlength="18">&nbsp;<font color="red">*</font>
												<label class="tips">由字母（区分大小写）、数字、符号组成6-18位</label>
												</p>
												<p class="pwdColor">
													<span class="c1"></span>
													<span class="c2"></span>
													<span class="c3"></span>
												</p>
												<p class="pwdText">
													<span>弱</span>
													<span>中</span>
													<span>强</span>
												</p>
											</div>
											<!-- <input type="password" name="newPass" id="newPass" class="easyui-validatebox Itext" required="true" validtype="length[6,18]" invalidMessage="<s:text name="system.login.passwordLength.title"/>"/>&nbsp;<font color="red">*</font> -->
										</td>
									  </tr>
									  <tr>
										<th><label for="confPass"><s:text name="system.login.confirmpassword.title" /></label></th>
										<td align="left"><input type="password" name="rePass" id="rePass" class="easyui-validatebox Itext" required="true" validType="equalTo['#newPass']" invalidMessage="<s:text name="system.login.passwordError.title"/>"/>&nbsp;<font color="red">*</font></td>
									  </tr>
								</tbody>
							   </table>
								</form> 
								</div>
								<div title="<s:text name="拥有角色" />"  style="padding:5px;">
									<div>
									  	<s:iterator value="#session.USER.roleList" id="array">
								  			<h4 style="display: inline-block;"><span class="label label-success"><s:property value="roleName"/></span></h4>
								  		</s:iterator>
									</div>
								</div>
							</div>
						</div>
						<div region="south" border="false" style="text-align: center; padding:10px; height:50px;">
							 <a id="mm" class="easyui-linkbutton" iconCls="icon-ok"
								href="javascript:void(0);" onclick="doUpdate();"><s:text
								name="system.button.save.title" /> </a></a>
					  	</div>
</div>
<%@ include file="/common/meta-js.jsp" %>
	<script type="text/javascript">
	function doUpdate(){
		var tab = $('#userInfo').tabs('getSelected');
		var tl = tab.panel('options').title;
		if(tl=='<s:text name="system.login.userInfo.title" />'){
			var options = {
					url:'${ctx}/sys/account/user!updateUserInfo.action',  
				    success:function(data){
				    	window.top.location.replace("${ctx}/index-v2.action");
				    }  
				};
			fnAjaxSubmitWithJson("userInfoForm",options);						
		}else{
			if(checkpwd($("#newPass"))<2) {
				alert("密码必须设置为中级等级及以上！");
				return false;
			}
			var options = {
					url:'${ctx}/sys/account/user!updatePassWord.action',  
				    success:function(data){  
				    	alert('<s:text name="system.javascript.alertinfo.titleInfo"/>:密码已修改成功，请重新登录！');
				    	setTimeout(function(){
				  			window.top.location.replace("${ctx}/logout.action");
				  		},1000);
				    	
				    }  
				};
			fnAjaxSubmitWithJson("passWordForm",options);							
		}				
	}
	
	$("#newPass").keyup(function(){
		/*var txt=$(this).val(); //获取密码框内容
		var len=txt.length; //获取内容长度
		if(txt=='' || len<6){
			$("p.pwdInput label").show();
			$("p.pwdInput label").addClass("tips");
		}else {
			$("p.pwdInput label").hide();
		}*/
		checkpwd($(this));
	});

	//全部都是灰色的
	function primary(){
		$("p.pwdColor span").removeClass("co1,co2,co3");
	}
	
	//密码强度为弱的时候
	function weak(){
		$("span.c1").addClass("co1");
		$("span.c2").removeClass("co2");
		$("span.c3").removeClass("co3");
	}
	//密码强度为中等的时候
	function middle(){
		$("span.c1").addClass("co1");
		$("span.c2").addClass("co2");
		$("span.c3").removeClass("co3");
	}
	
	//密码强度为强的时候
	function strong(){
		$("span.c1").addClass("co1");
		$("span.c2").addClass("co2");	
		$("span.c3").addClass("co3");
	}

	/**判断密码的强弱规则
	1、如果是单一的字符（全是数字 或 字母 ）长度小于 6  弱
	2、如果是两两混合 (数字+字母（大小） 或 数字+特殊字符  或 特殊字符+字母  长度大于 8  中)
	3、如果是三者组合 (数字 +大写字母+小写字母 或 数字+字母+特殊字符 长度>8  强）)
	**/
	
	//密码强弱判断函数
	function checkpwd(obj){
		var txt = $.trim(obj.val());//输入框内容 trim处理两端空格
		var len = txt.length;
		var num = /\d/.test(txt);//匹配数字
		var small = /[a-z]/.test(txt);//匹配小写字母
		var big = /[A-Z]/.test(txt);//匹配大写字母
		var corps = /\W/.test(txt);//特殊符号
		var val = num + small+big+corps; //四个组合


		if(len<1){
			primary();
		}else if(len<6){
			weak();
		}else if(len>6 && len<=8){
			if(val==1){
				weak();
			}else if(val==2){
				middle();
			}
		}else if(len>8){
			if(val==1){
				weak();
			}else if(val==2){
				middle();
			}else if(val==3){
				strong();
			}
		}
		return val;
	}

	</script>		
</body>
</html>