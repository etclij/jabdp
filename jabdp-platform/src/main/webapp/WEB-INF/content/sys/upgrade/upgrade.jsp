<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title"/></title>
<%@ include file="/common/meta-gs.jsp" %>
<!-- uploadify -->
<link href="${ctx}/js/uploadify/uploadify.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="${ctx}/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript" src="${ctx}/js/uploadify/swfobject.js"></script>
<script type="text/javascript">
function getOption() {
	return {
		title:'部署日志',
		width:600,
		height:350,
		nowrap: false,
		striped: true,
		fit: true,
		url:'${ctx}/sys/upgrade/upgrade!queryList.action',
		sortName: 'id',
		sortOrder: 'desc',
		idField:'id',
		frozenColumns:[[
          {title:'<s:text name="system.search.id.title"/>',field:'id',width:40,sortable:true,align:'right',hidden:true}
		]],
		columns:[[
					{field:'deployName',title:'<s:text name="部署名称"/>',width:120},
					{field:'deployType',title:'<s:text name="部署类型"/>',width:60,
					 formatter:function(value,rowData,rowIndex){
						if(value=="1"){
							return "<s:text name="设计器"/>";
						} else if(value=="0"){
							return "<s:text name="系统"/>";
						} else{
							return "";
						}
					}},
					{field:'deployFlag',title:'<s:text name="部署结果"/>',width:60,
						 formatter:function(value,rowData,rowIndex){
								if(value=="1"){
									return "<s:text name="成功"/>";
								} else if(value=="0"){
									return "<s:text name="失败"/>";
								} else{
									return "";
								}
							}},
					{field:'deployTimeStart',title:'<s:text name="部署开始时间"/>',width:150,align:'center'},
					{field:'deployTimeEnd',title:'<s:text name="部署结束时间"/>',width:150,align:'center'},
					{field:'loginName',title:'<s:text name="system.sysmng.log.loginUsername.title"/>',width:80},
					{field:'realName',title:'<s:text name="system.sysmng.log.realName.title"/>',width:80},
					{field:'orgName',title:'<s:text name="system.sysmng.log.orgName.title"/>',width:80},
					{field:'ip',title:'<s:text name="system.sysmng.log.operationIp.title"/>',width:90},
					{
						field : 'oper',
						title : '<s:text name="system.button.oper.title"/>',
						width : 180,
						align : 'center',
						formatter : operFormatterAttach
					}
				]],
		toolbar : [ {
				id : 'tb_upload_gs',
				text : '部署',
				iconCls : 'icon-add',
				handler : function() {
					doUploadGs();
				}
			}, '-', {
				id : 'bt_export_jwp',
				text : '导出最新版本jwp',
				iconCls : 'icon-export',
				handler : function() {
					doExportJwp("default");
				}
			}, '-', {
				id : 'bt_export_updatesql',
				text : '导出新平台升级sql',
				iconCls : 'icon-export',
				handler : function() {
					window.open("${ctx}/esapi/es/deploy/updatesql/download");
				}
			}],		
		pagination:true,
		rownumbers:true
	};
}

function operFormatterAttach(value, rowData, rowIndex) {
	var strArr = [];
	strArr.push('<a href="javascript:void(0);" onclick="doRedeploy(\'');
	strArr.push(rowData.id);
	strArr.push('\')"><s:text name="重新部署"/></a>&nbsp;');
	strArr.push('<a href="javascript:void(0);" onclick="doExportJwp(\'');
	strArr.push(rowData.deployUrl);
	strArr.push('\')"><s:text name="导出jwp"/></a>&nbsp;');
	if(rowData.deployFlag == "0") {
		strArr.push('<a href="javascript:void(0);" onclick="doViewExpLog(');
		strArr.push(rowIndex);
		strArr.push(')"><s:text name="异常日志"/></a>&nbsp;');
	}
	return strArr.join("");
}

function doRedeploy(id) {
	var options = {
			url : '${ctx}/sys/upgrade/upgrade!switchJwp.action',
			data : {
				"id" : id
			},
			success : function(data) {
				if(data.msg) {
					$.messager.alert('提示', 'jwp文件重新部署成功！','info');
				}
				doQuery();
			}
	};
	fnFormAjaxWithJson(options);
}

function doExportJwp(jwpName) {
	var expUrl = "${ctx}/sys/upgrade/upgrade!exportJwp.action?jwpName=" + jwpName;
	window.open(expUrl);
}

function doViewExpLog(index) {
	var rows = $("#logInfo").datagrid("getRows");
	var data = rows[index];
	if(data) {
		$("#win_message").window("open");
		$("#ta_message").text(data.exceptionLog);
	}
}

function doUploadGs() {
	$("#uploadGsWin").window("open");
}

function doQuery() {
	var param = $("#queryForm").serializeArrayToParam();
	$("#logInfo").datagrid("load", param);
}


$(document).ready(function() {
	focusEditor("loginName");
    doQuseryAction("queryForm");
	
	$('#logInfo').datagrid(getOption());
	$("#deployTimeStart").focus(function() {
		WdatePicker({maxDate:'#F{$dp.$D(\'deployTimeStart1\')}'});
	});
	$("#deployTimeStart1").focus(function() {
		WdatePicker({minDate:'#F{$dp.$D(\'deployTimeStart\')}'});
	});
	$("#bt_query").click(doQuery);
	$("#bt_reset").click(function() {
		$("#queryForm").form("clear");
		doQuery();
	});
	
	$("#uploadify_gs").uploadify({
        'uploader': '${ctx}/js/uploadify/uploadify.swf',
        'script': '${ctx}/sys/upgrade/upgrade!save.action;jsessionid=<%=session.getId()%>',
        'cancelImg': '${ctx}/js/uploadify/cancel.png',
        'folder': 'UploadFile',
        'queueID': 'uploadGsQueue',
        'auto': false,
        'multi': false,
        'fileExt':'*.jwp;',
        'fileDesc':'Jiawasoft Package Files',
        'buttonImg':'${ctx}/js/uploadify/selfile.png',
        'width':88,
        'height':20,
        'wmode':'transparent',
        'onComplete':function(event, id, fileObj, response, data) {
            var obj = $.parseJSON(response);
            if(obj) {
            	if(obj.flag=='1') {
            		if(obj.msg) {
            			if(obj.msg.flag == "0") {
            				$.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '您要部署的jwp包'+obj.msg.msg+'和服务器上的版本不一致，您确定要覆盖更新吗？', function(r) {
            					if (r) {
            						var jwpName = obj.msg.msg;
            						var isFullUpdate = ($("input[name=isFullUpdate]:checked").val()=="1"?true:false);
            						var options = {
            							url : '${ctx}/sys/upgrade/upgrade!forceUpgrade.action',
            							data : {
            								"jwpName" : jwpName,
            								"isFullUpdate":isFullUpdate
            							},
            							success : function(data) {
            								if(data.msg) {
            									$.messager.alert('提示', 'jwp文件覆盖更新部署成功！','info');
            								}
            								doQuery();
            							}
            						};
            						fnFormAjaxWithJson(options);
            					}
            				});
            			} else {
            				$.messager.alert('提示', 'GS文件升级部署成功！','info');
            				doQuery();
            			}
            		} else {
            			doQuery();
            		}
                } else {
                	$.messager.alert('提示', 'GS文件升级部署失败：' + obj.msg + '。请稍后重试！','warning');
                	doQuery();
                }
            } else {
            	$.messager.alert('提示','服务器出现异常，请稍后重试！','warning');
            	doQuery();
            }
        }
    });

	$('#uploadGsWin').window({
		width:500,
		height:280,
		onOpen:function() {
			$(this).window("move", {
				top:($(window).height()-280)*0.5,
				left:($(window).width()-500)*0.5
			});
		},
		onClose:function() {
			//$('#uploadify_gs').uploadifyClearQueue();
		}
	});
	
	$("#bt_upload_gs").click(function() {
		var isFullUpdate = ($("input[name=isFullUpdate]:checked").val()=="1"?true:false);
		$("#uploadify_gs").uploadifySettings("scriptData", {"isFullUpdate":isFullUpdate});
		$('#uploadify_gs').uploadifyUpload();
	});
	
	$("#bt_upload_gs_cancel").click(function() {
		$('#uploadify_gs').uploadifyClearQueue();
	});
	
	$("#bt_upload_gs_close").click(function() {
		$('#uploadGsWin').window("close");
	});
});
	</script>
</head>
<body class="easyui-layout">
    <div region="west" border="false" title="<s:text name="system.search.title"/>" split="true" style="width:260px;padding:0px;" iconCls="icon-search">
			<form action="" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="loginName"><s:text name="system.sysmng.log.loginUsername.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_loginName" id="loginName" class="Itext"></input></td>
						</tr>
						<tr>
							<th><label for="deployName"><s:text name="部署名称"/>:</label></th>
							<td><input type="text" name="filter_LIKES_deployName" id="deployName"  class="Itext"></input></td>
						</tr>
						<tr>
							<th><label for="operationDesc"><s:text name="部署类型"/>:</label></th>
							<td><select id="deployType" class="easyui-combobox" name="filter_EQS_deployType" style="width:160px">
									<option value="" selected="selected">所有类型</option>
									<option value="0">系统</option>
									<option value="1">设计器</option>
								</select>
							</td>
						</tr>
						<tr>
						    <th><label for="deployFlag"><s:text name="部署结果"/>:</label></th>
							<td><input type="radio" name="filter_EQS_deployFlag" id="deployFlag0" value="1"/><label>成功</label>
								&nbsp;&nbsp;<input type="radio" name="filter_EQS_deployFlag" id="deployFlag1" value="0"/><label>失败</label>
							</td>
						</tr>
						<tr>	
							<th><label for="deployTimeStart"><s:text name="部署开始时间"/>:</label></th>
							<td><input type="text" name="filter_GED_deployTimeStart" id="deployTimeStart" readonly="readonly" class="Idate Wdate" style="width:160px;"></input>
								<br/>--<br/><input type="text" name="filter_LED_deployTimeStart" id="deployTimeStart1" readonly="readonly" class="Idate Wdate" style="width:160px;"></input></td>
						</tr>
						<tr>
						    <th><label for="ip"><s:text name="system.sysmng.log.operationIp.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_ip" id="ip"  class="Itext"></input></td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
											<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
	</div>
	<div region="center" title="" style="overflow:hidden;" border="false">
		<table id="logInfo" border="false"></table>
	</div>
	<div style="display:none;">
		<div id="uploadGsWin" class="easyui-window" closed="true" modal="true" title="部署GS文件" resizable="false" minimizable="false">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
						<div>请浏览计算机，找到需要部署的GS文件：</div>
						<div>
							<label>更新方式：</label>
							<input id="rad_isFullUpdate_0" type="radio" name="isFullUpdate" value="0" checked="checked"/><label for="rad_isFullUpdate_0">增量更新</label>
							&nbsp;&nbsp;<input id="rad_isFullUpdate_1" type="radio" name="isFullUpdate" value="1"/><label for="rad_isFullUpdate_1">全量更新</label>
						</div>
						<div id="dv_upload_gs">
							<div class="file_upload_button">
								<input type="file" name="uploadify_gs" id="uploadify_gs" />
								<button class="upload-button-1 button_big" id="bt_upload_gs" type="button">开始部署</button>
							</div>
							<div id="uploadGsQueue" class="file_upload_queue"></div>
						</div>									
				</div>
				<div region="south" border="false" style="text-align:right;height:30px;line-height:30px;">
					<a class="easyui-linkbutton" href="javascript:void(0);" id="bt_upload_gs_close">关闭</a>
				</div>
			</div>	
		</div>	
	</div>	
	<div id="win_message" class="easyui-window" closed="true" modal="true" title="<s:text name="system.sysmng.exceptionLog.message.title"/>" style="width:500px;height:400px;padding:5px;background:#fafafa;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
					<div style="width:100%;height:100%;"><pre id="ta_message"></pre></div>
				</div>
			</div>
	</div>
</body>
</html>