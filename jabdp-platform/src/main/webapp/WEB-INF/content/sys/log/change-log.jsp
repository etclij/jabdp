<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title"/></title>
<%@ include file="/common/meta-gs.jsp" %>
<script type="text/javascript">
		function operFormatter(value, rowData, rowIndex) {
			return '<a href="javascript:void(0);" onclick="doView(' + rowIndex + ')"><s:text name="system.button.view.title"/><s:text name="system.sysmng.exceptionLog.message.title"/></a>';
		}
	
		function getOption() {
			return {
				title:'<s:text name="变更日志"/>',
				iconCls:"icon-search",
				width:600,
				height:350,
				nowrap: false,
				striped: true,
				fit: true,
				url:'${ctx}/sys/log/change-log!queryList.action',
				sortName: 'id',
				sortOrder: 'desc',
				idField:'id',
				frozenColumns:[[
	                {title:'<s:text name="system.search.id.title"/>',field:'id',width:50,sortable:true,align:'right'}
				]],
				columns:[[
							{field:'moduleNameCn',title:'<s:text name="模块名"/>',width:80},
							//{field:'entityName',title:'<s:text name="实体名"/>',width:100},
							{field:'tableNameCn',title:'<s:text name="修订范围"/>',width:100},
							{field:'entityId',title:'<s:text name="主实体ID"/>',width:50},
							{field:'subEntityId',title:'<s:text name="子实体ID"/>',width:50},
							{field:'fieldNameCn',title:'<s:text name="变更字段"/>',width:100},
							{field:'beforeVal',title:'<s:text name="变更前值"/>',width:100},
							{field:'afterVal',title:'<s:text name="变更后值"/>',width:100},
							{field:'changeDesc',title:'<s:text name="变更详情"/>',width:200},
							{field:'loginName',title:'<s:text name="登录账户"/>',width:80},
							{field:'realName',title:'<s:text name="账户名称"/>',width:80},
							{field:'employeeName',title:'<s:text name="员工姓名"/>',width:80},
							{field:'orgName',title:'<s:text name="所属组织"/>',width:80},
							{field:'operTime',title:'<s:text name="变更时间"/>',width:120,align:'center', sortable:true},
							{field:'operIp',title:'<s:text name="IP地址"/>',width:120}
						]],
				pagination:true,
				rownumbers:true
			};
		}
		
		function doQuery() {
			var param = $("#queryForm").serializeArrayToParam();
			$("#queryList").datagrid("load", param);
		}
		
		function doView(index) {
			/*var options = {
				url:'${ctx}/sys/log/exp-log!view.action',
				data:{"id":id},
				success:function(data) {
					if(data.msg) {
						$("#win_message").window("open");
						$("#ta_message").text(data.msg.message);
					}
				}
			};
			fnFormAjaxWithJson(options);*/
			var rows = $("#queryList").datagrid("getRows");
			var data = rows[index];
			if(data) {
				$("#win_message").window("open");
				$("#ta_message").text(data.message);
			}
		}
		


		$(document).ready(function() {
			focusEditor("moduleNameCn");
			doQuseryAction("queryForm");
			
			$("#queryList").datagrid(getOption());
			$("#operTimeStart").focus(function() {
				WdatePicker({maxDate:'#F{$dp.$D(\'operTimeEnd\')}'});
			});
			$("#operTimeEnd").focus(function() {
				WdatePicker({minDate:'#F{$dp.$D(\'operTimeStart\')}'});
			});
			$("#bt_query").click(doQuery);
			$("#bt_reset").click(function() {
				$("#queryForm")[0].reset();
				doQuery();
			});
			
			<%-- 
			$("#win_message").window({
				width:500,
				height:400,
				onOpen:function() {
					$(this).window("move", {
						top:($(window).height()-400)*0.5,
						left:($(window).width()-500)*0.5
					});
				}
			});--%>
		});
</script>
</head>
<body class="easyui-layout" fit="true">
		<div region="west" border="false" title="<s:text name="system.search.title"/>" split="true" style="width:260px;padding:0px;" iconCls="icon-search">
			<form action="" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="moduleNameCn"><s:text name="模块名"/>:</label></th>
							<td><input type="text" name="filter_LIKES_moduleNameCn" id="moduleNameCn" class="Itext"></input></td>
						</tr>
						<tr><th><label for="tableNameCn"><s:text name="修订范围"/>:</label></th>
							<td><input type="text" name="filter_LIKES_tableNameCn" id="tableNameCn" class="Itext"></input></td>
						</tr>
						<tr><th><label for="fieldNameCn"><s:text name="变更字段"/>:</label></th>
							<td><input type="text" name="filter_LIKES_fieldNameCn" id="fieldNameCn" class="Itext"></input></td>
						</tr>
						<tr><th><label for="beforeVal"><s:text name="变更前值"/>:</label></th>
							<td><input type="text" name="filter_LIKES_beforeVal" id="beforeVal" class="Itext"></input></td>
						</tr>
						<tr><th><label for="afterVal"><s:text name="变更后值"/>:</label></th>
							<td><input type="text" name="filter_LIKES_afterVal" id="afterVal" class="Itext"></input></td>
						</tr>
						<tr><th><label for="loginName"><s:text name="登录账户"/>:</label></th>
							<td><input type="text" name="filter_LIKES_loginName" id="loginName" class="Itext"></input></td>
						</tr>
						<tr><th><label for="realName"><s:text name="账户名称"/>:</label></th>
							<td><input type="text" name="filter_LIKES_realName" id="realName" class="Itext"></input></td>
						</tr>
						<tr><th><label for="employeeName"><s:text name="员工姓名"/>:</label></th>
							<td><input type="text" name="filter_LIKES_employeeName" id="employeeName" class="Itext"></input></td>
						</tr>
						<tr><th><label for="orgName"><s:text name="所属组织"/>:</label></th>
							<td><input type="text" name="filter_LIKES_orgName" id="orgName" class="Itext"></input></td>
						</tr>
						<tr>	
							<th><label for="operTimeStart"><s:text name="变更时间"/>:</label></th>
							<td><input type="text" name="filter_GED_operTime" id="operTimeStart" readonly="readonly" class="Idate Wdate" style="width:160px;"></input>
								<br/>--<br/><input type="text" name="filter_LED_operTime" id="operTimeEnd" readonly="readonly" class="Idate Wdate" style="width:160px;"></input></td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
								<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div region="center" title="" border="false">
			<table id="queryList" border="false"></table>
		</div>
		<%-- 
		<div id="win_message" class="easyui-window" closed="true" modal="true" title="<s:text name="system.sysmng.exceptionLog.message.title"/>" style="width:500px;height:400px;padding:5px;background:#fafafa;">
			<div class="easyui-layout" fit="true">
				<div region="center" border="false" style="padding:10px;background:#fff;border:1px solid #ccc;">
					<div style="width:100%;height:100%;"><pre id="ta_message"></pre></div>
				</div>
			</div>
		</div>--%>
</body>
</html>
