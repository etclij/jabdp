<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title"/></title>
<%@ include file="/common/meta-gs.jsp" %>
<script type="text/javascript">
function getOption() {
	return {
		title:'<s:text name="system.sysmng.log.list.title"/>',
		width:600,
		height:350,
		nowrap: false,
		striped: true,
		fit: true,
		url:'${ctx}/sys/log/log!queryList.action',
		sortName: 'id',
		sortOrder: 'desc',
		idField:'id',
		//frozenColumns:[[
        //  {title:'<s:text name="system.search.id.title"/>',field:'id',width:40,sortable:true,align:'right'}
		//]],
		columns:[[
					{field:'loginName',title:'<s:text name="system.sysmng.log.loginUsername.title"/>',width:80},
					{field:'realName',title:'<s:text name="system.sysmng.log.realName.title"/>',width:80},
					{field:'orgName',title:'<s:text name="system.sysmng.log.orgName.title"/>',width:80},
					{field:'operationTime',title:'<s:text name="system.sysmng.log.operationTime.title"/>',width:140,align:'center'},
					{field:'ip',title:'<s:text name="system.sysmng.log.operationIp.title"/>',width:90},
					//{field:'machineName',title:'<s:text name="system.sysmng.log.operationMachine.title"/>',width:90},
					{field:'operationUrl',title:'<s:text name="system.sysmng.log.operationUrl.title"/>',width:150},
					{field:'operationMethod',title:'<s:text name="system.sysmng.log.operationMethod.title"/>',width:80},
					{field:'operationName',title:'<s:text name="system.sysmng.log.operationName.title"/>',width:100},
					{field:'operationDesc',title:'<s:text name="system.sysmng.log.operationDesc.title"/>',width:100},
					{field:'remark',title:'<s:text name="system.sysmng.log.remark.title"/>',width:350}
				]],
		pagination:true,
		rownumbers:true
	};
}

function doQuery() {
	var param = $("#queryForm").serializeArrayToParam();
	$("#logInfo").datagrid("load", param);
}


$(document).ready(function() {
	focusEditor("loginName");
	doQuseryAction("queryForm");
	
	$('#logInfo').datagrid(getOption());
	$("#loginTimeStart").focus(function() {
		WdatePicker({maxDate:'#F{$dp.$D(\'loginTimeEnd\')}'});
	});
	$("#loginTimeEnd").focus(function() {
		WdatePicker({minDate:'#F{$dp.$D(\'loginTimeStart\')}'});
	});
	
	$("#bt_query").click(doQuery);
	$("#bt_reset").click(function() {
		$("#queryForm")[0].reset();
		doQuery();
	});
});
</script>
</head>
<body class="easyui-layout" fit="true">
    <div region="west" border="false" title="<s:text name="system.search.title"/>" split="true" style="width:260px;padding:0px;" iconCls="icon-search">
			<form action="" name="queryForm" id="queryForm">
				<table border="0" cellpadding="0" cellspacing="1" class="table_form">
					<tbody>
						<tr><th><label for="loginName"><s:text name="system.sysmng.log.loginUsername.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_loginName" id="loginName" class="Itext"></input></td>
						</tr>
						<tr>
							<th><label for="operationMethod"><s:text name="system.sysmng.log.operationMethod.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_operationMethod" id="operationMethod" class="Itext"></input></td>
						</tr>
						<tr>
						    <th><label for="ip"><s:text name="system.sysmng.log.operationIp.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_ip" id="ip" class="Itext"></input></td>
						</tr>
						<tr>
							<th><label for="operationName"><s:text name="system.sysmng.log.operationName.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_operationName" id="operationName" class="Itext"></input></td>
						</tr>
						<tr>
							<th><label for="operationDesc"><s:text name="system.sysmng.log.operationDesc.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_operationDesc" id="operationDesc" class="Itext"></input></td>
						</tr>
						<tr>
						    <th><label for="operationUrl"><s:text name="system.sysmng.log.operationUrl.title"/>:</label></th>
							<td><input type="text" name="filter_LIKES_operationUrl" id="operationUrl" class="Itext"></input></td>
						</tr>
						<tr>	
							<th><label for="loginTimeStart"><s:text name="system.sysmng.log.operationTime.title"/>:</label></th>
							<td><input type="text" name="filter_GED_operationTime" id="loginTimeStart" readonly="readonly" class="Idate Wdate" style="width:160px;"></input>
								<br/>--<br/><input type="text" name="filter_LED_operationTime" id="loginTimeEnd" readonly="readonly" class="Idate Wdate" style="width:160px;"></input></td>
						</tr>
						<tr>		
							<td colspan="2" align="center">
								<button type="button" id="bt_query"><s:text name="system.search.button.title"/></button>&nbsp;&nbsp;
											<button type="button" id="bt_reset"><s:text name="system.search.reset.title"/></button>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
	</div>
	<div region="center" title="" border="false">
			<table id="logInfo" border="false"></table>
	</div>
</body>
</html>
