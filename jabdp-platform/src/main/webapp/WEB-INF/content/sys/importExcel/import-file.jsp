<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/meta-css.jsp"%>
<style>
.es_file_upload_bt_2 {
    left:280px;
}
.es_file_upload_bt_3 {
    position: absolute;
    top: 4px;
    left: 185px;
}
</style>
</head>
<body class="easyui-layout">
	<div region="center" style="padding: 2px 2px;" border="false">
		<div id="_file_box_">
			<div class="file_upload_div" style="display: block;">
				<div class="es_file_upload_button">
					<div id="file_picker_" class="webuploader-container">
						<div class="webuploader-pick">选择文件</div>
					</div>
					<button class="webuploader-pick es_file_upload_bt_1"
						id="file_upload_" type="button">开始导入</button>
					<button class="webuploader-pick es_file_upload_bt_3" onclick="doExportModule();"
						type="button"><s:text name="导出模板"/></button>	
					<%-- <span class="es_file_upload_bt_2">[注：添加附件时，可用Ctrl或Shift+鼠标左键来选择多个文件]</span> --%>
				</div>
				<div id="file_queue_" class="es_file_upload_queue"></div>
			</div>
		</div>
		<div style="padding:10px;"><span id="resultDesc" style="color:red;"></span></div>
		<div style="padding:10px;"><span style="color:red;">操作步骤：<br/>1、请单击“导出模板”按钮，导出Excel数据模板；<br/>2、根据Excel数据模板填好数据；<br/>3、单击“选择文件”按钮选择填好的Excel数据模板；<br/>4、单击“开始导入”按钮完成导入操作。</span></div>
		<div style="display:none;"><input type="hidden" name="hid_expName" id="hid_expName" value=""/></div>
	</div>
<%@ include file="/common/meta-js.jsp"%>
<script type="text/javascript">
var entityName = "${param.entityName}";

function  doExportModule(){
	var expUrl = "${ctx}/gs/gs-mng!outputExcelModule.action?entityName=" + entityName;
	window.open(expUrl);
}

function doExportExpExcel(){
	var expName = $("#hid_expName").val();
	var expUrl = "${ctx}/gs/gs-mng!outputExpExcel.action?entityName=" + entityName + "&value=" + expName;
	window.open(expUrl);
}

$(document).ready(function() {
	var uploader = WebUploader.create({
		// swf文件路径
		swf:"${ctx}/js/webuploader/Uploader.swf",
		// 文件接收服务端。
		server:"${ctx}/gs/gs-mng!importModuleExcel.action",
		// 选择文件的按钮。可选。
		// 内部根据当前运行是创建，可能是input元素，也可能是flash.
		pick : {"id":'#file_picker_',"multiple":false},
		formData : {'entityName':entityName},
		fileVal:"filedata",
		accept: {
	        extensions: 'xls,xlsx', // 允许的文件后缀，不带点，多个用逗号分割，这里支持老版的Excel和新版的
	        mimeTypes: 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
	        ,title: 'Excel Files'
	    },
		chunked: false,  //分片处理
        chunkSize: 5*1024*1024, //每片5M  
        threads:3,//上传并发数。允许同时最大上传进程数。
		// 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
		resize : false,
		sendAsBinary:false,
		fileNumLimit:10,
		fileSizeLimit:500 * 1024 * 1024,
		fileSingleSizeLimit:100 * 1024 * 1024
	});
	
	uploader.on('fileQueued', function(file) {
		$("#file_queue_").append(
				['<div id="file_item_' , file.id , '" class="es_file_item">'
				        , '<div style="width:80%;display:inline-block;">', '<div>'
						, '<span class="info">' , file.name , '&nbsp;&nbsp;</span>'
						, '<span class="state">' , "Waiting for upload" , '</span>'
						, '</div>'
						, '<div class="es-progressbar" style="width:92%;"></div>'
						, '</div>', '<div style="display:inline-block;"><a href="javascript:;" class="es_file_item_remove">x</a></div>', 
						'</div>'
						].join(""));
		var fiId = "#file_item_" + file.id;
		$(fiId).find("div.es-progressbar").progressbar({value:0, height:12});
		$(fiId).find(".es_file_item_remove").click(function(event) {
			uploader.removeFile(file, true);
			$(fiId).remove();
		});
	});

	uploader.on('uploadProgress', function(file, percentage) {
		$('#file_item_' + file.id).find('div.es-progressbar').progressbar('setValue', Math.floor(percentage*100));
	});
	
	uploader.on('uploadBeforeSend', function(file, data, headers) {
		data.id = parseInt(file.id) || "";
	});
	
	uploader.on('uploadSuccess', function(file, response) {
		$('#file_item_' + file.id).find('span.state').text("Uploaded successfully");
		$('#file_item_' + file.id).fadeOut();
		var data = response;
		if(data.flag) {
			var msg = data;
			if(msg && msg.flag == "1") {
				if(parent.doRefreshDataGrid) {
					parent.doRefreshDataGrid();
				}
				if(msg.msg) {
					//存在导入异常数据
					$("#hid_expName").val(msg.msg);
					$("#resultDesc").html("部分数据导入失败（由于部分数据重复或者填写不正确），详见<a href='javascript:void(0);' onclick='doExportExpExcel()'>异常数据Excel</a>");
					doExportExpExcel();
				} else {
					$("#resultDesc").html("Excel数据全部导入成功");
				}
			} else {
				$("#resultDesc").html('文件' + file.name + '导入失败:' + msg.msg);
			}
		} else {
			$("#resultDesc").html('文件' + file.name + '导入失败，服务器处理出现异常!');
		}
	});

	uploader.on('uploadError', function(file, reason) {
		$('#file_item_' + file.id).find('span.state').text("Upload failed:" + reason);
		uploader.cancelFile(file);
        uploader.removeFile(file,true);
        uploader.reset();
	});

	uploader.on('uploadComplete', function(file) {
		$('#file_item_' + file.id).find('div.es-progressbar').fadeOut();
	});
	
	uploader.on('uploadFinished', function() {
		uploader.reset();
	});

	$("#file_upload_").on('click', function() {
		if ($(this).hasClass('disabled')) {
			return false;
		}
		$("#resultDesc").html("");
		$("#hid_expName").val("");
		uploader.upload();
	});
});
</script>	
</body>
</html>