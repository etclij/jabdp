<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<style>
span.tmpzTreeMove_arrow{z-index:999999;}
ul.ztree.zTreeDragUL {z-index:999999;}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		_initCommonTypeTree_();
		<c:if test="${empty param.isReadOnly}">
		$("#_commonTypeWin_").dialog({
			onOpen:function() {
				var om = $("#_commonTypeWin_").data("operMethod");
				var zTree = $.fn.zTree.getZTreeObj("_commonTypeTree_");
				var nodes = zTree.getSelectedNodes();
				switch(om) {
					case "add":
						var node = nodes[0];
						$("#_commonTypeForm_").form("load", {"pid":node.id,"typeVal":node.typeVal});
						break;
					case "modify":
						$("#_commonTypeForm_").form("load", nodes[0]);
						break;
					default:
						break;
				}
				$("#_commonTypeName_").focus();
			},onClose:function() {
				$("#_commonTypeForm_").form("clear");
			},buttons:[{
				text:'<s:text name="system.button.ok.title" />',
				iconCls:'icon-ok',
				handler:function(){
					_doSaveCommonType_();
				}
			},{
				text:'<s:text name="system.button.cancel.title" />',
				iconCls:'icon-cancel',
				handler:function(){
					_doCloseCommonTypeWin_();
				}
			}]
		});
		
		$("#_commonTypeName_").bind('keydown', 'return',function (evt){evt.preventDefault(); _doSaveCommonType_(); return false; });
		</c:if>
		$("#_dictTree_qd_").bind('keydown', 'return',function (evt){evt.preventDefault(); _doDictTreeQuery_(); return false; });
	});
	<c:if test="${param.isCanDrag=='true'}">
	function zTreeOnDrop(event, treeId, treeNodes, targetNode, moveType) {
	    if(!moveType) return false;
	    var moveId = treeNodes[0].id;
	    var targetId = targetNode.id;
		if(moveType == "prev" || moveType == "next") {
			targetId = targetNode.pid;
	    }
		//根据父节点获取子节点顺序
		var treeObj = $.fn.zTree.getZTreeObj(treeId);
		var parentNode = treeObj.getNodeByParam("id", targetId, null);
		var idSort = $.map(parentNode.children, function(node) {
			return node.id;
		});
	    var options = {
				url : '${ctx}/sys/common/common-type!save.action',
				data : {
					"id" : moveId,
					"pid": targetId||"",
					"idSort":idSort.join(",")
				},
				success : function(data) {
					if (data.msg) {
						treeNodes[0].pid = targetId;
					}
				}
		};
		fnFormAjaxWithJson(options);
	}
	
	function zTreeBeforeDrop(treeId, treeNodes, targetNode, moveType) {
	    var moveTypeVal = treeNodes[0].typeVal;
	    var targetVal = targetNode.typeVal;
		if(moveTypeVal != targetVal) {
	    	alert("不允许拖动操作！");
	    	return false;
	    }
	}
	</c:if>
	
	var _commonType_setting_ = {
			data : {
				simpleData : {
					enable : true,
					pIdKey: "pid"
				}
			},
			callback : {
				<c:if test="${empty param.isReadOnly}">
				onRightClick : _commonType_onRightClick_,
				</c:if>
				onClick : _commonType_onClick_
				<c:if test="${param.isCanDrag=='true'}">
				,beforeDrop: zTreeBeforeDrop
				,onDrop: zTreeOnDrop
				</c:if>
			}
			<c:if test="${param.isCanDrag=='true'}">
			,edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false,
				drag: {
					isCopy:false
				}
			}
			</c:if>
	};	
 	function _initCommonTypeTree_(){
		var options = {
			url : '${ctx}/sys/common/common-type!queryList.action',
			data : {
				"type" : "${param.type}",
				"isSys":"${param.isSys}"
			},
			success : function(data) {
				var jsonTreeObj = data.msg;
				var rootObj = {id:null, pid:null, name:"全部分类", typeVal:"${param.type}"};
				if(!jsonTreeObj || jsonTreeObj.length==0) {
					jsonTreeObj = [rootObj];
				} else {
					<c:if test="${empty param.isSys}">
					jsonTreeObj.push(rootObj);
					</c:if>
				}
				var zTreeObj = $.fn.zTree.init($("#_commonTypeTree_"),
							_commonType_setting_, jsonTreeObj);
				
				var isAutoCollapse = "${param.autoCollapse}";
				if(isAutoCollapse !== "true") {
					//zTreeObj.expandAll(true);
					zTreeObj.expandNode(zTreeObj.getNodes()[0], true);//默认展开第一级节点
				}
				
				<c:if test="${not empty param.isSys}">
				var funcName = "${param.fname}";
				if(funcName) {
					var callFunc = eval(funcName);
					if(callFunc) {
						var ids = _getCommonTypeIds_();
						callFunc(ids, true);
					}
				}
				</c:if>
			}
		};
		fnFormAjaxWithJson(options, true);
	}
 	function _getCommonTypeNode_() {
 		var zTree = $.fn.zTree.getZTreeObj("_commonTypeTree_");
		var nodes = zTree.getSelectedNodes();
		return nodes[0];
 	}
	function _commonType_onRightClick_(event, treeId, treeNode) {
		event.preventDefault();
		var zTree = $.fn.zTree.getZTreeObj("_commonTypeTree_");
		zTree.selectNode(treeNode);
		if(treeNode) {
			if(treeNode.id) {
				$('#_commonType_mm_').menu('show', {
					left : event.pageX,
					top : event.pageY
				});
			} else {
				$('#_commonType_mm_add_').menu('show', {
					left : event.pageX,
					top : event.pageY
				});
			}
		}
	}
	function _getCommonTypeIds_(treeNode) {
		var zTree = $.fn.zTree.getZTreeObj("_commonTypeTree_");
		var nodes = [];
		if(treeNode) {
			nodes = zTree.transformToArray(treeNode);
		} else {
			nodes = zTree.transformToArray(zTree.getNodes());
		}
		var ids = [];
		$.each(nodes, function(k,v) {
			if(v.id) {
				ids.push(v.id);
			}
		});
		return ids;
	}
	function _commonType_onClick_(event, treeId, treeNode) {
		var funcName = "${param.fname}";
		if(funcName) {
			var callFunc = eval(funcName);
			if(callFunc) {
				if(treeNode.id) {
					var ids = _getCommonTypeIds_(treeNode);
					callFunc(ids);
				} else {
					callFunc([]);
				}
			}
		}
	}
	function _doAfterSaveCommonType_(obj) {
		var funcName = "${param.fsname}";
		if(funcName) {
			var callFunc = eval(funcName);
			if(callFunc) {
				callFunc(obj);
			}
		}
	}
 	function _doAddCommonType_() {
 		$("#_commonTypeWin_").data("operMethod", "add");
 		$("#_commonTypeWin_").dialog("open");
 	}
 	function _doEditCommonType_() {
 		$("#_commonTypeWin_").data("operMethod", "modify");
 		$("#_commonTypeWin_").dialog("open");
 	}
 	function _doDelCommonType_() {
 		$.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r) {
			if (r) {
				var zTree = $.fn.zTree.getZTreeObj("_commonTypeTree_");
				var selNodes = zTree.getSelectedNodes();
				var nodes = zTree.transformToArray(selNodes[0]);
				var ids = [];
				$.each(nodes, function(k,v) {
					if(v.id) {
						ids.push(v.id);
					}
				});
				var options = {
					url : '${ctx}/sys/common/common-type!delete.action',
					data : {
						"ids" : ids
					},
					traditional:true,
					success : function(data) {
						if(data.msg) {
							_initCommonTypeTree_();
						}
					}
				};
				fnFormAjaxWithJson(options);

			}
		});
 	}
	function _doSaveCommonType_() {
			var options = {
				url : '${ctx}/sys/common/common-type!save.action',
				success : function(data) {
					if (data.msg) {
						_initCommonTypeTree_();
						_doAfterSaveCommonType_(data.msg);
						_doCloseCommonTypeWin_();
					}
				}
			};
			fnAjaxSubmitWithJson("_commonTypeForm_",options);
	}
	function _doCloseCommonTypeWin_() {
		$("#_commonTypeWin_").dialog("close");
	}
	//选中或取消选中
	function doCheckNodesOnTree(treeId, param) {
		var treeObj = $.fn.zTree.getZTreeObj(treeId);
		treeObj.cancelSelectedNode();
		if(!param) {
			return;
		}
		var nodes = treeObj.getNodesByParamFuzzy("name", param, null);
		//for (var i=0, l=nodes.length; i < l; i++) {
		for (var i=nodes.length-1; i >= 0; i--) {	
			treeObj.selectNode(nodes[i],true);
		}
	}
	
	function _doDictTreeQuery_() {
		var val = $("#_dictTree_qd_").val();
		doCheckNodesOnTree("_commonTypeTree_", val);
	}
</script>
<div>
<div><input type="text" id="_dictTree_qd_" style="width:130px" class="Itext"/>&nbsp;&nbsp;<button type="button" class="button_small" onclick="_doDictTreeQuery_()">树查询</button></div>
<ul style="text-align:center;" id="_commonTypeTree_" class="ztree"></ul>
</div>
<div style="display:none;">
		<div id="_commonType_mm_" class="easyui-menu" style="width:120px;">
			<div onclick="_doAddCommonType_()" iconCls="icon-add"><s:text name="system.button.add.title"/></div>
			<div onclick="_doEditCommonType_()" iconCls="icon-edit"><s:text name="system.button.modify.title"/></div>
			<div onclick="_doDelCommonType_()" iconCls="icon-remove"><s:text name="system.button.delete.title"/></div>
		</div>
		<div id="_commonType_mm_add_" class="easyui-menu" style="width:120px;">
			<div onclick="_doAddCommonType_()" iconCls="icon-add"><s:text name="system.button.add.title"/></div>
		</div>
		<div id="_commonTypeWin_" closed="true" collapsible="false"
			minimizable="false" maximizable="false" modal="true" title="<s:text name="分类编辑" />"
			style="width: 300px; height: 150px;">
			<div style="text-align: center; padding: 5px 5px;"
				class="easyui-layout" fit="true">
				<div region="center" border="false">
					<form action="" name="_commonTypeForm_" id="_commonTypeForm_" method="post"
						style="margin: 0px;">
						<input type="hidden" name="id"/>
						<input type="hidden" name="pid"/>
						<input type="hidden" name="typeVal"/>
						<input type="hidden" name="idSort"/>
						<table align="center" border="0" cellpadding="0" cellspacing="1"
							class="table_form">
							<tbody>
								<tr>
									<th><label for="_commonTypeName_"><s:text name="分类" />:</label>
									</th>
									<td align="left"><input type="text" name="name" id="_commonTypeName_"
										class="easyui-validatebox" required="true" style="width:200px;"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>
		</div>
</div>




