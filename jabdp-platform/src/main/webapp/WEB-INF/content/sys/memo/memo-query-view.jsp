<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title><s:text name="system.index.title"/></title>
<link href="${ctx}/js/picNews/style/css.css" rel="stylesheet" type="text/css" />
<%@ include file="/common/meta-gs.jsp" %>
</head>
<body id="layout_body" class="easyui-layout" fit="true">
 <div region="center" title="" border="false">
  <div id="wrapper" style="padding-top:5px;">
   <!--滚动看图-->
   <div id="picSlideWrap" class="clearfix">
    <div><h3 class="titleh3" id="showTitle" style="font-size:20px;"></h3></div>
    <div><h4 class="titleh4" id="lastUpdateTime"></h4></div> 
    <div id="content"><p id="showContent"></p></div>
   </div>
  </div>
  <!--end滚动看图-->
 </div>
 <div region="south" title="" border="false">
  <div style="overflow:hidden; text-align: center; padding:2px;">
   <a id="bt_read" class="easyui-linkbutton" iconCls="icon-ok" href="javascript:void(0)" onclick="doRead(${param.mtuId});">
    <s:text name="已阅读"/>
   </a>
  </div>
 </div>	
 <div style="display:none;">
  <form method="post" name="hidFrm" id="hidFrm">
   <input type="hidden" id="id" name="id" value="${param.id}"/>
  </form>
 </div>
<script type="text/javascript">
	function getJsonData(){
		return jsonData;
	}

	//定义一个全局变量保存操作状态
	var status = "";
	var _userList = {};
	var jsonData;
	var _nTouId = "${param.mtuId}";
	function doInitForm(data,callFun) {
		if (data.msg) {
			jsonData = data.msg;
			$("#showTitle").html(jsonData["title"]);
			$("#showContent").html(jsonData["content"]);
			var userName = "";
			var noticeStyle = "";
			var dtTime="";
			var uId = jsonData["createUser"];
			/*var uObj = _userList[uId];
			if(uObj) {
				userName = uObj.nickName || uObj.realName;
			} else {
				userName = "未知";
			}*/
			var userName = jsonData["createUserCaption"];
			if(jsonData["mtuStatus"]=="1") {
				noticeStyle = "正在提醒";
			} else if(jsonData["mtuStatus"]=="2") {
				noticeStyle = "过期提醒";
			} else {
				noticeStyle = "等待提醒";
			}
			if(jsonData["lastUpdateTime"]) {
				dtTime=jsonData["lastUpdateTime"];
			} else {
				dtTime=jsonData["createTime"];
			}
			var lut = [];
			lut.push("发布时间："+dtTime);
			lut.push("通知状态："+noticeStyle);
			lut.push("发布人："+userName);
			$("#lastUpdateTime").html(lut.join("&nbsp;&nbsp;"));
			if(jsonData["mtuStatus"] != "1") {
				doDisableOper();
			}
		}
		if(callFun){
			callFun();
		}
	}
	
	function doDisableOper() {
		$("#bt_read").linkbutton("disable");
		$("#bt_read").hide();
		$("#layout_body").layout("remove","south");
	}

	//页面数据初始化
	function doModify(id) {
		var options = {
			url : '${ctx}/sys/memo/memo!queryView.action',
			data : {
				mtuId : id
			},
			success : function(data) {
				doInitForm(data);
			}
		};
		fnFormAjaxWithJson(options);
	}
	
	function doRead(mtuId){
		var options = {
				url : '${ctx}/sys/memo/memo!doRead.action',
				data : {
					"mtuId" : mtuId
				},
				success : function(data) {
					if(data.msg){
						top.closeCurrentTab();
					}
				}
		};
		fnFormAjaxWithJson(options); 
	}
	
	//初始化
	$(function(){
		//_userList = findAllUser();
		doModify("${param.mtuId}");
	});
</script>
</body>
</html>
