<%@page import="java.util.Random"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style> 
	.no_select_img{
		border:0;
		cursor:hand;
		padding-left:2px;
		position:relative;
		filter:alpha(opacity=30);
	}
	.select_img{
		border:0;
		cursor:hand;
		position:relative;
	}
</style>

</head>
            
<body>
<%
    Random rad=new Random();
    int radParam=rad.nextInt();
%>

<div align="right"></div>
    <table id="table_${param.id}" cellspacing="8px" style="position:relative;right:-300px;" >
			<td align='right'>                   
				<img title="3D垂直柱型" onclick="changeReportType('Column3D','chartdiv_${param.id}_<%=radParam%> ')" src="${ctx}/js/FusionCharts/images/chart_bar.gif" class="no_select_img">
				
				<img title="2D垂直柱型" onclick="changeReportType('Column2D','chartdiv_${param.id}_<%=radParam%>')" src="${ctx}/js/FusionCharts/images/chart_x_bar.gif" class="no_select_img">
 
				<img title="2D横向柱型" onclick="changeReportType('Bar2D','chartdiv_${param.id}_<%=radParam%>')" src="${ctx}/js/FusionCharts/images/chart_y_bar.gif" class="no_select_img">
 
				<img title="饼形" onclick="changeReportType('Pie3D','chartdiv_${param.id}_<%=radParam%>')" src="${ctx}/js/FusionCharts/images/chart_pie.gif" class="no_select_img">
 
				<img title="动态饼形" onclick="changeReportType('Doughnut2D','chartdiv_${param.id}_<%=radParam%>')" src="${ctx}/js/FusionCharts/images/chart_pie_add.gif" class="no_select_img">
 
				<img title="线形" onclick="changeReportType('Line','chartdiv_${param.id}_<%=radParam%>')" src="${ctx}/js/FusionCharts/images/chart_line.gif" class="no_select_img"> 
				
				<img title="区域形" onclick="changeReportType('Area2D','chartdiv_${param.id}_<%=radParam%>')" src="${ctx}/js/FusionCharts/images/chart_curve.gif" class="no_select_img">			</td>
		</tr>
	</table> 
	
<div id="chartdiv_${param.id}_<%=radParam%>" align="center" style="width:100%; height:80%;">The chart will appear within this DIV. This text will be replaced by the chart.</div>
<div id="fcexpDiv_${param.id}_<%=radParam%>" align="center">FusionCharts Export Handler Component</div>

<script type="text/javascript"> 
 $(document).ready(function(){
	    $().ready(function() {
           $('.no_select_img').jfade();
        });   
	 var myChart = new FusionCharts("${ctx}/js/FusionCharts/swf/Column3D.swf", "viewAllChartId_${param.id}", "100%", "100%", "0", "1");
 	 myChart.setJSONData(
			{
				  "chart": {         
					 "caption" : "数据模型展示[2]" ,         
					 "xAxisName" : "Week",         
					 "yAxisName" : "销售额", 
					 "showBorder":"0",
					 "exportFormats":"PDF=导出为 PDF|JPEG=导出为 JPEG|PNG=导出为PNG",
					 "exportEnabled":"1",
					 "exportAtClient":"1",
					 "exportHandler":"fcExporter1_${param.id}",
					 "numberPrefix" : "$"   
					 },           
					 "data" :      [       
					                { "label" : "Week 1", "value" : "14400" },     
					                { "label" : "Week 2", "value" : "19600" },         
					                { "label" : "Week 3", "value" : "24000" },        
					                { "label" : "Week 4", "value" : "15700" }  
					                ]
	});   
	
	myChart.render("chartdiv_${param.id}_<%=radParam%>");

	var myExportComponent = new FusionChartsExportObject("fcExporter1_${param.id}", "${ctx}/js/FusionCharts/swf/FCExporter.swf");
	 myExportComponent.componentAttributes.borderThickness = '1';
	   myExportComponent.componentAttributes.borderColor = '0372AB';

	   //Font properties
	   myExportComponent.componentAttributes.fontFace = 'Arial';
	   myExportComponent.componentAttributes.fontColor = '0372AB';
	   myExportComponent.componentAttributes.fontSize = '12';

	   //Button visual configuration
	   myExportComponent.componentAttributes.btnWidth = '100';
	   myExportComponent.componentAttributes.btnHeight= '25';
	   myExportComponent.componentAttributes.btnColor = 'E1f5ff';
	   myExportComponent.componentAttributes.btnBorderColor = '0372AB';

	   //Button font properties
	   myExportComponent.componentAttributes.btnFontFace = 'Verdana';
	   myExportComponent.componentAttributes.btnFontColor = '0372AB';
	   myExportComponent.componentAttributes.btnFontSize = '15';

	   //Title of button
	   myExportComponent.componentAttributes.btnsavetitle = '保存'
	   myExportComponent.componentAttributes.btndisabledtitle = '等待导出'; 
	   
	  myExportComponent.Render("fcexpDiv_${param.id}_<%=radParam%>");
	  
	   }); 

</script> 
  
</body>
</html>