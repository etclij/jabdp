<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><s:text name="新闻公告"/></title>
	<%@ include file="/common/meta.jsp" %>
	<link href="${ctx}/js/easyui/${themeColor}/panel.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="${ctx}/js/easyui/scripts/easyui.xpPanel.js"></script>
            <script type="text/javascript">
            var _userList={};

            function getOption() {
                return {
                    width : 'auto',
                    height : 'auto',
                    nowrap : false,
                    striped : true,
                    fit : true,
                    url :'${ctx}/sys/notice/news!getAllNews.action',
                    sortName : 'createTime',
                    sortOrder : 'desc',
                    frozenColumns : [
                        [
                            {
								field : "atmId",
								title : "<s:text name='system.button.accessory.title'/>",
								width : 35,
								formatter:function(value, rowData, rowIndex) {
									if(value) {
										var astr = [];
										astr.push('<a class="easyui-linkbutton l-btn l-btn-plain" href="javascript:void(0);"');
										 <security:authorize url="/sys/attach/attach.action"> 
											astr.push(' onclick="doAccessory(');
											astr.push(value);
											astr.push(',');
											astr.push(rowData["id"]);
											astr.push(');" ');
										</security:authorize> 
										astr.push('>');
										astr.push('<span class="l-btn-left"><span class="l-btn-text"><span class="l-btn-empty icon-attach">&nbsp;</span></span></span></a>');
										return astr.join("");
									} else {
										return "<span></span>";
									}
								}
                            }  
                        ]
                    ],
                    columns : [
                        [
	                                        /* {
	                                            field : 'id',
	                                            title : '<s:text name="编号"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } , */
	                                        {
	                                            field : 'title',
	                                            title : '<s:text name="system.sysmng.desktop.modelTitle.title"/>',
	                                            width : 400,
	                                            sortable : true
	                                        } ,
	                                        {
	                                            field : 'keyworlds',
	                                            title : '<s:text name="system.sysmng.desktop.key.title"/>',
	                                            width : 100,
	                                            sortable : true
	                                        } /* ,
	                                        {
	                                            field : 'content',
	                                            title : '<s:text name="正文"/>',
	                                            width : 80,
	                                            sortable : true
	                                        } *//* ,
	                                        {
	                                            field : 'createUser',
	                                            title : '<s:text name="system.sysmng.desktop.createUser.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
	                                            	var uObj=_userList[value];
                                                    if(uObj){
                                                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
														return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                                                    }else{
                                                        return value;
                                                    }
                                                 }
	                                        }*/ ,
	                                        {
	                                            field : 'createTime',
	                                            title : '<s:text name="system.sysmng.desktop.createTime.title"/>',
	                                            width : 80,
	                                            sortable : true ,
	                                            formatter: function(value,row,index){
	                    							if(value){
	                    								return fnFormatDate(value, 'yyyy-MM-dd');
	                    							}
	                    						}
	                                              
	                                        } /* ,
	                                        {
	                                            field : 'lastUpdateUser',
	                                            title : '<s:text name="最后修改人"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
                                                    var uObj=_userList[value];
                                                    if(uObj){
                                                        return uObj.realName;
                                                    }else{
                                                        return value;
                                                    }
                                                    
                                                  }
	                                        } ,
	                                        {
	                                            field : 'lastUpdateTime',
	                                            title : '<s:text name="最后修改时间"/>',
	                                            width : 80,
	                                            sortable : true
	                                             
	                                        }  *//* ,
	                                        {
	                                            field : 'version',
	                                            title : '<s:text name="版本号"/>',
	                                            width : 80,
	                                            sortable : true
	                                            
	                                        } */ /* ,
	                                        {
	                                            field : 'status',
	                                            title : '<s:text name="状态"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value,rowData,rowIndex){
	                                            	if(value=="1"){
	                                            		return "已发布";
	                                            	}else if(value=="0"){
	                                            		return "草稿";
	                                            	}
	                                            }
	                                            
	                                        } */ ,
	                                        {
	                                            field : 'type',
	                                            title : '<s:text name="system.sysmng.news.newsType.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:getTypeValue
	                                        }  ,
	                                        {
	                                            field : 'clickCount',
	                                            title : '<s:text name="system.sysmng.news.clickCount.title"/>',
	                                            width : 80,
	                                            sortable : true,
	                                            formatter:function(value, rowData, rowIndex){
	                                            	if(value){
	                                            		return value+"次";
	                                            	}else{
	                                            		return "0次";
	                                            	}
	                                            }
	                                             
	                                        } 
                        ]
                    ],
                    onDblClickRow:function(rowIndex, rowData){
                    	doDblView(rowData.id);
                    	$('#queryList').datagrid('unselectRow',rowIndex);
                    },
                    
    				onRowContextMenu  : function(e, rowIndex, rowData){
    					e.preventDefault();
    					$('#mm').data("id", rowData.id);
    					$('#mm').menu('show', {
    						left: e.pageX,
    						top: e.pageY
    					});
    				},
                    pagination : true,
                    rownumbers : true/* ,
                    toolbar :[
                        {
                            id : 'bt_view',
                            text : '<s:text name="system.button.view.title"/>',
                            iconCls : "icon-search",
                            handler : function() {
                                doView();
                            }
                        }
                        
                    ] */
                };
            }
        
            //查询
            /*function doQuery() {
                var param = $("#queryForm").serializeArrayToParam();
                $("#queryList").datagrid("load", param);
            }*/
            
            //查询
            function doQuery(paramObj, isInit) {
                var param = $("#queryForm").serializeArrayToParam();
                if(paramObj) {
                	$.extend(true, param, paramObj);
                }
                if($("#tree_type").length) {
                	var obj = $("#tree_type").data("queryParam");
                	$.extend(true, param, obj);
                }
                if(isInit) {
                	var opt = getOption();
                	opt["queryParams"] = param;
                	$("#queryList").datagrid(opt);
                } else {
                	$('#queryList').datagrid('clearSelections');
                	$("#queryList").datagrid("load", param);
                }
            }

            function doView() {
            	 var id=0;
            	 var rows = $('#queryList').datagrid('getSelections');
            	  if(rows.length!=1){
            		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
            	     return ;
            	 } else{
            		 id=rows[0].id;
            	 }
            	 doDblView(id); 
            }
           //查看tab
            function doDblView(id) {
        	    if(top.addTab) {
        	    	top.addTab('<s:text name="system.sysmng.news.view.title"/>-'+id, '${ctx}/sys/notice/news!viewNews.action?id=' + id);
        	    } else {
        	    	window.open('${ctx}/sys/notice/news!viewNews.action?id=' + id, '<s:text name="system.sysmng.news.view.title"/>'+id);
        	    }
           }
            //右键查看
            function doCmView() {
            	var id = $("#mm").data("id");
            	doDblView(id);
            }

            var typeObj = {"id":"id","pid":"pid","text":"name"};
	   		var typeJsonParam = {"type":"news","url":"${ctx}/sys/common/common-type!queryList.action"};
	   		var typeJson = getJsonObjByParam(typeJsonParam);
            
	   		function getTypeValue(value, rowData, rowIndex) {
                return getColumnValueByIdText(typeObj, typeJson, value);
            }
            
             var doTreeQuery_type = function(ids) {
		  		var param = {"filter_INL_type":ids.join(",")};	
		  		$("#tree_type").data("queryParam", param);
	            doQuery(param);
			  };
            
            $(document).ready(function() {
            	 _userList=findAllUser();

               //回车查询
                 doQuseryAction("queryForm");
                //$("#queryList").datagrid(getOption());
                $("#bt_query").click(doQuery);
                $("#bt_reset").click(function() {
                    $("#queryForm")[0].reset();
                    doQuery();
                });
                
                var p = null;
                <c:if test="${not empty param.typeId}">
                p = {"filter_INL_type":"${param.typeId}"};
                </c:if>
                //初始化查询
                doQuery(p,true);
                
                $("#queryFormId").hide();
				 $("#a_exp_clp").hide();
					$("#a_switch_query").toggle(function() {
						$("#queryFormId").show();
						$("#a_exp_clp").show();
						$("#tree_type").hide();
					}, function() {
						$("#queryFormId").hide();
						$("#a_exp_clp").hide();
						$("#tree_type").show();
					});
				  $("#a_switch_query").show();
                 
            });
            
            //刷新列表
            function doRefreshDataGrid() {
    			$("#queryList").datagrid("load");
    		}
        	//查看附件
            function doAccessory(attachId,id) {
    			if(attachId && id){          
	           	var options ={"attachId":attachId,
	           				 "id":id,
	           				 "key":"",
	           				 "entityName":"",
	           				 "dgId":"queryList",
	           				 "flag":"mainQuery",
	           				 "readOnly" : true
	           				 };
            	 doAttach(options);
        		}
            }
            </script>
</head>            
         <body class="easyui-layout" fit="true">
          <div region="west" title="<s:text name='system.search.title'/>" border="false"
                 split="true" style="width:215px;padding:0px;background-color: #E0ECFF;"
                 iconCls="icon-search" tools="#pl_tt" >
                 	<div id="tree_type"><div class="easyui-panel" href="${ctx}/sys/common/dict-tree.action?type=news&fname=doTreeQuery_type&fsname=doTreeAfterSave_type&isReadOnly=true" border="0"></div></div>
			            <form action=""
	                      name="queryForm" id="queryForm">
	                       <div class="xpstyle-panel" id="queryFormId">
		                     
	  
								                                               <div class="easyui-panel xpstyle" title="<s:text name="system.sysmng.desktop.modelTitle.title"/>" collapsible="true" collapsed="true">
								                                                    <input type="text"
								                                                           name="filter_LIKES_title"
								                                                           id="title" />
								
								                                                </div> 
							                                    
							                                    
										
																		     <div class="easyui-panel xpstyle" title="<s:text name="system.sysmng.desktop.key.title"/>" collapsible="true" collapsed="true">
								                                                  <input type="text"
								                                                           name="filter_LIKES_keyworlds"
								                                                           id="key" />
								
								                                             </div>
							                                    
										
					                              <button type="button" id="bt_query" class="button_small">
								                                    <s:text name="system.search.button.title"/>
								                                </button>
								                                &nbsp;&nbsp;
								                                <button type="button" id="bt_reset" class="button_small">
								                                    <s:text name="system.search.reset.title"/>
								                                </button> 
	                    </div>
	                </form>           
      
            </div>
            <div region="center" title="" border="false">
                <table id="queryList" border="false"></table>

            </div>
             <div id="mm" class="easyui-menu" style="width:120px;">
					<div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
		    </div>
			 <div id="pl_tt">
				<a href="#" id="a_exp_clp" class="icon-expand" title="<s:text name="system.button.expand.title"/>"></a>
				<a href="#" id="a_switch_query" class="icon-menu" title="<s:text name="system.button.switchQueryType.title"/>" style="display:none;"></a>
			</div>
            </body>
</html>