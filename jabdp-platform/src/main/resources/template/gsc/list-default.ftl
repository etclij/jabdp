<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title><s:text name="system.index.title"/></title>
	<%@ include file="/common/meta-css.jsp" %>
	<#--<link href="${r"${ctx}"}/js/easyui/${r"${themeColor}"}/panel.css" rel="stylesheet" type="text/css"/>-->
<#import "common/field.ftl" as fieldftl>   
<#assign entityName=root.entityName /> 
<#assign formList=root.dataSource.formList/>
<#assign titleName=root.moduleProperties.caption/>
<#assign i18nKey=root.moduleProperties.i18nKey/>
<#assign moduleKey=root.moduleProperties.key/>

<#-- 关联表表 -->
<#assign realEntityName = entityName />
<#if root.moduleProperties.relevanceModule??>
<#assign relevanceModule=root.moduleProperties.relevanceModule/>
<#list formList as form>
<#if form.isMaster>
	<#assign realEntityName = form.entityName />
	<#break/>
</#if>
</#list>
<#else>
<#assign relevanceModule=""/>
</#if>

<#assign isShowInQueryList=false/><#--标识是否存在列表页面显示的tabs-->
<#assign flowList= root.flowList />
<#if formList??>
        <#list formList as form>
            <#if form.isMaster>
            <style type="text/css"><#-- 初始化样式 -->
			<@fieldftl.initQueryPageFormCssOrJavascript formList=formList eventKey="addCssStyle"/>
			</style>
</head>            
<body>
<@compress single_line=true>
 		<div class="easyui-layout" fit="true" id="layout_body">       
         <#--如果存在树形查询，显示左边的查询框
         <#assign encounterFormTree><@fieldftl.encounterFormTree form=form/></#assign>
         <#if encounterFormTree="true">
         --> 
         <#-- 
         <#if form.queryStyle="leftshow">
          <div region="west" title="<s:text name='system.search.title'/>" border="false"
                 split="true" style="width:215px;padding:2px;" 
                 iconCls="icon-search" tools="#pl_tt" >
                		<@fieldftl.initFormTree form=form/>
			            <form action="${r"${ctx}"}/gs/gs-mng!queryList.action?entityName=${entityName}&relevanceModule=${relevanceModule}"
	                      name="queryForm" id="queryForm">
	                       <div class="easyui-accordion" data-options="multiple:true" style="width:98%;" id="queryFormId">
		                     	<@fieldftl.getQueryView form=form/>
		                     	<#list formList as subForm>
						     		<#if !subForm.isMaster && !((subForm.isVirtual)!false)>
						     			<@fieldftl.getQueryView form=subForm/>
						     		</#if>
						     	</#list>  	
	                       </div>
	                       <div style="text-align:center;padding:8px 8px;">
						             <button type="button" id="bt_query" class="button_small">
									     <s:text name="system.search.button.title"/>
									 </button>&nbsp;&nbsp;
									 <button type="button" id="bt_reset" class="button_small">
									     <s:text name="system.search.reset.title"/>
									 </button>
						   </div> 
	                </form>
            </div>
            </#if>-->
            <@fieldftl.initFormTree form=form/>
            <div region="center" title="" border="false">
            <div class="easyui-layout" fit="true">
	            <@fieldftl.initFormTabQuery form=form isInitTab=true/>
	            <div region="center" title="" border="false">
	            	<table id="queryList" border="false"></table>
	            </div>
            	<#if form.enablePageSelect ><#-- 启用分页选择模式 -->
            		<div region="south" border="false" style="height:200px;" split="true" >
            			<table id="pageSelectList" border="false"></table>
            		</div>
            	</#if>
            	</div>
            	</div>
          </div>
            <div style="display:none;">
             <div id="queryListToolBar">
             	<@fieldftl.getQueryViewV2 formList=formList />
             	<div class="datagrid-toolbar" style="border-bottom-width:0;">
             		<security:authorize url="/gs/gs-mng!add.action?entityName=${entityName}">
             		<a id="bt_add" href="#" class="easyui-linkbutton" plain="true" icon="icon-add" onclick="doAdd()"><s:text name="system.button.add.title"/>(I)</a>
             		</security:authorize>
             		<security:authorize url="/gs/gs-mng!delete.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
             		<a id="bt_del" href="#" class="easyui-linkbutton" plain="true" icon="icon-remove" onclick="doDelete()"><s:text name="system.button.delete.title"/>(D)</a>
             		</security:authorize>
             		<security:authorize url="/gs/gs-mng!view.action?entityName=${entityName}">
             		<a id="bt_view" href="#" class="easyui-linkbutton" plain="true" icon="icon-search" onclick="doView()"><s:text name="system.button.view.title"/>(E)</a>
             		</security:authorize>
             		<security:authorize url="/gs/gs-mng!edit.action?entityName=${entityName}">
             		<a id="bt_copy" href="#" class="easyui-linkbutton" plain="true" icon="icon-copy" onclick="doCopyTbar()"><s:text name="system.button.copy.title"/>(G)</a>
             		</security:authorize>
             		<security:authorize url="/sys/attach/attach.action?entityName=${entityName}">
             		<a id="bt_accessory" href="#" class="easyui-linkbutton" plain="true" icon="icon-attach" onclick="doAccessory()"><s:text name="system.button.accessory.title"/>(Y)</a>
             		</security:authorize>
             		<security:authorize url="/gs/data-share-config!save.action?entityName=${entityName}">
             		<a id="bt_dataShare" href="#" class="easyui-linkbutton" plain="true" icon="icon-run" onclick='jwpf.doSetDataShareUser("${entityName}",jwpf.getTableVal("queryList",true,"id"),function() {$("#queryList").edatagrid("clearSelections");})'><s:text name="system.button.accessory.title"/>(Y)</a>
             		</security:authorize>
             		<#if flowList??&&flowList?size gt 0>
             		<#else>
                     	<security:authorize url="/gs/process!startApprove.action?entityName=${entityName}">
	                        <a id="bt_selfApprove" href="#" class="easyui-linkbutton" plain="true" icon="icon-flowStart" onclick="doSelfProcess()"><s:text name="system.button.selfApprove.title"/></a>
	             		</security:authorize>
	             		<security:authorize url="/gs/process!cancelApprove.action?entityName=${entityName}">
	             			<a id="bt_cancelSelfApprove" href="#" class="easyui-linkbutton" plain="true" icon="icon-user" onclick="doCancelSelfProcess()"><s:text name="system.button.cancelSelfApprove.title"/></a>
	             		</security:authorize>
	             	</#if>
	             	<#if form.enableImport>
                        <security:authorize url="/gs/gs-mng!importModuleExcel.action?entityName=${entityName}"> 
                        	<a id="bt_excelImport" href="#" class="easyui-linkbutton" plain="true" icon="icon-excelImport" onclick='doOpenExcelImportWin("${entityName}")'><s:text name="system.button.importExcel.title"/></a>
	             		</security:authorize>
	             	</#if>
                    <#if form.enableExport>
                        <security:authorize url="/gs/gs-mng!exportModuleExcel.action?entityName=${entityName}">
                        	<a id="bt_excelExport" href="#" class="easyui-linkbutton" plain="true" icon="icon-excelExport" onclick='doExportExcel("${entityName}", getExportParams("queryList"), jwpf.getTableVal("queryList",true,"id").join(","))'><s:text name="system.button.exportExcel.title"/></a>
	             		</security:authorize>
	             	</#if>
	             	<@fieldftl.initDatagridToolBarButtonV2 form=form />
             	</div>
             </div>
             <div id="mm" class="easyui-menu" style="width:120px;">
				<security:authorize
					url="/gs/gs-mng!add.action?entityName=${entityName}">
					<div onclick="doAdd()" iconCls="icon-add"><s:text name="system.button.add.title"/></div>
				</security:authorize>
				<security:authorize
					<#-- 关联表 -->
					url="/gs/gs-mng!delete.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
					<div onclick="doDelete()" iconCls="icon-remove"><s:text name="system.button.delete.title"/></div>
				</security:authorize>
				<security:authorize
					url="/gs/gs-mng!view.action?entityName=${entityName}">
					<div onclick="doCmView()" iconCls="icon-search"><s:text name="system.button.view.title"/></div>
				</security:authorize>
				<security:authorize
					url="/gs/gs-mng!edit.action?entityName=${entityName}">
					<div onclick="doCopy()" iconCls="icon-copy"><s:text name="system.button.copy.title"/></div>
				</security:authorize>
				<#-- <security:authorize
					url="/sys/report/report.action?entityName=${entityName}">
						<div onclick="doViewReport('${moduleKey}',$('#mm').data('id'))" iconCls = "icon-report"><s:text name="system.button.report.title"/></div>
				</security:authorize> --> 
				<security:authorize
					url="/sys/attach/attach.action?entityName=${entityName}">
						<div onclick="doAccessory($('#mm').data('id'),$('#mm').data('attachId'),$('#mm').data('status'))" iconCls = "icon-attach"><s:text name="system.button.accessory.title"/></div>
					 </security:authorize>
				<s:if test='#session.USER.isSuperAdmin=="1"'>
					<div onclick="doOpenModifyCreateUserWin()" iconCls="icon-user"><s:text name="修改所有者"/></div>
				</s:if>
			</div>
			<div id="psl_mm" class="easyui-menu" style="width:120px;">
				<div onclick="doRemoveDataOnPageSelectList()" iconCls="icon-remove"><s:text name="移除所选数据"/></div>
			</div>
			<div id="pl_tt">
				<a href="#" id="a_switch_query" class="icon-menu" title="<s:text name="system.button.switchQueryType.title"/>" style="display:none;"></a>
				<a href="#" id="a_exp_clp" class="accordion-expand" title="<s:text name="system.button.expand.title"/>"></a>
			</div>
		   </div>
			<#-- 新版easyui在初始化layout时，将会查询children中的form元素，如存在则初始化对form兄弟元素无效 -->
			<div style="display:none;">
			<form method="post" name="hidFrm" id="hidFrm" >
				<textarea style="display:none;" name="hid_queryParams" id="hid_queryParams">${r"${param.queryParams}"}</textarea><#-- 查询参数 -->
				<textarea style="display:none;" name="hid_addParams" id="hid_addParams">${r"${param.addParams}"}</textarea><#-- 新增参数 -->
				<input type="hidden" name="hid_relevanceModule" id="hid_relevanceModule" value="${entityName}"/><#-- 关联模块 -->
			</form>
			</div>
<div class="container-fluid advance-query-form" style="margin-top:10px;display:none;">
	<div class="row form-group">
		<div class="col-sm-12">
			<div class="input-group input-group-sm">
			  <span class="input-group-addon" id="sizing-addon3">自定义查询条件</span>
			  <select id="queryFieldSel"></select>
			  <span class="input-group-btn">
		        <a href="javascript:;" title="收藏常用查询字段" role="button" class="btn btn-default validInView" type="button" onclick="$ES.doAddAQvToFavorite(function(){alert('常用查询字段已收藏！');})"><span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span></a>
		      </span>
			</div>
		</div>
	</div>
	<form id="queryForm" onsubmit="return false;">
	</form>
</div>			
</@compress>		
			<%@ include file="/common/meta-js.jsp" %>
			<#-- <script type="text/javascript" src="${r"${ctx}"}/js/easyui-1.4/easyui.xpPanel.js"></script>-->
			<script type="text/javascript">
            var _userList={};
            
            function getAddParams() {
				var param =$("#hid_addParams").val();
				var qp = $.parseJSON(param);
				return qp;
			}

			function getCommonParams(paramObj) {
				paramObj = paramObj || {};
				if(window.onBeforeQuery) {
					window.onBeforeQuery(paramObj);
				}
				var param = $("#simpleQueryForm").serializeArrayToParam();
				$.extend(true, param, paramObj);<#-- 简单查询 -->
				if($("#ul_tab_query").length) {
					var obj = $("#ul_tab_query").data("queryParam");
					$.extend(true, param, obj);
				}
				if($("#_commonTypeTree_div_").length) {
					var obj = $("#_commonTypeTree_div_").data("queryParam");
					$.extend(true, param, obj);
				}
				$.extend(true, param, $("#queryForm").serializeArrayToParam());<#-- 高级查询 -->
				var qps = getFormQueryParams();<#-- 加入初始化查询条件 -->
				param = $.extend(true, qps, param);
				return param;
			}

			function getQueryParams() {
				var param = getCommonParams();
				param["sort"] = $("#queryList").datagrid("options")["sortName"];
				param["order"] = $("#queryList").datagrid("options")["sortOrder"];
				var strParam = JSON.stringify(param);
				var params = [];
				params.push("&queryParams=");
				params.push(encodeURIComponent(strParam));
				var ap = null;
				if($("#_commonTypeTree_div_").length) {
					ap = $("#_commonTypeTree_div_").data("addParam");
				}
				ap = $.extend({}, getAddParams(), ap);
				params.push("&addParams=");
				params.push(encodeURIComponent(JSON.stringify(ap)));
				return params.join("");
			}
			
            function getOption() {
            	var fcs1 =  [{
						field : 'ck',
						checkbox : true
						<#-- 新版easyui不能大于field数组length -->
					}
					,{
						field : "atmId",
						title : "<s:text name="system.button.accessory.title"/>",
						width : 35,
						formatter:function(value, rowData, rowIndex) {
							if(value) {
								var astr = [];
								astr.push('<a class="easyui-linkbutton l-btn l-btn-plain" href="javascript:void(0);"');
								<security:authorize url="/sys/attach/attach.action?entityName=${entityName}">
								astr.push(' onclick="doAccessory(');
								astr.push(rowData["id"]);
								astr.push(',');
								astr.push(value);
								astr.push(',\'');
								astr.push(rowData["status"]);
								astr.push('\')"');
								</security:authorize>
								astr.push('>');
								astr.push('<span class="l-btn-left"><span class="l-btn-text"><span class="l-btn-empty icon-attach">&nbsp;</span></span></span></a>');
								return astr.join("");
							} else {
								return "<span></span>";
							}
						}
                    }
                    <#if !((form.isHideStatus)!false) >
                    ,{
                    	field : "status",
						title : "<s:text name="system.module.status.title"/>",
						width : 60,
						sortable : true,
						isFilterData:true,
						formatter:function(value, rowData, rowIndex) {
							<#--if(value) {
								var val = $.jwpf.system.module.status[value];
								return (val?val:"");
							} else {
								return "";
							}-->
							return rowData["status_caption"];
						}
                    }</#if>];
                var fcs2 = [
                	<#assign isShowComma = false />
        			<#if form.isShowCreateUser >   
                     {
                            field : 'createUser',
                            title : '<s:text name="system.module.createUser.title"/>',
                            width : 120,
                            sortable : true,
							isFilterData:true,
                            formatter:function(value, rowData, rowIndex){
                                    <#-- var uObj=_userList[value];
                                    if(uObj){
                                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
										return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                                    }else{
                                        return value;
                                    }-->
                                    return rowData["createUser_caption"];
                            }
                     }<#assign isShowComma = true /></#if>
                     <#if form.isShowCreateTime ><#if isShowComma>,</#if> 
                     {
                            field : 'createTime',
                            title : '<s:text name="创建时间"/>',
                            width : 100,
							sortable : true,
							align : 'center'
                     }<#assign isShowComma = true /></#if>
                     <#if form.isShowLastUpdateUser ><#if isShowComma>,</#if> 
                     {
                            field : 'lastUpdateUser',
                            title : '<s:text name="最后修改者"/>',
                            width : 120,
                            sortable : true,
							isFilterData:true,
                            formatter:function(value, rowData, rowIndex){
                                    <#-- var uObj=_userList[value];
                                    if(uObj){
                                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
										return uObj.realName + "[" + uObj.loginName + "]" + nickName;
                                    }else{
                                        return value;
                                    }-->
                                    return rowData["lastUpdateUser_caption"];
                            }
                     }<#assign isShowComma = true /></#if>
                     <#if form.isShowLastUpdateTime ><#if isShowComma>,</#if>
                     {
                            field : 'lastUpdateTime',
                            title : '<s:text name="最后修改时间"/>',
                            width : 100,
							sortable : true,
							align : 'center'
                     }<#assign isShowComma = true /></#if>
                     <#if isShowComma>,</#if>
                     {
                            field : 'cancelReason',
                            title : '<s:text name="system.module.invalidReason.title"/>',
                            width : 120
                     }
                     <security:authorize url="shareUserVisible_${entityName}">
                     <#if isShowComma>,</#if>
                     {
                            field : 'dataShareLog',
                            title : '<s:text name="数据共享日志"/>',
                            width : 200
                     }
                     </security:authorize>
                ];
                <#assign fields=form.fieldList/>
                var frozenFieldscolumns = [[ <#-- 冻结列 -->
	                 <#if fields??&&fields?size gt 0>
         			    <#assign layerend = 0>
         			    <#assign nohasfield = 1>
                        <#list 1..fields?size as layer>
                        	<#assign i=0>
                        	<#assign layerend = 1>
                            <#list fields as field>
                            	<#if field.key?? && field.key != "">
                            	<#if (!field.editProperties.layer?? && layer = 1) || (field.editProperties.layer?? && field.editProperties.layer = layer)>
                                <#if field.queryProperties.showInGrid && (field.queryProperties.frozenColumn)!false>
                                  <#if layer gt 1 && i = 0>,[</#if>
                            	  <#assign layerend = 0>
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>     
                                	<@fieldftl.initMainQuery field=field prefix="dg_" form=form/>                               
                                  <#assign i=i+1>	
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>    
                                </#if>
                                </#if>
                                </#if>
                            </#list>
                            <#if layerend = 0 && i gt 0>]<#assign nohasfield = 0></#if><#if layerend = 1><#break></#if>
                        </#list>
                        <#if nohasfield = 1>]</#if>
                    </#if>
	                ];
	            var fieldscolumns = [[
         			<#if fields??&&fields?size gt 0>
         			    <#assign layerend = 0>
         			    <#assign nohasfield = 1>
                        <#list 1..fields?size as layer>
                        	<#assign i=0>
                        	<#assign layerend = 1>
                            <#list fields as field>
                            	<#if field.key?? && field.key != "">
                            	<#if (!field.editProperties.layer?? && layer = 1) || (field.editProperties.layer?? && field.editProperties.layer = layer)>
                                <#if field.queryProperties.showInGrid && !((field.queryProperties.frozenColumn)!false)>
                                  <#if layer gt 1 && i = 0>,[</#if>
                            	  <#assign layerend = 0>
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>     
                                	<@fieldftl.initMainQuery field=field prefix="dg_" form=form/>                      
                                  <#assign i=i+1>	
                                  <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>    
                                </#if>
                                </#if>
                                </#if>
                            </#list>
                            <#if layerend = 0 && i gt 0>]<#assign nohasfield = 0></#if><#if layerend = 1><#break></#if>
                        </#list>
                        <#if nohasfield = 1>]</#if>
                    </#if>
                ];
				var frozenColumns = [];
				var columns = [];
				for(var i = 0; i < frozenFieldscolumns.length; i++){
					if(i===0) {
						frozenColumns.push(fcs1.concat(frozenFieldscolumns[0]));
					} else {
						frozenColumns.push(frozenFieldscolumns[i]);
					}
				}
				for(var i = 0; i < fieldscolumns.length; i++){
					if(i===0) {
						columns.push(fieldscolumns[0].concat(fcs2));
					} else {
						columns.push(fieldscolumns[i]);
					}
				}
                return {
                	editing:false,//列表页面，取消单击编辑
                	url:'${r"${ctx}"}/gs/gs-mng!queryList.action?entityName=${entityName}&relevanceModule=${relevanceModule}',
                    width : 'auto',
                    height : 'auto',
                    nowrap : false,
                    autoRowHeight: false,
                    striped : true,
                    fit : true,                
                    sortName : <#if ((form.sortName)!"") != "">'${form.sortName}'<#else>'id'</#if>,
                    sortOrder : <#if ((form.sortOrder)!"") != "">'${form.sortOrder}'<#else>'desc'</#if>,
					pageList:[<#if ((form.pageSizeList)!"") != "">${form.pageSizeList}<#else>10,20,30,40,50,100,200,500</#if>],
					pageSize:<#if ((form.pageSize)!"") != "">${form.pageSize}<#else>20</#if>,
                    <#--frozenColumns : [
                        [
                            <#assign fields=form.fieldList/>
                            <#list fields as field>  列表显示且作为冻结列 
	                                <#if ((field.queryProperties.showInGrid)!false) && (field.queryProperties.frozenColumn)!false>
	                                	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>  
	                                    <@fieldftl.initMainQuery field=field prefix="dg_" form=form appendCommaFirst=true/>
	                                    <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
	                                </#if>
                            </#list>
                        ]
                    ],-->
                    frozenColumns : frozenColumns,
                    <#--columns : [
                        [
                            <#if fields??&&fields?size gt 0>
                                <#list fields as field>
	                                <#if ((field.queryProperties.showInGrid)!false) && !((field.queryProperties.frozenColumn)!false)>
	                                	<@fieldftl.initCheckFieldAuth field=field form=form isEnd=false/>  
	                                    <@fieldftl.initMainQuery field=field prefix="dg_" form=form/>
	                                    <@fieldftl.initCheckFieldAuth field=field form=form isEnd=true/>  
	                                </#if>
                                </#list>
                            </#if>      <#assign isShowComma = false />
                            			<#if form.isShowCreateUser >   
                                         {
	                                            field : 'createUser',
	                                            title : '<s:text name="system.module.createUser.title"/>',
	                                            width : 120,
	                                            sortable : true,
												isFilterData:true,
	                                            formatter:function(value, rowData, rowIndex){
	                                                    var uObj=_userList[value];
	                                                    if(uObj){
	                                                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
															return uObj.realName + "[" + uObj.loginName + "]" + nickName;
	                                                    }else{
	                                                        return value;
	                                                    }
	                                            }
                                         }<#assign isShowComma = true /></#if>
                                         <#if form.isShowCreateTime ><#if isShowComma>,</#if> 
                                         {
	                                            field : 'createTime',
	                                            title : '<s:text name="创建时间"/>',
	                                            width : 100,
												sortable : true,
												align : 'center'
                                         }<#assign isShowComma = true /></#if>
                                         <#if form.isShowLastUpdateUser ><#if isShowComma>,</#if> 
                                         {
	                                            field : 'lastUpdateUser',
	                                            title : '<s:text name="最后修改者"/>',
	                                            width : 120,
	                                            sortable : true,
												isFilterData:true,
	                                            formatter:function(value, rowData, rowIndex){
	                                                    var uObj=_userList[value];
	                                                    if(uObj){
	                                                        var nickName = (uObj.nickName)?("-" + uObj.nickName):"";
															return uObj.realName + "[" + uObj.loginName + "]" + nickName;
	                                                    }else{
	                                                        return value;
	                                                    }
	                                            }
                                         }<#assign isShowComma = true /></#if>
                                         <#if form.isShowLastUpdateTime ><#if isShowComma>,</#if>
                                         {
	                                            field : 'lastUpdateTime',
	                                            title : '<s:text name="最后修改时间"/>',
	                                            width : 100,
												sortable : true,
												align : 'center'
                                         }<#assign isShowComma = true /></#if>
                                         <#if isShowComma>,</#if>
                                         {
	                                            field : 'cancelReason',
	                                            title : '<s:text name="system.module.invalidReason.title"/>',
	                                            width : 120
                                         }
                        ]
                    ],-->
                    columns : columns,
                    onDblClickRow:function(rowIndex, rowData){
                    	<@fieldftl.initDatagridOnDblClickRow form=form />
                    	doDblView(rowData.id);
                    	<#-- $('#queryList').datagrid('unselectRow',rowIndex); -->
                    },
    				onRowContextMenu  : function(e, rowIndex, rowData){
    					e.preventDefault();
    					$('#mm').data("id", rowData.id);
    					$('#mm').data("attachId", rowData.atmId);
    					$('#mm').data("status", rowData.status);
    					$('#mm').menu('show', {
    						left: e.pageX,
    						top: e.pageY
    					});
    				},
    				onLoadError:function() {
    					$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="数据加载异常"/>', 'info');
    				},
    				onLoadSuccess :function(data){
    					<@fieldftl.initStatistics form=form isInit="true" tableKey="queryList" />
						<@fieldftl.initDatagridOnLoadSuccess form=form />
					},
                    pagination : true,
                    rownumbers : true,
                    toolbar :"#queryListToolBar"
                    <#--[
                        '-'
						<security:authorize url="/gs/gs-mng!add.action?entityName=${entityName}">       
                        ,{
                            id : 'bt_add',
                            text : '<s:text name="system.button.add.title"/>(I)',
                            iconCls : 'icon-add',
                            handler : function() {
                                doAdd();
                            }
                        }</security:authorize><security:authorize url="/gs/gs-mng!delete.action?entityName=${entityName}&relevanceModule=${relevanceModule}">
                        ,
                        {
                            id : 'bt_del',
                            text : '<s:text name="system.button.delete.title"/>(D)',
                            iconCls : 'icon-remove',
                            handler : function() {
                                doDelete();
                            }                           
                        }</security:authorize>
                        ,
                        {
                            id : 'bt_query_form',
                            text : '高级查询',
                            iconCls : 'icon-search',
                            handler : function() {
                                openFormWinV2({"url":"/sys/common/list-query-ad.action","isIframe":false,onInit:function() {
									alert("hello,bob");
								},"okTitle":"查询","cancelTitle":"重置"});
                            }                           
                        }
                        <security:authorize url="/gs/gs-mng!view.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_view',
                            text : '<s:text name="system.button.view.title"/>(E)',
                            iconCls : "icon-search",
                            handler : function() {
                                doView();
                            }
                        }</security:authorize><security:authorize url="/gs/gs-mng!edit.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_copy',
                            text : '<s:text name="system.button.copy.title"/>(G)',
                            iconCls : "icon-copy",
                            handler : function() {
                            	doCopyTbar();
                            }
                        }</security:authorize><security:authorize url="/sys/attach/attach.action?entityName=${entityName}">
                        ,
                        {
                            id : 'bt_accessory',
                            text : '<s:text name="system.button.accessory.title"/>(Y)',
                            iconCls : "icon-attach",
                            handler : function() {
                                doAccessory();
                            }
                        }</security:authorize>
                        <security:authorize url="/gs/data-share-config!save.action?entityName=${entityName}">
                        ,
                        {
                        	id : 'bt_dataShare',
                            text : '<s:text name="数据共享"/>',
                            iconCls : "icon-run",
                            handler : function() {
                               var ids = jwpf.getTableVal("queryList",true,"id");
                               jwpf.doSetDataShareUser("${entityName}",ids,function() {$("#queryList").edatagrid("clearSelections");});
                            }
                        }</security:authorize>
                        <#if flowList??&&flowList?size gt 0>
                         
                        <security:authorize
												url="/gs/process!startApprove.action?entityName=${entityName}">
		                ,
                        {
                        	id : 'bt_selfApprove',
                            text : '<s:text name="批量送审"/>',
                            iconCls : "icon-flowStart",
                            handler : function() {
                                var ids = [];
								var rows = jwpf.getTableVal("queryList",true);
								if(rows && rows.length) {
								   for(var i in rows) {
								      var obj = rows[i];
								      if(obj["status"]=="10") {
								        ids.push(obj["id"]);
								      } else {
								        alert("只能选择草稿状态的记录");
								        return;
								      }
								   }
								   jwpf.doBatchStartProcess(ids, "${entityName}", null, doQuery);
								} else {
								   alert("请选择记录");
								   return;
								}
                            }
                        }
		                </security:authorize>
                        <#else>
                        <security:authorize url="/gs/process!startApprove.action?entityName=${entityName}">
                        ,
                        {
                        	id : 'bt_selfApprove',
                            text : '<s:text name="system.button.selfApprove.title"/>',
                            iconCls : "icon-flowStart",
                            handler : function() {
                               doSelfProcess();
                            }
                        }</security:authorize><security:authorize url="/gs/process!cancelApprove.action?entityName=${entityName}">
                        ,
                        {
                        	id : 'bt_cancelSelfApprove',
                            text : '<s:text name="system.button.cancelSelfApprove.title"/>',
                            iconCls : "icon-user",
                            handler : function() {
                               doCancelSelfProcess();
                            }
                        }</security:authorize>
                        </#if>
                        <#if form.enableImport>
                        <security:authorize url="/gs/gs-mng!importModuleExcel.action?entityName=${entityName}"> 
                        ,
                        {
                        	id : 'bt_excelImport',
                            text : '<s:text name="system.button.importExcel.title"/>',
                            iconCls : "icon-excelImport",
                            handler : function() {
                               doOpenExcelImportWin("${entityName}");
                            }
                        }</security:authorize>
                        </#if>
                        <#if form.enableExport>
                        <security:authorize url="/gs/gs-mng!exportModuleExcel.action?entityName=${entityName}">
                        ,
                        {
                        	id : 'bt_excelExport',
                            text : '<s:text name="system.button.exportExcel.title"/>',
                            iconCls : "icon-excelExport",
                            handler : function() {
                               var rows = $('#queryList').datagrid('getChecked');
                               var ids = [];
                               if(rows && rows.length) {
                               	  $.each(rows, function(i, v) {
                               	  	ids.push(v.id);
                               	  });
                               }
                               doExportExcel("${entityName}", getExportParams("queryList"), ids.join(","));
                            }
                        }</security:authorize>
                        </#if>
                        <@fieldftl.initDatagridToolBarButton form=form />
                    ]-->
                    <@fieldftl.initDatagridParam form=form />
                };
            }
            
		    function doSelfProcess() {
		    	var ids = [];
                var rows = $('#queryList').datagrid('getChecked');
                for (var i = 0; i < rows.length; i++) {
                   	if(rows[i].status != "10") {
                    	$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.selfApproveWarn.title"/>', 'info');
                    	return;
                    }
                    ids.push(rows[i].id);
                }
                if(ids.length) {
	                $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.selfApproveSure"/>',
						function(r) {
			           		if(r) {
			           			if(window.onBeforeStartProcess) {
						           var flag = window.onBeforeStartProcess();
						           if(flag === false) {
						           	   return;
						           }
						        }
			                 	jwpf.doStartOrCancelApprove("${entityName}", ids, "startApprove", function() {
			                 		if(window.onAfterStartProcess) {
									   window.onAfterStartProcess();
									}
									doRefreshDataGridOnPage();
			                 	});
			                } 
			        	}
			        );
                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
                }
			}
			
			function doCancelSelfProcess() {
				var ids = [];
                var rows = $('#queryList').datagrid('getChecked');
                for (var i = 0; i < rows.length; i++) {
                   	if(rows[i].status != "35" && rows[i].status != "31") {
                    	$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.cancelSelfApproveWarn.title"/>', 'info');
                    	return;
                    }
                    ids.push(rows[i].id);
                }
				if(ids.length) {
	                $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.cancelSelfApproveSure"/>',
						function(r) {
			           		if(r) {
			           			if(!window.onBeforeCancelProcess || window.onBeforeCancelProcess()!=false) {
			           				jwpf.doStartOrCancelApprove("${entityName}", ids, "cancelApprove", function() {
				                 		if(window.onAfterCancelProcess) {
										   window.onAfterCancelProcess();
										}
										doRefreshDataGridOnPage();
				                 	});
			           			}
			                } 
			        	}
			        );
                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
                }
			}
					 
			   function formatterImage(value, rowData, rowIndex){
			           if(value){
			        	   var path="${r"${systemParam.virtualPicPath}"}"+value;
			               return  "<div><img onclick='$(body).gzoomExpand(\" "+path+" \")' src='"+path+"'  id='contractImgSrc' width='20px' height='20px' /></div>"; 
			           }
				 }
						            
				
			   <#-- 
                <#assign fields=form.fieldList/>
                <#if fields??&&fields?size gt 0>
                    <#list fields as field>
                        <#if (field.queryProperties.showInGrid)!false>
                           <@fieldftl.initControlsData field=field type="datagrid" prefix="dg_" entityName=form.entityName/>
                        </#if>
                    </#list>
                </#if>
                -->
                
                <@fieldftl.initDatagridToolBarButtonV2 form=form isInitEvent=true/>
                
                <@fieldftl.initFormFieldOnListEvent form=form /> <#-- 初始化列表按钮事件 -->
                
                <@fieldftl.initFormQueryPageEvent form=form /> <#-- 初始化查询页面事件 -->
                
                <@fieldftl.initFieldReviseEvent form=form formKey="form" /><#-- 初始化主表修订事件（上传文件后触发事件支持） -->
               
            //右键复制一条记录 
            function doCopy(){
             var id = $("#mm").data("id");
             if(id==null){
            	 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
                  return ;
             }else{
            	  top.addTab('<s:text name="system.button.copy.title"/><s:text name="${i18nKey!titleName}"/>-'+id, '${r"${ctx}"}/gs/gs-mng!edit.action?entityName=${entityName}&id=' + id+ getQueryParams());
             }
             
          }
           //工具条复制一条记录
           function doCopyTbar(){
        	   var id=0;
             	 var rows = $('#queryList').datagrid('getChecked');
             	 if(rows.length!=1){
             		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.copy"/>', 'info');
             	     return ;
             	 } else{
             		 id=rows[0].id;
             		 top.addTab('<s:text name="system.button.copy.title"/><s:text name="${i18nKey!titleName}"/>-'+id, '${r"${ctx}"}/gs/gs-mng!edit.action?entityName=${entityName}&id=' + id+ getQueryParams());
             	 }
           }
           //删除一条记录
            function doDelete() {
                var ids = [];
              <#--if (id == null) {-->  
                    var rows = $('#queryList').datagrid('getChecked');
                    for (var i = 0; i < rows.length; i++) {
                    	if(rows[i].status != "10") {
                    		$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.delApproveData"/>', 'info');
                    		return;
                    	}
                        ids.push(rows[i].id);
                    }
               <#--  } else {
                    ids.push(id);
                }--> 

                if (ids != null && ids.length > 0) {
                    if(window.onBeforeDelete) {
			           var flag = window.onBeforeDelete();
			           if(flag === false) {
			           	   return;
			           }
			        }
                    $.messager.confirm('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.info"/>', function(r) {
                        if (r) {
                            var options = {
                                url : '${r"${ctx}"}/gs/gs-mng!delete.action?entityName=${entityName}&relevanceModule=${relevanceModule}',
                                data : {
                                    "ids" : ids
                                },
                                success : function(data) {
                                    if (data.msg) {
                                        doQuery();
                                    }
                                },
                                traditional:true
                            };
                            fnFormAjaxWithJson(options);
                        }
                    })

                } else {
                    $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.question"/>', 'info');
                }
            }
            
            function doResetQuery(isResetAdQuery) {
            	!isResetAdQuery && $ES.doResetAdvanceQuery("simpleQueryForm");
            	$ES.doResetAdvanceQuery();
            	doQuery();
            }
            
            var _ad_query_opt_ = {"data":<@fieldftl.getQueryViewJson formList=formList/>,"isInit":false,"entityName":"${entityName}"};
            function doAdvanceQuery() {
            	openFormWinV2({"message":$(".advance-query-form"),"isIframe":false,"isModal":false,
            		"okTitle":"<s:text name="system.search.button.title"/>","cancelTitle":"<s:text name="system.search.reset.title"/>"
            		,onAfterSure:function(win) {
            			doQuery();
            		}, onAfterCancel:function(win) {
            			doResetQuery(true);
            		}});
            	$ES.initAdvanceQuery(_ad_query_opt_);
            }
            
            //查询
            function doQuery(paramObj, isInit) {
				<#-- paramObj = paramObj || {};
                if(window.onBeforeQuery) {
                   window.onBeforeQuery(paramObj);
                }
                var param = $("#simpleQueryForm").serializeArrayToParam();
                $.extend(true, param, paramObj); //简单查询
                if($("#ul_tab_query").length) {
                    var obj = $("#ul_tab_query").data("queryParam");
                    $.extend(true, param, obj);
                }
                if($("#_commonTypeTree_div_").length) {
                    var obj = $("#_commonTypeTree_div_").data("queryParam");
                    $.extend(true, param, obj);
                }
                $.extend(true, param, $("#queryForm").serializeArrayToParam()); //高级查询
                var qps = getFormQueryParams(); //加入初始化查询条件
                param = $.extend(true, qps, param);-->
				var param = getCommonParams(paramObj);
				if(isInit) { <#-- 初始化列表 -->
					var opt = getOption();
					opt["queryParams"] = param;
					$("#queryList").edatagrid(opt);
				} else {
					$('#queryList').datagrid('clearSelections');
					$("#queryList").edatagrid("load", param);
				}
            }

          
           //新增
            function doAdd() {
                top.addTab('<s:text name="system.button.add.title"/><s:text name="${i18nKey!titleName}"/>', '${r"${ctx}"}/gs/gs-mng!add.action?entityName=${entityName}'+ getQueryParams());
            }
            
            function doView() {
            	 var id=0;
            	 var rows = $('#queryList').datagrid('getChecked');
            	  if(rows.length!=1){
            		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
            	     return ;
            	 } else{
            		 id=rows[0].id;
            	 }
            	 doDblView(id); 
            }
           //查看tab
            function doDblView(id) {
            	top.addTab('<s:text name="system.button.view.title"/><s:text name="${i18nKey!titleName}"/>-'+id, '${r"${ctx}"}/gs/gs-mng!view.action?entityName=${entityName}&id=' + id+ getQueryParams());
            }
            
            //右键查看
            function doCmView() {
            	var id = $("#mm").data("id");
            	doDblView(id);
            }
            
            //重定义尺寸
            function doResizeDataGrid() {
            	$("#queryList").datagrid("resize");
            	$("#queryList").datagrid("fixColumnSize","ck");
            }
            
           //刷新列表
            function doRefreshDataGrid() {
    			$("#queryList").edatagrid("load");
    		}
    		
    		function doRefreshDataGridOnPage() {
    			$("#queryList").edatagrid("reload");
    		}
    		
			//添加附件
            function doAccessory(id, attachId, status) {
				 if(!id) {
	           	     var rows = $('#queryList').datagrid('getChecked');
		           	 if(rows.length!=1){
		           		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
		           	     return ;
		           	 } else{
		           		attachId=rows[0].atmId;
		           		id=rows[0].id;
		           		status=rows[0].status;
		           	 }
	           	 }
	           	 var opts ={"attachId":attachId,
           				 "id":id,
           				 "key":"",
           				 "entityName":"${entityName}",
           				 "dgId":"queryList",
           				 "flag":"mainQuery",
           				 "readOnly" : false
           		 };
	           	 if(status == "10") {
	           		opts.readOnly = false;
	           	 } else if(status == "onlyUpload") {
		    		opts.readOnly = "onlyUpload";
		    	 } else {
	           		opts.readOnly =true;
	           	 }
	           	 doAttach(opts);
            }

			//列表添加附件
			function doAccessoryOnList(fieldVal, fieldKey, tableKey, rowIndex, entityName, stat) {
				var opts ={"attachId":fieldVal,
						 "id":rowIndex,
						 "key":fieldKey,
						 "entityName":entityName||"",
						 "dgId":tableKey,
						 "flag":"subQuery",
						 "readOnly" : false
				};
				var status = stat || jwpf.getTableVal(tableKey, rowIndex, "status");
				if(status == "10") {
	           		opts.readOnly =false;
	           	 } else if(status == "onlyUpload") {
		    		opts.readOnly = "onlyUpload";
		    	 } else {
	           		opts.readOnly =true;
	           	 }
				doAttach(opts);
			}

			//更新附件ID flag: mainQuery-主实体查询，subEdit-子实体编辑，mainEdit-主实体编辑
            function doUpdateAttach(opts) {
            	if(opts.flag == "subQuery") {
					var rowIndex = parseInt(opts.id);
					var tableKey = opts.dgId;
					var row = jwpf.getTableVal(tableKey, rowIndex);
					if(row) {
						if(row[opts.key] != opts.attachId) {
								var options = {
									url : '${r"${ctx}"}/gs/gs-mng!updateAttach.action',
									data : {
										"entityName":opts.entityName,
										"id" : row["id"],
										"attachId": opts.attachId,
										"field" : opts.key,
										"relevanceModule":"${relevanceModule}"
									},
									async: false,
									success : function(data) {
										if(data.msg) {
											var eventKey = "onAfterUploadFileFor_form_"+opts.key;
											var func = window[eventKey];
											if(func) {
												func(opts.attachId, rowIndex, tableKey, row);
											}
											$("#" + tableKey).edatagrid("reload");//刷新表
										}
									}
								};
								fnFormAjaxWithJson(options);
						}
					}
				} else {
					var options = {
            			url : '${r"${ctx}"}/gs/gs-mng!updateAttach.action',
                        data : {
                        	"entityName":opts.entityName,
                            "id" : opts.id,
                            "attachId": opts.attachId,
                            "field" : "atmId",
                            "relevanceModule":"${relevanceModule}"
                        },
    					async: false,
    					success : function(data) {
    						if(data.msg) {
        						if(opts.flag == "mainQuery") {
        							$("#" + opts.dgId).datagrid("reload");
            					} else if(opts.flag == "subEdit"){
            						$("#" + opts.dgId).edatagrid("reload");
                				}
    						}
    					}
					};
					fnFormAjaxWithJson(options);
				}
            }
            
            
            function doViewReport(tptName,id,type) {
            	 if(!id) {
            	 	 var rows = $('#queryList').datagrid('getChecked');
            	 	 if(rows.length!=1){
		           		 $.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.view"/>', 'info');
		           	     return ;
		           	 } else{
		           		id=rows[0].id;
		           	 }
            	 }
            	 tptName = tptName.toLowerCase();
				 top.addTab("<s:text name="${i18nKey!titleName}"/>-<s:text name="system.button.report.title"/>-" + tptName + "-" + id,'${r"${ctx}"}/sys/report/report.action?rptName=' + tptName + "&id=" + id + "&type=" + (type?type:""));
			}
			
			function initQueryData() {
				<#assign fields=form.fieldList!>
                <#if fields??&&fields?size gt 0>
                    <#list fields as field>
	                        <#if (field.queryProperties.showInSearch)!false || (field.queryProperties.showInCommonSearch)!false>
		                         <@fieldftl.initControlsData field=field type="listQuery" prefix="query_" entityName=form.entityName 
		                         	formKey="" moduleTitle="" isAsync="true"/>
	                        </#if>
	                        <#if (field.queryProperties.showInTreeSearch)!false>
	                        	<@fieldftl.initTreeQuery field=field prefix="dg_" entityName=form.entityName/>
	                        </#if>
                    </#list>
                </#if>
                <#-- 初始化子表查询条件 -->
                <#list formList as subForm>
				     <#if !subForm.isMaster && !((subForm.isVirtual)!false)>
				         <#assign fields=subForm.fieldList!>
			                <#if fields??&&fields?size gt 0>
			                    <#list fields as field>
			                       <#if (field.queryProperties.showInSearch)!false  || (field.queryProperties.showInCommonSearch)!false>	
				                     <@fieldftl.initControlsData field=field type="listQuery" prefix=("query_"+subForm.key+"_") entityName=subForm.entityName formKey=subForm.key 
				                     	 moduleTitle="" isAsync="true"/>
			             		   </#if>
			             		</#list>
			          		</#if>
			        </#if>
			     </#list><#-- 系统状态初始化参数 -->
			     query_statusJson = $ES.getStatusList();
			     initTabControlsShowInList();
			}
			
			<#-- 初始化显示在列表页面的Tab页内容下包含控件数据 -->
			function initTabControlsShowInList(){
			      <#if isShowInQueryList>
			      	 <#list root.dataSource.tabsList as tabs >
                             <#list tabs.tabList as tab>
                                    <#if form.key==tab.form>
                                         <#if form.isMaster && tab.isShowInQueryList?? && tab.isShowInQueryList><#--主表分tab页面-->   
			         						<#assign fields=form.fieldList!>
								             <#if fields??&&fields?size gt 0>
								                  <#list fields as field>
								                  	<#if field.editProperties.visible && !(field.isCaption?? && field.isCaption)>														                 
									                   <#if field.tabRows==tab.rows && field.tabCols==tab.cols>
			                <@fieldftl.initControlsData field=field type="query" prefix="" entityName=form.entityName formKey="" moduleTitle=i18nKey!titleName />
			                <@fieldftl.initFieldEvent field=field/>
								             	       </#if>
								             	    </#if>
								             	  </#list>
								          	 </#if>
								         </#if>
								    </#if>
							 </#list>	          	 
		          	 </#list>
		          	 $("#showInQueryList :input").attr({"readonly":"readonly"});
		          </#if>
       		}
       		
       		<#-- 初始化tab查询 -->
			function initTabQuery(tabData, dataParam, fpName, tabKey, tabTitle) {
				if(tabData && tabData.length && dataParam) {
					var dtObj = {};
					dtObj[dataParam.key] = "";
					dtObj[dataParam.caption] = tabTitle + "(全部)";
					var arr = [dtObj];
					tabData = arr.concat(tabData);
					var ulHtmlArr = [];
					var tabKeyId = "ul_tab_query_"+tabKey;
					ulHtmlArr.push(['<div class="btn-toolbar" role="toolbar" id="', tabKeyId,'">'].join(''));
					for(var i=0,len=tabData.length;i<len;i++) {
						var key = tabData[i][dataParam.key];
						var caption = tabData[i][dataParam.caption];
						ulHtmlArr.push('<div class="btn-group btn-group-xs">');
						ulHtmlArr.push('<button type="button" class="btn btn-default navbar-btn');
						if(key !== "") {<#-- 可过滤按钮 -->
							ulHtmlArr.push(' btn-can-click');	
						}
						if(i == 0) {
							if(key !== "") {
								ulHtmlArr.push(' btn-success');							
							} else {
								ulHtmlArr.push(' btn-primary');
							}
							var paramObj = $("#ul_tab_query").data("queryParam") || {};
							paramObj[fpName] = key;
							$("#ul_tab_query").data("queryParam", paramObj);
						}
						ulHtmlArr.push('" keyVal="');
						ulHtmlArr.push(key);
						ulHtmlArr.push('">');
						ulHtmlArr.push(caption);
						ulHtmlArr.push('</button>');
						ulHtmlArr.push("</div>");
					}
					$("#ul_tab_query").append(ulHtmlArr.join(""));
					$("#"+tabKeyId+" button.btn-can-click").click(function(event) {
						var paramObj = $("#ul_tab_query").data("queryParam") || {};
						var keyVal = "";
						if($(this).hasClass("btn-success")) {
							$(this).removeClass("btn-success");
						} else {
							$(this).addClass("btn-success");
						}
						var valArr = [];
						$("#"+tabKeyId+" button.btn-success").each(function() {
							var val = $(this).attr("keyVal");
							if($.isNumeric(val) || val) {
								valArr.push(val);
							}
						});
						keyVal = valArr.join(",");
						paramObj[fpName] = keyVal;
						$("#ul_tab_query").data("queryParam", paramObj);
						doQuery();
					});
				}
			}
       		
       		function initTabQueryExpand() {
				var tbs = $("#ul_tab_query div.btn-toolbar");
				if(tbs.length > 1) {
					var tb = $(tbs.get(0));
					tb.append(['<div class="btn-group btn-group-xs pull-right">',
								'<button type="button" class="btn btn-default navbar-btn btn-expand" title="more shrink/expand">',
								'', '<i class="fa fa-chevron-up"></i>',
								'</button></div>'].join(""));
					tb.find("button.btn-expand").click(function() {
						tb.nextAll().fadeToggle("normal","linear");
						var ti = $(this).find("i");
						ti.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down");
					});
				}
			}
			
			<#-- 初始化tab查询
			function initTabQuery(tabData, dataParam, fpName) {
				if(tabData && tabData.length && dataParam) {
					var dtObj = {};
					dtObj[dataParam.key] = "";
					dtObj[dataParam.caption] = "全部";
					var arr = [dtObj];
					tabData = arr.concat(tabData);
					var ulHtmlArr = [];
					for(var i=0,len=tabData.length;i<len;i++) {
						var key = tabData[i][dataParam.key];
						var caption = tabData[i][dataParam.caption];
						if(i > 0) {
							ulHtmlArr.push("<li>");
						} else { 
							ulHtmlArr.push("<li class='tabs-selected'>");
							var paramObj = {};
							if(key !== "") paramObj[fpName] = key;
							$("#ul_tab_query").data("queryParam", paramObj);
						}
						ulHtmlArr.push('<a href="javascript:void(0)" class="tabs-inner" keyVal="');
						ulHtmlArr.push(key);
						ulHtmlArr.push('"><span class="tabs-title">');
						ulHtmlArr.push(caption);
						ulHtmlArr.push('</span></a>');
						ulHtmlArr.push("</li>");
					}
					$("#ul_tab_query").html(ulHtmlArr.join(""));
					$("#ul_tab_query li a").click(function(event) {
						$("#ul_tab_query li").removeClass("tabs-selected");
						$(this).parent().addClass("tabs-selected");
						var keyVal = $(this).attr("keyVal");
						var paramObj = {};
						if(keyVal !== "") paramObj[fpName] = keyVal;
						$("#ul_tab_query").data("queryParam", paramObj);
						doQuery(paramObj);
					});
				}
			} -->
			
				function __doListButtonClick(fieldVal, fieldKey, tableKey, rowIndex, callFunc) {
					if(callFunc) {
						callFunc(fieldVal, fieldKey, tableKey, rowIndex);
					}
				}
				
				function __getListButton(fieldVal, fieldKey, tableKey, rowIndex, title) {
					var astr = [];
					astr.push('<button type="button" class="button_big"');
					astr.push(' onclick="__doListButtonClick(\'');
					astr.push(fieldVal);
					astr.push('\',\'');
					astr.push(fieldKey);
					astr.push('\',\'');
					astr.push("queryList");
					astr.push('\',\'');
					astr.push(rowIndex);
					astr.push('\',');
					astr.push('onListClickFor_'+tableKey+'_'+fieldKey);
					astr.push(')"');
					astr.push('>');
					astr.push(title);
					astr.push('</button>');
					return astr.join("");
				}
				
				function __getListHyperLink(fieldVal, fieldKey, tableKey, rowIndex, title) {
					var astr = [];
					astr.push('<a href="javascript:void(0);" style="text-decoration: underline;"');
					astr.push(' onclick="__doListButtonClick(\'');
					astr.push(fieldVal);
					astr.push('\',\'');
					astr.push(fieldKey);
					astr.push('\',\'');
					astr.push("queryList");
					astr.push('\',\'');
					astr.push(rowIndex);
					astr.push('\',');
					astr.push('onListClickFor_'+tableKey+'_'+fieldKey);
					astr.push(')"');
					astr.push('>');
					astr.push(title);
					astr.push('</a>');
					return astr.join("");
				}
			
			<s:if test='#session.USER.isSuperAdmin=="1"'>
			function updateCreateUser() {
				 var rows = $('#queryList').datagrid('getChecked');
				 doUpdateCreateUser(rows,"${entityName}",doRefreshDataGridOnPage);
			}
			
			function doOpenModifyCreateUserWin() {
				var rows = $('#queryList').datagrid('getChecked');
				if(rows && rows.length) {
					doOpenUserWin('${r"${ctx}"}/gs/process!passOn.action','<s:text name="修改所有者"/>');
				} else {
					$.messager.alert('<s:text name="system.javascript.alertinfo.title"/>', '<s:text name="system.javascript.alertinfo.recordInfo"/>', 'info');
				}
			}
			</s:if>
			
	function getFormQueryParams() {
		var param =$("#hid_queryParams").val();
		var qp = $.parseJSON(param);
		if(qp) {
			for(var k in qp) {
				if(qp[k] === "$curUserId$") {
					qp[k] = $curUserId$;
				} else if(qp[k] === "$curOrgCode$%") {
					qp[k] = $curOrgCode$ + "%";
				} else if(qp[k] === "$curOrgId$") {
					qp[k] = $curOrgId$;
				}
			} 
		}
		return qp;
	}
	
	<#-- 初始化分页选择模式列表 -->
	function initPageSelectList() {
		var psOpt = getOption();
		psOpt["url"] = null;
		psOpt["pagination"] = false;
		psOpt["remoteSort"] = false;
		psOpt["onLoadSuccess"] = null;
		delete psOpt["toolbar"];
		psOpt["onRowContextMenu"] = function(e, rowIndex, rowData) {
					e.preventDefault();
					$('#psl_mm').menu('show', {
    						left: e.pageX,
    						top: e.pageY
    				});
		};
		$("#pageSelectList").edatagrid(psOpt);
	}
	
	function doRemoveDataOnPageSelectList() {
		var rows = $("#pageSelectList").edatagrid('getChecked');
		if (rows.length < 1) {
			$.messager
					.alert(
							'<s:text name="system.javascript.alertinfo.title"/>',
							'<s:text name="system.javascript.alertinfo.operRecord"/>',
							'info');
		} else {
			$("#pageSelectList").edatagrid("deleteRows", rows);
		}
	}
	
	<#-- 
	/*初始化查询框*/
	function initQueryView() {
		var mainsrp_nav = $("#mainsrp-nav");
		var bread_crumbs = mainsrp_nav.find(".bread-crumbs");
		var crumbs_cont = mainsrp_nav.find(".crumbs-cont");
		var easyui_layout = mainsrp_nav.parent().parent();
		/*为最后一行查询框添加last-block样式 */
		mainsrp_nav.find(".nav-block").last().addClass("last-block");
		/*添加隐藏显示查询框按钮事件*/
		mainsrp_nav.find(" a.nav-toggle-btn").click(function(){
			if($(this).children(".icon-btn-arrow-down-3").size()>0) {//显示查询面板
				$(this).attr({title:"隐藏查询"}).children(".icon-btn-arrow-down-3").removeClass("icon-btn-arrow-down-3").addClass("icon-btn-arrow-up-3");//修改按钮样式
				$(this).parents(".bread-crumbs").next().show();
				var height = mainsrp_nav.height() + mainsrp_nav.find(".nav-panel").height();//计算查询面板高度
				easyui_layout.layout('panel','north').panel('resize',{height:height});//修改panel高度
				easyui_layout.layout('resize');//调整layout布局
			} else {//隐藏查询面板
				$(this).attr({title:"显示查询"}).children(".icon-btn-arrow-up-3").removeClass("icon-btn-arrow-up-3").addClass("icon-btn-arrow-down-3");
				var height = mainsrp_nav.height()-mainsrp_nav.find(".nav-panel").height();//计算查询面板高度
				$(this).parents(".bread-crumbs").next().hide();
				easyui_layout.layout('panel','north').panel('resize',{height:height});//修改panel高度
				easyui_layout.layout('resize');//调整layout布局
			}
		});
		/*查询控件内容发生变化后，修改查询条件展示区标签*/
		mainsrp_nav.find("input:visible").bind("blur change",function(e){
			var key = $(this).parents(".cell").children('span:first-child').text();
			var value = $(this).val();
			if($(this).attr("type")=="checkbox") {
				var value_ = [];
				$("input[name='"+$(this).attr("name")+"']").each(function(){
					if($(this).attr("checked")=="checked") {
						value_.push($(this).next().text());
					}
				});
				value = value_.join(",");
			}
			var x = crumbs_cont.children("[name='"+($(this).attr('name')?$(this).attr('name'):$(this).parents(".cell").find("input[name]").attr('name'))+"']");
			/*从查询条件展示栏去除旧的查询条件*/
			if(x.size()>0) {
				x.remove();
			}
			/*将变化后的查询条件添加到查询条件展示栏*/
			if(value!="") {
				var a = "<a class='param-selected icon-tag J_Ajax' name='"
					+ ($(this).attr('name') ? $(this).attr('name') : $(this).parents(".cell").find("input[name]").attr('name'))
					+ "' href='#' title='"
					+ key
					+ ":"
					+ value
					+ "' >"
					+ key
					+ ":"
					+ value
					+ "<span class='close-icon icon-btn-x'></span></a>";
				crumbs_cont.append(a);
			}
			/*查询栏因宽度不足换行后，高度自适应*/
			bread_crumbs.height(crumbs_cont.height());
		});
		/*去除特定查询控件内容*/
		crumbs_cont.find("a").live("click",function(){
			mainsrp_nav.find("input[name='"+$(this).attr('name')+"']").clearFields();//jquery插件，去除表单元素内容
			$(this).remove();//从查询条件展示栏去除旧的查询条件
			bread_crumbs.height(crumbs_cont.height());//查询栏因宽度不足换行后，高度自适应
		});
	}-->
			function initSimpleQuery() {
				<@fieldftl.getQueryViewV2 formList=formList isInitEvent=true />
				<#--$("#simpleQueryForm").on('keypress', 'input.select2-search__field', function(e) {if(e.keyCode==13) {doQuery();return false;}});-->
			}			

            $(document).ready(function() {
                <#-- _userList=findAllUser();-->
			    initQueryData();
			    <#-- initQueryView(); -->
			    <@fieldftl.initFormTabQuery form=form isInitTab=false/>
			    
                <#-- 
                $("#bt_query").click(function() {doQuery();});
                $("#bt_reset").click(function() {
                    //$("#queryForm")[0].reset();
                    $("#queryForm").form("clear");
                    collapseAll();
                    doQuery();
                });-->
                <#--//回车查询
                doQuseryAction("queryForm");-->
                if($("#_commonTypeTree_div_").length == 0 || !$("#_commonTypeTree_div_").attr("issys")) { <#-- 无系统字典树查询时自动查询，有时，则由字典树触发查询 -->
                	doQuery(null, true);
                }
                jwpf.bindHotKeysOnListPage();
             	<c:if test="${r"${param.isHideQc=='1'}"}">
				$("#layout_body").layout("collapse", "west");
				</c:if>
				
				<#if form.enablePageSelect ><#-- 启用分页选择模式 -->
				initPageSelectList();
				</#if>
				
				initSimpleQuery();
            });

            </script>  
			<script type="text/javascript">
			    <#-- 初始化JS脚本 -->
    			<@fieldftl.initQueryPageFormCssOrJavascript formList=formList eventKey="addJavaScript"/>
			</script>
			<@fieldftl.initQueryPageFormCssOrJavascript formList=formList eventKey="referJsOrCss"/>
            </body>
            </#if>
      </#list>
</#if>
</html>