/*
 * @(#)Tab.java
 * 2012-5-8 下午04:46:13
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *  Tab标签页节点
 *  
 *  @author gxj
 *  @version 1.0 2012-5-8 下午04:46:13
 *  @since jdk1.6
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Tab {
	
	/**
	 * 表格布局
	 */
	public static final String LAYOUT_TABLE = "table";
	
	/**
	 * 绝对定位布局
	 */
	public static final String LAYOUT_ABSOLUTE = "absolute";
	
	/**
	 * Excel布局
	 */
	public static final String LAYOUT_EXCEL = "excel";
    
	@XmlAttribute
	private Integer rows;
	@XmlAttribute
	private Integer cols;
	@XmlAttribute
	private String form;
	@XmlAttribute
	private String caption;
	@XmlAttribute
	private String languageText;
	@XmlAttribute
	private Integer tableCols;
	
	@JsonIgnoreProperties
	@XmlTransient
	private String i18nKey;
	
	/**
	 * tab布局属性：表格布局和绝对定位布局
	 */
	@XmlAttribute
	private String layout = LAYOUT_TABLE;
	
	/**
	 * 绝对定位高度
	 */
	@XmlAttribute
	private Integer height;
	
	/**
	 * 是否在查询列表显示
	 */
	@XmlAttribute
	private boolean isShowInQueryList;
	
	public boolean getIsShowInQueryList() {
		return isShowInQueryList;
	}

	public void setIsShowInQueryList(boolean isShowInQueryList) {
		this.isShowInQueryList = isShowInQueryList;
	}

	/**
	 * tab页html内容
	 */
	@XmlElement
	private Page content = new Page();
	
	/**
	 * @return the rows
	 */
	public Integer getRows() {
		return rows;
	}
	
	/**
	 * @param rows the rows to set
	 */
	public void setRows(Integer rows) {
		this.rows = rows;
	}
	/**
	 * @return the cols
	 */
	public Integer getCols() {
		return cols;
	}
	/**
	 * @param cols the cols to set
	 */
	public void setCols(Integer cols) {
		this.cols = cols;
	}
	/**
	 * @return the form
	 */
	public String getForm() {
		return form;
	}
	/**
	 * @param form the form to set
	 */
	public void setForm(String form) {
		this.form = form;
	}
	/**
	 * @return the caption
	 */
	public String getCaption() {
		return caption;
	}
	/**
	 * @param caption the caption to set
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	/**
	 * @return the languageText
	 */
	public String getLanguageText() {
		return languageText;
	}
	/**
	 * @param languageText the languageText to set
	 */
	public void setLanguageText(String languageText) {
		this.languageText = languageText;
	}
	
	/**
	 * @return i18nKey
	 */
	public String getI18nKey() {
		return i18nKey;
	}
	
	/**
	 * @param i18nKey
	 */
	public void setI18nKey(String i18nKey) {
		this.i18nKey = i18nKey;
	}
	
	/**
	 * @return tableCols
	 */
	public Integer getTableCols() {
		return tableCols;
	}
	
	/**
	 * @param tableCols
	 */
	public void setTableCols(Integer tableCols) {
		this.tableCols = tableCols;
	}
	
	/**
	 * @return layout
	 */
	public String getLayout() {
		return layout;
	}
	
	/**
	 * @param layout
	 */
	public void setLayout(String layout) {
		this.layout = layout;
	}
	
	/**
	 * @return height
	 */
	public Integer getHeight() {
		return height;
	}
	
	/**
	 * @param height
	 */
	public void setHeight(Integer height) {
		this.height = height;
	}
	
	/**
	 * @return content
	 */
	public Page getContent() {
		return content;
	}
	
	/**
	 * @param content
	 */
	public void setContent(Page content) {
		this.content = content;
	}
	
}
