/*
 * @(#)DateAdapter.java
 * 2012-5-9 下午03:41:38
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang.StringUtils;


/**
 *  jaxb自定义日期转换适配器
 *  @author gxj
 *  @version 1.0 2012-5-9 下午03:41:38
 *  @since jdk1.6 
 */
public class DateAdapter extends XmlAdapter<String, Date> {

	private DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public Date unmarshal(String v) throws Exception {
		if(StringUtils.isNotBlank(v)) {
			return df.parse(v);
		} else {
			return null;
		}
	}

	@Override
	public String marshal(Date v) throws Exception {
		if(null!=v) {
			return df.format(v);
		} else {
			return "";
		}
		
	}
	
}
