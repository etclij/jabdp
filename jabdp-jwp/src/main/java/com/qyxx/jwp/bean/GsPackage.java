/*
 * @(#)GsPackage.java
 * 2011-9-24 下午11:21:59
 * 
 * Copyright (c) 2018-2028, HangZhou QiYun InfoTech Co.,Ltd. .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qyxx.jwp.bean;

import java.util.HashMap;
import java.util.Map;



/**
 *  GS包
 *  @author gxj
 *  @version 1.0
 *  @since 1.6 2011-9-24 下午11:21:59
 */
public class GsPackage {

	//GS包名
	private String name;
	
	private Map<String, GsModule> moduleMap = new HashMap<String, GsModule>();

	
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	
	/**
	 * @return moduleMap
	 */
	public Map<String, GsModule> getModuleMap() {
		return moduleMap;
	}

	
	/**
	 * @param moduleMap
	 */
	public void setModuleMap(Map<String, GsModule> moduleMap) {
		this.moduleMap = moduleMap;
	}
	
	/**
	 * 根据模块属性key及模块key获取主实体名称
	 * 
	 * @param modulesName 模块集合key
	 * @param moduleName 模块key
	 * @return
	 */
	public String getModuleEntityName(String modulesName, String moduleName) {
		String entityName = "";
		GsModule gm = this.moduleMap.get(modulesName);
		if(null!=gm) {
			Module md = gm.getModuleMap().get(moduleName);
			if(null!=md) {
				entityName = md.getEntityName();
			}
		}
		return entityName;
	}
	
}
